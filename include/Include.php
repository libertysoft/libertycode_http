<?php

// Init var
$strRootPath = dirname(__FILE__) . '/..';

// Include class
include($strRootPath . '/src/config/library/ConstConfig.php');
include($strRootPath . '/src/config/exception/HttpRequestSourceLinkInvalidFormatException.php');
include($strRootPath . '/src/config/exception/HttpResponseStatusCodeInvalidFormatException.php');
include($strRootPath . '/src/config/exception/HttpResponseStatusMsgInvalidFormatException.php');
include($strRootPath . '/src/config/exception/HttpResponseHeaderInvalidFormatException.php');
include($strRootPath . '/src/config/exception/HttpRedirectResponseStatusCodeInvalidFormatException.php');
include($strRootPath . '/src/config/exception/HttpRedirectResponseUrlInvalidFormatException.php');
include($strRootPath . '/src/config/exception/HttpRouteMethodInvalidFormatException.php');
include($strRootPath . '/src/config/model/DefaultConfig.php');

include($strRootPath . '/src/url_route/argument/library/ConstUrlRouteArgData.php');
include($strRootPath . '/src/url_route/argument/exception/DataSrcInvalidFormatException.php');
include($strRootPath . '/src/url_route/argument/exception/KeyInvalidFormatException.php');
include($strRootPath . '/src/url_route/argument/exception/ValueInvalidFormatException.php');
include($strRootPath . '/src/url_route/argument/model/UrlRouteArgData.php');

include($strRootPath . '/src/url_route/library/ToolBoxUrlRoute.php');

include($strRootPath . '/src/url_arg/library/ConstUrlArgData.php');
include($strRootPath . '/src/url_arg/exception/DataSrcInvalidFormatException.php');
include($strRootPath . '/src/url_arg/exception/PathInvalidFormatException.php');
include($strRootPath . '/src/url_arg/exception/ValueInvalidFormatException.php');
include($strRootPath . '/src/url_arg/model/UrlArgData.php');

include($strRootPath . '/src/header/library/ConstHeaderData.php');
include($strRootPath . '/src/header/library/ToolBoxHeader.php');
include($strRootPath . '/src/header/exception/DataSrcInvalidFormatException.php');
include($strRootPath . '/src/header/exception/KeyInvalidFormatException.php');
include($strRootPath . '/src/header/exception/ValueInvalidFormatException.php');
include($strRootPath . '/src/header/model/HeaderData.php');

include($strRootPath . '/src/multipart/library/ToolBoxMultipart.php');
include($strRootPath . '/src/multipart/data/library/ToolBoxMultipartData.php');

include($strRootPath . '/src/response/library/ToolBoxResponse.php');

include($strRootPath . '/src/curl/library/ToolBoxCurl.php');

include($strRootPath . '/src/cookie/library/ToolBoxCookie.php');

include($strRootPath . '/src/register/session/model/SessionRegister.php');

include($strRootPath . '/src/register/cookie/library/ConstCookieRegister.php');
include($strRootPath . '/src/register/cookie/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/register/cookie/exception/SetExecConfigInvalidFormatException.php');
include($strRootPath . '/src/register/cookie/model/CookieRegister.php');

include($strRootPath . '/src/route/library/ConstHttpRoute.php');
include($strRootPath . '/src/route/library/ToolBoxHttpRoute.php');
include($strRootPath . '/src/route/pattern/model/HttpPatternRoute.php');
include($strRootPath . '/src/route/param/model/HttpParamRoute.php');
include($strRootPath . '/src/route/fix/model/HttpFixRoute.php');
include($strRootPath . '/src/route/separator/model/HttpSeparatorRoute.php');

include($strRootPath . '/src/route/factory/library/ConstHttpRouteFactory.php');
include($strRootPath . '/src/route/factory/model/HttpRouteFactory.php');

include($strRootPath . '/src/file/response/library/ConstResponseFile.php');
include($strRootPath . '/src/file/response/library/ToolBoxResponseFile.php');
include($strRootPath . '/src/file/response/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/file/response/model/ResponseFile.php');

include($strRootPath . '/src/file/name/upload/library/ConstUploadFile.php');
include($strRootPath . '/src/file/name/upload/library/ToolBoxUploadFile.php');
include($strRootPath . '/src/file/name/upload/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/file/name/upload/model/UploadFile.php');

include($strRootPath . '/src/file/name/multipart_data/library/ConstMultipartDataFile.php');
include($strRootPath . '/src/file/name/multipart_data/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/file/name/multipart_data/model/MultipartDataFile.php');

include($strRootPath . '/src/file/name/download_response/library/ConstDnlResponseFile.php');
include($strRootPath . '/src/file/name/download_response/library/ToolBoxDnlResponseFile.php');
include($strRootPath . '/src/file/name/download_response/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/file/name/download_response/model/DnlResponseFile.php');

include($strRootPath . '/src/file/factory/library/ConstHttpFileFactory.php');
include($strRootPath . '/src/file/factory/library/ToolBoxHttpFactory.php');
include($strRootPath . '/src/file/factory/model/HttpFileFactory.php');

include($strRootPath . '/src/file/factory/name/library/ConstHttpNameFileFactory.php');
include($strRootPath . '/src/file/factory/name/model/HttpNameFileFactory.php');

include($strRootPath . '/src/parser/string_table/url_arg/model/UrlArgParser.php');

include($strRootPath . '/src/parser/string_table/multipart_data/library/ConstMultipartDataParser.php');
include($strRootPath . '/src/parser/string_table/multipart_data/library/ToolBoxMultipartDataParser.php');
include($strRootPath . '/src/parser/string_table/multipart_data/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/parser/string_table/multipart_data/exception/FileFactoryInvalidFormatException.php');
include($strRootPath . '/src/parser/string_table/multipart_data/model/MultipartDataParser.php');

include($strRootPath . '/src/parser/factory/string_table/library/ConstHttpStrTableParserFactory.php');
include($strRootPath . '/src/parser/factory/string_table/model/HttpStrTableParserFactory.php');

include($strRootPath . '/src/request_flow/request/library/ConstHttpRequest.php');
include($strRootPath . '/src/request_flow/request/model/HttpRequest.php');

include($strRootPath . '/src/request_flow/response/library/ConstHttpResponse.php');
include($strRootPath . '/src/request_flow/response/library/ToolBoxHttpResponse.php');
include($strRootPath . '/src/request_flow/response/exception/StatusCodeInvalidFormatException.php');
include($strRootPath . '/src/request_flow/response/exception/StatusMsgInvalidFormatException.php');
include($strRootPath . '/src/request_flow/response/model/HttpResponse.php');

include($strRootPath . '/src/request_flow/response/redirection/library/ConstRedirectResponse.php');
include($strRootPath . '/src/request_flow/response/redirection/exception/StatusCodeInvalidFormatException.php');
include($strRootPath . '/src/request_flow/response/redirection/exception/UrlInvalidFormatException.php');
include($strRootPath . '/src/request_flow/response/redirection/exception/GetArgInvalidFormatException.php');
include($strRootPath . '/src/request_flow/response/redirection/model/RedirectResponse.php');

include($strRootPath . '/src/request_flow/front/library/ConstHttpFrontController.php');
include($strRootPath . '/src/request_flow/front/exception/ActiveRequestInvalidFormatException.php');
include($strRootPath . '/src/request_flow/front/exception/DefaultResponseInvalidFormatException.php');
include($strRootPath . '/src/request_flow/front/model/HttpFrontController.php');

include($strRootPath . '/src/requisition/request/library/ConstHttpRequest.php');
include($strRootPath . '/src/requisition/request/library/ToolBoxHttpRequest.php');
include($strRootPath . '/src/requisition/request/exception/UrlRouteArgDataInvalidFormatException.php');
include($strRootPath . '/src/requisition/request/exception/UrlArgDataInvalidFormatException.php');
include($strRootPath . '/src/requisition/request/exception/HeaderDataInvalidFormatException.php');
include($strRootPath . '/src/requisition/request/exception/SndInfoInvalidFormatException.php');
include($strRootPath . '/src/requisition/request/model/HttpRequest.php');

include($strRootPath . '/src/requisition/request/data/library/ConstDataHttpRequest.php');
include($strRootPath . '/src/requisition/request/data/exception/ParserFactoryInvalidFormatException.php');
include($strRootPath . '/src/requisition/request/data/exception/BodyDataInvalidFormatException.php');
include($strRootPath . '/src/requisition/request/data/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/requisition/request/data/model/DataHttpRequest.php');

include($strRootPath . '/src/requisition/request/persistence/library/ConstPersistorHttpRequest.php');
include($strRootPath . '/src/requisition/request/persistence/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/requisition/request/persistence/exception/IdInvalidFormatException.php');
include($strRootPath . '/src/requisition/request/persistence/exception/QueryInvalidFormatException.php');
include($strRootPath . '/src/requisition/request/persistence/exception/TabDataInvalidFormatException.php');
include($strRootPath . '/src/requisition/request/persistence/model/PersistorHttpRequest.php');

include($strRootPath . '/src/requisition/request/factory/library/ConstHttpRequestFactory.php');
include($strRootPath . '/src/requisition/request/factory/model/HttpRequestFactory.php');

include($strRootPath . '/src/requisition/response/library/ConstHttpResponse.php');
include($strRootPath . '/src/requisition/response/exception/HeaderDataInvalidFormatException.php');
include($strRootPath . '/src/requisition/response/exception/RcpInfoInvalidFormatException.php');
include($strRootPath . '/src/requisition/response/model/HttpResponse.php');

include($strRootPath . '/src/requisition/response/data/library/ConstDataHttpResponse.php');
include($strRootPath . '/src/requisition/response/data/exception/ParserFactoryInvalidFormatException.php');
include($strRootPath . '/src/requisition/response/data/exception/BodyDataInvalidFormatException.php');
include($strRootPath . '/src/requisition/response/data/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/requisition/response/data/model/DataHttpResponse.php');

include($strRootPath . '/src/requisition/response/persistence/library/ConstPersistorHttpResponse.php');
include($strRootPath . '/src/requisition/response/persistence/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/requisition/response/persistence/exception/DataRequiredException.php');
include($strRootPath . '/src/requisition/response/persistence/exception/TabDataInvalidFormatException.php');
include($strRootPath . '/src/requisition/response/persistence/model/PersistorHttpResponse.php');

include($strRootPath . '/src/requisition/response/factory/library/ConstHttpResponseFactory.php');
include($strRootPath . '/src/requisition/response/factory/model/HttpResponseFactory.php');

include($strRootPath . '/src/requisition/client/library/ConstHttpClient.php');
include($strRootPath . '/src/requisition/client/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/requisition/client/exception/ExecConfigInvalidFormatException.php');
include($strRootPath . '/src/requisition/client/exception/SndInfoInvalidFormatException.php');
include($strRootPath . '/src/requisition/client/exception/CurlExecutionFailException.php');
include($strRootPath . '/src/requisition/client/model/HttpClient.php');

include($strRootPath . '/src/authentication/authentication/library/ConstHttpAuthentication.php');
include($strRootPath . '/src/authentication/authentication/exception/RequestInvalidFormatException.php');
include($strRootPath . '/src/authentication/authentication/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/authentication/authentication/model/HttpAuthentication.php');