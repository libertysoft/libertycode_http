<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\authentication\authentication\exception;

use liberty_code\http\request_flow\request\model\HttpRequest;
use liberty_code\http\authentication\authentication\library\ConstHttpAuthentication;



class RequestInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $request
     */
	public function __construct($request)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstHttpAuthentication::EXCEPT_MSG_REQUEST_INVALID_FORMAT,
            mb_strimwidth(strval($request), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified request has valid format.
	 * 
     * @param mixed $request
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($request)
    {
		// Init var
		$result = (
			(is_null($request)) ||
			($request instanceof HttpRequest)
		);
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($request);
		}
		
		// Return result
		return $result;
    }
	
	
	
}