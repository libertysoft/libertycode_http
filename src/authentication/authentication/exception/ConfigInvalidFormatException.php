<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\authentication\authentication\exception;

use liberty_code\http\authentication\authentication\library\ConstHttpAuthentication;



class ConfigInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstHttpAuthentication::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init index array of data check function
        $checkTabDataIsValid = function(array $tabData)
        {
            $result = true;
            $tabData = array_values($tabData);

            // Check each data is valid
            for($intCpt = 0; ($intCpt < count($tabData)) && $result; $intCpt++)
            {
                $data = $tabData[$intCpt];
                $result = (
                    // Check valid argument type
                    (
                        (!array_key_exists(ConstHttpAuthentication::TAB_CONFIG_KEY_ARG_TYPE, $data)) ||
                        (
                            is_string($data[ConstHttpAuthentication::TAB_CONFIG_KEY_ARG_TYPE]) &&
                            in_array(
                                $data[ConstHttpAuthentication::TAB_CONFIG_KEY_ARG_TYPE],
                                ConstHttpAuthentication::getTabConfigArgType()
                            )
                        )
                    ) &&

                    // Check valid argument key
                    isset($data[ConstHttpAuthentication::TAB_CONFIG_KEY_ARG_KEY]) &&
                    is_string($data[ConstHttpAuthentication::TAB_CONFIG_KEY_ARG_KEY]) &&
                    (trim($data[ConstHttpAuthentication::TAB_CONFIG_KEY_ARG_KEY]) != '') &&

                    // Check valid argument format callable
                    (
                        (!isset($data[ConstHttpAuthentication::TAB_CONFIG_KEY_ARG_FORMAT_CALLABLE])) ||
                        is_callable($data[ConstHttpAuthentication::TAB_CONFIG_KEY_ARG_FORMAT_CALLABLE])
                    ) &&

                    // Check valid data key
                    (
                        (!array_key_exists(ConstHttpAuthentication::TAB_CONFIG_KEY_DATA_KEY, $data)) ||
                        (
                            is_string($data[ConstHttpAuthentication::TAB_CONFIG_KEY_DATA_KEY]) &&
                            (trim($data[ConstHttpAuthentication::TAB_CONFIG_KEY_DATA_KEY]) != '')
                        )
                    ) &&

                    // Check valid data format callable
                    (
                        (!isset($data[ConstHttpAuthentication::TAB_CONFIG_KEY_DATA_FORMAT_CALLABLE])) ||
                        is_callable($data[ConstHttpAuthentication::TAB_CONFIG_KEY_DATA_FORMAT_CALLABLE])
                    )
                );
            }

            return $result;
        };

        // Init var
        $result =
            // Check valid identification data
            isset($config[ConstHttpAuthentication::TAB_CONFIG_KEY_ID_DATA]) &&
            is_array($config[ConstHttpAuthentication::TAB_CONFIG_KEY_ID_DATA]) &&
            (count($config[ConstHttpAuthentication::TAB_CONFIG_KEY_ID_DATA]) > 0) &&
            $checkTabDataIsValid($config[ConstHttpAuthentication::TAB_CONFIG_KEY_ID_DATA]) &&

            // Check valid authentication data
            (
                (!isset($config[ConstHttpAuthentication::TAB_CONFIG_KEY_AUTH_DATA])) ||
                (
                    is_array($config[ConstHttpAuthentication::TAB_CONFIG_KEY_AUTH_DATA]) &&
                    $checkTabDataIsValid($config[ConstHttpAuthentication::TAB_CONFIG_KEY_AUTH_DATA])
                )
            );

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format.
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($config)
    {
		// Init var
		$result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}