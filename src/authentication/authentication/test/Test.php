<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Load test
require_once($strRootAppPath . '/src/request_flow/request/test/HttpRequestTest.php');

// Use
use liberty_code\http\request_flow\request\test\HttpRequestTest;
use liberty_code\http\authentication\authentication\library\ConstHttpAuthentication;
use liberty_code\http\authentication\authentication\model\HttpAuthentication;



// Init var
/** @var HttpRequestTest $objRequest */
$objRequest = HttpRequestTest::instanceGetDefault();
$objHttpAuth = new HttpAuthentication($objRequest);



// Test secret authentication
$tabTabConfig = array(
    [
        ConstHttpAuthentication::TAB_CONFIG_KEY_ID_DATA => [
            [
                ConstHttpAuthentication::TAB_CONFIG_KEY_ARG_TYPE => ConstHttpAuthentication::CONFIG_ARG_TYPE_HEADER,
                ConstHttpAuthentication::TAB_CONFIG_KEY_ARG_KEY => 7,
                ConstHttpAuthentication::TAB_CONFIG_KEY_DATA_KEY => 'usr_lg'
            ]
        ]
    ], // Ko: Identification data bad format

    [
        ConstHttpAuthentication::TAB_CONFIG_KEY_ID_DATA => [
            [
                ConstHttpAuthentication::TAB_CONFIG_KEY_ARG_TYPE => ConstHttpAuthentication::CONFIG_ARG_TYPE_HEADER,
                ConstHttpAuthentication::TAB_CONFIG_KEY_ARG_KEY => 'X-User-Login',
                ConstHttpAuthentication::TAB_CONFIG_KEY_DATA_KEY => 'usr_lg'
            ]
        ]
    ], // Ok: Get arguments from request headers

    [
        ConstHttpAuthentication::TAB_CONFIG_KEY_AUTH_DATA => [
            [
                ConstHttpAuthentication::TAB_CONFIG_KEY_ARG_TYPE => ConstHttpAuthentication::CONFIG_ARG_TYPE_HEADER,
                ConstHttpAuthentication::TAB_CONFIG_KEY_ARG_KEY => 'X-User-Password',
                ConstHttpAuthentication::TAB_CONFIG_KEY_DATA_KEY => 'usr_pw'
            ]
        ]
    ], // Ko: Identification data not found

    [
        ConstHttpAuthentication::TAB_CONFIG_KEY_ID_DATA => [
            [
                ConstHttpAuthentication::TAB_CONFIG_KEY_ARG_TYPE => ConstHttpAuthentication::CONFIG_ARG_TYPE_HEADER,
                ConstHttpAuthentication::TAB_CONFIG_KEY_ARG_KEY => 'X-User-Id',
                ConstHttpAuthentication::TAB_CONFIG_KEY_DATA_KEY => 'usr_id'
            ],
            [
                ConstHttpAuthentication::TAB_CONFIG_KEY_ARG_TYPE => ConstHttpAuthentication::CONFIG_ARG_TYPE_HEADER,
                ConstHttpAuthentication::TAB_CONFIG_KEY_ARG_KEY => 'X-User-Login',
                ConstHttpAuthentication::TAB_CONFIG_KEY_DATA_KEY => 'usr_lg'
            ]
        ],
        ConstHttpAuthentication::TAB_CONFIG_KEY_AUTH_DATA => [
            [
                ConstHttpAuthentication::TAB_CONFIG_KEY_ARG_TYPE => ConstHttpAuthentication::CONFIG_ARG_TYPE_HEADER,
                ConstHttpAuthentication::TAB_CONFIG_KEY_ARG_KEY => 'X-User-Password',
                ConstHttpAuthentication::TAB_CONFIG_KEY_DATA_KEY => 'usr_pw'
            ]
        ]
    ], // Ok: Get arguments from request headers

    [
        ConstHttpAuthentication::TAB_CONFIG_KEY_ID_DATA => [
            [
                ConstHttpAuthentication::TAB_CONFIG_KEY_ARG_TYPE => ConstHttpAuthentication::CONFIG_ARG_TYPE_GET,
                ConstHttpAuthentication::TAB_CONFIG_KEY_ARG_KEY => 'user-id',
                ConstHttpAuthentication::TAB_CONFIG_KEY_DATA_KEY => 'usr_id'
            ],
            [
                ConstHttpAuthentication::TAB_CONFIG_KEY_ARG_TYPE => ConstHttpAuthentication::CONFIG_ARG_TYPE_GET,
                ConstHttpAuthentication::TAB_CONFIG_KEY_ARG_KEY => 'user-login',
                ConstHttpAuthentication::TAB_CONFIG_KEY_DATA_KEY => 'usr_lg'
            ]
        ],
        ConstHttpAuthentication::TAB_CONFIG_KEY_AUTH_DATA => [
            [
                ConstHttpAuthentication::TAB_CONFIG_KEY_ARG_TYPE => 'test',
                ConstHttpAuthentication::TAB_CONFIG_KEY_ARG_KEY => 'password',
                ConstHttpAuthentication::TAB_CONFIG_KEY_DATA_KEY => 'usr_pw'
            ]
        ]
    ], // Ko: Authentication data bad format

    [
        ConstHttpAuthentication::TAB_CONFIG_KEY_ID_DATA => [
            [
                ConstHttpAuthentication::TAB_CONFIG_KEY_ARG_TYPE => ConstHttpAuthentication::CONFIG_ARG_TYPE_GET,
                ConstHttpAuthentication::TAB_CONFIG_KEY_ARG_KEY => 'user-id',
                ConstHttpAuthentication::TAB_CONFIG_KEY_DATA_KEY => 'usr_id'
            ],
            [
                ConstHttpAuthentication::TAB_CONFIG_KEY_ARG_TYPE => ConstHttpAuthentication::CONFIG_ARG_TYPE_GET,
                ConstHttpAuthentication::TAB_CONFIG_KEY_ARG_KEY => 'user-login',
                ConstHttpAuthentication::TAB_CONFIG_KEY_DATA_KEY => 'usr_lg'
            ]
        ],
        ConstHttpAuthentication::TAB_CONFIG_KEY_AUTH_DATA => [
            [
                ConstHttpAuthentication::TAB_CONFIG_KEY_ARG_TYPE => ConstHttpAuthentication::CONFIG_ARG_TYPE_POST,
                ConstHttpAuthentication::TAB_CONFIG_KEY_ARG_KEY => 'password',
                ConstHttpAuthentication::TAB_CONFIG_KEY_DATA_KEY => 'usr_pw'
            ]
        ]
    ], // Ok: Get arguments from request GET and POST arguments

    [
        ConstHttpAuthentication::TAB_CONFIG_KEY_ID_DATA => [
            [
                ConstHttpAuthentication::TAB_CONFIG_KEY_ARG_TYPE => ConstHttpAuthentication::CONFIG_ARG_TYPE_HEADER,
                ConstHttpAuthentication::TAB_CONFIG_KEY_ARG_KEY => 'Authorization',
                ConstHttpAuthentication::TAB_CONFIG_KEY_ARG_FORMAT_CALLABLE => function($value)
                {
                    $tabMatch = array();
                    $strPattern = '#^Basic (.+)$#';
                    $result = (
                        (
                            is_string($value) &&
                            (preg_match($strPattern, $value, $tabMatch) !== false) &&
                            isset($tabMatch[1]) &&
                            is_string(base64_decode($tabMatch[1]))
                        ) ?
                            base64_decode($tabMatch[1]) :
                            null
                    );

                    return $result;
                },
                ConstHttpAuthentication::TAB_CONFIG_KEY_DATA_KEY => 'usr_lg',
                ConstHttpAuthentication::TAB_CONFIG_KEY_DATA_FORMAT_CALLABLE => function($value)
                {
                    $tabMatch = array();
                    $strPattern = '#^([^:]+):[^:]*$#';
                    $result = (
                    (
                        is_string($value) &&
                        (preg_match($strPattern, $value, $tabMatch) !== false) &&
                        isset($tabMatch[1])
                    ) ?
                        $tabMatch[1] :
                        null
                    );

                    return $result;
                }
            ]
        ],
        ConstHttpAuthentication::TAB_CONFIG_KEY_AUTH_DATA => [
            [
                ConstHttpAuthentication::TAB_CONFIG_KEY_ARG_TYPE => ConstHttpAuthentication::CONFIG_ARG_TYPE_HEADER,
                ConstHttpAuthentication::TAB_CONFIG_KEY_ARG_KEY => 'Authorization',
                ConstHttpAuthentication::TAB_CONFIG_KEY_ARG_FORMAT_CALLABLE => function($value)
                {
                    $tabMatch = array();
                    $strPattern = '#^Basic (.+)$#';
                    $result = (
                    (
                        is_string($value) &&
                        (preg_match($strPattern, $value, $tabMatch) !== false) &&
                        isset($tabMatch[1]) &&
                        is_string(base64_decode($tabMatch[1]))
                    ) ?
                        base64_decode($tabMatch[1]) :
                        null
                    );

                    return $result;
                },
                ConstHttpAuthentication::TAB_CONFIG_KEY_DATA_KEY => 'usr_pw',
                ConstHttpAuthentication::TAB_CONFIG_KEY_DATA_FORMAT_CALLABLE => function($value)
                {
                    $tabMatch = array();
                    $strPattern = '#^[^:]+:([^:]*)$#';
                    $result = (
                    (
                        is_string($value) &&
                        (preg_match($strPattern, $value, $tabMatch) !== false) &&
                        isset($tabMatch[1])
                    ) ?
                        $tabMatch[1] :
                        null
                    );

                    return $result;
                }
            ]
        ]
    ] // Ok: Get arguments from request headers
);

foreach($tabTabConfig as $tabConfig)
{
    echo('Test HTTP authentication : <pre>');
    var_dump($tabConfig);
    echo('</pre>');

    try{
        $objHttpAuth->setAuthConfig($tabConfig);

        echo('Get configuration: <pre>');var_dump($objHttpAuth->getTabAuthConfig());echo('</pre>');
        echo('Get identification: <pre>');var_dump($objHttpAuth->getTabIdData());echo('</pre>');
        echo('Get authentication: <pre>');var_dump($objHttpAuth->getTabAuthData());echo('</pre>');

    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
        echo('<br />');
    }
    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');


