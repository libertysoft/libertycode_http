<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\authentication\authentication\library;



class ConstHttpAuthentication
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_REQUEST = 'objRequest';



    // Configuration
    const TAB_CONFIG_KEY_ID_DATA = 'identification_data';
    const TAB_CONFIG_KEY_AUTH_DATA = 'authentication_data';
    const TAB_CONFIG_KEY_ARG_TYPE = 'arg_type';
    const TAB_CONFIG_KEY_ARG_KEY = 'arg_key';
    const TAB_CONFIG_KEY_ARG_FORMAT_CALLABLE = 'arg_format_callable';
    const TAB_CONFIG_KEY_DATA_KEY = 'data_key';
    const TAB_CONFIG_KEY_DATA_FORMAT_CALLABLE = 'data_format_callable';

    // Argument type configuration
    const CONFIG_ARG_TYPE_HEADER = 'header';
    const CONFIG_ARG_TYPE_GET = 'get';
    const CONFIG_ARG_TYPE_POST = 'post';
    const CONFIG_ARG_TYPE_PUT = 'put';
    const CONFIG_ARG_TYPE_ARGUMENT = 'argument';
    const CONFIG_ARG_TYPE_FILE = 'file';
    const CONFIG_ARG_TYPE_COOKIE = 'cookie';



    // Exception message constants
    const EXCEPT_MSG_REQUEST_INVALID_FORMAT = 'Following request "%1$s" invalid! It must be a HTTP request object.';
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array, not empty and following the HTTP authentication configuration standard.';





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods statics getters
    // ******************************************************************************

    /**
     * Get configuration argument types array.
     *
     * @return array
     */
    static public function getTabConfigArgType()
    {
        // Init var
        $result = array(
            self::CONFIG_ARG_TYPE_HEADER,
            self::CONFIG_ARG_TYPE_GET,
            self::CONFIG_ARG_TYPE_POST,
            self::CONFIG_ARG_TYPE_PUT,
            self::CONFIG_ARG_TYPE_ARGUMENT,
            self::CONFIG_ARG_TYPE_FILE,
            self::CONFIG_ARG_TYPE_COOKIE
        );

        // Return result
        return $result;
    }



}