<?php
/**
 * This class allows to define HTTP authentication class.
 * HTTP authentication uses HTTP request arguments,
 * to get all identification and authentication information.
 *
 * HTTP authentication uses the following specified configuration:
 * [
 *     identification_data(required: At least one identification data must be found): [
 *         // Identification data 1
 *         [
 *             arg_type(optional: got header if not found):
 *                 "string argument type" (@see ConstHttpAuthentication::getTabConfigArgType() ),
 *             arg_key(required): "string argument key",
 *             arg_format_callable(optional):
 *                 Get argument formatted value callback function: mixed function(mixed $value),
 *             data_key(optional: got arg_key if not found): "string data key",
 *             data_format_callable(optional):
 *                 Get data formatted value callback function: mixed function(mixed $value)
 *         ],
 *         ...,
 *         // Identification data N
 *         [...]
 *     ],
 *
 *     authentication_data(optional: got [] if not found): [
 *         // Authentication data 1
 *         [
 *             Same format as identification data (described above)
 *         ],
 *         ...,
 *         // Authentication data N
 *         [...]
 *     ]
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\authentication\authentication\model;

use liberty_code\authentication\authentication\model\DefaultAuthentication;

use liberty_code\authentication\authentication\library\ConstAuthentication;
use liberty_code\http\request_flow\request\model\HttpRequest;
use liberty_code\http\authentication\authentication\library\ConstHttpAuthentication;
use liberty_code\http\authentication\authentication\exception\RequestInvalidFormatException;
use liberty_code\http\authentication\authentication\exception\ConfigInvalidFormatException;



/**
 * @method null|HttpRequest getObjRequest() Get HTTP request object.
 * @method void setObjRequest(HttpRequest $objRequest) Set HTTP request object.
 */
class HttpAuthentication extends DefaultAuthentication
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param HttpRequest $objRequest = null
     */
    public function __construct(
        HttpRequest $objRequest = null,
        array $tabConfig = null
    )
    {
        // Call parent constructor
        parent::__construct($tabConfig);

        // Init HTTP request, if required
        if(!is_null($objRequest))
        {
            $this->setObjRequest($objRequest);
        }
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstHttpAuthentication::DATA_KEY_DEFAULT_REQUEST))
        {
            $this->__beanTabData[ConstHttpAuthentication::DATA_KEY_DEFAULT_REQUEST] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstHttpAuthentication::DATA_KEY_DEFAULT_REQUEST
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstHttpAuthentication::DATA_KEY_DEFAULT_REQUEST:
                    RequestInvalidFormatException::setCheck($value);
                    break;

                case ConstAuthentication::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get associative array of authentication data engine,
     * from specified index array of configured data.
     *
     * @param array $tabConfigData
     * @return array
     */
    protected function getTabDataEngine(array $tabConfigData)
    {
        // Init var
        $result = array();
        $objRequest = $this->getObjRequest();

        if(!is_null($objRequest))
        {
            // Run each configured data
            foreach($tabConfigData as $configData)
            {
                // Get info
                $strArgType = (
                    isset($configData[ConstHttpAuthentication::TAB_CONFIG_KEY_ARG_TYPE]) ?
                        $configData[ConstHttpAuthentication::TAB_CONFIG_KEY_ARG_TYPE] :
                        ConstHttpAuthentication::CONFIG_ARG_TYPE_HEADER
                );
                $strArgKey = $configData[ConstHttpAuthentication::TAB_CONFIG_KEY_ARG_KEY];
                $callArgFormat = (
                    isset($configData[ConstHttpAuthentication::TAB_CONFIG_KEY_ARG_FORMAT_CALLABLE]) ?
                        $configData[ConstHttpAuthentication::TAB_CONFIG_KEY_ARG_FORMAT_CALLABLE] :
                        null
                );
                $strKey = (
                    isset($configData[ConstHttpAuthentication::TAB_CONFIG_KEY_DATA_KEY]) ?
                        $configData[ConstHttpAuthentication::TAB_CONFIG_KEY_DATA_KEY] :
                        $strArgKey
                );
                $callFormat = (
                    isset($configData[ConstHttpAuthentication::TAB_CONFIG_KEY_DATA_FORMAT_CALLABLE]) ?
                        $configData[ConstHttpAuthentication::TAB_CONFIG_KEY_DATA_FORMAT_CALLABLE] :
                        null
                );

                // Get value
                switch($strArgType)
                {
                    case ConstHttpAuthentication::CONFIG_ARG_TYPE_HEADER:
                    default:
                        $value = $objRequest->getHeader($strArgKey, null);
                        break;

                    case ConstHttpAuthentication::CONFIG_ARG_TYPE_GET:
                        $value = $objRequest->getGet($strArgKey, null);
                        break;

                    case ConstHttpAuthentication::CONFIG_ARG_TYPE_POST:
                        $value = $objRequest->getPost($strArgKey, null);
                        break;

                    case ConstHttpAuthentication::CONFIG_ARG_TYPE_PUT:
                        $value = $objRequest->getPut($strArgKey, null);
                        break;

                    case ConstHttpAuthentication::CONFIG_ARG_TYPE_ARGUMENT:
                        $value = $objRequest->getArg($strArgKey, null);
                        break;

                    case ConstHttpAuthentication::CONFIG_ARG_TYPE_FILE:
                        $value = $objRequest->getFile($strArgKey, null);
                        break;

                    case ConstHttpAuthentication::CONFIG_ARG_TYPE_COOKIE:
                        $value = HttpRequest::getCookie($strArgKey, null);
                        break;
                }

                // Get formatted value
                $value = $this->getArgDataFormat($strArgKey, $value);
                $value = (is_callable($callArgFormat) ? $callArgFormat($value) : $value);
                $value = (is_callable($callFormat) ? $callFormat($value) : $value);

                // Set result info
                $result[$strKey] = $value;
            }
        }

        // Return result
        return $result;
    }



    /**
     * Get specified argument formatted data.
     * Overwrite it to implement specific format.
     *
     * @param string $strKey
     * @param mixed $value
     * @return mixed
     */
    protected function getArgDataFormat($strKey, $value)
    {
        // Format by data key
        switch($strKey)
        {
            default:
                $result = $value;
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getTabIdDataEngine()
    {
        // Init var
        $tabConfig = $this->getTabAuthConfig();
        $result = $this->getTabDataEngine($tabConfig[ConstHttpAuthentication::TAB_CONFIG_KEY_ID_DATA]);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getTabAuthDataEngine()
    {
        // Init var
        $tabConfig = $this->getTabAuthConfig();
        $result = (
            isset($tabConfig[ConstHttpAuthentication::TAB_CONFIG_KEY_AUTH_DATA]) ?
                $this->getTabDataEngine($tabConfig[ConstHttpAuthentication::TAB_CONFIG_KEY_AUTH_DATA]) :
                array()
        );

        // Return result
        return $result;
    }



}