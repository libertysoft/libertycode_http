<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\url_arg\exception;

use Exception;

use liberty_code\http\url_arg\library\ConstUrlArgData;



class PathInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $path
     */
	public function __construct($path)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
        $this->message = sprintf
        (
            ConstUrlArgData::EXCEPT_MSG_PATH_INVALID_FORMAT,
            mb_strimwidth(strval($path), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified path has valid format.
     *
     * @param mixed $path
     * @param string $strPathSeparator
     * @return boolean
     * @throws static
     */
    public static function checkPathIsValid($path, $strPathSeparator)
    {
        // Init var
        $result =
            // Check valid string
            is_string($path) &&
            (trim($path) != '') &&
            is_string($strPathSeparator) &&
            (!is_numeric(explode($strPathSeparator, $path)[0]));

        // Return result
        return $result;
    }



	/**
	 * Check if specified path has valid format.
	 * 
     * @param mixed $path
     * @param string $strPathSeparator
	 * @return boolean
	 * @throws static
     */
    public static function setCheck($path, $strPathSeparator)
    {
		// Init var
		$result = static::checkPathIsValid($path, $strPathSeparator);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($path);
		}
		
		// Return result
		return $result;
    }
	
	
	
}