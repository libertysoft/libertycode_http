<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\url_arg\library;



class ConstUrlArgData
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

	// Exception message constants
    const EXCEPT_MSG_DATA_SRC_INVALID_FORMAT =
        'Following data source "%1$s" invalid! 
        The data source must be a valid array, with valid key and valid value.';
    const EXCEPT_MSG_PATH_INVALID_FORMAT =
        'Following path "%1$s" invalid! 
        The path must be a valid string.';
    const EXCEPT_MSG_VALUE_INVALID_FORMAT =
        'Following value "%1$s" invalid! 
        The value must be a string or a valid array of strings.';
}