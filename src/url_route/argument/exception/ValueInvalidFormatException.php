<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\url_route\argument\exception;

use Exception;

use liberty_code\library\str\library\ToolBoxString;
use liberty_code\http\url_route\argument\library\ConstUrlRouteArgData;



class ValueInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $value
     */
	public function __construct($value)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf(ConstUrlRouteArgData::EXCEPT_MSG_VALUE_INVALID_FORMAT, strval($value));
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified value has valid format.
     *
     * @param mixed $value
     * @return boolean
     */
    public static function checkValueIsValid($value)
    {
        // Init var
        $result =
            // Check is valid string convertible
            is_string($value) ||
            ToolBoxString::checkConvertString($value);

        // Return result
        return $result;
    }



	/**
	 * Check if specified value has valid format.
	 * 
     * @param mixed $value
	 * @return boolean
	 * @throws static
     */
    public static function setCheck($value)
    {
		// Init var
		$result = static::checkValueIsValid($value);
		
		// Throw exception if check not pass
		if(!$result)
		{
            throw new static(
                (
                    is_array($value) ||
                    (!ToolBoxString::checkConvertString($value))
                ) ?
                    serialize($value) :
                    $value
            );
		}
		
		// Return result
		return $result;
    }
	
	
	
}