<?php
/**
 * Description :
 * This class allows to define URL route argument data.
 * - Data source is array.
 * - Key is string, not empty
 * - Value is string
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\url_route\argument\model;

use liberty_code\data\data\table\model\TableData;

use liberty_code\data\data\library\ConstData;
use liberty_code\http\url_route\argument\exception\DataSrcInvalidFormatException;
use liberty_code\http\url_route\argument\exception\KeyInvalidFormatException;
use liberty_code\http\url_route\argument\exception\ValueInvalidFormatException;



class UrlRouteArgData extends TableData
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods validation
	// ******************************************************************************

	/**
	 * @inheritdoc
	 */
	public function beanCheckValidValue($key, $value, &$error = null)
	{
		// Init var
		$result = true;

		// Validation
		try
		{
			switch($key)
			{
				case ConstData::DATA_KEY_DEFAULT_DATA_SRC:
					DataSrcInvalidFormatException::setCheck($value);
					break;

				default:
					$result = parent::beanCheckValidValue($key, $value, $error);
					break;
			}
		}
		catch(\Exception $e)
		{
			$result = false;
			$error = $e;
		}

		// Return result
		return $result;
	}



    /**
     * @inheritdoc
     */
    public function checkValidKey($key, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            KeyInvalidFormatException::setCheck($key);
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }



	/**
	 * @inheritdoc
	 */
	public function checkValidValue($strPath, $value, &$error = null)
	{
		// Init var
		$result = true;

		// Validation
		try
		{
			ValueInvalidFormatException::setCheck($value);
		}
		catch(\Exception $e)
		{
			$result = false;
			$error = $e;
		}

		// Return result
		return $result;
	}
	
	
	
}