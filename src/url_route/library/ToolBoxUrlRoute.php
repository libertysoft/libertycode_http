<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\url_route\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\http\url_route\argument\exception\DataSrcInvalidFormatException;
use liberty_code\http\url_route\argument\exception\KeyInvalidFormatException;
use liberty_code\http\url_route\argument\exception\ValueInvalidFormatException;
use liberty_code\http\url_route\argument\model\UrlRouteArgData;



class ToolBoxUrlRoute extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods check
    // ******************************************************************************

    /**
     * Check if specified argument specification is valid.
     *
     * Argument specification array format:
     * ['string argument key' => null|'string default argument value']
     *
     * @param array $tabArgSpec
     * @return boolean
     */
    public static function checkArgSpecIsValid(array $tabArgSpec)
    {
        // Init var
        $result = true;

        // Run each argument spec
        $tabKey = array_keys($tabArgSpec);
        for($intCpt = 0; $result && ($intCpt < count($tabKey)); $intCpt++)
        {
            // Get info
            $key = $tabKey[$intCpt];
            $value = $tabArgSpec[$key];

            // Check valid argument spec
            $result = (
                KeyInvalidFormatException::checkKeyIsValid($key) &&
                (is_null($value) || ValueInvalidFormatException::checkValueIsValid($value))
            );
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get specified arguments data.
     * If argument specification provided, return only argument data,
     * specified on specification, where argument value found.
     *
     * Argument data array format:
     * @see UrlRouteArgData data source array format.
     *
     * Argument specification format:
     * null
     * OR
     * @see checkArgSpecIsValid() argument specification array format.
     *
     * Return format:
     * If success: @see UrlRouteArgData data source array format.
     * Else: null.
     *
     * @param array $tabArgData = array()
     * @param null|array $tabArgSpec = null
     * @return null|array
     */
    public static function getTabArgData(
        array $tabArgData = array(),
        array $tabArgSpec = null
    )
    {
        // Init var
        $result = null;

        if(
            DataSrcInvalidFormatException::checkDataSrcIsValid($tabArgData) &&
            (is_null($tabArgSpec) || static::checkArgSpecIsValid($tabArgSpec))
        )
        {
            // Init var
            $result = $tabArgData;

            if(!is_null($tabArgSpec))
            {
                // Init var
                $result = array();

                // Get arguments
                $boolSuccess = true;
                $tabKey = array_keys($tabArgSpec);
                // Run each argument (specified on specification)
                for($intCpt = 0; ($intCpt < count($tabKey)) && $boolSuccess; $intCpt++)
                {
                    $strKey = $tabKey[$intCpt];
                    $strValue = (
                        array_key_exists($strKey, $tabArgData) ?
                            // Get argument value, from data
                            strval($tabArgData[$strKey]) :
                            // Get default argument value, from specification
                            ((!is_null($tabArgSpec[$strKey])) ? strval($tabArgSpec[$strKey]) : $tabArgSpec[$strKey])
                    );
                    $boolSuccess = (!is_null($strValue));

                    // Register argument, if required (argument value found)
                    if($boolSuccess)
                    {
                        $result[$strKey] = $strValue;
                    }
                }

                // Get result
                $result = ($boolSuccess ? $result : null);
            }
        }

        // Return result
        return $result;
    }



    /**
     * Get specified route,
     * from specified arguments.
     * If arguments provided, replace argument keys, found on route,
     * with argument values.
     *
     * Argument data array format:
     * @see getTabArgData argument data array format.
     *
     * Argument specification format:
     * @see getTabArgData argument specification format.
     *
     * @param string $strRoute
     * @param array $tabArgData = array()
     * @param null|array $tabArgSpec = null
     * @return null|string
     */
    public static function getStrRoute(
        $strRoute,
        array $tabArgData = array(),
        array $tabArgSpec = null
    )
    {
        // Init var
        $tabArgData = static::getTabArgData($tabArgData, $tabArgSpec);
        $result = (
            (is_string($strRoute) && (!is_null($tabArgData))) ?
                str_replace(array_keys($tabArgData), array_values($tabArgData), $strRoute) :
                null
        );

        // Return result
        return $result;
    }



}