<?php
/**
 * Description :
 * This class allows to define http front controller class.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\request_flow\front\model;

use liberty_code\request_flow\front\model\DefaultFrontController;

use liberty_code\route\router\api\RouterInterface;
use liberty_code\request_flow\front\library\ConstFrontController;
use liberty_code\http\config\model\DefaultConfig;
use liberty_code\http\request_flow\request\model\HttpRequest;
use liberty_code\http\request_flow\response\model\HttpResponse;
use liberty_code\http\request_flow\front\exception\ActiveRequestInvalidFormatException;
use liberty_code\http\request_flow\front\exception\DefaultResponseInvalidFormatException;



class HttpFrontController extends DefaultFrontController
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor / Others
    // ******************************************************************************
	
	/**
     * @inheritdoc
     * @param RouterInterface $objRouter = null
     * @param HttpRequest $objActiveRequest = null
	 * @param HttpResponse $objDefaultResponse = null
	 * @param boolean $boolDefaultResponseUse = null
	 * @param string|integer $optSelectReponse = null
     */
    public function __construct(
        RouterInterface $objRouter = null,
        HttpRequest $objActiveRequest = null,
        HttpResponse $objDefaultResponse = null,
        $boolDefaultResponseUse = null,
        $optSelectReponse = null
    )
    {
        // Call parent constructor
        parent::__construct(
            $objRouter,
            $objActiveRequest,
            $objDefaultResponse,
            $boolDefaultResponseUse,
            $optSelectReponse
        );
    }





	// Methods validation
	// ******************************************************************************

	/**
	 * @inheritdoc
	 */
	public function beanCheckValidValue($key, $value, &$error = null)
	{
		// Init var
		$result = true;

		// Validation
		try
		{
			switch($key)
			{
				case ConstFrontController::DATA_KEY_ACTIVE_REQUEST:
					ActiveRequestInvalidFormatException::setCheck($value);
					break;

				case ConstFrontController::DATA_KEY_DEFAULT_RESPONSE:
					DefaultResponseInvalidFormatException::setCheck($value);
					break;

				default:
					$result = parent::beanCheckValidValue($key, $value, $error);
					break;
			}
		}
		catch(\Exception $e)
		{
			$result = false;
			$error = $e;
		}

		// Return result
		return $result;
	}

	
	
	

	// Methods execute
	// ******************************************************************************
	
	/**
	 * @inheritdoc
	 * @return null|HttpResponse
	 */
	protected function getObjResponseFromExec($objResponse)
	{
		// Init var
		$result = null;
		$objDefaultResponse = $this->getObjDefaultResponse();
		$boolDefaultResponse = $this->checkDefaultResponseUse();
		
		// Get specified response if valid
		if($objResponse instanceof HttpResponse)
		{
			$result = $objResponse;
		}
		// Get default response if required and valid
		else if($boolDefaultResponse && (!is_null($objDefaultResponse)))
		{
			$result = $objDefaultResponse;
		}
		
		// Return result
        return $result;
	}





    // Methods statics
    // ******************************************************************************

    /**
     * Get configuration.
     *
     * @return DefaultConfig
     */
    public static function getObjConfigHttp()
    {
        // Return result
        return DefaultConfig::instanceGetDefault();
    }



}