<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\request_flow\front\exception;

use liberty_code\http\request_flow\response\model\HttpResponse;
use liberty_code\http\request_flow\front\library\ConstHttpFrontController;



class DefaultResponseInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $response
     */
	public function __construct($response)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstHttpFrontController::EXCEPT_MSG_DEFAULT_RESPONSE_INVALID_FORMAT,
            mb_strimwidth(strval($response), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified response has valid format
	 * 
     * @param mixed $response
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($response)
    {
		// Init var
		$result = (
			(is_null($response)) ||
			($response instanceof HttpResponse)
		);
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($response);
		}
		
		// Return result
		return $result;
    }
	
	
	
}