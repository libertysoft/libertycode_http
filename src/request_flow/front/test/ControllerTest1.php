<?php

namespace liberty_code\http\request_flow\front\test;

use liberty_code\http\request_flow\response\library\ToolBoxHttpResponse;
use liberty_code\http\request_flow\response\redirection\model\RedirectResponse;
use liberty_code\http\request_flow\front\model\HttpFrontController;



class ControllerTest1
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods action
    // ******************************************************************************
	
    public function action($strAdd = '', $strAdd2 = '')
    {
		// Init var
		/** @var HttpFrontController $objFrontController */
		$objFrontController = HttpFrontController::instanceGet(0);
		
		// Init active route(s)
		$tabStrRoute = array();
		$tabRoute = $objFrontController->getTabActiveRoute();
		foreach($tabRoute as $objRoute)
		{
			$tabStrRoute[] = $objRoute->getStrKey();
		}
		
		// Get data
		$tabData = array(
			'argument' => [
				'add_1' => $strAdd,
				'add_2' => $strAdd2,
			],
			'route' => $tabStrRoute
		);
		
		// Get response
		$objResponse = ToolBoxHttpResponse::getObjJsonResponse($tabData);
		
        // Return result
        return $objResponse;
    }
	
	
	
	public function action2($strAdd = '')
    {
		// Init var
		/** @var HttpFrontController $objFrontController */
		// $objFrontController = HttpFrontController::instanceGet(0);
		
		// Get response
		$objResponse = new RedirectResponse();
		$objResponse->setStrUrl('/LibertyCode/libertycode_http/request_flow/response/test/Test.php');
		$objResponse->setTabGetArg(array(
			'test_1' => $strAdd,
		));
		
        // Return result
        return $objResponse;
    }
	
	
	
	public function action3($strAdd = '')
    {
		// Init var
		/** @var HttpFrontController $objFrontController */
		// $objFrontController = HttpFrontController::instanceGet(0);
    }
	
	
	
}