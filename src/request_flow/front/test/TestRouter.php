<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Load test
require_once($strRootAppPath . '/src/request_flow/front/test/ControllerTest1.php');
require_once($strRootAppPath . '/src/request_flow/front/test/ControllerTest2.php');

// Use
use liberty_code\register\register\memory\model\MemoryRegister;
use liberty_code\di\dependency\preference\model\Preference;
use liberty_code\di\dependency\model\DefaultDependencyCollection;
use liberty_code\di\provider\model\DefaultProvider;
use liberty_code\call\call\file\library\ConstFileCall;
use liberty_code\call\call\di\func\library\ConstFunctionCall;
use liberty_code\call\call\factory\file\model\FileCallFactory;
use liberty_code\call\call\factory\di\model\DiCallFactory;
use liberty_code\route\route\model\DefaultRouteCollection;
use liberty_code\route\build\model\DefaultBuilder;
use liberty_code\route\router\model\DefaultRouter;
use liberty_code\http\route\factory\model\HttpRouteFactory;



// Init DI
$objRegister = new MemoryRegister();
$objDepCollection = new DefaultDependencyCollection($objRegister);
$objProvider = new DefaultProvider($objDepCollection);

$objPref = new Preference(array(
    'source' => 'liberty_code\\di\\provider\\api\\ProviderInterface',
    'set' =>  ['type' => 'instance', 'value' => $objProvider],
    'option' => [
        'shared' => true
    ]
));
$objProvider->getObjDependencyCollection()->setDependency($objPref);



// Init call factory
$tabConfig = array(
    ConstFileCall::TAB_CONFIG_KEY_FILE_PATH_FORMAT_PATTERN => $strRootAppPath . '/%1$s',
    ConstFileCall::TAB_CONFIG_KEY_FILE_PATH_FORMAT_REQUIRE => true,
    ConstFunctionCall::TAB_CONFIG_KEY_FILE_PATH_FORMAT_PATTERN => $strRootAppPath . '/%1$s',
    ConstFunctionCall::TAB_CONFIG_KEY_FILE_PATH_FORMAT_REQUIRE => true
);

$objCallFactory = new FileCallFactory(
    $tabConfig
);

$objCallFactory = new DiCallFactory(
    $objProvider,
    $tabConfig,
    null,
    null,
    $objCallFactory
);



// Init router
$objRouteCollection = new DefaultRouteCollection($objCallFactory);
$objRouteCollection->setMultiMatch(false);
//$objRouteCollection->setMultiMatch(true);
$objRouter = new DefaultRouter($objRouteCollection);



// Hydrate route collection
$objRouteFactory = new HttpRouteFactory();
$objRouteBuilder = new DefaultBuilder($objRouteFactory);

$tabDataSrc = array(
    'route_1' => [
        'source' => 'hello/{strAdd}/{strAdd2}/test',
        'call' => [
            'class_path_pattern' => 'liberty_code\\http\\request_flow\\front\\test\\ControllerTest1:action'
        ]
    ],
    'route_2' => [
        'source' => 'hello/{strAdd}/test2',
        'source_method' => 'GET',
        'call' => [
            'class_path_pattern' => 'liberty_code\\http\\request_flow\\front\\test\\ControllerTest2:action'
        ]
    ],
    'route_3' => [
        'source' => 'hello/{strAdd}/test3',
        'source_method' => 'POST',
        'call' => [
            'class_path_pattern' => 'liberty_code\\http\\request_flow\\front\\test\\ControllerTest2:action2'
        ]
    ],
    'route_4' => [
        'source' => 'hello/{strAdd}/test4',
        //'source_method' => 'GET',
        'call' => [
            'class_path_pattern' => 'liberty_code\\http\\request_flow\\front\\test\\ControllerTest1:action3'
        ]
    ],
    'route_pattern_1' => [
        'type' => 'http_pattern',
        'source' => '.*/([^/]+)/test\d$',
        'source_method' => 'GET',
        'call' => [
            'class_path_pattern' => 'liberty_code\\http\\request_flow\\front\\test\\ControllerTest1:action2'
        ]
    ]
);

$objRouteBuilder->setTabDataSrc($tabDataSrc);
$objRouteBuilder->hydrateRouteCollection($objRouteCollection);


