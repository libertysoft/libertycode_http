<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../../..';

// Load test
require_once($strRootAppPath . '/src/request_flow/front/test/TestRouter.php');
require_once($strRootAppPath . '/src/request_flow/request/test/HttpRequestTest.php');

// Use
use liberty_code\request_flow\front\library\ConstFrontController;
use liberty_code\http\config\model\DefaultConfig;
use liberty_code\http\request_flow\request\test\HttpRequestTest;
use liberty_code\http\request_flow\response\model\HttpResponse;
use liberty_code\http\request_flow\front\model\HttpFrontController;



// Init var
ob_start();
/** @var DefaultConfig $objConfig */
$objConfig = DefaultConfig::instanceGetDefault();
/** @var HttpRequestTest $objActiveRequest */
$objActiveRequest = HttpRequestTest::instanceGetDefault();
$objFrontController = new HttpFrontController();



// Init default response
$objDefaultResponse = new HttpResponse();
$objDefaultResponse->setIntStatusCode(201);
$objDefaultResponse->setStrStatusMsg('Default response send');



// Test get properties
echo('Test get properties : <br />');

echo('Get router: <pre>');print_r($objFrontController->getObjRouter());echo('</pre>');
echo('Get active request: <pre>');print_r($objFrontController->getObjActiveRequest());echo('</pre>');
echo('Get default response: <pre>');print_r($objFrontController->getObjDefaultResponse());echo('</pre>');
echo('Check use default response: <pre>');var_dump($objFrontController->checkDefaultResponseUse());echo('</pre>');
echo('Get select response option: <pre>');print_r($objFrontController->getOptSelectResponse());echo('</pre>');
echo('<br /><br /><br />');



// Test set properties
echo('Test set properties : <br />');

/*
try{
	//$objFrontController->setRouter('test');
} catch(\Exception $e) {
	echo($e->getMessage());
	echo('<br />');
}
//*/

/*
try{
	//$objFrontController->setActiveRequest('test');
} catch(\Exception $e) {
	echo($e->getMessage());
	echo('<br />');
}
//*/

/*
try{
	//$objFrontController->setDefaultResponse('test');
} catch(\Exception $e) {
	echo($e->getMessage());
	echo('<br />');
}
//*/

try{
	$objFrontController->setDefaultResponseUse('test');
} catch(\Exception $e) {
	echo($e->getMessage());
	echo('<br />');
}

try{
	$objFrontController->setOptSelectResponse('test');
} catch(\Exception $e) {
	echo($e->getMessage());
	echo('<br />');
}

$objFrontController->setRouter($objRouter);
$objFrontController->setActiveRequest($objActiveRequest);
$objFrontController->setDefaultResponse($objDefaultResponse);
$objFrontController->setDefaultResponseUse(true);
$objFrontController->setOptSelectResponse(ConstFrontController::OPTION_SELECT_RESPONSE_VALUE_FIRST);

echo('Get router: <pre>');print_r($objFrontController->getObjRouter());echo('</pre>');
echo('Get active request: <pre>');print_r($objFrontController->getObjActiveRequest());echo('</pre>');
echo('Get default response: <pre>');print_r($objFrontController->getObjDefaultResponse());echo('</pre>');
echo('Check use default response: <pre>');var_dump($objFrontController->checkDefaultResponseUse());echo('</pre>');
echo('Get select response option: <pre>');print_r($objFrontController->getOptSelectResponse());echo('</pre>');
echo('<br /><br /><br />');



// Test execute
$strContent = ob_get_clean();
$objFrontController->getObjDefaultResponse()->setContent($strContent);
// Normal case
try{
	$objResponse = $objFrontController->execute();
// Exception case
} catch(\Exception $e) {
	$objResponse = $objFrontController->getObjDefaultResponse();
	$objResponse->setIntStatusCode(404);
	$objResponse->setStrStatusMsg('Http request test not found');
	$objResponse->setContent($objResponse->getContent() . $e->getMessage() . '<br />');
}



// Test send
$objResponse->send();


