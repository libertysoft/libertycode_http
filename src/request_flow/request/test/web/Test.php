<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Load test
require_once($strRootAppPath . '/src/request_flow/request/test/HttpRequestTest.php');

// Use
use liberty_code\http\config\model\DefaultConfig;
use liberty_code\http\request_flow\request\library\ConstHttpRequest;
use liberty_code\http\request_flow\request\test\HttpRequestTest;



// Init var
/** @var DefaultConfig $objConfig */
$objConfig = DefaultConfig::instanceGetDefault();
/** @var HttpRequestTest $objRequest */
$objRequest = HttpRequestTest::instanceGetDefault();



// Test configuration
echo('Test configuration: <br />');
$objConfig->setStrHttpRequestSourceLink('//');
echo('Get source link: <pre>');var_dump($objRequest::getObjConfig()->getStrHttpRequestSourceLink());echo('</pre>');
echo('<br /><br /><br />');



// Test items
$tabKey = array(
	'test_1', 
	'test_2'
);

foreach($tabKey as $strKey)
{
	// Test arguments GET
	echo('Test argument GET "' . $strKey . '": <br />');
	if($objRequest->checkGetExists($strKey))
	{
		echo('Argument GET : <pre>');var_dump($objRequest->getGet($strKey));echo('</pre>');
	}
	else
	{
		echo('Argument GET unfound<br />');
	}
	
	echo('<br />');
	
	// Test arguments POST
	echo('Test argument POST "' . $strKey . '": <br />');
	if($objRequest->checkPostExists($strKey))
	{
		echo('Argument POST : <pre>');var_dump($objRequest->getPost($strKey));echo('</pre>');
	}
	else
	{
		echo('Argument POST unfound<br />');
	}
	
	echo('<br />');
	
	// Test arguments PUT
	echo('Test argument PUT "' . $strKey . '": <br />');
	if($objRequest->checkPutExists($strKey))
	{
		echo('Argument PUT : <pre>');var_dump($objRequest->getPut($strKey));echo('</pre>');
	}
	else
	{
		echo('Argument PUT unfound<br />');
	}
	
	echo('<br />');
	
	// Test arguments
	echo('Test argument "' . $strKey . '": <br />');
	if($objRequest->checkArgExists($strKey))
	{
		echo('Argument : <pre>');var_dump($objRequest->getArg($strKey));echo('</pre>');
	}
	else
	{
		echo('Argument unfound<br />');
	}
	
	echo('<br />');
	
	// Test cookies
	echo('Test cookie "' . $strKey . '": <br />');
	if($objRequest->checkCookieExists($strKey))
	{
		echo('Cookie : <pre>');var_dump($objRequest->getCookie($strKey));echo('</pre>');
	}
	else
	{
		echo('Cookie unfound<br />');
	}
	
	echo('<br />');
	
	// Test headers
	echo('Test header "' . $strKey . '": <br />');
	if($objRequest->checkHeaderExists($strKey))
	{
		echo('Header : <pre>');var_dump($objRequest->getHeader($strKey));echo('</pre>');
	}
	else
	{
		echo('Header unfound<br />');
	}
	
	echo('<br />');
	
	// Test files
	echo('Test file "' . $strKey . '": <br />');
	if($objRequest->checkFileExists($strKey))
	{
		echo('File : <pre>');var_dump($objRequest->getFile($strKey));echo('</pre>');
	}
	else
	{
		echo('File unfound<br />');
	}
	
	echo('<br />');
}

echo('<br /><br /><br />');



// Test arguments GET
echo('All arguments GET : <pre>');var_dump($objRequest->getGet());echo('</pre>');
echo('<br /><br /><br />');

// Test arguments POST
echo('All arguments POST : <pre>');var_dump($objRequest->getPost());echo('</pre>');
echo('<br /><br /><br />');

// Test arguments PUT
echo('All arguments PUT : <pre>');var_dump($objRequest->getPut());echo('</pre>');
echo('<br /><br /><br />');

// Test arguments
echo('All arguments : <pre>');var_dump($objRequest->getArg());echo('</pre>');
echo('<br /><br /><br />');

// Test cookies
echo('All cookies : <pre>');var_dump($objRequest->getCookie());echo('</pre>');
echo('<br /><br /><br />');

// Test headers
echo('All headers : <pre>');var_dump($objRequest->getHeader());echo('</pre>');
echo('<br /><br /><br />');

// Test files
echo('All files : <pre>');var_dump($objRequest->getFile());echo('</pre>');
echo('<br /><br /><br />');



// Test body
if($objRequest->checkBodyExists())
{
	echo('Body : <pre>');var_dump($objRequest->getStrBody());echo('</pre>');
}
else
{
	echo('Body unfound<br />');
}

echo('<br /><br /><br />');



// Test schema
echo('Schema : <pre>');var_dump($objRequest->getStrSchema());echo('</pre>');
echo('Schema is secure: <pre>');var_dump($objRequest->checkIsSecure());echo('</pre>');

$callIsHttps = function()
{
	/** @var HttpRequestTest $objRequest */
	$objRequest = HttpRequestTest::instanceGetDefault();
	$strHeader = $objRequest->getHeader('HTTP_X_FORWARDED_PROTO', ConstHttpRequest::CONF_SCHEMA_HTTP);
	return ($strHeader == ConstHttpRequest::CONF_SCHEMA_HTTPS);
};
echo('Schema (with callback https): <pre>');var_dump($objRequest->getStrSchema(true, true, $callIsHttps));echo('</pre>');
echo('Schema is secure (with callback https): <pre>');var_dump($objRequest->checkIsSecure(true, true, $callIsHttps));echo('</pre>');

echo('<br /><br /><br />');



// Test ajax
echo('Request is ajax call: <pre>');var_dump($objRequest->checkIsAjax());echo('</pre>');

$callIsAjax = function()
{
	/** @var HttpRequestTest $objRequest */
	$objRequest = HttpRequestTest::instanceGetDefault();
	$strHeader = $objRequest->getHeader('Requested-With', ''); // X-Requested-With
	return (strtolower($strHeader) == strtolower(ConstHttpRequest::CONF_AJAX_ENGINE));
};
echo('Request is ajax call (with callback ajax): <pre>');var_dump($objRequest->checkIsAjax($callIsAjax));echo('</pre>');

echo('<br /><br /><br />');



// Test structure
echo('Host : <pre>');var_dump($objRequest->getStrHost());echo('</pre>');
echo('URL : <pre>');var_dump($objRequest->getStrUrl());echo('</pre>');
echo('Method : <pre>');var_dump($objRequest->getStrMethod());echo('</pre>');
echo('Route source : <pre>');var_dump($objRequest->getStrRouteSrc());echo('</pre>');

echo('<br /><br /><br />');


