<?php
/**
 * Description :
 * This class allows to define HTTP request test class.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\request_flow\request\test;

use liberty_code\http\request_flow\request\model\HttpRequest;



class HttpRequestTest extends HttpRequest
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	// static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	// static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods getters
    // ******************************************************************************
	
	/**
	 * @inheritdoc
	 */
	public function getStrRoute()
	{
		// Return result
		return $this->getGet('route', 'N/A');
	}
	
	
	
}