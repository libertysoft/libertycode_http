<?php
/**
 * Description :
 * This class allows to define http request class.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\request_flow\request\model;

use liberty_code\request_flow\request\model\DefaultRequest;

use liberty_code\library\table\library\ToolBoxTable;
use liberty_code\http\config\model\DefaultConfig;
use liberty_code\http\request_flow\request\library\ConstHttpRequest;



abstract class HttpRequest extends DefaultRequest
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	/** @var null|array */
	protected $tabPut;
	
	/** @var null|array */
	protected $tabHeader;
	
	/** @var null|string */
	protected $strBody;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * Constructor
     */
    public function __construct()
    {
		// Call parent constructor
		parent::__construct();

        // Init var
		$this->tabPut = null;
		$this->tabHeader = null;
        $this->strBody = null;
    }
	
	
	
	
	
    // Methods check
	// ******************************************************************************

	/**
     * Check if specified GET argument exists.
     *
	 * @param string $strKey
     * @return boolean
     */
    public function checkGetExists($strKey)
	{
		// Return result
		return (!is_null($this->getGet($strKey, null)));
	}
	
	
	
	/**
     * Check if specified POST argument exists.
     *
	 * @param string $strKey
     * @return boolean
     */
    public function checkPostExists($strKey)
	{
		// Return result
		return (!is_null($this->getPost($strKey, null)));
	}
	
	
	
	/**
     * Check if specified PUT argument exists.
     *
	 * @param string $strKey
     * @return boolean
     */
    public function checkPutExists($strKey)
	{
		// Return result
		return (!is_null($this->getPut($strKey, null)));
	}
	
	
	
	/**
     * Check if specified argument exists (in $_REQUEST).
     *
	 * @param string $strKey
     * @return boolean
     */
    public function checkArgExists($strKey)
	{
		// Return result
		return (!is_null($this->getArg($strKey, null)));
	}
	
	
	
	/**
     * Check if specified cookie exists.
     *
	 * @param string $strKey
     * @return boolean
     */
    public function checkCookieExists($strKey)
	{
		// Return result
		return (!is_null($this->getCookie($strKey, null)));
	}
	
	
	
	/**
     * Check if specified header exists.
     *
	 * @param string $strKey
     * @return boolean
     */
    public function checkHeaderExists($strKey)
	{
		// Return result
		return (!is_null($this->getHeader($strKey, null)));
	}
	
	
	
	/**
     * Check if body found.
     *
     * @return boolean
     */
    public function checkBodyExists()
	{
		// Return result
		return (!is_null($this->getStrBody()));
	}
	
	
	
	/**
     * Check if request is an AJAX call.
     *
	 * @param callable $callIsAjax = null: boolean checkIsAjax() Callback specifying if current request is an ajax call
     * @return boolean
     */
    public function checkIsAjax($callIsAjax = null)
	{
		// Init var
        $result = (
			isset($_SERVER['HTTP_X_REQUESTED_WITH']) && 
			(strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == strtolower(ConstHttpRequest::CONF_AJAX_ENGINE))
		) || 
		((!is_null($callIsAjax)) && is_callable($callIsAjax) && ($callIsAjax() != false));
		
		// Return result
		return $result;
	}
	
	
	
	/**
     * Check if request is secured (https).
     *
	 * @param $boolForwardProto = true: Check if forwarded proxy is https, to specify if current request is https
	 * @param $boolForwardSsl = true: Check if forwarded ssl is https, to specify if current request is https
	 * @param callable $callIsHttps = null: boolean checkIsHttps() Callback specifying if current request is https
     * @return boolean
     */
    public function checkIsSecure($boolForwardProto = true, $boolForwardSsl = true, $callIsHttps = null)
	{
		// Return result
		return ($this->getStrSchema($boolForwardProto, $boolForwardSsl, $callIsHttps) == ConstHttpRequest::CONF_SCHEMA_HTTPS);
	}
	
	
	
	/**
     * Check if specified file exists.
     *
	 * @param string $strKey
     * @return boolean
     */
    public function checkFileExists($strKey)
	{
		// Return result
		return (!is_null($this->getFile($strKey, null)));
	}
	
	
	
	
	
	// Methods getters
    // ******************************************************************************

	/**
     * Get GET argument(s).
	 * If specified key provided: get specified argument.
	 * Else: get associative array of arguments.
     *
	 * @param string $strKey = null
	 * @param mixed $default = null
     * @return array|string|mixed
     */
    public function getGet($strKey = null, $default = null)
	{
		// Return result
		return ToolBoxTable::getItem($_GET, $strKey, $default);
	}
	
	
	
	/**
     * Get POST argument(s).
	 * If specified key provided: get specified argument.
	 * Else: get associative array of arguments.
     *
	 * @param string $strKey = null
	 * @param mixed $default = null
     * @return array|string|mixed
     */
    public function getPost($strKey = null, $default = null)
	{
		// Return result
		return ToolBoxTable::getItem($_POST, $strKey, $default);
	}
	
	
	
	/**
     * Get PUT argument(s).
	 * If specified key provided: get specified argument.
	 * Else: get associative array of arguments.
     *
	 * @param string $strKey = null
	 * @param mixed $default = null
     * @return array|string|mixed
     */
    public function getPut($strKey = null, $default = null)
	{
		// Init put if required
		if(is_null($this->tabPut))
		{
			parse_str($this->getStrBody(), $this->tabPut);
			if(!is_array($this->tabPut))
			{
				$this->tabPut = array();
			}
		}
		
		// Return result
		return ToolBoxTable::getItem($this->tabPut , $strKey, $default);
	}
	
	
	
	/**
     * Get argument(s) (in $_REQUEST).
	 * If specified key provided: get specified argument.
	 * Else: get associative array of arguments.
     *
	 * @param string $strKey = null
	 * @param mixed $default = null
     * @return array|string|mixed
     */
    public function getArg($strKey = null, $default = null)
	{
		// Return result
		return ToolBoxTable::getItem($_REQUEST , $strKey, $default);
	}
	
	
	
	/**
     * Get cookie(s).
	 * If specified key provided: get specified cookie.
	 * Else: get associative array of cookies.
     *
	 * @param string $strKey = null
	 * @param mixed $default = null
     * @return array|string|mixed
     */
    public static function getCookie($strKey = null, $default = null)
	{
		// Return result
		return ToolBoxTable::getItem($_COOKIE, $strKey, $default);
	}
	
	
	
	/**
     * Get header(s).
	 * If specified key provided: get specified header.
	 * Else: get associative array of headers.
     *
	 * @param string $strKey = null
	 * @param mixed $default = null
     * @return array|string|mixed
     */
    public function getHeader($strKey = null, $default = null)
	{
		// Init header if required
		if(is_null($this->tabHeader))
		{
			$this->tabHeader = (
			    // Check if function exists in case of command line call
			    function_exists('getallheaders') ?
                    getallheaders() :
                    array()
            );
			if(!is_array($this->tabHeader))
			{
				$this->tabHeader = array();
			}
		}
		
		// Return result
		return ToolBoxTable::getItem($this->tabHeader, $strKey, $default);
	}
	
	
	
	/**
     * Get body.
	 * 
     * @return null|string
     */
	public function getStrBody()
	{
		// Init body if required
		if(is_null($this->strBody))
		{
			$this->strBody = static::getStrRequestBody();
			if(!is_string($this->strBody))
			{
				$this->strBody = '';
			}
		}
		
		// Return result
		return $this->strBody;
	}
	
	
	
	/**
     * Get string schema (http/https)
     *
	 * @param $boolForwardProto = true: Check if forwarded proxy is https, to specify if current request is https
	 * @param $boolForwardSsl = true: Check if forwarded ssl is https, to specify if current request is https
	 * @param callable $callIsHttps = null: boolean checkIsHttps() Callback specifying if current request is https
     * @return string: ConstHttpRequest::CONF_SCHEMA_HTTP, ConstHttpRequest::CONF_SCHEMA_HTTPS
     */
    public function getStrSchema($boolForwardProto = true, $boolForwardSsl = true, $callIsHttps = null)
    {
        // Init var
        $result = ConstHttpRequest::CONF_SCHEMA_HTTP;

        // Check if request is https
        if(
            (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == 'on')) || 
            (
                is_bool($boolForwardProto) && $boolForwardProto &&
                isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && ($_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')
            ) ||
            (
                is_bool($boolForwardSsl) && $boolForwardSsl &&
                isset($_SERVER['HTTP_X_FORWARDED_SSL']) && ($_SERVER['HTTP_X_FORWARDED_SSL'] == 'on')
            ) ||
            ((!is_null($callIsHttps)) && is_callable($callIsHttps) && ($callIsHttps() != false))
        )
        {
            $result = ConstHttpRequest::CONF_SCHEMA_HTTPS;
        }

        // Return result
        return $result;
    }
	
	
	
	/**
     * Get Host.
	 * 
     * @return null|string
     */
	public function getStrHost()
	{
		// Init var
		$result = null;
		$tabKey = array(
			'SERVER_NAME', 
			'HTTP_HOST'
		);
		
		// Run all variables keys
		foreach($tabKey as $strKey)
		{
			// Get variable value if found
			if(
				isset($_SERVER[$strKey]) && 
				is_string($_SERVER[$strKey]) && 
				(trim($_SERVER[$strKey]) != '')
			)
			{
				$result = $_SERVER[$strKey];
				break;
			}
		}
		
		// Return result
        return $result;
	}
	
	
	
	/**
     * Get URL.
	 * 
     * @return string
     */
	public function getStrUrl()
	{
		// Init var
		$result = $_SERVER['REQUEST_URI'];
		
		// Remove GET arguments if found
		if(strpos($result, '?') !== false)
		{
			$tabStr = explode('?', $result);
			$result = $tabStr[0];
		}
		
		// Return result
		return $result;
	}
	
	
	
	/**
     * Get method (Upper case).
	 * 
     * @return string
     */
	public function getStrMethod()
	{
		// Return result
		return strtoupper($_SERVER['REQUEST_METHOD']);
	}
	
	
	
	/**
     * Get file(s).
	 * If specified key provided: get specified array of file info.
	 * Else: get associative array of files.
     *
	 * @param string $strKey = null
	 * @param mixed $default = null
     * @return array|mixed
     */
    public function getFile($strKey = null, $default = null)
	{
		// Return result
		return ToolBoxTable::getItem($_FILES, $strKey, $default);
	}
	
	
	
	/**
	 * @inheritdoc
	 */
	public function getStrRouteSrc()
	{
		// Return result
		return $this->getStrMethod() . static::getObjConfig()->getStrHttpRequestSourceLink() . $this->getStrRoute();
	}
	
	
	
	
	
	// Methods overwrite
    // ******************************************************************************
	
	/**
	 * Get string route, used in route source.
	 * 
	 * @return string
	 */
	abstract public function getStrRoute();
	
	
	
	
	
	// Methods static getters
    // ******************************************************************************

    /**
     * Get HTTP request flow configuration.
     *
     * @inheritdoc
     * @return DefaultConfig
     */
    public static function getObjConfig()
    {
        // Return result
        return DefaultConfig::instanceGetDefault();
    }



	/**
     * Get request body.
	 * 
     * @return null|string
     */
	protected static function getStrRequestBody()
	{
		// Init var
		$result = null;
		$strBody = file_get_contents('php://input');
		
		// Get body if found
		if(is_string($strBody))
		{
			$result = $strBody;
		}
		
		// Return result
		return $result;
	}
	
	
	
}