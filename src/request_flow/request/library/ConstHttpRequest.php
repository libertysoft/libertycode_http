<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\request_flow\request\library;



class ConstHttpRequest
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

	// Configuration schema
    const CONF_SCHEMA_HTTP = 'http';
	const CONF_SCHEMA_HTTPS = 'https';
	
	// Configuration ajax
	const CONF_AJAX_ENGINE = 'XMLHttpRequest';
}