<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Use
use liberty_code\http\request_flow\response\library\ToolBoxHttpResponse;



// Init var
$tabData = array(
	'test_1' => [
		'test_1_1' => 'value "1 1"',
		'test_1_2' => 'value "1 2"',
	],
	'test_2' => 'value 2'
);
$objResponse = ToolBoxHttpResponse::getObjYmlResponse($tabData);



// Set status
$objResponse->setIntStatusCode(202);
$objResponse->setStrStatusMsg('Response http YML Ok');



// Test send
$objResponse->send();


