<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Use
use liberty_code\http\request_flow\response\library\ToolBoxHttpResponse;
use liberty_code\http\request_flow\response\model\HttpResponse;



// Init var
$tabData = array(
	'root' => [
		'test_1_1' => 'value "1 1"',
		'test_1_2' => 'value "1 2"',
	]
);
$objResponse = new HttpResponse();



// Set status
$objResponse->setIntStatusCode(201);
$objResponse->setStrStatusMsg('Response http XML Ok');



// Set content
//ToolBoxHttpResponse::getObjXmlResponse($tabData, 'ISO-8859-1', $objResponse);
ToolBoxHttpResponse::getObjXmlResponse($tabData, null, $objResponse);



// Test send
$objResponse->send();


