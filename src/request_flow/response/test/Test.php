<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Use
use liberty_code\request_flow\config\model\DefaultConfig as BaseDefaultConfig;
use liberty_code\http\config\model\DefaultConfig;
use liberty_code\http\request_flow\response\model\HttpResponse;



// Init var
ob_start();
/** @var BaseDefaultConfig $objConfigBase */
$objConfigBase = BaseDefaultConfig::instanceGetDefault();
/** @var DefaultConfig $objConfig */
$objConfig = DefaultConfig::instanceGetDefault();
$objResponse = new HttpResponse();



// Test configuration
echo('Test configuration: <br />');
$objConfigBase->setStrDefaultResponseContent('Default content');
$objConfig->setIntHttpResponseStatusCode(201);
$objConfig->setStrHttpResponseStatusMsg('Test Ok');
$objConfig->setTabHttpResponseHeader(array(
	'Content-Type: text/html'
));
$objConfig->setTabHttpResponseHeaderAdd(array(
	'Accept-Language: fr-FR'
));
echo('Get default content: <pre>');var_dump($objResponse::getObjConfig()->getStrDefaultResponseContent());echo('</pre>');
echo('Get default status code: <pre>');var_dump($objResponse::getObjConfigHttp()->getIntHttpResponseStatusCode());echo('</pre>');
echo('Get default status message: <pre>');var_dump($objResponse::getObjConfigHttp()->getStrHttpResponseStatusMsg());echo('</pre>');
echo('Get default headers: <pre>');var_dump($objResponse::getObjConfigHttp()->getTabHttpResponseHeader());echo('</pre>');
echo('Get additional headers: <pre>');var_dump($objResponse::getObjConfigHttp()->getTabHttpResponseHeaderAdd());echo('</pre>');
echo('<br /><br /><br />');



// Test properties
echo('Test properties : <br />');

try{
	$objResponse->setIntStatusCode('test');
} catch(\Exception $e) {
	echo($e->getMessage());
	echo('<br />');
}

try{
	$objResponse->setStrStatusMsg('');
} catch(\Exception $e) {
	echo($e->getMessage());
	echo('<br />');
}

try{
	$objResponse->setContent(true);
} catch(\Exception $e) {
	echo($e->getMessage());
	echo('<br />');
}

$objResponse->setIntStatusCode(203);
$objResponse->setStrStatusMsg('Response http Ok');
$objResponse->setContent('Response http content');
echo('Get status code: <pre>');print_r($objResponse->getIntStatusCode());echo('</pre>');
echo('Get status message: <pre>');print_r($objResponse->getStrStatusMsg());echo('</pre>');
echo('Get content: <pre>');print_r($objResponse->getContent());echo('</pre>');

echo('<br /><br /><br />');



// Test headers
echo('Test headers : <br />');
$objResponse->getObjHeader()->putValue('Content-Type', 'text');
$objResponse->getObjHeader()->putValue('Header-Test-1', 'Value 1');
$objResponse->getObjHeader()->putValue('Header-Test-2', 'Value 2');
$objResponse->getObjHeader()->putValue('Header-Test-3', 'Value 3');
echo('Get headers: <pre>');print_r($objResponse->getObjHeader()->getDataSrc());echo('</pre>');
echo('<br /><br /><br />');



// Test send
$strContent = ob_get_clean();
//$objResponse->setContent($strContent);
$objResponse->send();


