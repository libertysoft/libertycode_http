<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\request_flow\response\library;



class ConstHttpResponse
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************
	
	// Data constants
	const DATA_KEY_STATUS_CODE = 'intStatusCode';
	const DATA_KEY_STATUS_MSG = 'strStatusMsg';
    const DATA_KEY_HEADER = 'objHeader';
	
	
	
	// Configuration
    const CONF_HEADER_FORMAT = '%1$s: %2$s';
	const CONF_HEADER_STATUS_FORMAT = 'HTTP/1.0 %1$s %2$s';
	const CONF_HEADER_CONTENT_TYPE_KEY = 'Content-Type';
	const CONF_HEADER_CONTENT_TYPE_VALUE_CHARSET_FORMAT = 'charset=%1$s';
	const CONF_HEADER_CONTENT_TYPE_VALUE_CHARSET_DEFAULT = 'UTF-8';
	const CONF_HEADER_CONTENT_TYPE_VALUE_JSON = 'application/json';
	const CONF_HEADER_CONTENT_TYPE_VALUE_XML = 'application/xml';
	const CONF_HEADER_CONTENT_TYPE_VALUE_YML = 'application/yaml';
	
	
	
	// Exception message constants
	const EXCEPT_MSG_STATUS_CODE_INVALID_FORMAT = 'Following status code "%1$s" invalid! The status code must be null or a positive integer.';
	const EXCEPT_MSG_STATUS_MSG_INVALID_FORMAT = 'Following status message "%1$s" invalid! The status message must be null or a string, not empty.';
}