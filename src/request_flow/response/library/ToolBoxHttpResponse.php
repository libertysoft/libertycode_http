<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\request_flow\response\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\request_flow\response\library\ToolBoxResponse;
use liberty_code\http\request_flow\response\library\ConstHttpResponse;
use liberty_code\http\request_flow\response\model\HttpResponse;



class ToolBoxHttpResponse extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods getters
	// ******************************************************************************
	
	/**
     * Get header content type charset value.
     *
	 * @param string $strCharset = ConstHttpResponse::CONF_HEADER_CONTENT_TYPE_VALUE_CHARSET_DEFAULT
	 * @param boolean $boolFormat = true
     * @return string
     */
    public static function getStrHeaderContentTypeCharset($strCharset = ConstHttpResponse::CONF_HEADER_CONTENT_TYPE_VALUE_CHARSET_DEFAULT, $boolFormat = true)
    {
		// Init var
		$result = ConstHttpResponse::CONF_HEADER_CONTENT_TYPE_VALUE_CHARSET_DEFAULT;
		
		// Get content type charset
		if(is_string($strCharset) && (trim($strCharset) !=''))
		{
			$result = $strCharset;
		}
		
		// Get formatted value if required
		if($boolFormat)
		{
			$result = sprintf(ConstHttpResponse::CONF_HEADER_CONTENT_TYPE_VALUE_CHARSET_FORMAT, $result);
		}
		
		// Return result
		return $result;
    }
	
	
	
	/**
     * Get header content type value.
     *
	 * @param string $strType
	 * @param string $strCharset = ConstHttpResponse::CONF_HEADER_CONTENT_TYPE_VALUE_CHARSET_DEFAULT
     * @return null|string
     */
    public static function getStrHeaderContentType($strType, $strCharset = ConstHttpResponse::CONF_HEADER_CONTENT_TYPE_VALUE_CHARSET_DEFAULT)
    {
		// Init var
		$result = null;
		
		// Get content type
		if(is_string($strType) && (trim($strType) !=''))
		{
			$result = $strType . '; ' . static::getStrHeaderContentTypeCharset($strCharset, true);
		}
		
		// Return result
		return $result;
    }
	
	
	
	/**
     * Get JSON response.
     *
	 * @param array $tabData
	 * @param string $strContentCharset = ConstHttpResponse::CONF_HEADER_CONTENT_TYPE_VALUE_CHARSET_DEFAULT
	 * @param HttpResponse $objResponse = null
     * @return HttpResponse
     */
    public static function getObjJsonResponse(array $tabData, $strContentCharset = ConstHttpResponse::CONF_HEADER_CONTENT_TYPE_VALUE_CHARSET_DEFAULT, HttpResponse $objResponse = null)
    {
		// Init var
		$result = $objResponse;
		if(is_null($result))
		{
			$result = new HttpResponse();
		}
		
		// Set header
		static::setHeaderContentType($result, ConstHttpResponse::CONF_HEADER_CONTENT_TYPE_VALUE_JSON, $strContentCharset);
		
		// Return result
        /** @var HttpResponse $result */
        $result = ToolBoxResponse::getObjJsonResponse($tabData, $result);
		return $result;
    }
	
	
	
	/**
     * Get XML response.
     *
	 * @param array $tabData
	 * @param string $strContentCharset = ConstHttpResponse::CONF_HEADER_CONTENT_TYPE_VALUE_CHARSET_DEFAULT
	 * @param HttpResponse $objResponse = null
     * @return HttpResponse
     */
    public static function getObjXmlResponse(array $tabData, $strContentCharset = ConstHttpResponse::CONF_HEADER_CONTENT_TYPE_VALUE_CHARSET_DEFAULT, HttpResponse $objResponse = null)
    {
		// Init var
		$result = $objResponse;
		if(is_null($result))
		{
			$result = new HttpResponse();
		}
		
		// Set header
		static::setHeaderContentType($result, ConstHttpResponse::CONF_HEADER_CONTENT_TYPE_VALUE_XML, $strContentCharset);
		
		// Return result
        /** @var HttpResponse $result */
        $result = ToolBoxResponse::getObjXmlResponse($tabData, $result);
		return $result;
    }
	
	
	
	/**
     * Get XML response (with attributes).
     *
	 * @param array $tabData
	 * @param string $strContentCharset = ConstHttpResponse::CONF_HEADER_CONTENT_TYPE_VALUE_CHARSET_DEFAULT
	 * @param HttpResponse $objResponse = null
     * @return HttpResponse
     */
    public static function getObjXmlAttributeResponse(array $tabData, $strContentCharset = ConstHttpResponse::CONF_HEADER_CONTENT_TYPE_VALUE_CHARSET_DEFAULT, HttpResponse $objResponse = null)
    {
		// Init var
		$result = $objResponse;
		if(is_null($result))
		{
			$result = new HttpResponse();
		}
		
		// Set header
		static::setHeaderContentType($result, ConstHttpResponse::CONF_HEADER_CONTENT_TYPE_VALUE_XML, $strContentCharset);
		
		// Return result
        /** @var HttpResponse $result */
        $result = ToolBoxResponse::getObjXmlAttributeResponse($tabData, $result);
		return $result;
    }
	
	
	
	/**
     * Get YML response.
     *
	 * @param array $tabData
	 * @param string $strContentCharset = ConstHttpResponse::CONF_HEADER_CONTENT_TYPE_VALUE_CHARSET_DEFAULT
	 * @param HttpResponse $objResponse = null
     * @return HttpResponse
     */
    public static function getObjYmlResponse(array $tabData, $strContentCharset = ConstHttpResponse::CONF_HEADER_CONTENT_TYPE_VALUE_CHARSET_DEFAULT, HttpResponse $objResponse = null)
    {
		// Init var
		$result = $objResponse;
		if(is_null($result))
		{
			$result = new HttpResponse();
		}
		
		// Set header
		static::setHeaderContentType($result, ConstHttpResponse::CONF_HEADER_CONTENT_TYPE_VALUE_YML, $strContentCharset);

		// Return result
        /** @var HttpResponse $result */
        $result = ToolBoxResponse::getObjYmlResponse($tabData, $result);
		return $result;
    }
	
	
	
	
	
	// Methods setters
	// ******************************************************************************
	
	/**
	 * Set specified header content type in specified response.
     *
	 * @param HttpResponse $objResponse
	 * @param string $strType
	 * @param string $strCharset = ConstHttpResponse::CONF_HEADER_CONTENT_TYPE_VALUE_CHARSET_DEFAULT
     */
    public static function setHeaderContentType(HttpResponse $objResponse, $strType, $strCharset = ConstHttpResponse::CONF_HEADER_CONTENT_TYPE_VALUE_CHARSET_DEFAULT)
    {
		// Init var
		$strHeaderContentType = static::getStrHeaderContentType($strType, $strCharset);
		
		// Set header if valid
		if(!is_null($strHeaderContentType) && is_string($strHeaderContentType))
		{
			$objResponse->getObjHeader()->putValue(ConstHttpResponse::CONF_HEADER_CONTENT_TYPE_KEY, $strHeaderContentType);
		}
    }
	
	
	
}