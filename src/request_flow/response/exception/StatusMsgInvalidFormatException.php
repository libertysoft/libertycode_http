<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\request_flow\response\exception;

use liberty_code\http\request_flow\response\library\ConstHttpResponse;



class StatusMsgInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $statusMsg
     */
	public function __construct($statusMsg)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf(ConstHttpResponse::EXCEPT_MSG_STATUS_MSG_INVALID_FORMAT, strval($statusMsg));
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified status message has valid format.
	 * 
     * @param mixed $statusMsg
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($statusMsg)
    {
		// Init var
		$result = 
			is_null($statusMsg) || // Check is null
			(is_string($statusMsg) && (trim($statusMsg) != '')); // Check is valid string
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($statusMsg);
		}
		
		// Return result
		return $result;
    }
	
	
	
}