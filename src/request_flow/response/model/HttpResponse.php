<?php
/**
 * Description :
 * This class allows to define http response class.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\request_flow\response\model;

use liberty_code\request_flow\response\model\DefaultResponse;

use liberty_code\http\config\library\ConstConfig;
use liberty_code\http\config\model\DefaultConfig;
use liberty_code\http\header\model\HeaderData;
use liberty_code\http\request_flow\response\library\ConstHttpResponse;
use liberty_code\http\request_flow\response\exception\StatusCodeInvalidFormatException;
use liberty_code\http\request_flow\response\exception\StatusMsgInvalidFormatException;



/**
 * @method null|integer getIntStatusCode() Get status code.
 * @method null|string getStrStatusMsg() Get status message.
 * @method HeaderData getObjHeader() Get headers data object.
 * @method void setIntStatusCode(integer $intStatusCode) Set status code.
 * @method void setStrStatusMsg(string $strStatusMsg) Set status message.
 * @method void setObjHeader(HeaderData $objHeader) Set headers data object.
 */
class HttpResponse extends DefaultResponse
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods initialize
    // ******************************************************************************
	
    /**
     * Overwrite hydrateDefault().
     */
    protected function beanHydrateDefault()
    {	
        // Init bean data
		if(!$this->beanExists(ConstHttpResponse::DATA_KEY_STATUS_CODE))
        {
            $this->beanAdd(ConstHttpResponse::DATA_KEY_STATUS_CODE, null);
        }
		
		if(!$this->beanExists(ConstHttpResponse::DATA_KEY_STATUS_MSG))
        {
            $this->beanAdd(ConstHttpResponse::DATA_KEY_STATUS_MSG, null);
        }
		
		if(!$this->beanExists(ConstHttpResponse::DATA_KEY_HEADER))
        {
            $this->beanAdd(ConstHttpResponse::DATA_KEY_HEADER, new HeaderData());
        }
		
		// Call parent method
        parent::beanHydrateDefault();
    }





	// Methods validation
	// ******************************************************************************

	/**
	 * @inheritdoc
	 */
	public function beanCheckValidKey($key, &$error = null)
	{
		// Init var
		$tabKey = array(
			ConstHttpResponse::DATA_KEY_STATUS_CODE,
			ConstHttpResponse::DATA_KEY_STATUS_MSG,
			ConstHttpResponse::DATA_KEY_HEADER
		);
		$result =
			in_array($key, $tabKey) ||
			parent::beanCheckValidKey($key, $error);

		// Return result
		return $result;
	}



	/**
	 * @inheritdoc
	 */
	public function beanCheckValidValue($key, $value, &$error = null)
	{
		// Init var
		$result = true;

		// Validation
		try
		{
			switch($key)
			{
				case ConstHttpResponse::DATA_KEY_STATUS_CODE:
					StatusCodeInvalidFormatException::setCheck($value);
					break;

				case ConstHttpResponse::DATA_KEY_STATUS_MSG:
					StatusMsgInvalidFormatException::setCheck($value);
					break;

				case ConstHttpResponse::DATA_KEY_HEADER:
					$result = ($value instanceof HeaderData);
					break;

				default:
					$result = parent::beanCheckValidValue($key, $value, $error);
					break;
			}
		}
		catch(\Exception $e)
		{
			$result = false;
			$error = $e;
		}

		// Return result
		return $result;
	}




	
	// Methods getters
    // ******************************************************************************
	
	/**
     * Get status code (ready to send).
     * 
	 * @return integer
     */
    protected function getIntStatusCodeToSend()
    {
		// Init var
        $result = ConstConfig::DATA_DEFAULT_VALUE_HTTP_RESPONSE_STATUS_CODE;
		$intDefaultStatusCode = static::getObjConfigHttp()->getIntHttpResponseStatusCode();
		$intStatusCode = $this->getIntStatusCode();
		
		// Register status code in result if found
		if((!is_null($intStatusCode)) && is_integer($intStatusCode))
		{
			$result = $intStatusCode;
		}
		// Register default status code in result if found
		else if((!is_null($intDefaultStatusCode)) && is_integer($intDefaultStatusCode))
		{
			$result = $intDefaultStatusCode;
		}
		
		// Return result
		return $result;
    }
	
	
	
	/**
     * Get status message (ready to send).
     * 
	 * @return null|string
     */
    protected function getStrStatusMsgToSend()
    {
		// Init var
        $result = ConstConfig::DATA_DEFAULT_VALUE_HTTP_RESPONSE_STATUS_MSG;
		$strDefaultStatusMsg = static::getObjConfigHttp()->getStrHttpResponseStatusMsg();
		$strStatusMsg = $this->getStrStatusMsg();
		
		// Register status message in result if found
		if((!is_null($strStatusMsg)) && is_string($strStatusMsg))
		{
			$result = $strStatusMsg;
		}
		// Register default status message in result if found
		else if((!is_null($strDefaultStatusMsg)) && is_string($strDefaultStatusMsg))
		{
			$result = $strDefaultStatusMsg;
		}
		
		// Return result
		return $result;
    }
	
	
	
    /**
     * Get index array of headers.
	 * 
	 * @return array
     */
    protected function getTabHeader()
    {
		// Init var
        $result = array();
		$tabHeader = $this->getObjHeader()->getDataSrc();
		
		// Run all headers
		foreach($tabHeader as $strKey => $strValue)
		{
			// Get header and register in result
			$strHeader = sprintf(ConstHttpResponse::CONF_HEADER_FORMAT, $strKey, $strValue);
			$result[] = $strHeader;
		}
		
		// Return result
		return $result;
    }
	
	
	
	/**
     * Get index array of headers (ready to send).
	 * 
	 * @return array
     */
    protected function getTabHeaderToSend()
    {
		// Init var
        $result = array();
		$tabHeader = $this->getTabHeader();
		$tabDefaultHeader = static::getObjConfigHttp()->getTabHttpResponseHeader();
		$tabHeaderAdd = static::getObjConfigHttp()->getTabHttpResponseHeaderAdd();
		
		// Register headers in result if found
		if(is_array($tabHeader) && (count($tabHeader) > 0))
		{
			$result = $tabHeader;
		}
		// Register default headers in result if found
		else if(is_array($tabDefaultHeader) && (count($tabDefaultHeader) > 0))
		{
			$result = $tabDefaultHeader;
		}
		
		// Add additional headers in result if found
		if(is_array($tabHeaderAdd) && (count($tabHeaderAdd) > 0))
		{
			$result = array_merge($result, $tabHeaderAdd);
		}
		
		// Return result
		return $result;
    }
	
	
	
	
	
	// Methods execute
	// ******************************************************************************
	
	/**
     * Send status code (and status message if found).
     */
    protected function sendStatus()
    {
		// Init var
		$intStatusCode = $this->getIntStatusCodeToSend();
		$strStatusMsg = $this->getStrStatusMsgToSend();
		
		// Set status with code and message
		if((!is_null($strStatusMsg)) && is_string($strStatusMsg))
		{
			$strHeader = sprintf(ConstHttpResponse::CONF_HEADER_STATUS_FORMAT, strval($intStatusCode), $strStatusMsg);
			header($strHeader, true);
		}
		// Set status only with code
		else
		{
			http_response_code($intStatusCode);
		}
    }
	
	
	
	/**
     * Send headers.
     */
    protected function sendHeader()
    {
		// Init var
		$tabHeader = $this->getTabHeaderToSend();
		
		// Run all headers
		foreach($tabHeader as $strHeader)
		{
			// Set header
			header($strHeader, true);
		}
    }
	
	
	
	/**
	 * @inheritdoc
	 */
	public function send()
	{
		// Set header
		$this->sendHeader();
		
		// Set status
		$this->sendStatus();
		
		// Call parent method
        parent::send();
	}





    // Methods statics
    // ******************************************************************************

    /**
     * Get configuration.
     *
     * @return DefaultConfig
     */
    public static function getObjConfigHttp()
    {
        // Return result
        return DefaultConfig::instanceGetDefault();
    }



}