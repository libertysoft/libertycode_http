<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Use
use liberty_code\http\config\model\DefaultConfig;
use liberty_code\http\request_flow\response\redirection\model\RedirectResponse;



// Init var
ob_start();
/** @var DefaultConfig $objConfig */
$objConfig = DefaultConfig::instanceGetDefault();
$objResponse = new RedirectResponse();



// Test configuration
echo('Test configuration: <br />');
$objConfig->setIntHttpRedirectResponseStatusCode(303);
$objConfig->setStrHttpRedirectResponseUrl('/LibertyCode/libertycode_requestflow/request/http/test/Test.php');
echo('Get default redirection status code: <pre>');var_dump($objResponse::getObjConfigHttp()->getIntHttpRedirectResponseStatusCode());echo('</pre>');
echo('Get default redirection URL: <pre>');var_dump($objResponse::getObjConfigHttp()->getStrHttpRedirectResponseUrl());echo('</pre>');
echo('<br /><br /><br />');



// Test properties
echo('Test properties : <br />');

try{
	$objResponse->setIntStatusCode(203);
} catch(\Exception $e) {
	echo($e->getMessage());
	echo('<br />');
}

try{
	$objResponse->setStrUrl(1);
} catch(\Exception $e) {
	echo($e->getMessage());
	echo('<br />');
}

try{
	$objResponse->setTabGetArg('');
} catch(\Exception $e) {
	echo($e->getMessage());
	echo('<br />');
}

$objResponse->setIntStatusCode(302);
$objResponse->setStrStatusMsg('Response http redirect Ok');
//$objResponse->setStrUrl('https://www.google.fr/');
//$objResponse->setStrUrl('../../test/Test.php'); // response/http/test
$objResponse->setStrUrl('/LibertyCode/libertycode_http/request_flow/response/test/Test.php');
//*
$objResponse->setTabGetArg(array(
	'test_1' => 'Value redirect 1',
	'test_2' => 'Value redirect 2'
));
//*/

echo('Get status code: <pre>');print_r($objResponse->getIntStatusCode());echo('</pre>');
echo('Get status message: <pre>');print_r($objResponse->getStrStatusMsg());echo('</pre>');
echo('Get URL: <pre>');print_r($objResponse->getStrUrl());echo('</pre>');
echo('Get GET arguments: <pre>');print_r($objResponse->getTabGetArg());echo('</pre>');

echo('<br /><br /><br />');



// Test send
$strContent = ob_get_clean();
$objResponse->setContent($strContent);
//echo($strContent);
$objResponse->send();


