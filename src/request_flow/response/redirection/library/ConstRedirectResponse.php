<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\request_flow\response\redirection\library;



class ConstRedirectResponse
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************
	
	// Data constants
	const DATA_KEY_URL = 'strUrl';
	const DATA_KEY_GET_ARG = 'tabGetArg';
	
	// Configuration
	const CONF_HEADER_REDIRECT_URL_FORMAT = 'Location: %1$s';
	
	// Exception message constants
	const EXCEPT_MSG_STATUS_CODE_INVALID_FORMAT = 'Following status code "%1$s" invalid! The status code must be null or a positive integer, like 3xx.';
	const EXCEPT_MSG_URL_INVALID_FORMAT = 'Following URL "%1$s" invalid! The URL must be a string, not empty.';
	const EXCEPT_MSG_GET_ARG_INVALID_FORMAT = 'Following GET arguments "%1$s" invalid! The GET arguments must be a valid array, with key and value as string, not empty.';
}