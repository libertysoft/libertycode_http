<?php
/**
 * Description :
 * This class allows to define http redirection response class.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\request_flow\response\redirection\model;

use liberty_code\http\request_flow\response\model\HttpResponse;

use liberty_code\http\config\library\ConstConfig;
use liberty_code\http\request_flow\response\library\ConstHttpResponse;
use liberty_code\http\request_flow\response\redirection\library\ConstRedirectResponse;
use liberty_code\http\request_flow\response\redirection\exception\StatusCodeInvalidFormatException;
use liberty_code\http\request_flow\response\redirection\exception\UrlInvalidFormatException;
use liberty_code\http\request_flow\response\redirection\exception\GetArgInvalidFormatException;



/**
 * @method null|string getStrUrl() Get URL redirection target.
 * @method array getTabGetArg() Get associative array of GET arguments.
 * @method void setStrUrl(string $strUrl) Set URL redirection target.
 * @method void setTabGetArg(array $tabGetArg) Set associative array of GET arguments.
 */
class RedirectResponse extends HttpResponse
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods initialize
    // ******************************************************************************
	
    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {	
        // Init bean data
		if(!$this->beanExists(ConstHttpResponse::DATA_KEY_STATUS_CODE))
        {
            $this->beanAdd(ConstHttpResponse::DATA_KEY_STATUS_CODE, null);
        }
		
		if(!$this->beanExists(ConstRedirectResponse::DATA_KEY_URL))
        {
            $this->beanAdd(ConstRedirectResponse::DATA_KEY_URL, null);
        }
		
		if(!$this->beanExists(ConstRedirectResponse::DATA_KEY_GET_ARG))
        {
            $this->beanAdd(ConstRedirectResponse::DATA_KEY_GET_ARG, array());
        }
		
		// Call parent method
        parent::beanHydrateDefault();
    }





	// Methods validation
	// ******************************************************************************

	/**
	 * @inheritdoc
	 */
	public function beanCheckValidKey($key, &$error = null)
	{
		// Init var
		$tabKey = array(
			ConstRedirectResponse::DATA_KEY_URL,
			ConstRedirectResponse::DATA_KEY_GET_ARG
		);
		$result =
			in_array($key, $tabKey) ||
			parent::beanCheckValidKey($key, $error);

		// Return result
		return $result;
	}



	/**
	 * @inheritdoc
	 */
	public function beanCheckValidValue($key, $value, &$error = null)
	{
		// Init var
		$result = true;

		// Validation
		try
		{
			switch($key)
			{
				case ConstHttpResponse::DATA_KEY_STATUS_CODE:
					StatusCodeInvalidFormatException::setCheck($value);
					break;

				case ConstRedirectResponse::DATA_KEY_URL:
					UrlInvalidFormatException::setCheck($value);
					break;

				case ConstRedirectResponse::DATA_KEY_GET_ARG:
					GetArgInvalidFormatException::setCheck($value);
					break;

				default:
					$result = parent::beanCheckValidValue($key, $value, $error);
					break;
			}
		}
		catch(\Exception $e)
		{
			$result = false;
			$error = $e;
		}

		// Return result
		return $result;
	}





	// Methods getters
    // ******************************************************************************
	
	/**
     * @inheritdoc
     */
    protected function getIntStatusCodeToSend()
    {
		// Init var
        $result = ConstConfig::DATA_DEFAULT_VALUE_HTTP_REDIRECT_RESPONSE_STATUS_CODE;
		$intDefaultStatusCode = static::getObjConfigHttp()->getIntHttpRedirectResponseStatusCode();
		$intStatusCode = $this->getIntStatusCode();
		
		// Register status code in result if found
		if((!is_null($intStatusCode)) && is_integer($intStatusCode))
		{
			$result = $intStatusCode;
		}
		// Register default status code in result if found
		else if((!is_null($intDefaultStatusCode)) && is_integer($intDefaultStatusCode))
		{
			$result = $intDefaultStatusCode;
		}
		
		// Return result
		return $result;
    }
	
	
	
	/**
     * Get URL redirection target (ready to send).
     * 
	 * @return string
     */
    protected function getStrUrlToSend()
    {
		// Init var
        $result = ConstConfig::DATA_DEFAULT_VALUE_HTTP_REDIRECT_RESPONSE_URL;
		$strDefaultUrl = static::getObjConfigHttp()->getStrHttpRedirectResponseUrl();
		$strUrl = $this->getStrUrl();
		
		// Register URL in result if found
		if((!is_null($strUrl)) && is_string($strUrl))
		{
			$result = $strUrl;
		}
		// Register default URL in result if found
		else if((!is_null($strDefaultUrl)) && is_string($strDefaultUrl))
		{
			$result = $strDefaultUrl;
		}
		
		// Return result
		return $result;
    }
	
	
	
	/**
     * Get header redirection (ready to send).
     * 
	 * @return string
     */
    protected function getStrHeaderRedirectToSend()
    {
		// Init var
		$strUrl = $this->getStrUrlToSend();
		
		// Init arguments
		$strArg = '';
		$tabArg = $this->getTabGetArg();
		if(count($tabArg) > 0)
		{
			$strArg = '?' . http_build_query($tabArg);
		}
		
		// Set result
		$result = sprintf(ConstRedirectResponse::CONF_HEADER_REDIRECT_URL_FORMAT, $strUrl . $strArg);
		
		// Return result
		return $result;
    }
	
	
	
	/**
     * @inheritdoc
     */
    protected function getTabHeaderToSend()
    {
		// Init var
        $result = parent::getTabHeaderToSend();
		
		// Insert header redirection
		$result[] = $this->getStrHeaderRedirectToSend();
		
		// Return result
		return $result;
    }
	
	
	
}