<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\request_flow\response\redirection\exception;

use liberty_code\http\request_flow\response\redirection\library\ConstRedirectResponse;



class StatusCodeInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $statusCode
     */
	public function __construct($statusCode)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf(ConstRedirectResponse::EXCEPT_MSG_STATUS_CODE_INVALID_FORMAT, strval($statusCode));
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified status code has valid format.
	 * 
     * @param mixed $statusCode
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($statusCode)
    {
		// Init var
		$result = 
			is_null($statusCode) || // Check is null
			(
				is_integer($statusCode) && ($statusCode > 0) && // Check is valid integer
				($statusCode >= 300) && ($statusCode < 400)// Check code like 3xx
			);
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($statusCode);
		}
		
		// Return result
		return $result;
    }
	
	
	
}