<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\request_flow\response\redirection\exception;

use liberty_code\http\request_flow\response\redirection\library\ConstRedirectResponse;



class GetArgInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $getArg
     */
	public function __construct($getArg)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
		(
			ConstRedirectResponse::EXCEPT_MSG_GET_ARG_INVALID_FORMAT, 
			mb_strimwidth(strval($getArg), 0, 10, "...")
		);
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified get arguments have valid format
	 * 
     * @param mixed $getArg
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($getArg)
    {
		// Init var
		$result = is_array($getArg);
		
		// Run all data if required
		if($result)
		{
			$tabKey = array_keys($getArg);
			for($intCpt = 0; $result && ($intCpt < count($tabKey)); $intCpt++)
			{
				// Get data
				$strKey = $tabKey[$intCpt];
				$strValue = $getArg[$strKey];
				
				// Check data
				$result = 
					(is_string($strKey) && (trim($strKey) !== '')) && // Check key is valid string
					(is_string($strValue) && (trim($strValue) !== '')); // Check value is valid string
			}
		}
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($getArg) ? serialize($getArg) : $getArg));
		}
		
		// Return result
		return $result;
    }
	
	
	
}