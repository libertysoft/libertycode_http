<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\response\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\http\header\library\ToolBoxHeader;



class ToolBoxResponse extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get protocol version number,
     * from specified status line.
     *
     * @param string $strStatusLine
     * @return null|string
     */
    public static function getStrProtocolVersionNum($strStatusLine)
    {
        // Init var
        $strStatusLine = ToolBoxHeader::getStrHeaderLineFormat($strStatusLine);
        $strPattern = '#^HTTP/([\d\.]+)[ ]?.*$#';
        $tabMatch = array();
        $result = (
            (
                is_string($strStatusLine) &&
                (preg_match($strPattern, $strStatusLine, $tabMatch) === 1)
            ) ?
                $tabMatch[1] :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get status code,
     * from specified status line.
     *
     * @param string $strStatusLine
     * @return null|integer
     */
    public static function getIntStatusCode($strStatusLine)
    {
        // Init var
        $strStatusLine = ToolBoxHeader::getStrHeaderLineFormat($strStatusLine);
        $strPattern = '#^HTTP/[\d\.]+ (\d+)[ ]?.*$#';
        $tabMatch = array();
        $result = (
            (
                is_string($strStatusLine) &&
                (preg_match($strPattern, $strStatusLine, $tabMatch) === 1)
            ) ?
                intval($tabMatch[1]) :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get status message,
     * from specified status line.
     *
     * @param string $strStatusLine
     * @return null|string
     */
    public static function getStrStatusMsg($strStatusLine)
    {
        // Init var
        $strStatusLine = ToolBoxHeader::getStrHeaderLineFormat($strStatusLine);
        $strPattern = '#^HTTP/[\d\.]+ \d+ (.+)$#';
        $tabMatch = array();
        $result = (
            (
                is_string($strStatusLine) &&
                (preg_match($strPattern, $strStatusLine, $tabMatch) === 1)
            ) ?
                $tabMatch[1] :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get status line,
     * from specified protocol version number,
     * and specified status code,
     * and specified status message.
     *
     * @param string $strProtocolVersionNum
     * @param integer $intStatusCode
     * @param string $strStatusMsg = null
     * @return null|string
     */
    public static function getStrStatusLine(
        $strProtocolVersionNum,
        $intStatusCode,
        $strStatusMsg = null
    )
    {
        // Return result
        return (
            (
                is_string($strProtocolVersionNum) && (trim($strProtocolVersionNum) != '') &&
                is_int($intStatusCode) && ($intStatusCode >= 0)
            ) ?
                ToolBoxHeader::getStrHeaderLineFormat((
                    is_string($strStatusMsg) ?
                        sprintf('%1$s: %2$d %3$s', trim($strProtocolVersionNum), $intStatusCode, $strStatusMsg) :
                        sprintf('%1$s: %2$d', trim($strProtocolVersionNum), $intStatusCode)
                )) :
                null
        );
    }



}