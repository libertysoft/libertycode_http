<?php
/**
 * Description :
 * This class allows to provide configuration for HTTP context features.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\config\model;

use liberty_code\library\bean\model\FixBean;

use liberty_code\http\config\library\ConstConfig;
use liberty_code\http\config\exception\HttpRequestSourceLinkInvalidFormatException;
use liberty_code\http\config\exception\HttpResponseStatusCodeInvalidFormatException;
use liberty_code\http\config\exception\HttpResponseStatusMsgInvalidFormatException;
use liberty_code\http\config\exception\HttpResponseHeaderInvalidFormatException;
use liberty_code\http\config\exception\HttpRedirectResponseStatusCodeInvalidFormatException;
use liberty_code\http\config\exception\HttpRedirectResponseUrlInvalidFormatException;
use liberty_code\http\config\exception\HttpRouteMethodInvalidFormatException;



/**
 * @method string getStrHttpRequestSourceLink() Get http request source link.
 * @method null|integer getIntHttpResponseStatusCode() Get http response status code to send if response status code is null.
 * @method null|string getStrHttpResponseStatusMsg() Get http response status message to send if response status message is null.
 * @method array getTabHttpResponseHeader() Get index array of string http response headers to send if no response headers registered.
 * @method array getTabHttpResponseHeaderAdd() Get index array of string http response headers to send in additional of response headers registered.
 * @method null|integer getIntHttpRedirectResponseStatusCode() Get http redirection response status code to send if response status code is null.
 * @method null|string getStrHttpRedirectResponseUrl() Get http redirection response URL to send if response URL is null.
 * @method null|string getStrHttpRouteMethod() Get http route method by default.
 * @method void setStrHttpRequestSourceLink(string $strLink) Set http request source link.
 * @method void setIntHttpResponseStatusCode(integer $intStatusCode) Set http response status code to send if response status code is null.
 * @method void setStrHttpResponseStatusMsg(string $strStatusMsg) Set http response status message to send if response status message is null.
 * @method void setTabHttpResponseHeader(array $tabHeader) Set index array of string http response headers to send if no response headers registered.
 * @method void setTabHttpResponseHeaderAdd(array $tabHeader) Set index array of string http response headers to send in additional of response headers registered.
 * @method void setIntHttpRedirectResponseStatusCode(integer $intStatusCode) Set http redirection response status code to send if response status code is null.
 * @method void setStrHttpRedirectResponseUrl(string $strUrl) Set http redirection response URL to send if response URL is null.
 * @method void setStrHttpRouteMethod(string $strMethod) Set http route method by default.
 */
class DefaultConfig extends FixBean
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods initialize
	// ******************************************************************************
	
	/**
	 * @inheritdoc
	 */
	protected function beanHydrateDefault()
	{
		if(!$this->beanExists(ConstConfig::DATA_KEY_HTTP_REQUEST_SOURCE_LINK))
		{
			$this->beanAdd(ConstConfig::DATA_KEY_HTTP_REQUEST_SOURCE_LINK, ConstConfig::DATA_DEFAULT_VALUE_HTTP_REQUEST_SOURCE_LINK);
		}
		
		if(!$this->beanExists(ConstConfig::DATA_KEY_HTTP_RESPONSE_STATUS_CODE))
        {
            $this->beanAdd(ConstConfig::DATA_KEY_HTTP_RESPONSE_STATUS_CODE, ConstConfig::DATA_DEFAULT_VALUE_HTTP_RESPONSE_STATUS_CODE);
        }
		
		if(!$this->beanExists(ConstConfig::DATA_KEY_HTTP_RESPONSE_STATUS_MSG))
        {
            $this->beanAdd(ConstConfig::DATA_KEY_HTTP_RESPONSE_STATUS_MSG, ConstConfig::DATA_DEFAULT_VALUE_HTTP_RESPONSE_STATUS_MSG);
        }
		
		if(!$this->beanExists(ConstConfig::DATA_KEY_HTTP_RESPONSE_HEADER))
        {
            $this->beanAdd(ConstConfig::DATA_KEY_HTTP_RESPONSE_HEADER, ConstConfig::DATA_DEFAULT_VALUE_HTTP_RESPONSE_HEADER);
        }
		
		if(!$this->beanExists(ConstConfig::DATA_KEY_HTTP_RESPONSE_HEADER_ADD))
        {
            $this->beanAdd(ConstConfig::DATA_KEY_HTTP_RESPONSE_HEADER_ADD, ConstConfig::DATA_DEFAULT_VALUE_HTTP_RESPONSE_HEADER_ADD);
        }
		
		if(!$this->beanExists(ConstConfig::DATA_KEY_HTTP_REDIRECT_RESPONSE_STATUS_CODE))
        {
            $this->beanAdd(ConstConfig::DATA_KEY_HTTP_REDIRECT_RESPONSE_STATUS_CODE, ConstConfig::DATA_DEFAULT_VALUE_HTTP_REDIRECT_RESPONSE_STATUS_CODE);
        }
		
		if(!$this->beanExists(ConstConfig::DATA_KEY_HTTP_REDIRECT_RESPONSE_URL))
        {
            $this->beanAdd(ConstConfig::DATA_KEY_HTTP_REDIRECT_RESPONSE_URL, ConstConfig::DATA_DEFAULT_VALUE_HTTP_REDIRECT_RESPONSE_URL);
        }
		
		if(!$this->beanExists(ConstConfig::DATA_KEY_HTTP_ROUTE_METHOD))
        {
            $this->beanAdd(ConstConfig::DATA_KEY_HTTP_ROUTE_METHOD, ConstConfig::DATA_DEFAULT_VALUE_HTTP_ROUTE_METHOD);
        }
	}





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstConfig::DATA_KEY_HTTP_REQUEST_SOURCE_LINK,
            ConstConfig::DATA_KEY_HTTP_RESPONSE_STATUS_CODE,
            ConstConfig::DATA_KEY_HTTP_RESPONSE_STATUS_MSG,
            ConstConfig::DATA_KEY_HTTP_RESPONSE_HEADER,
            ConstConfig::DATA_KEY_HTTP_RESPONSE_HEADER_ADD,
            ConstConfig::DATA_KEY_HTTP_REDIRECT_RESPONSE_STATUS_CODE,
            ConstConfig::DATA_KEY_HTTP_REDIRECT_RESPONSE_URL,
            ConstConfig::DATA_KEY_HTTP_ROUTE_METHOD
        );
        $result = in_array($key, $tabKey);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstConfig::DATA_KEY_HTTP_REQUEST_SOURCE_LINK:
                    HttpRequestSourceLinkInvalidFormatException::setCheck($value);
                    break;

                case ConstConfig::DATA_KEY_HTTP_RESPONSE_STATUS_CODE:
                    HttpResponseStatusCodeInvalidFormatException::setCheck($value);
                    break;

                case ConstConfig::DATA_KEY_HTTP_RESPONSE_STATUS_MSG:
                    HttpResponseStatusMsgInvalidFormatException::setCheck($value);
                    break;

                case ConstConfig::DATA_KEY_HTTP_RESPONSE_HEADER:
                    HttpResponseHeaderInvalidFormatException::setCheck($value);
                    break;

                case ConstConfig::DATA_KEY_HTTP_RESPONSE_HEADER_ADD:
                    HttpResponseHeaderInvalidFormatException::setCheck($value);
                    break;

                case ConstConfig::DATA_KEY_HTTP_REDIRECT_RESPONSE_STATUS_CODE:
                    HttpRedirectResponseStatusCodeInvalidFormatException::setCheck($value);
                    break;

                case ConstConfig::DATA_KEY_HTTP_REDIRECT_RESPONSE_URL:
                    HttpRedirectResponseUrlInvalidFormatException::setCheck($value);
                    break;

                case ConstConfig::DATA_KEY_HTTP_ROUTE_METHOD:
                    HttpRouteMethodInvalidFormatException::setCheck($value);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidRemove($key, &$error = null)
    {
        // Return result
        return false;
    }
	
	
	
}