<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\config\exception;

use liberty_code\http\config\library\ConstConfig;



class HttpRouteMethodInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $method
     */
	public function __construct($method)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf(ConstConfig::EXCEPT_MSG_HTTP_ROUTE_METHOD_INVALID_FORMAT, strval($method));
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified method has valid format.
	 * 
     * @param mixed $method
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($method)
    {
		// Init var
		$result = 
			is_null($method) || // Check is null
			(is_string($method) && (trim($method) != '')); // Check is valid string
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($method);
		}
		
		// Return result
		return $result;
    }
	
	
	
}