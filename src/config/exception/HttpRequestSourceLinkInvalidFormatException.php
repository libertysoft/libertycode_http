<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\config\exception;

use liberty_code\http\config\library\ConstConfig;



class HttpRequestSourceLinkInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $link
     */
	public function __construct($link)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf(ConstConfig::EXCEPT_MSG_HTTP_REQUEST_SOURCE_LINK_INVALID_FORMAT, strval($link));
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified link has valid format.
	 * 
     * @param mixed $link
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($link)
    {
		// Init var
		$result =
            (is_string($link) && (trim($link) !== '')); // Check is valid string
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($link);
		}
		
		// Return result
		return $result;
    }
	
	
	
}