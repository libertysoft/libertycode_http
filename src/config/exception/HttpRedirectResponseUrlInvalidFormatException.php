<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\config\exception;

use liberty_code\http\config\library\ConstConfig;



class HttpRedirectResponseUrlInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $url
     */
	public function __construct($url)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
		(
			ConstConfig::EXCEPT_MSG_HTTP_REDIRECT_RESPONSE_URL_INVALID_FORMAT, 
			mb_strimwidth(strval($url), 0, 50, "...")
		);
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified URL has valid format.
	 * 
     * @param mixed $url
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($url)
    {
		// Init var
		$result = 
			is_null($url) || // Check is null
			(is_string($url) && (trim($url) != '')); // Check is valid string
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($url);
		}
		
		// Return result
		return $result;
    }
	
	
	
}