<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\config\exception;

use liberty_code\http\config\library\ConstConfig;



class HttpResponseHeaderInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $header
     */
	public function __construct($header)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
		(
            ConstConfig::EXCEPT_MSG_HTTP_RESPONSE_HEADER_INVALID_FORMAT,
            mb_strimwidth(strval($header), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if header has valid format.
	 * 
     * @param mixed $header
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($header)
    {
		// Init var
		$result = is_array($header); // Check is valid array
		
		// Run all headers
		for($intCpt = 0; $result && ($intCpt < count($header)); $intCpt++)
		{
			// Check header is valid string
			$result = (is_string($header[$intCpt]) && (trim($header[$intCpt]) !== ''));
		}
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($header) ? serialize($header) : $header));
		}
		
		// Return result
		return $result;
    }
	
	
	
}