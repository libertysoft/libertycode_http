<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\config\library;



class ConstConfig
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
	const DATA_KEY_HTTP_REQUEST_SOURCE_LINK = 'strHttpRequestSourceLink';
	const DATA_KEY_HTTP_RESPONSE_STATUS_CODE = 'intHttpResponseStatusCode';
	const DATA_KEY_HTTP_RESPONSE_STATUS_MSG = 'strHttpResponseStatusMsg';
	const DATA_KEY_HTTP_RESPONSE_HEADER = 'tabHttpResponseHeader';
	const DATA_KEY_HTTP_RESPONSE_HEADER_ADD = 'tabHttpResponseHeaderAdd';
	const DATA_KEY_HTTP_REDIRECT_RESPONSE_STATUS_CODE = 'intHttpRedirectResponseStatusCode';
	const DATA_KEY_HTTP_REDIRECT_RESPONSE_URL = 'strHttpRedirectResponseUrl';
	const DATA_KEY_HTTP_ROUTE_METHOD = 'strHttpRouteMethod';
	
	const DATA_DEFAULT_VALUE_HTTP_REQUEST_SOURCE_LINK = ':';
	const DATA_DEFAULT_VALUE_HTTP_RESPONSE_STATUS_CODE = 200; // Default status code: 200 OK
	const DATA_DEFAULT_VALUE_HTTP_RESPONSE_STATUS_MSG = null;
	const DATA_DEFAULT_VALUE_HTTP_RESPONSE_HEADER = array(
		'Content-Type: text/html' // Default header: considered response is HTML
	);
	const DATA_DEFAULT_VALUE_HTTP_RESPONSE_HEADER_ADD = array();
	const DATA_DEFAULT_VALUE_HTTP_REDIRECT_RESPONSE_STATUS_CODE = 302; // Default redirection status code: 302 temporary redirection
	const DATA_DEFAULT_VALUE_HTTP_REDIRECT_RESPONSE_URL = '/';
	const DATA_DEFAULT_VALUE_HTTP_ROUTE_METHOD = '.*';
	
	
	
    // Exception message constants
    const EXCEPT_MSG_HTTP_REQUEST_SOURCE_LINK_INVALID_FORMAT = 'Following http request source link "%1$s" invalid! The link must be a string, not empty.';
    const EXCEPT_MSG_HTTP_RESPONSE_STATUS_CODE_INVALID_FORMAT = 'Following http response status code "%1$s" invalid! The status code must be null or a positive integer.';
	const EXCEPT_MSG_HTTP_RESPONSE_STATUS_MSG_INVALID_FORMAT = 'Following http response status message "%1$s" invalid! The status message must be null or a string, not empty.';
	const EXCEPT_MSG_HTTP_RESPONSE_HEADER_INVALID_FORMAT = 'Following http response header "%1$s" invalid! The header must be an array of string, not empty.';
	const EXCEPT_MSG_HTTP_REDIRECT_RESPONSE_STATUS_CODE_INVALID_FORMAT = 'Following http redirection response status code "%1$s" invalid! The status code must be null or a positive integer, like 3xx.';
	const EXCEPT_MSG_HTTP_REDIRECT_RESPONSE_URL_INVALID_FORMAT = 'Following http redirection response URL "%1$s" invalid! The URL must be null or a string, not empty.';
	const EXCEPT_MSG_HTTP_ROUTE_METHOD_INVALID_FORMAT = 'Following http route method "%1$s" invalid! The method must be null or a string, not empty.';
}