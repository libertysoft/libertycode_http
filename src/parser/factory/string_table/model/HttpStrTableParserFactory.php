<?php
/**
 * Description :
 * This class allows to define HTTP string table parser factory class.
 * HTTP string table parser factory allows to provide and hydrate HTTP string table parser instance.
 *
 * HTTP string table parser factory uses the following specified configuration, to get and hydrate parser:
 * [
 *
 *     type(required): "http_string_table_url_arg",
 *
 *     @see UrlArgParser configuration array format,
 *
 *     OR
 *
 *     type(required): "http_string_table_multipart_data",
 *
 *     @see MultipartDataParser configuration array format
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\parser\factory\string_table\model;

use liberty_code\parser\parser\factory\model\DefaultParserFactory;

use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\parser\parser\api\ParserInterface;
use liberty_code\parser\parser\factory\api\ParserFactoryInterface;
use liberty_code\file\file\factory\api\FileFactoryInterface;
use liberty_code\http\parser\string_table\url_arg\model\UrlArgParser;
use liberty_code\http\parser\string_table\multipart_data\exception\FileFactoryInvalidFormatException;
use liberty_code\http\parser\string_table\multipart_data\model\MultipartDataParser;
use liberty_code\http\parser\factory\string_table\library\ConstHttpStrTableParserFactory;



/**
 * @method null|FileFactoryInterface getObjFileFactory() Get file factory object.
 * @method void setObjFileFactory(null|FileFactoryInterface $objFileFactory) Set file factory object.
 */
class HttpStrTableParserFactory extends DefaultParserFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param FileFactoryInterface $objFileFactory = null
     */
    public function __construct(
        $callableSourceFormatGet = null,
        $callableSourceFormatSet = null,
        $callableDataFormatGet = null,
        $callableDataFormatSet = null,
        ParserFactoryInterface $objFactory = null,
        ProviderInterface $objProvider = null,
        FileFactoryInterface $objFileFactory = null
    )
    {
        // Call parent constructor
        parent::__construct(
            $callableSourceFormatGet,
            $callableSourceFormatSet,
            $callableDataFormatGet,
            $callableDataFormatSet,
            $objFactory,
            $objProvider
        );

        // Init file factory, if required
        if(!is_null($objFileFactory))
        {
            $this->setObjFileFactory($objFileFactory);
        }
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstHttpStrTableParserFactory::DATA_KEY_DEFAULT_FILE_FACTORY))
        {
            $this->__beanTabData[ConstHttpStrTableParserFactory::DATA_KEY_DEFAULT_FILE_FACTORY] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }



    /**
     * @inheritdoc
     */
    protected function hydrateParser(ParserInterface $objParser, array $tabConfigFormat)
    {
        // Call parent method
        parent::hydrateParser($objParser, $tabConfigFormat);

        // Hydrate multipart data parser, if required
        if($objParser instanceof MultipartDataParser)
        {
            // Init file factory object
            $objFileFactory = $this->getObjFileFactory();
            $objParser->setObjFileFactory($objFileFactory);
        }
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstHttpStrTableParserFactory::DATA_KEY_DEFAULT_FILE_FACTORY
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstHttpStrTableParserFactory::DATA_KEY_DEFAULT_FILE_FACTORY:
                    FileFactoryInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





	// Methods getters
	// ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrParserClassPathFromType($strConfigType)
    {
        // Init var
        $result = null;

        // Get class path of parser, from type
        switch($strConfigType)
        {
            case ConstHttpStrTableParserFactory::CONFIG_TYPE_HTTP_STR_TABLE_URL_ARG:
                $result = UrlArgParser::class;
                break;

            case ConstHttpStrTableParserFactory::CONFIG_TYPE_HTTP_STR_TABLE_MULTIPART_DATA:
                $result = MultipartDataParser::class;
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getObjParserNew($strConfigType)
    {
        // Init var
        $result = null;

        // Get parser, from type
        switch($strConfigType)
        {
            case ConstHttpStrTableParserFactory::CONFIG_TYPE_HTTP_STR_TABLE_URL_ARG:
                $result = new UrlArgParser();
                break;

            case ConstHttpStrTableParserFactory::CONFIG_TYPE_HTTP_STR_TABLE_MULTIPART_DATA:
                $result = new MultipartDataParser();
                break;
        }

        // Return result
        return $result;
    }



}