<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\parser\factory\string_table\library;



class ConstHttpStrTableParserFactory
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_FILE_FACTORY = 'objFileFactory';



    // Type configuration
    const CONFIG_TYPE_HTTP_STR_TABLE_URL_ARG = 'http_string_table_url_arg';
    const CONFIG_TYPE_HTTP_STR_TABLE_MULTIPART_DATA = 'http_string_table_multipart_data';
}