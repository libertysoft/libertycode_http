<?php
/**
 * Description :
 * This class allows to define URL argument string table parser.
 * URL argument string table parser is string table parser,
 * using URL argument format, as string source.
 *
 * URL argument string table parser uses the following specified configuration:
 * [
 *     String table parser configuration
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\parser\string_table\url_arg\model;

use liberty_code\parser\parser\string_table\model\StrTableParser;

use liberty_code\http\url_arg\exception\DataSrcInvalidFormatException;



class UrlArgParser extends StrTableParser
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param array $data
     * @return string
     * @throws DataSrcInvalidFormatException
     */
    protected function getSourceEngine($data)
    {
        DataSrcInvalidFormatException::setCheck($data);

        // Return result
        return http_build_query($data);
    }



    /**
     * @inheritdoc
     * @param string $src
     * @return array
     */
    protected function getDataEngine($src)
    {
        // Init var
        $result = array();

        // Get parsed data
        parse_str($src, $result);

        // Return result
        return $result;
    }
	
	
	
}