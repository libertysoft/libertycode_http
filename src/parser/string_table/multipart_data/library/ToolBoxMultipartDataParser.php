<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\parser\string_table\multipart_data\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\library\table\library\ToolBoxTable;
use liberty_code\file\file\factory\api\FileFactoryInterface;
use liberty_code\file\file\factory\library\ConstFileFactory;
use liberty_code\http\multipart\library\ToolBoxMultipart;
use liberty_code\http\multipart\data\library\ToolBoxMultipartData;
use liberty_code\http\file\name\multipart_data\library\ConstMultipartDataFile;
use liberty_code\http\file\factory\name\library\ConstHttpNameFileFactory;



class ToolBoxMultipartDataParser extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods getters
	// ***************************************************************************************************

    /**
     * Get index array of string parts,
     * from specified parsed data.
     *
     * @param array $tabData
     * @param string $strPrefixKey = null
     * @param string $strDefaultFileNm = null
     * @return null|array
     */
    protected static function getTabPart(
        array $tabData,
        $strPrefixKey = null,
        $strDefaultFileNm = null
    )
    {
        // Init var
        $strPrefixKey = (is_string($strPrefixKey) ? $strPrefixKey : null);
        $result = null;

        // Run each data
        foreach($tabData as $key => $data)
        {
            // Get array of parts
            $strKey = (
                (!is_null($strPrefixKey)) ?
                    sprintf('%1$s[%2$s]', strval($strPrefixKey), strval($key)) :
                    strval($key)
            );
            $tabPart = (
                is_array($data) ?
                    static::getTabPart(
                        $data,
                        $strKey,
                        $strDefaultFileNm
                    ) :
                    (
                        (
                            (!is_null($strHeaderLines = ToolBoxMultipartData::getStrPartHeaderLines($strKey, $data, $strDefaultFileNm))) &&
                            (!is_null($strBody = ToolBoxMultipartData::getStrPartBody($data)))
                        ) ?
                            array(ToolBoxMultipart::getStrPart($strHeaderLines, $strBody)) :
                            null
                    )
            );

            // Build result
            $result = (
                (!is_null($tabPart)) ?
                    (
                        (!is_null($result)) ?
                            array_merge($result, $tabPart) :
                            $tabPart
                    ) :
                    $result
            );
        }

        // Return result
        return $result;
    }



	/**
	 * Get string multipart,
     * from specified parsed data.
	 * 
	 * @param array $tabData
	 * @param string $strBoundary
     * @param string $strDefaultFileNm = null
     * @return null|string
	 */
	public static function getStrMultipart(
		array $tabData,
        $strBoundary,
        $strDefaultFileNm = null
	)
    {
        // Init var
        $tabPart = static::getTabPart(
            $tabData,
            null,
            $strDefaultFileNm
        );
        $result = (
            (!is_null($tabPart)) ?
                ToolBoxMultipart::getStrMultipart($tabPart, $strBoundary) :
                null
        );

		// Return result
		return $result;
    }
	
	
	
	/**
	 * Get parsed data,
     * from specified string multipart.
	 * 
	 * @param string $strMultipart
     * @param string $strBoundary
     * @param FileFactoryInterface $objFileFactory = null
     * @return null|array
	 */
	public static function getTabData(
        $strMultipart,
        $strBoundary,
        FileFactoryInterface $objFileFactory = null
    )
    {
        // Init var
        $tabPart = ToolBoxMultipart::getTabPart(
            $strMultipart,
            $strBoundary
        );
        $result = null;

        // Run each part, if required
        if(!is_null($tabPart))
        {
            foreach($tabPart as $strPart)
            {
                $strHeaderLines = ToolBoxMultipart::getStrPartHeaderLines($strPart);

                // Get key, path
                $strKey = ToolBoxMultipartData::getStrKey($strHeaderLines);
                $tabPattern = array(
                    '#^([^\[]*)\[*#',
                    '#\[(.*?)\]#'
                );
                $tabKey = array();
                foreach($tabPattern as $strPattern)
                {
                    $tabMatch = array();
                    $tabKey = (
                        (preg_match_all($strPattern, $strKey, $tabMatch, PREG_PATTERN_ORDER) > 0) ?
                            array_merge($tabKey, $tabMatch[1]) :
                            $tabKey
                    );
                }

                // Get value
                $value = (
                    ToolBoxMultipartData::checkIsValue($strHeaderLines) ?
                        // Get value, if required
                        ToolBoxMultipart::getStrPartBody($strPart) :
                        (
                            (
                                ToolBoxMultipartData::checkIsFile($strHeaderLines) &&
                                (!is_null($objFileFactory))
                            ) ?
                                // Try to get file, as value, if required
                                $objFileFactory->getObjFile(array(
                                    ConstFileFactory::TAB_CONFIG_KEY_TYPE =>
                                        ConstHttpNameFileFactory::CONFIG_TYPE_HTTP_NAME_MULTIPART_DATA,
                                    ConstMultipartDataFile::TAB_CONFIG_KEY_DATA_SOURCE => $strPart
                                )) :
                                null
                        )
                );

                // Set result, if required
                if((count($tabKey) > 0) && (!is_null($value)))
                {
                    $result = (is_null($result) ? array() : $result);
                    ToolBoxTable::setItem(
                        $result,
                        $tabKey,
                        $value,
                        true,
                        ''
                    );
                }
            }
        }

        // Return result
        return $result;
    }
	
	
	
}