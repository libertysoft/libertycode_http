<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\parser\string_table\multipart_data\library;



class ConstMultipartDataParser
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_FILE_FACTORY = 'objFileFactory';



    // Configuration
    const TAB_CONFIG_KEY_BOUNDARY = 'boundary';
    const TAB_CONFIG_KEY_CACHE_BOUNDARY_REQUIRE = 'cache_boundary_require';
    const TAB_CONFIG_KEY_DEFAULT_FILE_NAME = 'default_file_name';



    // Exception message constants
    const EXCEPT_MSG_FILE_FACTORY_INVALID_FORMAT =
        'Following file factory "%1$s" invalid! It must be a file factory object.';
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the multipart data string table parser configuration standard.';
}