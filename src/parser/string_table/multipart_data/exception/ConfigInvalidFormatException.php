<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\parser\string_table\multipart_data\exception;

use Exception;

use liberty_code\http\parser\string_table\multipart_data\library\ConstMultipartDataParser;



class ConfigInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstMultipartDataParser::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $result =
            // Check valid boundary
            (
                (!isset($config[ConstMultipartDataParser::TAB_CONFIG_KEY_BOUNDARY])) ||
                (
                    is_string($config[ConstMultipartDataParser::TAB_CONFIG_KEY_BOUNDARY]) &&
                    (trim($config[ConstMultipartDataParser::TAB_CONFIG_KEY_BOUNDARY]) != '')
                )

            ) &&

            // Check valid cache boundary required option
            (
                (!isset($config[ConstMultipartDataParser::TAB_CONFIG_KEY_CACHE_BOUNDARY_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstMultipartDataParser::TAB_CONFIG_KEY_CACHE_BOUNDARY_REQUIRE]) ||
                    is_int($config[ConstMultipartDataParser::TAB_CONFIG_KEY_CACHE_BOUNDARY_REQUIRE]) ||
                    (
                        is_string($config[ConstMultipartDataParser::TAB_CONFIG_KEY_CACHE_BOUNDARY_REQUIRE]) &&
                        ctype_digit($config[ConstMultipartDataParser::TAB_CONFIG_KEY_CACHE_BOUNDARY_REQUIRE])
                    )
                )
            ) &&

            // Check valid default file name
            (
                (!isset($config[ConstMultipartDataParser::TAB_CONFIG_KEY_DEFAULT_FILE_NAME])) ||
                (
                    is_string($config[ConstMultipartDataParser::TAB_CONFIG_KEY_DEFAULT_FILE_NAME]) &&
                    (trim($config[ConstMultipartDataParser::TAB_CONFIG_KEY_DEFAULT_FILE_NAME]) != '')
                )

            );

        // Return result
        return $result;
    }



    /**
	 * Check if specified config has valid format.
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
    public static function setCheck($config)
    {
		// Init var
		$result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}