<?php
/**
 * Description :
 * This class allows to define cookie register class.
 * Cookie register is table register,
 * using cookie array ($_COOKIE), as storage support.
 * It uses PHP serialization formatting, for items storing, on cookie.
 *
 * Items are stored in array like:
 * [
 *     TableRegister stored item array format,
 *
 *     cookie_expire_timeout_datetime(required): DateTime object,
 *
 *     cookie_path(required): 'string path',
 *
 *     cookie_domain_name(required): 'string (sub)domain name',
 *
 *     cookie_secure_require(required): true / false,
 *
 *     cookie_http_only_require(required): true / false
 * ]
 *
 * Cookie register uses the following specified configuration:
 * [
 *     Table register configuration,
 *
 *     cookie_expire_timeout(optional: got @see ConstCookieRegister::DEFAULT_COOKIE_EXPIRE_TIMEOUT_SECOND if not found):
 *         DateTime object OR integer additional seconds, used on cookie setting (@see setcookie() ),
 *
 *     cookie_path(optional: got @see ConstCookieRegister::DEFAULT_COOKIE_PATH if not found):
 *         'string path used on cookie setting (@see setcookie() )',
 *
 *     cookie_domain_name(optional: got @see ConstCookieRegister::DEFAULT_COOKIE_DOMAIN_NAME if not found):
 *         'string (sub)domain name, used on cookie setting (@see setcookie() )',
 *
 *     cookie_secure_require(optional: got @see ConstCookieRegister::DEFAULT_COOKIE_SECURE_REQUIRE if not found):
 *         true / false, used on cookie setting (@see setcookie() ),
 *
 *     cookie_http_only_require(optional: got @see ConstCookieRegister::DEFAULT_COOKIE_HTTP_ONLY_REQUIRE if not found):
 *         true / false, used on cookie setting (@see setcookie() )
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\register\cookie\model;

use liberty_code\register\register\table\model\TableRegister;

use DateTime;
use DateTimeZone;
use liberty_code\library\error\library\ToolBoxError;
use liberty_code\register\register\table\library\ConstTableRegister;
use liberty_code\http\cookie\library\ToolBoxCookie;
use liberty_code\http\register\cookie\library\ConstCookieRegister;
use liberty_code\http\register\cookie\exception\ConfigInvalidFormatException;
use liberty_code\http\register\cookie\exception\SetExecConfigInvalidFormatException;



class CookieRegister extends TableRegister
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function __construct(
        array $tabConfig = null,
        array $tabItem = null
    )
    {
        // Initialize cookie
        $this->initialize();

        // Call parent constructor
        parent::__construct(
            $tabConfig,
            $tabItem
        );
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * Initialize cookie.
     */
    protected function initialize()
    {
        // Format cookie data, if required
        foreach($_COOKIE as $strKey => $value)
        {
            $_COOKIE[$strKey] = $this->getDataFromCookieItem($strKey, $value);
        }
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        //$result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstTableRegister::DATA_KEY_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check if cookie secure required,
     * from specified item data.
     *
     * Item data array format:
     * @see getUseTabItemData() items data array format, for one item data.
     *
     * @param array $itemData
     * @return boolean
     */
    protected function checkCookieSecureRequiredFromItemData(array $itemData)
    {
        // Return result
        return $itemData[ConstCookieRegister::TAB_ITEM_DATA_CONFIG_KEY_COOKIE_SECURE_REQUIRE];
    }



    /**
     * Check if cookie HTTP only required,
     * from specified item data.
     *
     * Item data array format:
     * @see getUseTabItemData() items data array format, for one item data.
     *
     * @param array $itemData
     * @return boolean
     */
    protected function checkCookieHttpOnlyRequiredFromItemData(array $itemData)
    {
        // Return result
        return $itemData[ConstCookieRegister::TAB_ITEM_DATA_CONFIG_KEY_COOKIE_SECURE_REQUIRE];
    }



    /**
     * Check if cookie secure required,
     * from specified key.
     *
     * @param string $strKey
     * @return boolean
     */
    public function checkCookieSecureRequired($strKey)
    {
        // Return result
        return (
            (!is_null($itemData = $this->getItemDataFromKey($strKey))) ?
                $this->checkCookieSecureRequiredFromItemData($itemData) :
                false
        );
    }



    /**
     * Check if cookie HTTP only required,
     * from specified key.
     *
     * @param string $strKey
     * @return boolean
     */
    public function checkCookieHttpOnlyRequired($strKey)
    {
        // Return result
        return (
            (!is_null($itemData = $this->getItemDataFromKey($strKey))) ?
                $this->checkCookieHttpOnlyRequiredFromItemData($itemData) :
                false
        );
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function &getUseTabItemData()
    {
        return $_COOKIE;
    }



    /**
     * Get array data,
     * from specified cookie item.
     * Overwrite it to implement specific feature.
     *
     * @param string $strKey
     * @param string $strCookieItem
     * @return mixed
     */
    protected function getDataFromCookieItem($strKey, $strCookieItem)
    {
        // Init var
        $item = ToolBoxError::executeSafe(function() use ($strCookieItem) {return unserialize($strCookieItem);});
        $result = (
            (
                ($item !== false) ||
                ($strCookieItem === serialize(false))
            ) ?
                $item :
                $strCookieItem
        );

        // Return result
        return $result;
    }



    /**
     * Get cookie item,
     * from specified item data.
     * Overwrite it to implement specific feature.
     *
     * Item data array format:
     * @see getUseTabItemData() items data array format, for one item data.
     *
     * @param string $strKey
     * @param array $itemData
     * @return string
     */
    protected function getStrCookieItemFromItemData($strKey, array $itemData)
    {
        // Return result
        return serialize($itemData);
    }



    /**
     * Configuration array format:
     * Set execution configuration can be provided.
     * [
     *     Table register set execution configuration,
     *
     *     cookie_expire_timeout(optional: got configuration cookie_expire_timeout if not found):
     *         DateTime object OR integer additional seconds, used on cookie setting (@see setcookie() ),
     *
     *     cookie_path(optional: got configuration cookie_path if not found):
     *         'string path used on cookie setting (@see setcookie() )',
     *
     *     cookie_domain_name(optional: got configuration cookie_domain_name if not found):
     *         'string (sub)domain name, used on cookie setting (@see setcookie() )',
     *
     *     cookie_secure_require(optional: got configuration cookie_secure_require if not found):
     *         true / false, used on cookie setting (@see setcookie() ),
     *
     *     cookie_http_only_require(optional: got configuration cookie_http_only_require if not found):
     *         true / false, used on cookie setting (@see setcookie() )
     * ]
     *
     * @inheritdoc
     */
    protected function getItemDataFromItem($item, array $tabConfig = null)
    {
        // Check configuration
        SetExecConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $tabExecConfig = $tabConfig;
        $tabConfig = $this->getTabConfig();
        $result = parent::getItemDataFromItem($item, $tabExecConfig);

        // Set cookie expire timout datetime
        $objTz = new DateTimeZone($this->getStrTimezoneName());
        $objCookieExpireTimeoutDt = (
            isset($tabExecConfig[ConstCookieRegister::TAB_SET_EXEC_CONFIG_KEY_COOKIE_EXPIRE_TIMEOUT]) ?
                $tabExecConfig[ConstCookieRegister::TAB_SET_EXEC_CONFIG_KEY_COOKIE_EXPIRE_TIMEOUT] :
                (
                    isset($tabConfig[ConstCookieRegister::TAB_CONFIG_KEY_COOKIE_EXPIRE_TIMEOUT]) ?
                        $tabConfig[ConstCookieRegister::TAB_CONFIG_KEY_COOKIE_EXPIRE_TIMEOUT] :
                        null
                )
        );
        $objCookieExpireTimeoutDt = (
            (!is_null($objCookieExpireTimeoutDt)) ?
                // Set specific time
                (
                ($objCookieExpireTimeoutDt instanceof DateTime) ?
                    $objCookieExpireTimeoutDt
                        ->setTimezone($objTz) :
                    (new DateTime())
                        ->setTimestamp(time() + $objCookieExpireTimeoutDt)
                        ->setTimezone($objTz)
                ) :
                (new DateTime())
                    ->setTimestamp(time() + ConstCookieRegister::DEFAULT_COOKIE_EXPIRE_TIMEOUT_SECOND)
                    ->setTimezone($objTz)
        );
        $result[ConstCookieRegister::TAB_ITEM_DATA_CONFIG_KEY_COOKIE_EXPIRE_TIMEOUT_DATETIME] = $objCookieExpireTimeoutDt;

        // Set cookie path
        $strCookiePath = (
            isset($tabExecConfig[ConstCookieRegister::TAB_SET_EXEC_CONFIG_KEY_COOKIE_PATH]) ?
                $tabExecConfig[ConstCookieRegister::TAB_SET_EXEC_CONFIG_KEY_COOKIE_PATH] :
                (
                    isset($tabConfig[ConstCookieRegister::TAB_CONFIG_KEY_COOKIE_PATH]) ?
                        $tabConfig[ConstCookieRegister::TAB_CONFIG_KEY_COOKIE_PATH] :
                        ConstCookieRegister::DEFAULT_COOKIE_PATH
                )
        );
        $result[ConstCookieRegister::TAB_ITEM_DATA_CONFIG_KEY_COOKIE_PATH] = $strCookiePath;

        // Set cookie domain name
        $strCookieDomainNm = (
            isset($tabExecConfig[ConstCookieRegister::TAB_SET_EXEC_CONFIG_KEY_COOKIE_DOMAIN_NAME]) ?
                $tabExecConfig[ConstCookieRegister::TAB_SET_EXEC_CONFIG_KEY_COOKIE_DOMAIN_NAME] :
                (
                    isset($tabConfig[ConstCookieRegister::TAB_CONFIG_KEY_COOKIE_DOMAIN_NAME]) ?
                        $tabConfig[ConstCookieRegister::TAB_CONFIG_KEY_COOKIE_DOMAIN_NAME] :
                        ConstCookieRegister::DEFAULT_COOKIE_DOMAIN_NAME
                )
        );
        $result[ConstCookieRegister::TAB_ITEM_DATA_CONFIG_KEY_COOKIE_DOMAIN_NAME] = $strCookieDomainNm;

        // Set cookie secure required option
        $boolCookieSecureRequired = (
            isset($tabExecConfig[ConstCookieRegister::TAB_SET_EXEC_CONFIG_KEY_COOKIE_SECURE_REQUIRE]) ?
                (intval($tabExecConfig[ConstCookieRegister::TAB_SET_EXEC_CONFIG_KEY_COOKIE_SECURE_REQUIRE]) === 1) :
                (
                    isset($tabConfig[ConstCookieRegister::TAB_ITEM_DATA_CONFIG_KEY_COOKIE_SECURE_REQUIRE]) ?
                        (intval($tabConfig[ConstCookieRegister::TAB_ITEM_DATA_CONFIG_KEY_COOKIE_SECURE_REQUIRE]) === 1) :
                        ConstCookieRegister::DEFAULT_COOKIE_SECURE_REQUIRE
                )
        );
        $result[ConstCookieRegister::TAB_ITEM_DATA_CONFIG_KEY_COOKIE_SECURE_REQUIRE] = $boolCookieSecureRequired;

        // Set cookie HTTP only required option
        $boolCookieHttpOnlyRequired = (
            isset($tabExecConfig[ConstCookieRegister::TAB_SET_EXEC_CONFIG_KEY_COOKIE_HTTP_ONLY_REQUIRE]) ?
                (intval($tabExecConfig[ConstCookieRegister::TAB_SET_EXEC_CONFIG_KEY_COOKIE_HTTP_ONLY_REQUIRE]) === 1) :
                (
                    isset($tabConfig[ConstCookieRegister::TAB_ITEM_DATA_CONFIG_KEY_COOKIE_HTTP_ONLY_REQUIRE]) ?
                        (intval($tabConfig[ConstCookieRegister::TAB_ITEM_DATA_CONFIG_KEY_COOKIE_HTTP_ONLY_REQUIRE]) === 1) :
                        ConstCookieRegister::DEFAULT_COOKIE_HTTP_ONLY_REQUIRE
                )
        );
        $result[ConstCookieRegister::TAB_ITEM_DATA_CONFIG_KEY_COOKIE_HTTP_ONLY_REQUIRE] = $boolCookieHttpOnlyRequired;

        // Return result
        return $result;
    }



    /**
     * Get cookie expiration timeout datetime,
     * from specified item data.
     *
     * Item data array format:
     * @see getUseTabItemData() items data array format, for one item data.
     *
     * @param array $itemData
     * @return DateTime
     */
    protected function getObjCookieExpireTimeoutDtFromItemData(array $itemData)
    {
        $objTz = new DateTimeZone($this->getStrTimezoneName());
        /** @var DateTime $objDt */
        $objDt = $itemData[ConstCookieRegister::TAB_ITEM_DATA_CONFIG_KEY_COOKIE_EXPIRE_TIMEOUT_DATETIME];
        $result = $objDt->setTimezone($objTz);

        // Return result
        return $result;
    }



    /**
     * Get cookie path,
     * from specified item data.
     *
     * Item data array format:
     * @see getUseTabItemData() items data array format, for one item data.
     *
     * @param array $itemData
     * @return string
     */
    protected function getStrCookiePathFromItemData(array $itemData)
    {
        // Return result
        return $itemData[ConstCookieRegister::TAB_ITEM_DATA_CONFIG_KEY_COOKIE_PATH];
    }



    /**
     * Get cookie domain name,
     * from specified item data.
     *
     * Item data array format:
     * @see getUseTabItemData() items data array format, for one item data.
     *
     * @param array $itemData
     * @return string
     */
    protected function getStrCookieDomainNameFromItemData(array $itemData)
    {
        // Return result
        return $itemData[ConstCookieRegister::TAB_ITEM_DATA_CONFIG_KEY_COOKIE_DOMAIN_NAME];
    }



    /**
     * Get cookie expiration timeout datetime,
     * from specified key.
     *
     * @param string $strKey
     * @return null|DateTime
     */
    public function getObjCookieExpireTimeoutDt($strKey)
    {
        // Return result
        return (
            (!is_null($itemData = $this->getItemDataFromKey($strKey))) ?
                $this->getObjCookieExpireTimeoutDtFromItemData($itemData) :
                null
        );
    }



    /**
     * Get cookie path,
     * from specified key.
     *
     * @param string $strKey
     * @return null|string
     */
    public function getStrCookiePath($strKey)
    {
        // Return result
        return (
            (!is_null($itemData = $this->getItemDataFromKey($strKey))) ?
                $this->getStrCookiePathFromItemData($itemData) :
                null
        );
    }



    /**
     * Get cookie domain name,
     * from specified key.
     *
     * @param string $strKey
     * @return null|string
     */
    public function getStrCookieDomainName($strKey)
    {
        // Return result
        return (
            (!is_null($itemData = $this->getItemDataFromKey($strKey))) ?
                $this->getStrCookieDomainNameFromItemData($itemData) :
                null
        );
    }





    // Methods setters
    // ******************************************************************************

    /**
     * Set cookie,
     * from specified item.
     *
     * @param string $strKey
     * @param boolean $boolRemoveRequired = false
     * @return boolean
     */
    protected function setCookie($strKey, $boolRemoveRequired = false)
    {
        // Init var
        $boolRemoveRequired = (is_bool($boolRemoveRequired) ? $boolRemoveRequired : false);
        $itemData = $this->getItemDataFromKey($strKey);
        $result = false;

        // Set cookie, if required
        if(!is_null($itemData))
        {
            // Get cookie item
            $strItem = (
                $boolRemoveRequired ?
                    ConstCookieRegister::REMOVE_COOKIE_VALUE :
                    $this->getStrCookieItemFromItemData($strKey, $itemData)
            );

            // Get cookie info
            $objExpireTimeoutSecond = (
                $boolRemoveRequired ?
                    (time() + ConstCookieRegister::REMOVE_COOKIE_EXPIRE_TIMEOUT_SECOND) :
                    $this->getObjCookieExpireTimeoutDtFromItemData($itemData)->getTimestamp()
            );
            $strPath = $this->getStrCookiePathFromItemData($itemData);
            $strDomainNm = $this->getStrCookieDomainNameFromItemData($itemData);
            $boolSecureRequired = $this->checkCookieSecureRequiredFromItemData($itemData);
            $boolHttpOnlyRequired = $this->checkCookieHttpOnlyRequiredFromItemData($itemData);

            // Set cookie
            $result = ToolBoxCookie::set(
                $strKey,
                $strItem,
                $objExpireTimeoutSecond,
                $strPath,
                $strDomainNm,
                $boolSecureRequired,
                $boolHttpOnlyRequired
            );
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function addItem($strKey, $item, array $tabConfig = null)
    {
        // Return result
        return (
            parent::addItem($strKey, $item, $tabConfig) &&
            $this->setCookie($strKey)
        );
    }



    /**
     * @inheritdoc
     */
    public function setItem($strKey, $item, array $tabConfig = null)
    {
        // Return result
        return (
            parent::setItem($strKey, $item, $tabConfig) &&
            $this->setCookie($strKey)
        );
    }



    /**
     * @inheritdoc
     */
    public function removeItem($strKey)
    {
        // Return result
        return (
            $this->setCookie($strKey, true) &&
            parent::removeItem($strKey)
        );
    }



}