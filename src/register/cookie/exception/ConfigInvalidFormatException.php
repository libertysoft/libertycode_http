<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\register\cookie\exception;

use Exception;

use DateTime;
use liberty_code\http\register\cookie\library\ConstCookieRegister;



class ConfigInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstCookieRegister::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 50, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $result =
            // Check valid cookie expire timeout
            (
                (!isset($config[ConstCookieRegister::TAB_CONFIG_KEY_COOKIE_EXPIRE_TIMEOUT])) ||
                is_int($config[ConstCookieRegister::TAB_CONFIG_KEY_COOKIE_EXPIRE_TIMEOUT]) ||
                ($config[ConstCookieRegister::TAB_CONFIG_KEY_COOKIE_EXPIRE_TIMEOUT] instanceof DateTime)
            ) &&

            // Check valid cookie path
            (
                (!isset($config[ConstCookieRegister::TAB_CONFIG_KEY_COOKIE_PATH])) ||
                is_string($config[ConstCookieRegister::TAB_CONFIG_KEY_COOKIE_PATH])
            ) &&

            // Check valid cookie domain name
            (
                (!isset($config[ConstCookieRegister::TAB_CONFIG_KEY_COOKIE_DOMAIN_NAME])) ||
                is_string($config[ConstCookieRegister::TAB_CONFIG_KEY_COOKIE_DOMAIN_NAME])
            ) &&

            // Check valid cookie secure required option
            (
                (!isset($config[ConstCookieRegister::TAB_CONFIG_KEY_COOKIE_SECURE_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstCookieRegister::TAB_CONFIG_KEY_COOKIE_SECURE_REQUIRE]) ||
                    is_int($config[ConstCookieRegister::TAB_CONFIG_KEY_COOKIE_SECURE_REQUIRE]) ||
                    (
                        is_string($config[ConstCookieRegister::TAB_CONFIG_KEY_COOKIE_SECURE_REQUIRE]) &&
                        ctype_digit($config[ConstCookieRegister::TAB_CONFIG_KEY_COOKIE_SECURE_REQUIRE])
                    )
                )
            ) &&

            // Check valid cookie HTTP only required option
            (
                (!isset($config[ConstCookieRegister::TAB_CONFIG_KEY_COOKIE_HTTP_ONLY_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstCookieRegister::TAB_CONFIG_KEY_COOKIE_HTTP_ONLY_REQUIRE]) ||
                    is_int($config[ConstCookieRegister::TAB_CONFIG_KEY_COOKIE_HTTP_ONLY_REQUIRE]) ||
                    (
                        is_string($config[ConstCookieRegister::TAB_CONFIG_KEY_COOKIE_HTTP_ONLY_REQUIRE]) &&
                        ctype_digit($config[ConstCookieRegister::TAB_CONFIG_KEY_COOKIE_HTTP_ONLY_REQUIRE])
                    )
                )
            );

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format.
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	public static function setCheck($config)
    {
		// Init var
		$result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}