<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\register\cookie\library;



class ConstCookieRegister
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration
    const TAB_CONFIG_KEY_COOKIE_EXPIRE_TIMEOUT = 'cookie_expire_timeout';
    const TAB_CONFIG_KEY_COOKIE_PATH = 'cookie_path';
    const TAB_CONFIG_KEY_COOKIE_DOMAIN_NAME = 'cookie_domain_name';
    const TAB_CONFIG_KEY_COOKIE_SECURE_REQUIRE = 'cookie_secure_require';
    const TAB_CONFIG_KEY_COOKIE_HTTP_ONLY_REQUIRE = 'cookie_http_only_require';

    // Set execution configuration
    const TAB_SET_EXEC_CONFIG_KEY_COOKIE_EXPIRE_TIMEOUT = 'cookie_expire_timeout';
    const TAB_SET_EXEC_CONFIG_KEY_COOKIE_PATH = 'cookie_path';
    const TAB_SET_EXEC_CONFIG_KEY_COOKIE_DOMAIN_NAME = 'cookie_domain_name';
    const TAB_SET_EXEC_CONFIG_KEY_COOKIE_SECURE_REQUIRE = 'cookie_secure_require';
    const TAB_SET_EXEC_CONFIG_KEY_COOKIE_HTTP_ONLY_REQUIRE = 'cookie_http_only_require';

    // Item data configuration
    const TAB_ITEM_DATA_CONFIG_KEY_COOKIE_EXPIRE_TIMEOUT_DATETIME = 'cookie_expire_timeout_datetime';
    const TAB_ITEM_DATA_CONFIG_KEY_COOKIE_PATH = 'cookie_path';
    const TAB_ITEM_DATA_CONFIG_KEY_COOKIE_DOMAIN_NAME = 'cookie_domain_name';
    const TAB_ITEM_DATA_CONFIG_KEY_COOKIE_SECURE_REQUIRE = 'cookie_secure_require';
    const TAB_ITEM_DATA_CONFIG_KEY_COOKIE_HTTP_ONLY_REQUIRE = 'cookie_http_only_require';

    // Default cookie configuration
    const DEFAULT_COOKIE_EXPIRE_TIMEOUT_SECOND = (60 * 60 * 24 * 400); // 400 days
    const DEFAULT_COOKIE_PATH = '/';
    const DEFAULT_COOKIE_DOMAIN_NAME = '';
    const DEFAULT_COOKIE_SECURE_REQUIRE = false;
    const DEFAULT_COOKIE_HTTP_ONLY_REQUIRE = false;

    // Remove cookie configuration
    const REMOVE_COOKIE_VALUE = '';
    const REMOVE_COOKIE_EXPIRE_TIMEOUT_SECOND = -60;



    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the cookie register configuration standard.';
    const EXCEPT_MSG_SET_EXEC_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array, following the cookie register set execution configuration standard.';



}