<?php
/**
 * GET arguments:
 * - clean: = 1: remove cookies.
 */

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Use
use liberty_code\library\table\library\ToolBoxTable;
use liberty_code\http\register\cookie\model\CookieRegister;



// Start capture output (due to update cookie, it update headers, so render only at the end)
ob_start();



// Init var
$objRegister = new CookieRegister();
$tabGetConfig = array(
    'select_key_regexp_pattern' => '#^test_lib_code_(.+)#'
);



// Test config
$tabConfig = array(
    'cookie_expire_timeout' => 'test',
    'cookie_path' => '/LibertyCode/LibertyCode/libertycode_http',
    'cookie_domain_name' => '',
    'cookie_secure_require' => false,
    'cookie_http_only_require' => false
);
try{
    $objRegister->setTabConfig($tabConfig);
} catch(\Exception $e) {
    echo(get_class($e) . ' - ' . htmlentities($e->getMessage()));
    echo('<br />');
}
echo('Get config: <pre>');var_dump($objRegister->getTabConfig());echo('</pre>');
echo('Get config timezone: <pre>');var_dump($objRegister->getStrTimezoneName());echo('</pre>');
echo('Get config format data required option: <pre>');var_dump($objRegister->checkFormatDataRequired());echo('</pre>');

$tabConfig = array(
    'cookie_expire_timeout' => (60 * 60 * 24 * 100),
    'cookie_path' => '/LibertyCode/LibertyCode/libertycode_http',
    'cookie_domain_name' => '',
    'cookie_secure_require' => 'test',
    'cookie_http_only_require' => false
);
try{
    $objRegister->setTabConfig($tabConfig);
} catch(\Exception $e) {
    echo(get_class($e) . ' - ' . htmlentities($e->getMessage()));
    echo('<br />');
}
echo('Get config: <pre>');var_dump($objRegister->getTabConfig());echo('</pre>');
echo('Get config timezone: <pre>');var_dump($objRegister->getStrTimezoneName());echo('</pre>');
echo('Get config format data required option: <pre>');var_dump($objRegister->checkFormatDataRequired());echo('</pre>');

$tabConfig = array(
    'cookie_expire_timeout' => (60 * 60 * 24 * 100),
    'cookie_path' => '/LibertyCode/LibertyCode/libertycode_http',
    'cookie_domain_name' => '',
    'cookie_secure_require' => false,
    'cookie_http_only_require' => false
);
$objRegister->setTabConfig($tabConfig);
echo('Get config: <pre>');var_dump($objRegister->getTabConfig());echo('</pre>');
echo('Get config timezone: <pre>');var_dump($objRegister->getStrTimezoneName());echo('</pre>');
echo('Get config format data required option: <pre>');var_dump($objRegister->checkFormatDataRequired());echo('</pre>');

echo('<br /><br /><br />');



// Test cookie
echo('Test cookie: <br />');
foreach($objRegister->getTabKey($tabGetConfig) as $strKey)
{
    echo('Get "'.$strKey.'": <pre>');var_dump($objRegister->getItem($strKey));echo('</pre>');
    echo('Get "'.$strKey.'": <pre>');
    var_dump(array(
        'timeout' => $objRegister->getObjExpireTimeoutDt($strKey),
        'cookie_timeout' => $objRegister->getObjCookieExpireTimeoutDt($strKey),
        'cookie_path' => $objRegister->getStrCookiePath($strKey),
        'cookie_domain_nm' => $objRegister->getStrCookieDomainName($strKey),
        'cookie_secure' => $objRegister->checkCookieSecureRequired($strKey),
        'cookie_http_only' => $objRegister->checkCookieHttpOnlyRequired($strKey)
    ));
    echo('</pre>');
}

echo('<br /><br /><br />');



// Test add
$test1 = 'Test 1';
$test3 = true;
$tabData = array(
    [
        'test_lib_code_key_1',
        'Value 1',
        [
            'expire_timeout' => 10,
            'cookie_expire_timeout' => (60 * 60 * 24 * 1),
            'cookie_path' => '/LibertyCode/LibertyCode/libertycode_http/src/register',
            //'cookie_domain_name' => '',
            //'cookie_secure_require' => false,
            //'cookie_http_only_require' => false
        ]
    ], // Ok
    ['test_lib_code_key_2', 2], // Ok
    ['test_lib_code_key_3', true], // Ok
    [
        'test_lib_code_key_4',
        [
            'key_4_1' => 'Value 4 1',
            'key_4_2' => 4,
            'key_4_3' => false
        ]
    ],// Ok
    ['test_lib_code_key_1', 'Value 1 duplicate'], // Ko: key found
    [
        'test_lib_code_key_5',
        'test',
        [
            'expire_timeout' => 10,
            'cookie_expire_timeout' => 'test',
            'cookie_path' => '/LibertyCode/LibertyCode/libertycode_http/src/register',
            //'cookie_domain_name' => '',
            //'cookie_secure_require' => false,
            //'cookie_http_only_require' => false
        ]
    ] // Ko: bad config format
);

foreach($tabData as $data)
{
    $strKey = $data[0];
    $value = $data[1];
    $tabConfig = (isset($data[2]) ? $data[2] : null);

    echo('Test add "'.$strKey.'": <br />');
    try{
        $objRegister->addItem($strKey, $value, $tabConfig);
        echo('Ok <br />');
    } catch(\Exception $e) {
        echo(get_class($e) . ' - ' . htmlentities($e->getMessage()));
        echo('<br />');
    }
    echo('<br />');
}

echo('Test adding: <br />');
foreach($objRegister->getTabKey($tabGetConfig) as $strKey)
{
    echo('Get "'.$strKey.'": <pre>');var_dump($objRegister->getItem($strKey));echo('</pre>');
    echo('Get "'.$strKey.'": <pre>');
    var_dump(array(
        'timeout' => $objRegister->getObjExpireTimeoutDt($strKey),
        'cookie_timeout' => $objRegister->getObjCookieExpireTimeoutDt($strKey),
        'cookie_path' => $objRegister->getStrCookiePath($strKey),
        'cookie_domain_nm' => $objRegister->getStrCookieDomainName($strKey),
        'cookie_secure' => $objRegister->checkCookieSecureRequired($strKey),
        'cookie_http_only' => $objRegister->checkCookieHttpOnlyRequired($strKey)
    ));
    echo('</pre>');
}

echo('<br /><br /><br />');



// Test check/get
$tabKey = array(
    'test_lib_code_key_1', // Found
    'test_lib_code_key_2', // Found
    'test_lib_code_key_3', // Found
    'test_lib_code_key_4', // Found
    'test_lib_code_key_5', // Not found
    true // Ko: bad key format
);

foreach($tabKey as $strKey)
{
    echo('Test check, get "'.$strKey.'": <br />');
    try{
        echo('Exists: <pre>');var_dump($objRegister->checkItemExists($strKey));echo('</pre>');
        echo('Get: <pre>');var_dump($objRegister->getItem($strKey));echo('</pre>');
    } catch(\Exception $e) {
        echo(get_class($e) . ' - ' . htmlentities($e->getMessage()));
        echo('<br />');
    }
    echo('<br />');
}

echo('<br /><br /><br />');



// Test set
$tabData = array(
    [
        'test_lib_code_key_2',
        2.7,
        [
            'expire_timeout' => 100,
            'cookie_expire_timeout' => (60 * 60 * 24 * 1),
            'cookie_path' => '/LibertyCode/LibertyCode/libertycode_http/src/register',
            //'cookie_domain_name' => '',
            //'cookie_secure_require' => false,
            //'cookie_http_only_require' => false
        ]
    ], // Ok
    ['test_lib_code_key_5', 'Value 5'] // Ko: Key not found
);

foreach($tabData as $data)
{
    $strKey = $data[0];
    $value = $data[1];
    $tabConfig = (isset($data[2]) ? $data[2] : null);

    echo('Test set "'.$strKey.'": <br />');
    try{
        $objRegister->setItem($strKey, $value, $tabConfig);
        echo('Ok <br />');
    } catch(\Exception $e) {
        echo(get_class($e) . ' - ' . htmlentities($e->getMessage()));
        echo('<br />');
    }
    echo('<br />');
}

echo('Test setting: <br />');
foreach($objRegister->getTabKey($tabGetConfig) as $strKey)
{
    echo('Get "'.$strKey.'": <pre>');var_dump($objRegister->getItem($strKey));echo('</pre>');
    echo('Get "'.$strKey.'": <pre>');
    var_dump(array(
        'timeout' => $objRegister->getObjExpireTimeoutDt($strKey),
        'cookie_timeout' => $objRegister->getObjCookieExpireTimeoutDt($strKey),
        'cookie_path' => $objRegister->getStrCookiePath($strKey),
        'cookie_domain_nm' => $objRegister->getStrCookieDomainName($strKey),
        'cookie_secure' => $objRegister->checkCookieSecureRequired($strKey),
        'cookie_http_only' => $objRegister->checkCookieHttpOnlyRequired($strKey)
    ));
    echo('</pre>');
}

echo('<br /><br /><br />');



// Test put
$tabData = array(
    [
        'test_lib_code_key_4',
        [
            'key_4_1' => 'Value 4 1 update',
            'key_4_2' => 4,
            'key_4_3' => false
        ]
    ], // Ok: update
    [
        'test_lib_code_key_4',
        [
            'key_4_1' => 'Value 4 1 update',
            'key_4_2' => 4,
            'key_4_3' => false
        ],
        [
            'expire_timeout' => 100,
            'cookie_expire_timeout' => (60 * 60 * 24 * 1),
            'cookie_path' => true,
            //'cookie_domain_name' => '',
            //'cookie_secure_require' => false,
            //'cookie_http_only_require' => false
        ]
    ], // Ko: bad config format
    [
        'test_lib_code_key_5',
        'Value 5',
        [
            'expire_timeout' => 10,
            'cookie_expire_timeout' => (60 * 60 * 24 * 1),
            'cookie_path' => '/LibertyCode/LibertyCode/libertycode_http/src/register',
            //'cookie_domain_name' => '',
            //'cookie_secure_require' => false,
            //'cookie_http_only_require' => false
        ]
    ], // Ok: create
    ['test_lib_code_key_6', 6] // Ok: create
);

foreach($tabData as $data)
{
    $strKey = $data[0];
    $value = $data[1];
    $tabConfig = (isset($data[2]) ? $data[2] : null);

    echo('Test put "'.$strKey.'": <br />');
    try{
        $objRegister->putItem($strKey, $value, $tabConfig);
        echo('Ok <br />');
    } catch(\Exception $e) {
        echo(get_class($e) . ' - ' . htmlentities($e->getMessage()));
        echo('<br />');
    }
    echo('<br />');
}

echo('Test putting: <br />');
foreach($objRegister->getTabKey($tabGetConfig) as $strKey)
{
    echo('Get "'.$strKey.'": <pre>');var_dump($objRegister->getItem($strKey));echo('</pre>');
    echo('Get "'.$strKey.'": <pre>');
    var_dump(array(
        'timeout' => $objRegister->getObjExpireTimeoutDt($strKey),
        'cookie_timeout' => $objRegister->getObjCookieExpireTimeoutDt($strKey),
        'cookie_path' => $objRegister->getStrCookiePath($strKey),
        'cookie_domain_nm' => $objRegister->getStrCookieDomainName($strKey),
        'cookie_secure' => $objRegister->checkCookieSecureRequired($strKey),
        'cookie_http_only' => $objRegister->checkCookieHttpOnlyRequired($strKey)
    ));
    echo('</pre>');
}

echo('<br /><br /><br />');



// Test remove
$tabKey = array(
    'test_lib_code_key_5', // Ok
    'test_lib_code_key_6', // Ok
    'test_lib_code_key_7', // Ko: not found
    true // Ko: bad key format
);

foreach($tabKey as $strKey)
{
    echo('Test remove "'.$strKey.'": <br />');
    try{
        $objRegister->removeItem($strKey);
        echo('Ok <br />');
    } catch(\Exception $e) {
        echo(htmlentities($e->getMessage()));
        echo('<br />');
    }
    echo('<br />');
}

echo('Test removing: <br />');
foreach($objRegister->getTabKey($tabGetConfig) as $strKey)
{
    echo('Get "'.$strKey.'": <pre>');var_dump($objRegister->getItem($strKey));echo('</pre>');
}

echo('<br /><br /><br />');



// Close session, if required
if(trim(ToolBoxTable::getItem($_GET, 'clean', '0')) == '1')
{
    $objRegister->removeItemAll($tabGetConfig);
}



/*
echo('<pre>');print_r(headers_list());echo('</pre>');

// TODO maybe toolbox cookie, with set cookie feature
if (!headers_sent()) {
    $tabCookieHeader = array();
    foreach(headers_list() as $strHeaderLine)
    {
        // Init var
        $strPattern = '#^Set\-Cookie\:([^=]+)=.*$#';
        $tabMatch = array();
        if(preg_match($strPattern, $strHeaderLine, $tabMatch) === 1)
        {
            $strCookieKey = trim($tabMatch[1]);
            $tabCookieHeader[$strCookieKey] = $strHeaderLine;
        }
    }

    header_remove('Set-Cookie');

    foreach($tabCookieHeader as $strHeaderLine)
    {
        header($strHeaderLine, false);
    }
}

echo('<pre>');print_r(headers_list());echo('</pre>');
//*/



// End capture output
$strContent = ob_get_clean();
echo($strContent);


