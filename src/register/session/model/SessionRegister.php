<?php
/**
 * Description :
 * This class allows to define session register class.
 * Session register is table register,
 * using session array ($_SESSION), as storage support.
 *
 * Session register uses the following specified configuration:
 * [
 *     Table register configuration
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\register\session\model;

use liberty_code\register\register\table\model\TableRegister;



class SessionRegister extends TableRegister
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function __construct(
        array $tabConfig = null,
        array $tabItem = null
    )
    {
        // Initialize session
        $this->initialize();

        // Call parent constructor
        parent::__construct(
            $tabConfig,
            $tabItem
        );
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * Initialize session.
     */
    protected function initialize()
    {
        // Start session, if required
        if(session_status() == PHP_SESSION_NONE)
        {
            session_start();
        }
    }



    /**
     * Close session.
     * Required only, when session need to be destroyed.
     */
    public function close()
    {
        // Destroy session
        session_destroy();
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function &getUseTabItemData()
    {
        return $_SESSION;
    }
	
	
	
}