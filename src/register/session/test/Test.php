<?php
/**
 * GET arguments:
 * - clean: = 1: Close session.
 */

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Use
use liberty_code\library\table\library\ToolBoxTable;
use liberty_code\http\register\session\model\SessionRegister;



// Init var
$objRegister = new SessionRegister();



// Test config
/*
$tabConfig = array(
    'set_timezone_name' => 'UTC',
    'format_data_require' => false
);
$objRegister->setTabConfig($tabConfig);
//*/
echo('Get config: <pre>');var_dump($objRegister->getTabConfig());echo('</pre>');
echo('Get config timezone: <pre>');var_dump($objRegister->getStrTimezoneName());echo('</pre>');
echo('Get config format data required option: <pre>');var_dump($objRegister->checkFormatDataRequired());echo('</pre>');

echo('<br /><br /><br />');



// Test session
echo('Test session: <br />');
foreach($objRegister->getTabKey() as $strKey)
{
    echo('Get "'.$strKey.'": <pre>');var_dump($objRegister->getItem($strKey));echo('</pre>');
    echo('Get "'.$strKey.'" timeout: <pre>');var_dump($objRegister->getObjExpireTimeoutDt($strKey));echo('</pre>');
}

echo('<br /><br /><br />');



// Test add
$test1 = 'Test 1';
$test3 = true;
$tabData = array(
    ['key_1', 'Value 1', ['expire_timeout' => 10]], // Ok
    ['key_2', 2], // Ok
    ['key_3', true], // Ok
    [
        'key_4',
        [
            'key_4_1' => 'Value 4 1',
            'key_4_2' => 4,
            'key_4_3' => false
        ]
    ],// Ok
    ['key_1', 'Value 1 duplicate'], // Ko: key found
);

foreach($tabData as $data)
{
    $strKey = $data[0];
    $value = $data[1];
    $tabConfig = (isset($data[2]) ? $data[2] : null);

    echo('Test add "'.$strKey.'": <br />');
    try{
        $objRegister->addItem($strKey, $value, $tabConfig);
        echo('Ok <br />');
    } catch(\Exception $e) {
        echo(get_class($e) . ' - ' . htmlentities($e->getMessage()));
        echo('<br />');
    }
    echo('<br />');
}

echo('Test adding: <br />');
foreach($objRegister->getTabKey() as $strKey)
{
    echo('Get "'.$strKey.'": <pre>');var_dump($objRegister->getItem($strKey));echo('</pre>');
    echo('Get "'.$strKey.'" timeout: <pre>');var_dump($objRegister->getObjExpireTimeoutDt($strKey));echo('</pre>');
}

echo('<br /><br /><br />');



// Test check/get
$tabKey = array(
	'key_1', // Found
	'key_2', // Found
	'key_3', // Found
	'key_4', // Found
	'key_5', // Not found
    true // Ko: bad key format
);

foreach($tabKey as $strKey)
{
	echo('Test check, get "'.$strKey.'": <br />');
	try{
		echo('Exists: <pre>');var_dump($objRegister->checkItemExists($strKey));echo('</pre>');
		echo('Get: <pre>');var_dump($objRegister->getItem($strKey));echo('</pre>');
	} catch(\Exception $e) {
		echo(get_class($e) . ' - ' . htmlentities($e->getMessage()));
		echo('<br />');
	}
	echo('<br />');
}

echo('<br /><br /><br />');



// Test set
$tabData = array(
    ['key_2', 2.7, ['expire_timeout' => 100]], // Ok
	['key_5', 'Value 5'] // Ko: Key not found
);

foreach($tabData as $data)
{
    $strKey = $data[0];
    $value = $data[1];
    $tabConfig = (isset($data[2]) ? $data[2] : null);

	echo('Test set "'.$strKey.'": <br />');
	try{
		$objRegister->setItem($strKey, $value, $tabConfig);
        echo('Ok <br />');
	} catch(\Exception $e) {
		echo(get_class($e) . ' - ' . htmlentities($e->getMessage()));
		echo('<br />');
	}
	echo('<br />');
}

echo('Test setting: <br />');
foreach($objRegister->getTabKey() as $strKey)
{
    echo('Get "'.$strKey.'": <pre>');var_dump($objRegister->getItem($strKey));echo('</pre>');
    echo('Get "'.$strKey.'" timeout: <pre>');var_dump($objRegister->getObjExpireTimeoutDt($strKey));echo('</pre>');
}

echo('<br /><br /><br />');



// Test put
$tabData = array(
    [
        'key_4',
        [
            'key_4_1' => 'Value 4 1 update',
            'key_4_2' => 4,
            'key_4_3' => false
        ]
    ], // Ok: update
    ['key_4', [], ['expire_timeout' => 'test']], // Ko: bad config format
	['key_5', 'Value 5', ['expire_timeout' => 3000]], // Ok: create
	['key_6', 6] // Ok: create
);

foreach($tabData as $data)
{
    $strKey = $data[0];
    $value = $data[1];
    $tabConfig = (isset($data[2]) ? $data[2] : null);

	echo('Test put "'.$strKey.'": <br />');
	try{
		$objRegister->putItem($strKey, $value, $tabConfig);
        echo('Ok <br />');
	} catch(\Exception $e) {
		echo(get_class($e) . ' - ' . htmlentities($e->getMessage()));
		echo('<br />');
	}
	echo('<br />');
}

echo('Test putting: <br />');
foreach($objRegister->getTabKey() as $strKey)
{
    echo('Get "'.$strKey.'": <pre>');var_dump($objRegister->getItem($strKey));echo('</pre>');
    echo('Get "'.$strKey.'" timeout: <pre>');var_dump($objRegister->getObjExpireTimeoutDt($strKey));echo('</pre>');
}

echo('<br /><br /><br />');



// Test remove
$tabKey = array(
	'key_5', // Ok
	'key_6', // Ok
	'key_7', // Ko: not found
    true // Ko: bad key format
);

foreach($tabKey as $strKey)
{
	echo('Test remove "'.$strKey.'": <br />');
	try{
		$objRegister->removeItem($strKey);
        echo('Ok <br />');
	} catch(\Exception $e) {
		echo(htmlentities($e->getMessage()));
		echo('<br />');
	}
	echo('<br />');
}

echo('Test removing: <br />');
foreach($objRegister->getTabKey() as $strKey)
{
    echo('Get "'.$strKey.'": <pre>');var_dump($objRegister->getItem($strKey));echo('</pre>');
}

echo('<br /><br /><br />');



// Test hydrate
//*
$tabItem = array(
    'key_ses_1' => 'Value SESSION 1', // Ok: create
    'key_ses_2' => 2, // Ok: create
    'key_ses_3' => true, // Ok: create
    'key_ses_4' => array(
        'key_ses_4_1' => 'Value SESSION 4 1',
        'key_ses_4_2' => 4,
        'key_ses_4_3' => false
    ) // Ok: create
);
$objRegister->hydrateItem($tabItem);
echo('Test hydrate: <br />');
foreach($objRegister->getTabKey() as $strKey)
{
    $item = $objRegister->getItem($strKey);
    echo('Get "'.$strKey.'": <pre>');var_dump($objRegister->getItem($strKey));echo('</pre>');
}

echo('<br /><br /><br />');
//*/



// Close session, if required
if(trim(ToolBoxTable::getItem($_GET, 'clean', '0')) == '1')
{
    $objRegister->close();
}


