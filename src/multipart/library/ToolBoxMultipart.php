<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\multipart\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\library\str\library\ToolBoxString;
use liberty_code\library\random\library\ToolBoxRandom;
use liberty_code\http\header\library\ToolBoxHeader;



class ToolBoxMultipart extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods getters
	// ******************************************************************************

    /**
     * Get new boundary.
     *
     * @return string
     */
    public static function getStrNewBoundary()
    {
        // Return result
        return ToolBoxRandom::getStrRandom(50, '#^\w$#');
    }



    /**
     * Get string specified formatted part.
     *
     * @param string $strPart
     * @param boolean $boolFormatEnd = false
     * @return null|string
     */
    public static function getStrPartFormat($strPart, $boolFormatEnd = false)
    {
        // Init var
        $boolFormatEnd = (is_bool($boolFormatEnd) ? $boolFormatEnd : false);
        $result = (
            is_string($strPart) ?
                ltrim($strPart, "\r\n") :
                null
        );

        // Format end, if required
        if($boolFormatEnd)
        {
            // Get end value
            $strEndValue = null;
            $tabEndValue = array(
                "\r\n",
                "\r",
                "\n"
            );
            for($intCpt = 0; ($intCpt < count($tabEndValue)) && is_null($strEndValue); $intCpt++)
            {
                $strEndValue = (
                    ToolBoxString::checkEndsWith($result, $tabEndValue[$intCpt]) ?
                        $tabEndValue[$intCpt] :
                        $strEndValue
                );
            }

            // Truncate end value, if required
            $result = (
                (!is_null($strEndValue)) ?
                    mb_substr($result, 0 , (-1 * mb_strlen($strEndValue))) :
                    $result
            );
        }

        // Return result
        return $result;
    }



    /**
     * Get string multipart,
     * from specified index array of string parts.
     *
     * @param array $tabPart
     * @param string $strBoundary
     * @return null|string
     */
    public static function getStrMultipart(
        array $tabPart,
        $strBoundary
    )
    {
        // Init var
        $tabPart = array_filter(
            $tabPart,
            function($strPart) {return is_string($strPart);}
        );
        $tabPart = array_map(
            function($strPart) {return static::getStrPartFormat($strPart, false);},
            $tabPart
        );
        $strBoundary = (
            (is_string($strBoundary) && (trim($strBoundary) !== '')) ?
                sprintf('--%1$s', $strBoundary) :
                null
        );
        $result = (
            ((count($tabPart) > 0) && (!is_null($strBoundary))) ?
                (
                    $strBoundary . "\r\n" .
                    implode(
                        "\r\n" . $strBoundary . "\r\n",
                        $tabPart
                    ) . "\r\n" .
                    $strBoundary . '--'
                ) :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get index array of string parts,
     * from specified string multipart.
     *
     * @param string $strMultipart
     * @param string $strBoundary
     * @return null|array
     */
    public static function getTabPart(
        $strMultipart,
        $strBoundary
    )
    {
        // Init var
        $strMultipart = (
            (is_string($strMultipart) && (trim($strMultipart) !== '')) ?
                $strMultipart :
                null
        );
        $strBoundary = (
            (is_string($strBoundary) && (trim($strBoundary) !== '')) ?
                (sprintf('--%1$s', $strBoundary)) :
                null
        );
        $result = (
            ((!is_null($strMultipart)) && (!is_null($strBoundary))) ?
                explode($strBoundary, $strMultipart) :
                null
        );

        // Format result, if required
        if(!is_null($result))
        {
            array_shift($result);
            array_pop($result);
            $result = (
                (count($result) > 0) ?
                    array_map(
                        function($strPart) {return static::getStrPartFormat($strPart, true);},
                        $result
                    ) :
                    null
            );
        }

        // Return result
        return $result;
    }



    /**
     * Get string header lines,
     * from specified string part.
     *
     * @param string $strPart
     * @return null|string
     */
    public static function getStrPartHeaderLines($strPart)
    {
        // Init var
        $tabMatch = array();
        $result = (
            (
                is_string($strPart) &&
                (preg_match(
                    '#^(.+?)(?>\r\n|\n|\r){2}.*$#ms',
                        $strPart,
                    $tabMatch
                ) === 1)
            ) ?
                $tabMatch[1] :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get string body,
     * from specified string part.
     *
     * @param string $strPart
     * @return null|string
     */
    public static function getStrPartBody($strPart)
    {
        // Init var
        $result = (
            (
                is_string($strPart) &&
                (preg_match(
                        '#^.+?(?>\r\n|\n|\r){2}(.*)$#ms',
                        $strPart,
                        $tabMatch
                    ) === 1)
            ) ?
                $tabMatch[1] :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get string part,
     * from specified string header lines,
     * and specified string body.
     *
     * @param string $strHeaderLines
     * @param string $strBody
     * @return null|string
     */
    public static function getStrPart($strHeaderLines, $strBody)
    {
        // Init var
        $result = (
            (is_string($strHeaderLines) && is_string($strBody)) ?
                (ToolBoxHeader::getStrHeaderLineFormat($strHeaderLines) . "\r\n" . "\r\n" . $strBody) :
                null
        );

        // Return result
        return $result;
    }



}