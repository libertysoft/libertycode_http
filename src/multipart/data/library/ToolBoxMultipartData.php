<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\multipart\data\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\library\str\library\ToolBoxString;
use liberty_code\file\file\api\FileInterface;
use liberty_code\file\file\name\api\NameFileInterface;
use liberty_code\http\header\library\ToolBoxHeader;



class ToolBoxMultipartData extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods check
    // ******************************************************************************

    /**
     * Check if specified string header lines are data.
     *
     * @param string $strHeaderLines
     * @return null|string
     */
    public static function checkIsData($strHeaderLines)
    {
        // Return result
        return (!is_null(static::getStrKey($strHeaderLines)));
    }



    /**
     * Check if specified string header lines are a value.
     *
     * @param string $strHeaderLines
     * @return null|string
     */
    public static function checkIsValue($strHeaderLines)
    {
        // Return result
        return (
            static::checkIsData($strHeaderLines) &&
            is_null(static::getStrFileName($strHeaderLines))
        );
    }



    /**
     * Check if specified string header lines are a file.
     *
     * @param string $strHeaderLines
     * @return null|string
     */
    public static function checkIsFile($strHeaderLines)
    {
        // Return result
        return (
            static::checkIsData($strHeaderLines) &&
            (!is_null(static::getStrFileName($strHeaderLines)))
        );
    }





	// Methods getters
	// ******************************************************************************

    /**
     * Get string key,
     * from specified string header lines.
     *
     * @param string $strHeaderLines
     * @return null|string
     */
    public static function getStrKey($strHeaderLines)
    {
        // Init var
        $tabData = ToolBoxHeader::getTabData($strHeaderLines);
        $tabData = (
            (!is_null($tabData)) ?
                array_combine(
                    array_map(
                        function($strKey) {return strtolower($strKey);},
                        array_keys($tabData)
                    ),
                    array_values($tabData)
                ) :
                $tabData
        );
        $tabMatch = array();
        $result = (
            (
                isset($tabData['content-disposition']) &&
                (preg_match(
                    '#;[^\w]*name="(.+?)"#i',
                    $tabData['content-disposition'],
                    $tabMatch
                ) === 1) &&
                (trim($tabMatch[1]) != '')
            ) ?
                $tabMatch[1] :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get string file name,
     * from specified string header lines.
     *
     * @param string $strHeaderLines
     * @return null|string
     */
    public static function getStrFileName($strHeaderLines)
    {
        // Init var
        $tabData = ToolBoxHeader::getTabData($strHeaderLines);
        $tabData = (
            (!is_null($tabData)) ?
                array_combine(
                    array_map(
                        function($strKey) {return strtolower($strKey);},
                        array_keys($tabData)
                    ),
                    array_values($tabData)
                ) :
                $tabData
        );
        $tabMatch = array();
        $result = (
            (
                isset($tabData['content-disposition']) &&
                (preg_match(
                    '#;[^\w]*filename="(.+?)"#i',
                    $tabData['content-disposition'],
                    $tabMatch
                ) === 1) &&
                (trim($tabMatch[1]) != '')
            ) ?
                $tabMatch[1] :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get string mime type,
     * from specified string header lines.
     *
     * @param string $strHeaderLines
     * @return null|string
     */
    public static function getStrMimeType($strHeaderLines)
    {
        // Init var
        $tabData = ToolBoxHeader::getTabData($strHeaderLines);
        $tabData = (
            (!is_null($tabData)) ?
                array_combine(
                    array_map(
                        function($strKey) {return strtolower($strKey);},
                        array_keys($tabData)
                    ),
                    array_values($tabData)
                ) :
                $tabData
        );
        $result = (
            isset($tabData['content-type']) ?
                $tabData['content-type'] :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get integer size,
     * from specified string header lines.
     *
     * @param string $strHeaderLines
     * @return null|integer
     */
    public static function getIntSize($strHeaderLines)
    {
        // Init var
        $tabData = ToolBoxHeader::getTabData($strHeaderLines);
        $tabData = (
            (!is_null($tabData)) ?
                array_combine(
                    array_map(
                        function($strKey) {return strtolower($strKey);},
                        array_keys($tabData)
                    ),
                    array_values($tabData)
                ) :
                $tabData
        );
        $result = (
            (
                isset($tabData['content-length']) &&
                is_numeric($tabData['content-length']) &&
                (intval($tabData['content-length']) >= 0)
            ) ?
                intval($tabData['content-length']) :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get string part header lines,
     * from specified string key,
     * and specified value.
     *
     * @param string $strKey
     * @param string|FileInterface $value
     * @param string $strDefaultFileNm = null
     * @return null|string
     */
    public static function getStrPartHeaderLines(
        $strKey,
        $value,
        $strDefaultFileNm = null
    )
    {
        // Ini var
        $strKey = (
            (is_string($strKey) && (trim($strKey) != '')) ?
                $strKey :
                null
        );
        $strDefaultFileNm = (
            (is_string($strDefaultFileNm) && (trim($strDefaultFileNm) != '')) ?
                $strDefaultFileNm :
                null
        );

        // Init header data array
        $tabHeaderData = null;
        if(!is_null($strKey))
        {
            // Case file
            if($value instanceof FileInterface)
            {
                $strFileNm = (
                    ($value instanceof NameFileInterface) ?
                        $value->getStrName() :
                        ((!is_null($strDefaultFileNm)) ? $strDefaultFileNm : null)
                );
                if(!is_null($strFileNm))
                {
                    $tabHeaderData = array(
                        'Content-Disposition' => sprintf(
                            'form-data; name="%1$s"; filename="%2$s"',
                            $strKey,
                            $strFileNm
                        ),
                        'Content-Transfer-Encoding' => 'binary',
                    );

                    if(!is_null($type = $value->getStrMimeType()))
                    {
                        $tabHeaderData['Content-Type'] = $type;
                    }

                    if(!is_null($size = $value->getIntSize()))
                    {
                        $tabHeaderData['Content-Length'] = $size;
                    }
                }
            }
            // Case string convertible
            else if(is_string($value) || ToolBoxString::checkConvertString($value))
            {
                $tabHeaderData = array(
                    'Content-Disposition' => sprintf('form-data; name="%1$s"', $strKey)
                );
            }
        }

        // Init result
        $result = (
            (!is_null($tabHeaderData)) ?
                ToolBoxHeader::getHeaderLine($tabHeaderData, false) :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get string part body,
     * from specified value.
     *
     * @param string|FileInterface $value
     * @return null|string
     */
    public static function getStrPartBody($value)
    {
        // Init var
        $result = (
            ($value instanceof FileInterface) ?
                $value->getStrContent() :
                (
                    (is_bool($value)) ?
                        ($value ? '1' : '0') :
                        (
                            (is_string($value) || ToolBoxString::checkConvertString($value)) ?
                                strval($value) :
                                null
                        )
                )
        );

        // Return result
        return $result;
    }



}