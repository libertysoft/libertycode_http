<?php
/**
 * Description :
 * This class allows to define HTTP response class.
 * HTTP response is response,
 * allowing to handle all HTTP received information.
 * Can be consider is base of all HTTP response type.
 *
 * HTTP response uses the following specified configuration:
 * [
 *     Default response configuration
 * ]
 *
 * HTTP response handles the following specified reception information:
 * [
 *     Default response reception information,
 *
 *     protocol_version_num(optional): "string HTTP protocol version number",
 *
 *     status_code(optional): integer HTTP status code,
 *
 *     status_msg(optional): "string HTTP status message",
 *
 *     header(optional): [
 *         @see HeaderData data source array format
 *     ],
 *
 *     body(optional): "string body content"
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\requisition\response\model;

use liberty_code\requisition\response\model\DefaultResponse;

use liberty_code\requisition\response\library\ConstResponse;
use liberty_code\http\header\model\HeaderData;
use liberty_code\http\requisition\response\library\ConstHttpResponse;
use liberty_code\http\requisition\response\exception\HeaderDataInvalidFormatException;
use liberty_code\http\requisition\response\exception\RcpInfoInvalidFormatException;



/**
 * @method null|HeaderData getObjHeaderData() Get header data object.
 */
class HttpResponse extends DefaultResponse
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();


	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param HeaderData $objHeaderData = null
     */
    public function __construct(
        HeaderData $objHeaderData = null,
        array $tabConfig = null,
        array $tabRcpInfo = null
    )
    {
        // Call parent constructor
        parent::__construct($tabConfig);

        // Init header data, if required
        if(!is_null($objHeaderData))
        {
            $this->setObjHeaderData($objHeaderData);
        }

        // Init reception information, if required
        if(!is_null($tabRcpInfo))
        {
            $this->setRcpInfo($tabRcpInfo);
        }
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstHttpResponse::DATA_KEY_DEFAULT_HEADER_DATA))
        {
            $this->__beanTabData[ConstHttpResponse::DATA_KEY_DEFAULT_HEADER_DATA] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstHttpResponse::DATA_KEY_DEFAULT_HEADER_DATA
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstHttpResponse::DATA_KEY_DEFAULT_HEADER_DATA:
                    HeaderDataInvalidFormatException::setCheck($value);
                    break;

                case ConstResponse::DATA_KEY_DEFAULT_RCP_INFO:
                    RcpInfoInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getTabRcpInfo()
    {
        // Init var
        $result = parent::getTabRcpInfo();

        // Register header data, if required
        $objHeaderData = $this->getObjHeaderData();
        if(!is_null($objHeaderData))
        {
            $result[ConstHttpResponse::TAB_RCP_INFO_KEY_HEADER] = $objHeaderData->getDataSrc();
        }

        // Return result
        return $result;
    }



    /**
     * Get reception information value.
     *
     * @param string $strKey
     * @param mixed $default = null
     * @return mixed
     */
    protected function getRcpInfoValue($strKey, $default = null)
    {
        // Init var
        $tabInfo = $this->getTabRcpInfo();
        $result = (
            array_key_exists($strKey, $tabInfo) ?
                $tabInfo[$strKey] :
                $default
        );

        // Return result
        return $result;
    }



    /**
     * Get string protocol version number.
     *
     * @return null|string
     */
    public function getStrProtocolVersionNum()
    {
        // Return result
        return $this->getRcpInfoValue(ConstHttpResponse::TAB_RCP_INFO_KEY_PROTOCOL_VERSION_NUM);
    }



    /**
     * Get integer status code.
     *
     * @return null|integer
     */
    public function getIntStatusCode()
    {
        // Init var
        $result = $this->getRcpInfoValue(ConstHttpResponse::TAB_RCP_INFO_KEY_STATUS_CODE);
        $result = ((!is_null($result)) ? intval($result) : $result);

        // Return result
        return $result;
    }



    /**
     * Get string status message.
     *
     * @return null|string
     */
    public function getStrStatusMsg()
    {
        // Return result
        return $this->getRcpInfoValue(ConstHttpResponse::TAB_RCP_INFO_KEY_STATUS_MSG);
    }



    /**
     * Get headers array.
     *
     * @return array
     */
    public function getTabHeader()
    {
        // Return result
        return $this->getRcpInfoValue(
            ConstHttpResponse::TAB_RCP_INFO_KEY_HEADER,
            array()
        );
    }



    /**
     * Get string body.
     *
     * @return null|string
     */
    public function getStrBody()
    {
        // Return result
        return $this->getRcpInfoValue(ConstHttpResponse::TAB_RCP_INFO_KEY_BODY);
    }





    // Methods setters
    // ******************************************************************************

    /**
     * Set header data object.
     *
     * @param null|HeaderData $objHeaderData
     */
    public function setObjHeaderData($objHeaderData)
    {
        // Init var
        $objHeaderDataBase = $this->getObjHeaderData();

        // Set header data
        $this->beanPut(ConstHttpResponse::DATA_KEY_DEFAULT_HEADER_DATA, $objHeaderData);

        // Init reception information headers, if required
        $tabDataSrc = (
            (!is_null($objHeaderData)) ?
                $objHeaderData->getDataSrc() :
                (
                    (!is_null($objHeaderDataBase)) ?
                        $objHeaderDataBase->getDataSrc() :
                        null
                )
        );
        if(!is_null($tabDataSrc))
        {
            $this->setHeader($tabDataSrc);
        }
    }



    /**
     * @inheritdoc
     */
    public function setRcpInfo(array $tabInfo)
    {
        // Call parent method
        parent::setRcpInfo($tabInfo);

        // Init header data, if required
        $objHeaderData = $this->getObjHeaderData();
        if(
            (!is_null($objHeaderData)) &&
            isset($tabInfo[ConstHttpResponse::TAB_RCP_INFO_KEY_HEADER])
        ) {
            $objHeaderData->setDataSrc($tabInfo[ConstHttpResponse::TAB_RCP_INFO_KEY_HEADER]);
        }
    }



    /**
     * Set specified reception information value.
     *
     * @param string $strKey
     * @param mixed $value
     * @return $this
     */
    protected function setRcpInfoValue($strKey, $value)
    {
        // Init var
        $tabInfo = $this->beanGet(ConstResponse::DATA_KEY_DEFAULT_RCP_INFO);

        // Set value
        $tabInfo[$strKey] = $value;

        // Set reception information
        $this->setRcpInfo($tabInfo);

        // Return result
        return $this;
    }



    /**
     * Set specified protocol version number.
     *
     * @param null|string $strVersionNum
     * @return $this
     */
    public function setProtocolVersion($strVersionNum)
    {
        // Set protocol version number and return result
        return $this->setRcpInfoValue(
            ConstHttpResponse::TAB_RCP_INFO_KEY_PROTOCOL_VERSION_NUM,
            $strVersionNum
        );
    }



    /**
     * Set specified status code.
     *
     * @param null|int $intCode
     * @return $this
     */
    public function setStatusCode($intCode)
    {
        // Set status code and return result
        return $this->setRcpInfoValue(
            ConstHttpResponse::TAB_RCP_INFO_KEY_STATUS_CODE,
            $intCode
        );
    }



    /**
     * Set specified status message.
     *
     * @param null|string $strMsg
     * @return $this
     */
    public function setStatusMsg($strMsg)
    {
        // Set status message and return result
        return $this->setRcpInfoValue(
            ConstHttpResponse::TAB_RCP_INFO_KEY_STATUS_MSG,
            $strMsg
        );
    }



    /**
     * Set specified headers array.
     *
     * @param array $tabHeader
     * @return $this
     */
    public function setHeader(array $tabHeader)
    {
        // Set headers and return result
        return $this->setRcpInfoValue(
            ConstHttpResponse::TAB_RCP_INFO_KEY_HEADER,
            $tabHeader
        );
    }



    /**
     * Set specified body.
     *
     * @param null|string $strBody
     * @return $this
     */
    public function setBody($strBody)
    {
        // Set body and return result
        return $this->setRcpInfoValue(
            ConstHttpResponse::TAB_RCP_INFO_KEY_BODY,
            $strBody
        );
    }



}