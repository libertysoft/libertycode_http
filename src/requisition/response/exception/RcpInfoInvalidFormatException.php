<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\requisition\response\exception;

use Exception;

use liberty_code\http\header\exception\DataSrcInvalidFormatException;
use liberty_code\http\requisition\response\library\ConstHttpResponse;



class RcpInfoInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $info
     */
	public function __construct($info)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstHttpResponse::EXCEPT_MSG_RCP_INFO_INVALID_FORMAT,
            mb_strimwidth(strval($info), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified info has valid format.
     *
     * @param mixed $info
     * @return boolean
     */
    protected static function checkInfoIsValid($info)
    {
        // Init var
        $result =
            // Check valid protocol version number
            (
                (!isset($info[ConstHttpResponse::TAB_RCP_INFO_KEY_PROTOCOL_VERSION_NUM])) ||
                (
                    is_string($info[ConstHttpResponse::TAB_RCP_INFO_KEY_PROTOCOL_VERSION_NUM]) &&
                    (trim($info[ConstHttpResponse::TAB_RCP_INFO_KEY_PROTOCOL_VERSION_NUM]) != '')
                )
            ) &&

            // Check valid status code
            (
                (!isset($info[ConstHttpResponse::TAB_RCP_INFO_KEY_STATUS_CODE])) ||
                (
                    (
                        (
                            is_string($info[ConstHttpResponse::TAB_RCP_INFO_KEY_STATUS_CODE]) &&
                            ctype_digit($info[ConstHttpResponse::TAB_RCP_INFO_KEY_STATUS_CODE])
                        ) ||
                        is_int($info[ConstHttpResponse::TAB_RCP_INFO_KEY_STATUS_CODE])
                    ) &&
                    (intval($info[ConstHttpResponse::TAB_RCP_INFO_KEY_STATUS_CODE]) > 0)
                )
            ) &&

            // Check valid status message
            (
                (!isset($info[ConstHttpResponse::TAB_RCP_INFO_KEY_STATUS_MSG])) ||
                (
                    is_string($info[ConstHttpResponse::TAB_RCP_INFO_KEY_STATUS_MSG]) &&
                    (trim($info[ConstHttpResponse::TAB_RCP_INFO_KEY_STATUS_MSG]) != '')
                )
            ) &&

            // Check valid headers
            (
                (!isset($info[ConstHttpResponse::TAB_RCP_INFO_KEY_HEADER])) ||
                DataSrcInvalidFormatException::checkDataSrcIsValid($info[ConstHttpResponse::TAB_RCP_INFO_KEY_HEADER])
            ) &&

            // Check valid body
            (
                (!isset($info[ConstHttpResponse::TAB_RCP_INFO_KEY_BODY])) ||
                is_string($info[ConstHttpResponse::TAB_RCP_INFO_KEY_BODY])
            );

        // Return result
        return $result;
    }



	/**
	 * Check if specified info has valid format.
	 * 
     * @param mixed $info
	 * @return boolean
	 * @throws static
     */
    public static function setCheck($info)
    {
		// Init var
		$result =
            // Check valid array
            is_array($info) &&

            // Check valid config
            static::checkInfoIsValid($info);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($info) ? serialize($info) : $info));
		}
		
		// Return result
		return $result;
    }
	
	
	
}