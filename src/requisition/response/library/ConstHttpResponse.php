<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\requisition\response\library;



class ConstHttpResponse
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_HEADER_DATA = 'objHeaderData';



    // Reception information configuration
    const TAB_RCP_INFO_KEY_PROTOCOL_VERSION_NUM = 'protocol_version_num';
    const TAB_RCP_INFO_KEY_STATUS_CODE = 'status_code';
    const TAB_RCP_INFO_KEY_STATUS_MSG = 'status_msg';
    const TAB_RCP_INFO_KEY_HEADER = 'header';
    const TAB_RCP_INFO_KEY_BODY = 'body';


	
    // Exception message constants
    const EXCEPT_MSG_HEADER_DATA_INVALID_FORMAT = 'Following header data "%1$s" invalid! It must be a header data object.';
    const EXCEPT_MSG_RCP_INFO_INVALID_FORMAT =
        'Following info "%1$s" invalid! 
        The info must be an array and following the HTTP response reception information standard.';



}