<?php
/**
 * Description :
 * This class allows to define data HTTP response class.
 * Data HTTP response is HTTP response,
 * allowing to handle data, on received information.
 *
 * Data HTTP response uses the following specified configuration:
 * [
 *     HTTP response configuration,
 *
 *     body_parser_config(required): [
 *         @see ParserFactoryInterface configuration array format
 *     ]
 * ]
 *
 * Data HTTP response handles the following specified reception information:
 * [
 *     HTTP response reception information
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\requisition\response\data\model;

use liberty_code\http\requisition\response\model\HttpResponse;

use liberty_code\parser\parser\factory\api\ParserFactoryInterface;
use liberty_code\data\data\table\model\TableData;
use liberty_code\requisition\response\library\ConstResponse;
use liberty_code\http\header\model\HeaderData;
use liberty_code\http\requisition\response\library\ConstHttpResponse;
use liberty_code\http\requisition\response\data\library\ConstDataHttpResponse;
use liberty_code\http\requisition\response\data\exception\ParserFactoryInvalidFormatException;
use liberty_code\http\requisition\response\data\exception\BodyDataInvalidFormatException;
use liberty_code\http\requisition\response\data\exception\ConfigInvalidFormatException;



/**
 * @method ParserFactoryInterface getObjParserFactory() Get parser factory object.
 * @method void setObjParserFactory(ParserFactoryInterface $objParserFactory) Set parser factory object.
 * @method null|TableData getObjBodyData() Get body data object.
 */
class DataHttpResponse extends HttpResponse
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();


	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param ParserFactoryInterface $objParserFactory
     * @param TableData $objBodyData = null
     */
    public function __construct(
        ParserFactoryInterface $objParserFactory,
        HeaderData $objHeaderData = null,
        TableData $objBodyData = null,
        array $tabConfig = null,
        array $tabRcpInfo = null
    )
    {
        // Call parent constructor
        parent::__construct(
            $objHeaderData,
            $tabConfig
        );

        // Init parser factory
        $this->setObjParserFactory($objParserFactory);

        // Init body data, if required
        if(!is_null($objBodyData))
        {
            $this->setObjBodyData($objBodyData);
        }

        // Init reception information, if required
        if(!is_null($tabRcpInfo))
        {
            $this->setRcpInfo($tabRcpInfo);
        }
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstDataHttpResponse::DATA_KEY_DEFAULT_PARSER_FACTORY))
        {
            $this->__beanTabData[ConstDataHttpResponse::DATA_KEY_DEFAULT_PARSER_FACTORY] = null;
        }

        if(!$this->beanExists(ConstDataHttpResponse::DATA_KEY_DEFAULT_BODY_DATA))
        {
            $this->__beanTabData[ConstDataHttpResponse::DATA_KEY_DEFAULT_BODY_DATA] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstDataHttpResponse::DATA_KEY_DEFAULT_PARSER_FACTORY,
            ConstDataHttpResponse::DATA_KEY_DEFAULT_BODY_DATA
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstDataHttpResponse::DATA_KEY_DEFAULT_PARSER_FACTORY:
                    ParserFactoryInvalidFormatException::setCheck($value);
                    break;

                case ConstDataHttpResponse::DATA_KEY_DEFAULT_BODY_DATA:
                    BodyDataInvalidFormatException::setCheck($value);
                    break;

                case ConstResponse::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get body parser configuration array.
     *
     * @return array
     */
    protected function getTabBodyParserConfig()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = $tabConfig[ConstDataHttpResponse::TAB_CONFIG_KEY_BODY_PARSER_CONFIG];

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getTabRcpInfo()
    {
        // Init var
        $result = parent::getTabRcpInfo();

        // Register body data, if required
        $objBodyData = $this->getObjBodyData();
        if(!is_null($objBodyData))
        {
            $tabBodyData = $objBodyData->getDataSrc();
            if(
                (count($tabBodyData) > 0) ||
                isset($result[ConstHttpResponse::TAB_RCP_INFO_KEY_BODY])
            )
            {
                $result[ConstHttpResponse::TAB_RCP_INFO_KEY_BODY] = $this->getStrBodyFromData($tabBodyData);
            }
        }

        // Return result
        return $result;
    }



    /**
     * Get string body,
     * from specified body data array.
     *
     * @param null|array $tabData
     * @return string
     * @throws ConfigInvalidFormatException
     */
    protected function getStrBodyFromData($tabData)
    {
        // Init var
        $tabData = (is_array($tabData) ? $tabData : null);
        $result = null;

        // Get source, if required
        if(!is_null($tabData))
        {
            $objParserFactory = $this->getObjParserFactory();
            $tabParserConfig = $this->getTabBodyParserConfig();
            $objParser = $objParserFactory->getObjParser($tabParserConfig);
            $result = (
                (
                    (!is_null($objParser)) &&
                    is_string($body = $objParser->getSource($tabData))
                ) ?
                    $body :
                    null
            );

            // Check result
            if(is_null($result))
            {
                throw new ConfigInvalidFormatException(serialize($this->getTabConfig()));
            }
        }

        // Return result
        return $result;
    }



    /**
     * Get body data array,
     * from specified string body.
     *
     * @param null|string $strBody
     * @return array
     * @throws ConfigInvalidFormatException
     */
    protected function getTabDataFromBody($strBody)
    {
        // Init var
        $strBody = (is_string($strBody) ? $strBody : null);
        $result = null;

        // Get parsed data, if required
        if(!is_null($strBody))
        {
            $objParserFactory = $this->getObjParserFactory();
            $tabParserConfig = $this->getTabBodyParserConfig();
            $objParser = $objParserFactory->getObjParser($tabParserConfig);
            $result = (
                (
                    (!is_null($objParser)) &&
                    is_array($data = $objParser->getData($strBody))
                ) ?
                    $data :
                    null
            );

            // Check result
            if(is_null($result))
            {
                throw new ConfigInvalidFormatException(serialize($this->getTabConfig()));
            }
        }

        // Return result
        return $result;
    }



    /**
     * Get body data array.
     *
     * @return null|array
     * @throws ConfigInvalidFormatException
     */
    public function getTabBodyData()
    {
        // Init var
        $strBody = $this->getStrBody();
        $result = $this->getTabDataFromBody($strBody);

        // Return result
        return $result;
    }





    // Methods setters
    // ******************************************************************************

    /**
     * Set body data object.
     *
     * @param null|TableData $objBodyData
     */
    public function setObjBodyData($objBodyData)
    {
        // Init var
        $objBodyDataBase = $this->getObjBodyData();

        // Set header data
        $this->beanPut(ConstDataHttpResponse::DATA_KEY_DEFAULT_BODY_DATA, $objBodyData);

        // Init reception information headers, if required
        $tabDataSrc = (
            (!is_null($objBodyData)) ?
                $objBodyData->getDataSrc() :
                (
                    (!is_null($objBodyDataBase)) ?
                        $objBodyDataBase->getDataSrc() :
                        null
                )
        );
        if(!is_null($tabDataSrc))
        {
            $this->setBodyData($tabDataSrc);
        }
    }



    /**
     * @inheritdoc
     */
    public function setRcpInfo(array $tabInfo)
    {
        // Call parent method
        parent::setRcpInfo($tabInfo);

        // Init body data, if required
        $objBodyData = $this->getObjBodyData();
        if(
            (!is_null($objBodyData)) &&
            array_key_exists(ConstHttpResponse::TAB_RCP_INFO_KEY_BODY, $tabInfo)
        ) {
            $tabBodyData = (
                array_key_exists(ConstHttpResponse::TAB_RCP_INFO_KEY_BODY, $tabInfo) ?
                    (
                        (!is_null($tabInfo[ConstHttpResponse::TAB_RCP_INFO_KEY_BODY])) ?
                            $this->getTabDataFromBody($tabInfo[ConstHttpResponse::TAB_RCP_INFO_KEY_BODY]) :
                            array()
                    ) :
                    null
            );

            if(!is_null($tabBodyData))
            {
                $objBodyData->setDataSrc($tabBodyData);
            }
        }
    }



    /**
     * Set specified body data array.
     *
     * @param null|array $tabData
     * @return $this
     */
    public function setBodyData($tabData)
    {
        // Init var
        $strBody = $this->getStrBodyFromData($tabData);
        $result = $this->setBody($strBody);

        // Return result
        return $result;
    }



}