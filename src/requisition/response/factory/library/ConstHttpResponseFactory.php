<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\requisition\response\factory\library;



class ConstHttpResponseFactory
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_PARSER_FACTORY = 'objParserFactory';



    // Type configuration
    const CONFIG_TYPE_HTTP = 'http';
    const CONFIG_TYPE_HTTP_DATA = 'http_data';
    const CONFIG_TYPE_HTTP_PERSISTOR = 'http_persistor';



}