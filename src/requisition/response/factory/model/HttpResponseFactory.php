<?php
/**
 * Description :
 * This class allows to define HTTP response factory class.
 * HTTP response factory allows to provide and hydrate HTTP response instance.
 *
 * HTTP response factory uses the following specified configuration, to get and hydrate response:
 * [
 *     Default response factory configuration,
 *
 *     type(required): "http",
 *
 *     @see HttpResponse configuration array format,
 *
 *     rcp_info(optional): [
 *         @see HttpResponse reception information array format
 *     ]
 *
 *     OR
 *
 *     Default response factory configuration,
 *
 *     type(required): "http_data",
 *
 *     @see DataHttpResponse configuration array format,
 *
 *     rcp_info(optional): [
 *         @see DataHttpResponse reception information array format
 *     ]
 *
 *     OR
 *
 *     Default response factory configuration,
 *
 *     type(required): "http_persistor",
 *
 *     @see PersistorHttpResponse configuration array format,
 *
 *     rcp_info(optional): [
 *         @see PersistorHttpResponse reception information array format
 *     ]
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\requisition\response\factory\model;

use liberty_code\requisition\response\factory\model\DefaultResponseFactory;

use liberty_code\parser\parser\factory\api\ParserFactoryInterface;
use liberty_code\data\data\table\path\model\PathTableData;
use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\requisition\response\api\ResponseInterface;
use liberty_code\requisition\response\factory\api\ResponseFactoryInterface;
use liberty_code\http\header\model\HeaderData;
use liberty_code\http\requisition\response\model\HttpResponse;
use liberty_code\http\requisition\response\data\exception\ParserFactoryInvalidFormatException;
use liberty_code\http\requisition\response\data\model\DataHttpResponse;
use liberty_code\http\requisition\response\persistence\model\PersistorHttpResponse;
use liberty_code\http\requisition\response\factory\library\ConstHttpResponseFactory;



/**
 * @method ParserFactoryInterface getObjParserFactory() Get parser factory object.
 * @method void setObjParserFactory(ParserFactoryInterface $objParserFactory) Set parser factory object.
 */
class HttpResponseFactory extends DefaultResponseFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param ParserFactoryInterface $objParserFactory
     */
    public function __construct(
        ParserFactoryInterface $objParserFactory,
        ResponseFactoryInterface $objFactory = null,
        ProviderInterface $objProvider = null
    )
    {
        // Call parent constructor
        parent::__construct(
            $objFactory,
            $objProvider
        );

        // Init parser factory
        $this->setObjParserFactory($objParserFactory);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstHttpResponseFactory::DATA_KEY_DEFAULT_PARSER_FACTORY))
        {
            $this->__beanTabData[ConstHttpResponseFactory::DATA_KEY_DEFAULT_PARSER_FACTORY] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }



    /**
     * @inheritdoc
     */
    protected function hydrateResponse(ResponseInterface $objResponse, array $tabConfigFormat)
    {
        // Call parent method
        parent::hydrateResponse($objResponse, $tabConfigFormat);

        // Hydrate HTTP response, if required
        if($objResponse instanceof HttpResponse)
        {
            // Init header data object
            $objHeaderData = new HeaderData($objResponse->getTabHeader());
            $objResponse->setObjHeaderData($objHeaderData);

            // Hydrate data HTTP response, if required
            if(
                ($objResponse instanceof DataHttpResponse) &&
                (!is_null($objResponse->getStrBody()))
            )
            {
                // Init body data object
                $objBodyData = new PathTableData(null, $objResponse->getTabBodyData());
                $objResponse->setObjBodyData($objBodyData);
            }
        }
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstHttpResponseFactory::DATA_KEY_DEFAULT_PARSER_FACTORY
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstHttpResponseFactory::DATA_KEY_DEFAULT_PARSER_FACTORY:
                    ParserFactoryInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





	// Methods getters
	// ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrResponseClassPathFromType($strConfigType)
    {
        // Init var
        $result = null;

        // Get class path of response, from type
        switch($strConfigType)
        {
            case ConstHttpResponseFactory::CONFIG_TYPE_HTTP:
                $result = HttpResponse::class;
                break;

            case ConstHttpResponseFactory::CONFIG_TYPE_HTTP_DATA:
                $result = DataHttpResponse::class;
                break;

            case ConstHttpResponseFactory::CONFIG_TYPE_HTTP_PERSISTOR:
                $result = PersistorHttpResponse::class;
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getObjResponseNew($strConfigType)
    {
        // Init var
        $result = null;
        $objParserFactory = $this->getObjParserFactory();

        // Get response, from type
        switch($strConfigType)
        {
            case ConstHttpResponseFactory::CONFIG_TYPE_HTTP:
                $result = new HttpResponse();
                break;

            case ConstHttpResponseFactory::CONFIG_TYPE_HTTP_DATA:
                $result = new DataHttpResponse($objParserFactory);
                break;

            case ConstHttpResponseFactory::CONFIG_TYPE_HTTP_PERSISTOR:
                $result = new PersistorHttpResponse($objParserFactory);
                break;
        }

        // Return result
        return $result;
    }



}