<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\requisition\response\persistence\library;



class ConstPersistorHttpResponse
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration
    const TAB_CONFIG_KEY_PATH_SEPARATOR = 'path_separator';
    const TAB_CONFIG_KEY_DATA_ID_KEY = 'data_id_key';
    const TAB_CONFIG_KEY_SUCCESS_STATUS_CODE = 'success_status_code';
    const TAB_CONFIG_KEY_DATA_SELECT_PATH = 'data_select_path';
    const TAB_CONFIG_KEY_DATA_INCLUDE_KEY = 'data_include_key';
    const TAB_CONFIG_KEY_DATA_EXCLUDE_KEY = 'data_exclude_key';
    const TAB_CONFIG_KEY_MULTI_DATA_SELECT_PATH = 'multi_data_select_path';
    const TAB_CONFIG_KEY_MULTI_DATA_INCLUDE_KEY = 'multi_data_include_key';
    const TAB_CONFIG_KEY_MULTI_DATA_EXCLUDE_KEY = 'multi_data_exclude_key';
    const TAB_CONFIG_KEY_SEARCH_DATA_SELECT_PATH = 'search_data_select_path';
    const TAB_CONFIG_KEY_SEARCH_DATA_INCLUDE_KEY = 'search_data_include_key';
    const TAB_CONFIG_KEY_SEARCH_DATA_EXCLUDE_KEY = 'search_data_exclude_key';
    const TAB_CONFIG_KEY_CREATE_DATA_RETURN_STATUS = 'create_data_return_status';
    const TAB_CONFIG_KEY_CREATE_DATA_SELECT_PATH = 'create_data_select_path';
    const TAB_CONFIG_KEY_CREATE_DATA_INCLUDE_KEY = 'create_data_include_key';
    const TAB_CONFIG_KEY_CREATE_DATA_EXCLUDE_KEY = 'create_data_exclude_key';
    const TAB_CONFIG_KEY_CREATE_MULTI_DATA_RETURN_STATUS = 'create_multi_data_return_status';
    const TAB_CONFIG_KEY_CREATE_MULTI_DATA_SELECT_PATH = 'create_multi_data_select_path';
    const TAB_CONFIG_KEY_CREATE_MULTI_DATA_INCLUDE_KEY = 'create_multi_data_include_key';
    const TAB_CONFIG_KEY_CREATE_MULTI_DATA_EXCLUDE_KEY = 'create_multi_data_exclude_key';
    const TAB_CONFIG_KEY_UPDATE_DATA_RETURN_STATUS = 'update_data_return_status';
    const TAB_CONFIG_KEY_UPDATE_DATA_SELECT_PATH = 'update_data_select_path';
    const TAB_CONFIG_KEY_UPDATE_DATA_INCLUDE_KEY = 'update_data_include_key';
    const TAB_CONFIG_KEY_UPDATE_DATA_EXCLUDE_KEY = 'update_data_exclude_key';
    const TAB_CONFIG_KEY_UPDATE_MULTI_DATA_RETURN_STATUS = 'update_multi_data_return_status';
    const TAB_CONFIG_KEY_UPDATE_MULTI_DATA_SELECT_PATH = 'update_multi_data_select_path';
    const TAB_CONFIG_KEY_UPDATE_MULTI_DATA_INCLUDE_KEY = 'update_multi_data_include_key';
    const TAB_CONFIG_KEY_UPDATE_MULTI_DATA_EXCLUDE_KEY = 'update_multi_data_exclude_key';
    const TAB_CONFIG_KEY_DELETE_DATA_RETURN_STATUS = 'delete_data_return_status';
    const TAB_CONFIG_KEY_DELETE_DATA_SELECT_PATH = 'delete_data_select_path';
    const TAB_CONFIG_KEY_DELETE_DATA_INCLUDE_KEY = 'delete_data_include_key';
    const TAB_CONFIG_KEY_DELETE_DATA_EXCLUDE_KEY = 'delete_data_exclude_key';
    const TAB_CONFIG_KEY_DELETE_MULTI_DATA_RETURN_STATUS = 'delete_multi_data_return_status';
    const TAB_CONFIG_KEY_DELETE_MULTI_DATA_SELECT_PATH = 'delete_multi_data_select_path';
    const TAB_CONFIG_KEY_DELETE_MULTI_DATA_INCLUDE_KEY = 'delete_multi_data_include_key';
    const TAB_CONFIG_KEY_DELETE_MULTI_DATA_EXCLUDE_KEY = 'delete_multi_data_exclude_key';

    // Data return status configuration
    const CONFIG_DATA_RETURN_STATUS_REQUIRED = 'required';
    const CONFIG_DATA_RETURN_STATUS_OPTIONAL = 'optional';
    const CONFIG_DATA_RETURN_STATUS_NONE = 'none';



    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the persistor HTTP response configuration standard.';
    const EXCEPT_MSG_DATA_REQUIRED = 'Data required!';
    const EXCEPT_MSG_TAB_DATA_INVALID_FORMAT =
        'Following array of data "%1$s" invalid! 
        It must be an array of data, where each data must be an array.';





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get configuration array of data return status.
     *
     * @return array
     */
    public static function getTabConfigDataReturnStatus()
    {
        // Init var
        $result = array(
            self::CONFIG_DATA_RETURN_STATUS_REQUIRED,
            self::CONFIG_DATA_RETURN_STATUS_OPTIONAL,
            self::CONFIG_DATA_RETURN_STATUS_NONE
        );

        // Return result
        return $result;
    }



}