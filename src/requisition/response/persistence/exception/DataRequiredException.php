<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\requisition\response\persistence\exception;

use Exception;

use liberty_code\http\requisition\response\persistence\library\ConstPersistorHttpResponse;



class DataRequiredException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     */
	public function __construct()
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = ConstPersistorHttpResponse::EXCEPT_MSG_DATA_REQUIRED;
	}
	
	
	
}