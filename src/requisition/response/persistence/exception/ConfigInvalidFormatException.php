<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\requisition\response\persistence\exception;

use Exception;

use liberty_code\http\requisition\response\persistence\library\ConstPersistorHttpResponse;



class ConfigInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstPersistorHttpResponse::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init string index array check function
        $checkTabStrIsValid = function(array $tabStr)
        {
            $result = true;
            $tabStr = array_values($tabStr);

            // Check each value is valid
            for($intCpt = 0; ($intCpt < count($tabStr)) && $result; $intCpt++)
            {
                $strValue = $tabStr[$intCpt];
                $result = (
                    // Check valid string
                    is_string($strValue) &&
                    (trim($strValue) != '')
                );
            }

            return $result;
        };

        // Init integer index array check function
        $checkTabIntIsValid = function(array $tabInt)
        {
            $result = true;
            $tabInt = array_values($tabInt);

            // Check each value is valid
            for($intCpt = 0; ($intCpt < count($tabInt)) && $result; $intCpt++)
            {
                $intValue = $tabInt[$intCpt];
                $result = (
                    // Check valid integer
                    (
                        (is_string($intValue) && ctype_digit($intValue)) ||
                        is_int($intValue)
                    ) &&
                    (intval($intValue) > 0)
                );
            }

            return $result;
        };

        // Init var
        $result =
            // Check valid path separator
            (
                (!isset($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_PATH_SEPARATOR])) ||
                (
                    is_string($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_PATH_SEPARATOR]) &&
                    (trim($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_PATH_SEPARATOR]) != '')
                )
            ) &&

            // Check valid data id key
            (
                (!isset($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_DATA_ID_KEY])) ||
                (
                    is_string($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_DATA_ID_KEY]) &&
                    (trim($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_DATA_ID_KEY]) != '')
                )
            ) &&

            // Check valid success status code
            (
                (!isset($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_SUCCESS_STATUS_CODE])) ||
                // Check valid integer
                (
                    (
                        (
                            is_string($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_SUCCESS_STATUS_CODE]) &&
                            ctype_digit($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_SUCCESS_STATUS_CODE])
                        ) ||
                        is_int($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_SUCCESS_STATUS_CODE])
                    ) &&
                    (intval($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_SUCCESS_STATUS_CODE]) > 0)
                ) ||
                // Check valid array of integers
                (
                    is_array($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_SUCCESS_STATUS_CODE]) &&
                    $checkTabIntIsValid($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_SUCCESS_STATUS_CODE])
                ) ||
                // Check valid REGEXP
                (
                    is_string($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_SUCCESS_STATUS_CODE]) &&
                    (trim($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_SUCCESS_STATUS_CODE]) != '')
                )
            ) &&

            // Check valid path, to select data
            (
                (!isset($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_DATA_SELECT_PATH])) ||
                (
                    is_string($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_DATA_SELECT_PATH]) &&
                    (trim($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_DATA_SELECT_PATH]) != '')
                )
            ) &&

            // Check valid include keys, to filter data
            (
                (!isset($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_DATA_INCLUDE_KEY])) ||
                (
                    is_array($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_DATA_INCLUDE_KEY]) &&
                    $checkTabStrIsValid($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_DATA_INCLUDE_KEY])
                )
            ) &&

            // Check valid exclude keys, to filter data
            (
                (!isset($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_DATA_EXCLUDE_KEY])) ||
                (
                    is_array($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_DATA_EXCLUDE_KEY]) &&
                    $checkTabStrIsValid($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_DATA_EXCLUDE_KEY])
                )
            ) &&

            // Check valid path, to select array of data
            (
                (!isset($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_MULTI_DATA_SELECT_PATH])) ||
                (
                    is_string($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_MULTI_DATA_SELECT_PATH]) &&
                    (trim($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_MULTI_DATA_SELECT_PATH]) != '')
                )
            ) &&

            // Check valid include keys, to filter array of data
            (
                (!isset($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_MULTI_DATA_INCLUDE_KEY])) ||
                (
                    is_array($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_MULTI_DATA_INCLUDE_KEY]) &&
                    $checkTabStrIsValid($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_MULTI_DATA_INCLUDE_KEY])
                )
            ) &&

            // Check valid exclude keys, to filter array of data
            (
                (!isset($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_MULTI_DATA_EXCLUDE_KEY])) ||
                (
                    is_array($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_MULTI_DATA_EXCLUDE_KEY]) &&
                    $checkTabStrIsValid($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_MULTI_DATA_EXCLUDE_KEY])
                )
            ) &&

            // Check valid path, to select array of searched data
            (
                (!isset($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_SEARCH_DATA_SELECT_PATH])) ||
                (
                    is_string($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_SEARCH_DATA_SELECT_PATH]) &&
                    (trim($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_SEARCH_DATA_SELECT_PATH]) != '')
                )
            ) &&

            // Check valid include keys, to filter array of searched data
            (
                (!isset($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_SEARCH_DATA_INCLUDE_KEY])) ||
                (
                    is_array($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_SEARCH_DATA_INCLUDE_KEY]) &&
                    $checkTabStrIsValid($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_SEARCH_DATA_INCLUDE_KEY])
                )
            ) &&

            // Check valid exclude keys, to filter array of searched data
            (
                (!isset($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_SEARCH_DATA_EXCLUDE_KEY])) ||
                (
                    is_array($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_SEARCH_DATA_EXCLUDE_KEY]) &&
                    $checkTabStrIsValid($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_SEARCH_DATA_EXCLUDE_KEY])
                )
            ) &&

            // Check valid data return status, for created data
            (
                (!isset($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_CREATE_DATA_RETURN_STATUS])) ||
                (
                    is_string($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_CREATE_DATA_RETURN_STATUS]) &&
                    in_array(
                        $config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_CREATE_DATA_RETURN_STATUS],
                        ConstPersistorHttpResponse::getTabConfigDataReturnStatus()
                    )
                )
            ) &&

            // Check valid path, to select created data
            (
                (!isset($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_CREATE_DATA_SELECT_PATH])) ||
                (
                    is_string($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_CREATE_DATA_SELECT_PATH]) &&
                    (trim($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_CREATE_DATA_SELECT_PATH]) != '')
                )
            ) &&

            // Check valid include keys, to filter created data
            (
                (!isset($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_CREATE_DATA_INCLUDE_KEY])) ||
                (
                    is_array($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_CREATE_DATA_INCLUDE_KEY]) &&
                    $checkTabStrIsValid($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_CREATE_DATA_INCLUDE_KEY])
                )
            ) &&

            // Check valid exclude keys, to filter created data
            (
                (!isset($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_CREATE_DATA_EXCLUDE_KEY])) ||
                (
                    is_array($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_CREATE_DATA_EXCLUDE_KEY]) &&
                    $checkTabStrIsValid($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_CREATE_DATA_EXCLUDE_KEY])
                )
            ) &&

            // Check valid data return status, for array of created data
            (
                (!isset($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_CREATE_MULTI_DATA_RETURN_STATUS])) ||
                (
                    is_string($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_CREATE_MULTI_DATA_RETURN_STATUS]) &&
                    in_array(
                        $config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_CREATE_MULTI_DATA_RETURN_STATUS],
                        ConstPersistorHttpResponse::getTabConfigDataReturnStatus()
                    )
                )
            ) &&

            // Check valid path, to select array of created data
            (
                (!isset($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_CREATE_MULTI_DATA_SELECT_PATH])) ||
                (
                    is_string($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_CREATE_MULTI_DATA_SELECT_PATH]) &&
                    (trim($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_CREATE_MULTI_DATA_SELECT_PATH]) != '')
                )
            ) &&

            // Check valid include keys, to filter array of created data
            (
                (!isset($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_CREATE_MULTI_DATA_INCLUDE_KEY])) ||
                (
                    is_array($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_CREATE_MULTI_DATA_INCLUDE_KEY]) &&
                    $checkTabStrIsValid($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_CREATE_MULTI_DATA_INCLUDE_KEY])
                )
            ) &&

            // Check valid exclude keys, to filter array of created data
            (
                (!isset($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_CREATE_MULTI_DATA_EXCLUDE_KEY])) ||
                (
                    is_array($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_CREATE_MULTI_DATA_EXCLUDE_KEY]) &&
                    $checkTabStrIsValid($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_CREATE_MULTI_DATA_EXCLUDE_KEY])
                )
            ) &&

            // Check valid data return status, for updated data
            (
                (!isset($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_UPDATE_DATA_RETURN_STATUS])) ||
                (
                    is_string($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_UPDATE_DATA_RETURN_STATUS]) &&
                    in_array(
                        $config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_UPDATE_DATA_RETURN_STATUS],
                        ConstPersistorHttpResponse::getTabConfigDataReturnStatus()
                    )
                )
            ) &&

            // Check valid path, to select updated data
            (
                (!isset($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_UPDATE_DATA_SELECT_PATH])) ||
                (
                    is_string($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_UPDATE_DATA_SELECT_PATH]) &&
                    (trim($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_UPDATE_DATA_SELECT_PATH]) != '')
                )
            ) &&

            // Check valid include keys, to filter updated data
            (
                (!isset($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_UPDATE_DATA_INCLUDE_KEY])) ||
                (
                    is_array($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_UPDATE_DATA_INCLUDE_KEY]) &&
                    $checkTabStrIsValid($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_UPDATE_DATA_INCLUDE_KEY])
                )
            ) &&

            // Check valid exclude keys, to filter updated data
            (
                (!isset($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_UPDATE_DATA_EXCLUDE_KEY])) ||
                (
                    is_array($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_UPDATE_DATA_EXCLUDE_KEY]) &&
                    $checkTabStrIsValid($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_UPDATE_DATA_EXCLUDE_KEY])
                )
            ) &&

            // Check valid data return status, for array of updated data
            (
                (!isset($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_UPDATE_MULTI_DATA_RETURN_STATUS])) ||
                (
                    is_string($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_UPDATE_MULTI_DATA_RETURN_STATUS]) &&
                    in_array(
                        $config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_UPDATE_MULTI_DATA_RETURN_STATUS],
                        ConstPersistorHttpResponse::getTabConfigDataReturnStatus()
                    )
                )
            ) &&

            // Check valid path, to select array of updated data
            (
                (!isset($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_UPDATE_MULTI_DATA_SELECT_PATH])) ||
                (
                    is_string($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_UPDATE_MULTI_DATA_SELECT_PATH]) &&
                    (trim($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_UPDATE_MULTI_DATA_SELECT_PATH]) != '')
                )
            ) &&

            // Check valid include keys, to filter array of updated data
            (
                (!isset($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_UPDATE_MULTI_DATA_INCLUDE_KEY])) ||
                (
                    is_array($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_UPDATE_MULTI_DATA_INCLUDE_KEY]) &&
                    $checkTabStrIsValid($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_UPDATE_MULTI_DATA_INCLUDE_KEY])
                )
            ) &&

            // Check valid exclude keys, to filter array of updated data
            (
                (!isset($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_UPDATE_MULTI_DATA_EXCLUDE_KEY])) ||
                (
                    is_array($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_UPDATE_MULTI_DATA_EXCLUDE_KEY]) &&
                    $checkTabStrIsValid($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_UPDATE_MULTI_DATA_EXCLUDE_KEY])
                )
            ) &&

            // Check valid data return status, for deleted data
            (
                (!isset($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_DELETE_DATA_RETURN_STATUS])) ||
                (
                    is_string($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_DELETE_DATA_RETURN_STATUS]) &&
                    in_array(
                        $config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_DELETE_DATA_RETURN_STATUS],
                        ConstPersistorHttpResponse::getTabConfigDataReturnStatus()
                    )
                )
            ) &&

            // Check valid path, to select deleted data
            (
                (!isset($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_DELETE_DATA_SELECT_PATH])) ||
                (
                    is_string($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_DELETE_DATA_SELECT_PATH]) &&
                    (trim($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_DELETE_DATA_SELECT_PATH]) != '')
                )
            ) &&

            // Check valid include keys, to filter deleted data
            (
                (!isset($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_DELETE_DATA_INCLUDE_KEY])) ||
                (
                    is_array($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_DELETE_DATA_INCLUDE_KEY]) &&
                    $checkTabStrIsValid($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_DELETE_DATA_INCLUDE_KEY])
                )
            ) &&

            // Check valid exclude keys, to filter deleted data
            (
                (!isset($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_DELETE_DATA_EXCLUDE_KEY])) ||
                (
                    is_array($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_DELETE_DATA_EXCLUDE_KEY]) &&
                    $checkTabStrIsValid($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_DELETE_DATA_EXCLUDE_KEY])
                )
            ) &&

            // Check valid data return status, for array of deleted data
            (
                (!isset($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_DELETE_MULTI_DATA_RETURN_STATUS])) ||
                (
                    is_string($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_DELETE_MULTI_DATA_RETURN_STATUS]) &&
                    in_array(
                        $config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_DELETE_MULTI_DATA_RETURN_STATUS],
                        ConstPersistorHttpResponse::getTabConfigDataReturnStatus()
                    )
                )
            ) &&

            // Check valid path, to select array of deleted data
            (
                (!isset($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_DELETE_MULTI_DATA_SELECT_PATH])) ||
                (
                    is_string($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_DELETE_MULTI_DATA_SELECT_PATH]) &&
                    (trim($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_DELETE_MULTI_DATA_SELECT_PATH]) != '')
                )
            ) &&

            // Check valid include keys, to filter array of deleted data
            (
                (!isset($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_DELETE_MULTI_DATA_INCLUDE_KEY])) ||
                (
                    is_array($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_DELETE_MULTI_DATA_INCLUDE_KEY]) &&
                    $checkTabStrIsValid($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_DELETE_MULTI_DATA_INCLUDE_KEY])
                )
            ) &&

            // Check valid exclude keys, to filter array of deleted data
            (
                (!isset($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_DELETE_MULTI_DATA_EXCLUDE_KEY])) ||
                (
                    is_array($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_DELETE_MULTI_DATA_EXCLUDE_KEY]) &&
                    $checkTabStrIsValid($config[ConstPersistorHttpResponse::TAB_CONFIG_KEY_DELETE_MULTI_DATA_EXCLUDE_KEY])
                )
            );

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format.
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
    public static function setCheck($config)
    {
		// Init var
		$result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}