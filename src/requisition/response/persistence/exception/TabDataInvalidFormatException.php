<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\requisition\response\persistence\exception;

use Exception;

use liberty_code\http\requisition\response\persistence\library\ConstPersistorHttpResponse;



class TabDataInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $data
     */
	public function __construct($data)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstPersistorHttpResponse::EXCEPT_MSG_TAB_DATA_INVALID_FORMAT,
            mb_strimwidth(strval($data), 0, 50, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified data has valid format.
	 * 
     * @param mixed $data
	 * @return boolean
	 * @throws static
     */
	public static function setCheck($data)
    {
        // Init var
        $tabData = $data;
        $result =
            // Check is valid array
            is_array($tabData);

        // Run each data, if required
        if($result)
        {
            $tabData = array_values($tabData);
            for($intCpt = 0; $result && ($intCpt < count($tabData)); $intCpt++)
            {
                $data = $tabData[$intCpt];

                // Check valid array
                $result =
                    is_array($data);
            }
        }

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($tabData) ? serialize($tabData) : $tabData));
		}
		
		// Return result
		return $result;
    }
	
	
	
}