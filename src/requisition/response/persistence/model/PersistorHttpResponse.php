<?php
/**
 * Description :
 * This class allows to define persistor HTTP response class.
 * Persistor HTTP response is data HTTP response,
 * allowing to design persistor response,
 * to get all received information, from storage support, via HTTP protocol.
 *
 * Persistor HTTP response uses the following specified configuration:
 * [
 *     Data HTTP response configuration,
 *
 *     path_separator(optional: got "/", if not found): "string path separator",
 *
 *     data_id_key(optional): "string id key, used on provided data",
 *
 *     success_status_code(optional: got "#^2\d+$#", if not found):
 *         strict positive integer code
 *         OR
 *         [strict positive integer code 1, ..., strict positive integer code N]
 *         OR
 *         "string REGEXP code pattern",
 *
 *     data_select_path (optional: got "", if not found):
 *         "string path, to select data",
 *
 *     data_include_key (optional: got null, if not found):
 *         Null
 *         OR
 *         [string key 1 (used on provided data), ..., string key N],
 *
 *     data_exclude_key (optional: got null, if not found):
 *         Null
 *         OR
 *         [string key 1 (used on provided data), ..., string key N],
 *
 *     multi_data_select_path (optional: got select_data_path, if not found):
 *         "string path, to select array of data",
 *
 *     multi_data_include_key (optional: got null, if not found):
 *         Null
 *         OR
 *         [string key 1 (used on provided data), ..., string key N],
 *
 *     multi_data_exclude_key (optional: got null, if not found):
 *         Null
 *         OR
 *         [string key 1 (used on provided data), ..., string key N],
 *
 *     search_data_select_path (optional: got "", if not found):
 *         "string path, to select array of searched data",
 *
 *     search_data_include_key (optional: got null, if not found):
 *         Null
 *         OR
 *         [string key 1 (used on provided data), ..., string key N],
 *
 *     search_data_exclude_key (optional: got null, if not found):
 *         Null
 *         OR
 *         [string key 1 (used on provided data), ..., string key N],
 *
 *     create_data_return_status(optional: got "optional", if not found):
 *         "string data return status, to specify if it's required, to get created data.
 *         Scope of available values: @see ConstPersistorHttpResponse::getTabConfigDataReturnStatus()",
 *
 *     create_data_select_path (optional: got "" if not found):
 *         "string path, to select created data",
 *
 *     create_data_include_key (optional: got null, if not found):
 *         Null
 *         OR
 *         [string key 1 (used on provided data), ..., string key N],
 *
 *     create_data_exclude_key (optional: got null, if not found):
 *         Null
 *         OR
 *         [string key 1 (used on provided data), ..., string key N],
 *
 *     create_multi_data_return_status(optional: got create_data_return_status, if not found):
 *         "string data return status, to specify if it's required, to get array of created data.
 *         Scope of available values: @see ConstPersistorHttpResponse::getTabConfigDataReturnStatus()",
 *
 *     create_multi_data_select_path (optional: got create_select_data_path, if not found):
 *         "string path, to select array of created data",
 *
 *     create_multi_data_include_key (optional: got null, if not found):
 *         Null
 *         OR
 *         [string key 1 (used on provided data), ..., string key N],
 *
 *     create_multi_data_exclude_key (optional: got null, if not found):
 *         Null
 *         OR
 *         [string key 1 (used on provided data), ..., string key N],
 *
 *     update_data_return_status(optional: got "optional", if not found):
 *         "string data return status, to specify if it's required, to get updated data.
 *         Scope of available values: @see ConstPersistorHttpResponse::getTabConfigDataReturnStatus()",
 *
 *     update_data_select_path (optional: got "", if not found):
 *         "string path, to select updated data",
 *
 *     update_data_include_key (optional: got null, if not found):
 *         Null
 *         OR
 *         [string key 1 (used on provided data), ..., string key N],
 *
 *     update_data_exclude_key (optional: got null, if not found):
 *         Null
 *         OR
 *         [string key 1 (used on provided data), ..., string key N],
 *
 *     update_multi_data_return_status(optional: got update_data_return_status, if not found):
 *         "string data return status, to specify if it's required, to get array of updated data.
 *         Scope of available values: @see ConstPersistorHttpResponse::getTabConfigDataReturnStatus()",
 *
 *     update_multi_data_select_path (optional: got update_select_data_path, if not found):
 *         "string path, to select array of updated data",
 *
 *     update_multi_data_include_key (optional: got null, if not found):
 *         Null
 *         OR
 *         [string key 1 (used on provided data), ..., string key N],
 *
 *     update_multi_data_exclude_key (optional: got null, if not found):
 *         Null
 *         OR
 *         [string key 1 (used on provided data), ..., string key N],
 *
 *     delete_data_return_status(optional: got "optional", if not found):
 *         "string data return status, to specify if it's required, to get deleted data.
 *         Scope of available values: @see ConstPersistorHttpResponse::getTabConfigDataReturnStatus()",
 *
 *     delete_data_select_path (optional: got "", if not found):
 *         "string path, to select deleted data",
 *
 *     delete_data_include_key (optional: got null, if not found):
 *         Null
 *         OR
 *         [string key 1 (used on provided data), ..., string key N],
 *
 *     delete_data_exclude_key (optional: got null, if not found):
 *         Null
 *         OR
 *         [string key 1 (used on provided data), ..., string key N],
 *
 *     delete_multi_data_return_status(optional: got delete_data_return_status, if not found):
 *         "string data return status, to specify if it's required, to get array of deleted data.
 *         Scope of available values: @see ConstPersistorHttpResponse::getTabConfigDataReturnStatus()",
 *
 *     delete_multi_data_select_path (optional: got delete_select_data_path, if not found):
 *         "string path, to select array of deleted data",
 *
 *     delete_multi_data_include_key (optional: got null, if not found):
 *         Null
 *         OR
 *         [string key 1 (used on provided data), ..., string key N],
 *
 *     delete_multi_data_exclude_key (optional: got null, if not found):
 *         Null
 *         OR
 *         [string key 1 (used on provided data), ..., string key N]
 * ]
 *
 * Persistor HTTP response handles the following specified reception information:
 * [
 *     Data HTTP response reception information
 * ]
 *
 * Note:
 * -> Configuration data support type:
 *     All data support types are on body.
 * -> Configuration data return status:
 *     - "required":
 *         Get data from body. Error is thrown, if not found.
 *     - "optional":
 *         Try to get data from body. No data returned, if not found.
 *     - "none":
 *         No data returned.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\requisition\response\persistence\model;

use liberty_code\requisition\response\persistence\api\PersistorResponseInterface;
use liberty_code\http\requisition\response\data\model\DataHttpResponse;

use liberty_code\data\data\table\path\library\ToolBoxPathTableData;
use liberty_code\requisition\response\library\ConstResponse;
use liberty_code\http\requisition\response\persistence\library\ConstPersistorHttpResponse;
use liberty_code\http\requisition\response\persistence\exception\ConfigInvalidFormatException;
use liberty_code\http\requisition\response\persistence\exception\DataRequiredException;
use liberty_code\http\requisition\response\persistence\exception\TabDataInvalidFormatException;



class PersistorHttpResponse extends DataHttpResponse implements PersistorResponseInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();


	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        // $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstResponse::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check if response is succeeded.
     *
     * @return boolean
     */
    public function checkIsSucceeded()
    {
        // Init var
        $successCode = $this->getConfigValue(
            ConstPersistorHttpResponse::TAB_CONFIG_KEY_SUCCESS_STATUS_CODE,
            null,
            '#^2\d+$#'
        );
        $intStatusCode = $this->getIntStatusCode();
        $result = (
            is_null($intStatusCode) ||
            (
                // Case success code is integer: check status code equals to success code
                ((is_string($successCode) && ctype_digit($successCode)) || is_int($successCode)) ?
                    ($intStatusCode == intval($successCode)) :
                    (
                        // Case success code is array (of integers): check status code in success codes
                        is_array($successCode) ?
                            in_array(
                                $intStatusCode,
                                array_map(
                                    function($successCode) {return intval($successCode);},
                                    array_values($successCode)
                                )
                            ) :
                            // Case else (success code is REGEXP): check status code matches with success code REGEXP pattern
                            (preg_match($successCode, strval($intStatusCode)) === 1)
                    )
            )
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get specified configuration value.
     *
     * @param string $strConfigKey
     * @param null|string $strDefaultConfigKey = null
     * @param null|mixed $default = null
     * @return null|mixed
     * @throws ConfigInvalidFormatException
     */
    protected function getConfigValue(
        $strConfigKey,
        $strDefaultConfigKey = null,
        $default = null
    )
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            isset($tabConfig[$strConfigKey]) ?
                $tabConfig[$strConfigKey] :
                (
                    isset($tabConfig[$strDefaultConfigKey]) ?
                        $tabConfig[$strDefaultConfigKey] :
                        $default
                )
        );

        // Return result
        return $result;
    }



    /**
     * Get string path separator.
     *
     * @return string
     */
    protected function getStrPathSeparator()
    {
        // Return result
        return $this->getConfigValue(
            ConstPersistorHttpResponse::TAB_CONFIG_KEY_PATH_SEPARATOR,
            null,
            '/'
        );
    }



    /**
     * Get specified data id key.
     *
     * @return null|string
     */
    protected function getStrDataIdKey()
    {
        // Return result
        return $this->getConfigValue(
            ConstPersistorHttpResponse::TAB_CONFIG_KEY_DATA_ID_KEY
        );
    }



    /**
     * Get data engine.
     *
     * @param string $strDataReturnStatus
     * @param null|string $strPath = null
     * @return null|array
     * @throws DataRequiredException
     */
    protected function getDataEngine(
        $strDataReturnStatus,
        $strPath = null
    )
    {
        // Init var
        $result = null;

        // Get data, if required
        if($strDataReturnStatus !== ConstPersistorHttpResponse::CONFIG_DATA_RETURN_STATUS_NONE)
        {
            $result = $this->getTabBodyData();
            $result = (
                (!is_null($result)) && (!is_null($strPath)) ?
                    // Select data, if required
                    ToolBoxPathTableData::getValue(
                        $result,
                        $strPath,
                        $this->getStrPathSeparator()
                    ) :
                    $result
            );
            $result = (is_array($result) ? $result : null);

            // Check data, if required
            if (
                is_null($result) &&
                ($strDataReturnStatus == ConstPersistorHttpResponse::CONFIG_DATA_RETURN_STATUS_REQUIRED)
            )
            {
                throw new DataRequiredException();
            }
        }

        // Return result
        return $result;
    }



    /**
     * Get specified formatted data.
     * Index array of string data keys, to include|exclude,
     * can be provided.
     *
     * @param array $data
     * @param null|array $tabIncludeKey = null
     * @param null|array $tabExcludeKey = null
     * @return array
     */
    protected function getFormatData(
        array $data,
        array $tabIncludeKey = null,
        array $tabExcludeKey = null
    )
    {
        // Init var
        $result = array_filter(
            $data,
            function($key) use ($tabIncludeKey, $tabExcludeKey) {
                return (
                    (!is_string($key)) ||
                    (
                        (
                            is_null($tabIncludeKey) ||
                            in_array($key, $tabIncludeKey)
                        ) &&
                        (
                            is_null($tabExcludeKey) ||
                            (!in_array($key, $tabExcludeKey))
                        )
                    )
                );
            },
            ARRAY_FILTER_USE_KEY
        );

        // Return result
        return $result;
    }



    /**
     * Get specified index array of formatted data.
     * Index array of string data keys, to include|exclude,
     * can be provided.
     *
     * @param array $tabData
     * @param null|array $tabIncludeKey = null
     * @param null|array $tabExcludeKey = null
     * @return array
     */
    protected function getTabFormatData(
        array $tabData,
        array $tabIncludeKey = null,
        array $tabExcludeKey = null
    )
    {
        // Init var
        $result = array_map(
            function(array $data) use ($tabIncludeKey, $tabExcludeKey)
            {
                return $this->getFormatData($data, $tabIncludeKey, $tabExcludeKey);
            },
            $tabData
        );

        // Return result
        return $result;
    }



    /**
     * Get specified id,
     * from specified data.
     *
     * @param array $data
     * @return null|mixed
     * @throws ConfigInvalidFormatException
     */
    protected function getDataId(array $data)
    {
        // Init var
        $strIdKey = $this->getStrDataIdKey();
        $result = null;

        // Get id, if required
        if(!is_null($strIdKey))
        {
            $result = (isset($data[$strIdKey]) ? $data[$strIdKey] : $result);

            // Check valid id
            if(is_null($result))
            {
                throw new ConfigInvalidFormatException(serialize($this->getTabConfig()));
            }
        }

        // Return result
        return $result;
    }



    /**
     * Get specified index array of ids,
     * from specified index array of data.
     *
     * @param array $tabData
     * @return null|array
     */
    protected function getTabDataId(array $tabData)
    {
        // Init var
        $strIdKey = $this->getStrDataIdKey();
        $result = (
            (!is_null($strIdKey)) ?
                array_values(array_map(
                    function(array $data) {
                        return $this->getDataId($data);
                    },
                    $tabData
                )) :
                null
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getData()
    {
        // Init var
        $strPath = $this->getConfigValue(
            ConstPersistorHttpResponse::TAB_CONFIG_KEY_DATA_SELECT_PATH
        );
        $result = $this->getDataEngine(
            ConstPersistorHttpResponse::CONFIG_DATA_RETURN_STATUS_OPTIONAL,
            $strPath
        );

        if(!is_null($result))
        {
            // Format result
            $tabIncludeKey = $this->getConfigValue(
                ConstPersistorHttpResponse::TAB_CONFIG_KEY_DATA_INCLUDE_KEY
            );
            $tabExcludeKey = $this->getConfigValue(
                ConstPersistorHttpResponse::TAB_CONFIG_KEY_DATA_EXCLUDE_KEY
            );
            $result = $this->getFormatData(
                $result,
                $tabIncludeKey,
                $tabExcludeKey
            );
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws TabDataInvalidFormatException
     */
    public function getTabData()
    {
        // Init var
        $strPath = $this->getConfigValue(
            ConstPersistorHttpResponse::TAB_CONFIG_KEY_MULTI_DATA_SELECT_PATH,
            ConstPersistorHttpResponse::TAB_CONFIG_KEY_DATA_SELECT_PATH
        );
        $result = $this->getDataEngine(
            ConstPersistorHttpResponse::CONFIG_DATA_RETURN_STATUS_REQUIRED,
            $strPath
        );

        // Check valid result
        TabDataInvalidFormatException::setCheck($result);

        // Format result
        $tabIncludeKey = $this->getConfigValue(
            ConstPersistorHttpResponse::TAB_CONFIG_KEY_MULTI_DATA_INCLUDE_KEY
        );
        $tabExcludeKey = $this->getConfigValue(
            ConstPersistorHttpResponse::TAB_CONFIG_KEY_MULTI_DATA_EXCLUDE_KEY
        );
        $result = $this->getTabFormatData(
            $result,
            $tabIncludeKey,
            $tabExcludeKey
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws TabDataInvalidFormatException
     */
    public function getTabSearchData()
    {
        // Init var
        $strPath = $this->getConfigValue(
            ConstPersistorHttpResponse::TAB_CONFIG_KEY_SEARCH_DATA_SELECT_PATH
        );
        $result = $this->getDataEngine(
            ConstPersistorHttpResponse::CONFIG_DATA_RETURN_STATUS_REQUIRED,
            $strPath
        );

        // Check valid result
        TabDataInvalidFormatException::setCheck($result);

        // Format result
        $tabIncludeKey = $this->getConfigValue(
            ConstPersistorHttpResponse::TAB_CONFIG_KEY_SEARCH_DATA_INCLUDE_KEY
        );
        $tabExcludeKey = $this->getConfigValue(
            ConstPersistorHttpResponse::TAB_CONFIG_KEY_SEARCH_DATA_EXCLUDE_KEY
        );
        $result = $this->getTabFormatData(
            $result,
            $tabIncludeKey,
            $tabExcludeKey
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getCreateId()
    {
        // Init var
        $data = $this->getCreateData();
        $result = (
            (!is_null($data)) ?
                $this->getDataId($data) :
                null
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getTabCreateId()
    {
        // Init var
        $tabData = $this->getTabCreateData();
        $result = (
            (!is_null($tabData)) ?
                $this->getTabDataId($tabData) :
                null
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getCreateData()
    {
        // Init var
        $strDataReturnStatus = $this->getConfigValue(
            ConstPersistorHttpResponse::TAB_CONFIG_KEY_CREATE_DATA_RETURN_STATUS,
            null,
            ConstPersistorHttpResponse::CONFIG_DATA_RETURN_STATUS_OPTIONAL
        );
        $strPath = $this->getConfigValue(
            ConstPersistorHttpResponse::TAB_CONFIG_KEY_CREATE_DATA_SELECT_PATH
        );
        $result = $this->getDataEngine(
            $strDataReturnStatus,
            $strPath
        );

        if(!is_null($result))
        {
            // Format result
            $tabIncludeKey = $this->getConfigValue(
                ConstPersistorHttpResponse::TAB_CONFIG_KEY_CREATE_DATA_INCLUDE_KEY
            );
            $tabExcludeKey = $this->getConfigValue(
                ConstPersistorHttpResponse::TAB_CONFIG_KEY_CREATE_DATA_EXCLUDE_KEY
            );
            $result = $this->getFormatData(
                $result,
                $tabIncludeKey,
                $tabExcludeKey
            );
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws TabDataInvalidFormatException
     */
    public function getTabCreateData()
    {
        // Init var
        $strDataReturnStatus = $this->getConfigValue(
            ConstPersistorHttpResponse::TAB_CONFIG_KEY_CREATE_MULTI_DATA_RETURN_STATUS,
            ConstPersistorHttpResponse::TAB_CONFIG_KEY_CREATE_DATA_RETURN_STATUS,
            ConstPersistorHttpResponse::CONFIG_DATA_RETURN_STATUS_OPTIONAL
        );
        $strPath = $this->getConfigValue(
            ConstPersistorHttpResponse::TAB_CONFIG_KEY_CREATE_MULTI_DATA_SELECT_PATH,
            ConstPersistorHttpResponse::TAB_CONFIG_KEY_CREATE_DATA_SELECT_PATH
        );
        $result = $this->getDataEngine(
            $strDataReturnStatus,
            $strPath
        );

        if(!is_null($result))
        {
            // Check valid result
            TabDataInvalidFormatException::setCheck($result);

            // Format result
            $tabIncludeKey = $this->getConfigValue(
                ConstPersistorHttpResponse::TAB_CONFIG_KEY_CREATE_MULTI_DATA_INCLUDE_KEY
            );
            $tabExcludeKey = $this->getConfigValue(
                ConstPersistorHttpResponse::TAB_CONFIG_KEY_CREATE_MULTI_DATA_EXCLUDE_KEY
            );
            $result = $this->getTabFormatData(
                $result,
                $tabIncludeKey,
                $tabExcludeKey
            );
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getUpdateData()
    {
        // Init var
        $strDataReturnStatus = $this->getConfigValue(
            ConstPersistorHttpResponse::TAB_CONFIG_KEY_UPDATE_DATA_RETURN_STATUS,
            null,
            ConstPersistorHttpResponse::CONFIG_DATA_RETURN_STATUS_OPTIONAL
        );
        $strPath = $this->getConfigValue(
            ConstPersistorHttpResponse::TAB_CONFIG_KEY_UPDATE_DATA_SELECT_PATH
        );
        $result = $this->getDataEngine(
            $strDataReturnStatus,
            $strPath
        );

        if(!is_null($result))
        {
            // Format result
            $tabIncludeKey = $this->getConfigValue(
                ConstPersistorHttpResponse::TAB_CONFIG_KEY_UPDATE_DATA_INCLUDE_KEY
            );
            $tabExcludeKey = $this->getConfigValue(
                ConstPersistorHttpResponse::TAB_CONFIG_KEY_UPDATE_DATA_EXCLUDE_KEY
            );
            $result = $this->getFormatData(
                $result,
                $tabIncludeKey,
                $tabExcludeKey
            );
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws TabDataInvalidFormatException
     */
    public function getTabUpdateData()
    {
        // Init var
        $strDataReturnStatus = $this->getConfigValue(
            ConstPersistorHttpResponse::TAB_CONFIG_KEY_UPDATE_MULTI_DATA_RETURN_STATUS,
            ConstPersistorHttpResponse::TAB_CONFIG_KEY_UPDATE_DATA_RETURN_STATUS,
            ConstPersistorHttpResponse::CONFIG_DATA_RETURN_STATUS_OPTIONAL
        );
        $strPath = $this->getConfigValue(
            ConstPersistorHttpResponse::TAB_CONFIG_KEY_UPDATE_MULTI_DATA_SELECT_PATH,
            ConstPersistorHttpResponse::TAB_CONFIG_KEY_UPDATE_DATA_SELECT_PATH
        );
        $result = $this->getDataEngine(
            $strDataReturnStatus,
            $strPath
        );

        if(!is_null($result))
        {
            // Check valid result
            TabDataInvalidFormatException::setCheck($result);

            // Format result
            $tabIncludeKey = $this->getConfigValue(
                ConstPersistorHttpResponse::TAB_CONFIG_KEY_UPDATE_MULTI_DATA_INCLUDE_KEY
            );
            $tabExcludeKey = $this->getConfigValue(
                ConstPersistorHttpResponse::TAB_CONFIG_KEY_UPDATE_MULTI_DATA_EXCLUDE_KEY
            );
            $result = $this->getTabFormatData(
                $result,
                $tabIncludeKey,
                $tabExcludeKey
            );
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getDeleteData()
    {
        // Init var
        $strDataReturnStatus = $this->getConfigValue(
            ConstPersistorHttpResponse::TAB_CONFIG_KEY_DELETE_DATA_RETURN_STATUS,
            null,
            ConstPersistorHttpResponse::CONFIG_DATA_RETURN_STATUS_OPTIONAL
        );
        $strPath = $this->getConfigValue(
            ConstPersistorHttpResponse::TAB_CONFIG_KEY_DELETE_DATA_SELECT_PATH
        );
        $result = $this->getDataEngine(
            $strDataReturnStatus,
            $strPath
        );

        if(!is_null($result))
        {
            // Format result
            $tabIncludeKey = $this->getConfigValue(
                ConstPersistorHttpResponse::TAB_CONFIG_KEY_DELETE_DATA_INCLUDE_KEY
            );
            $tabExcludeKey = $this->getConfigValue(
                ConstPersistorHttpResponse::TAB_CONFIG_KEY_DELETE_DATA_EXCLUDE_KEY
            );
            $result = $this->getFormatData(
                $result,
                $tabIncludeKey,
                $tabExcludeKey
            );
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws TabDataInvalidFormatException
     */
    public function getTabDeleteData()
    {
        // Init var
        $strDataReturnStatus = $this->getConfigValue(
            ConstPersistorHttpResponse::TAB_CONFIG_KEY_DELETE_MULTI_DATA_RETURN_STATUS,
            ConstPersistorHttpResponse::TAB_CONFIG_KEY_DELETE_DATA_RETURN_STATUS,
            ConstPersistorHttpResponse::CONFIG_DATA_RETURN_STATUS_OPTIONAL
        );
        $strPath = $this->getConfigValue(
            ConstPersistorHttpResponse::TAB_CONFIG_KEY_DELETE_MULTI_DATA_SELECT_PATH,
            ConstPersistorHttpResponse::TAB_CONFIG_KEY_DELETE_DATA_SELECT_PATH
        );
        $result = $this->getDataEngine(
            $strDataReturnStatus,
            $strPath
        );

        if(!is_null($result))
        {
            // Check valid result
            TabDataInvalidFormatException::setCheck($result);

            // Format result
            $tabIncludeKey = $this->getConfigValue(
                ConstPersistorHttpResponse::TAB_CONFIG_KEY_DELETE_MULTI_DATA_INCLUDE_KEY
            );
            $tabExcludeKey = $this->getConfigValue(
                ConstPersistorHttpResponse::TAB_CONFIG_KEY_DELETE_MULTI_DATA_EXCLUDE_KEY
            );
            $result = $this->getTabFormatData(
                $result,
                $tabIncludeKey,
                $tabExcludeKey
            );
        }

        // Return result
        return $result;
    }



}