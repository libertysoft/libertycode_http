<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\requisition\client\exception;

use Exception;

use liberty_code\http\requisition\request\library\ConstHttpRequest;
use liberty_code\http\requisition\request\exception\SndInfoInvalidFormatException as BaseSndInfoInvalidFormatException;
use liberty_code\http\requisition\client\library\ConstHttpClient;



class SndInfoInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $info
     */
	public function __construct($info)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstHttpClient::EXCEPT_MSG_SND_INFO_INVALID_FORMAT,
            mb_strimwidth(strval($info), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified info has valid format.
     *
     * @param mixed $info
     * @return boolean
     */
    protected static function checkInfoIsValid($info)
    {
        // Init var
        $result =
            // Check valid method
            isset($info[ConstHttpRequest::TAB_SND_INFO_KEY_METHOD]) &&

            // Check valid URL schema
            isset($info[ConstHttpRequest::TAB_SND_INFO_KEY_URL_SCHEMA]) &&

            // Check valid URL host
            isset($info[ConstHttpRequest::TAB_SND_INFO_KEY_URL_HOST]) &&

            // Check valid URL route
            isset($info[ConstHttpRequest::TAB_SND_INFO_KEY_URL_ROUTE]);

        // Return result
        return $result;
    }



	/**
	 * Check if specified info has valid format.
	 * 
     * @param mixed $info
	 * @return boolean
	 * @throws static
     */
    public static function setCheck($info)
    {
		// Init var
		$result =
            // Check valid info
            BaseSndInfoInvalidFormatException::checkInfoIsValid($info) &&
            static::checkInfoIsValid($info);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($info) ? serialize($info) : $info));
		}
		
		// Return result
		return $result;
    }
	
	
	
}