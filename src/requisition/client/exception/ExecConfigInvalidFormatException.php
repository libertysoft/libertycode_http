<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\requisition\client\exception;

use Exception;

use liberty_code\http\requisition\client\library\ConstHttpClient;



class ExecConfigInvalidFormatException extends Exception
{
    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor
    // ******************************************************************************

    /**
     * Constructor
     *
     * @param mixed $config
     */
    public function __construct($config)
    {
        // Call parent constructor
        parent::__construct();

        // Init var
        $this->message = sprintf
        (
            ConstHttpClient::EXCEPT_MSG_EXEC_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 50, "...")
        );
    }





    // Methods statics security (throw exception if check not pass)
    // ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $result =
            // Check valid after execution callable
            (
                (!isset($config[ConstHttpClient::TAB_EXEC_CONFIG_KEY_AFTER_EXECUTION_CALLABLE])) ||
                is_callable($config[ConstHttpClient::TAB_EXEC_CONFIG_KEY_AFTER_EXECUTION_CALLABLE])
            ) &&

            // Check valid redirects maximum count
            (
                (!isset($config[ConstHttpClient::TAB_EXEC_CONFIG_KEY_REDIRECT_COUNT_MAX])) ||
                (
                    is_int($config[ConstHttpClient::TAB_EXEC_CONFIG_KEY_REDIRECT_COUNT_MAX]) &&
                    ($config[ConstHttpClient::TAB_EXEC_CONFIG_KEY_REDIRECT_COUNT_MAX] >= -1)
                )
            ) &&

            // Check valid redirect before execution callable
            (
                (!isset($config[ConstHttpClient::TAB_EXEC_CONFIG_KEY_REDIRECT_BEFORE_EXECUTION_CALLABLE])) ||
                is_callable($config[ConstHttpClient::TAB_EXEC_CONFIG_KEY_REDIRECT_BEFORE_EXECUTION_CALLABLE])
            ) &&

            // Check valid cURL additional options
            (
                (!isset($config[ConstHttpClient::TAB_EXEC_CONFIG_KEY_CURL_ADD_OPTION])) ||
                is_array($config[ConstHttpClient::TAB_EXEC_CONFIG_KEY_CURL_ADD_OPTION])
            );

        // Return result
        return $result;
    }



    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     * @throws static
     */
    public static function setCheck($config)
    {
        // Init var
        $result =
            is_null($config) ||
            (
                // Check valid array
                is_array($config) &&
                static::checkConfigIsValid($config)
            );

        // Throw exception if check not pass
        if(!$result)
        {
            throw new static((is_array($config) ? serialize($config) : $config));
        }

        // Return result
        return $result;
    }
	
	
	
}