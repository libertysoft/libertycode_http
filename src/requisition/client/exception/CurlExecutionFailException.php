<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\requisition\client\exception;

use Exception;

use liberty_code\http\requisition\client\library\ConstHttpClient;



class CurlExecutionFailException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     *
	 * @param string $strErrorMsg
     */
	public function __construct($strErrorMsg)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$strErrorMsg =
			(is_string($strErrorMsg) && (trim($strErrorMsg) != '')) ?
				' ' . trim($strErrorMsg) :
				'';
		$this->message = sprintf(
		    ConstHttpClient::EXCEPT_MSG_CURL_EXECUTION_FAIL,
            $strErrorMsg
        );
	}



}