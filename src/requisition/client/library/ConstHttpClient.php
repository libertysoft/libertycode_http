<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\requisition\client\library;



class ConstHttpClient
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration
    const TAB_CONFIG_KEY_AFTER_EXECUTION_CALLABLE = 'after_execution_callable';
    const TAB_CONFIG_KEY_REDIRECT_COUNT_MAX = 'redirect_count_max';
    const TAB_CONFIG_KEY_REDIRECT_BEFORE_EXECUTION_CALLABLE = 'redirect_before_execution_callable';
    const TAB_CONFIG_KEY_CURL_ADD_OPTION = 'curl_add_option';

    // Configuration execution
    const TAB_EXEC_CONFIG_KEY_AFTER_EXECUTION_CALLABLE = 'after_execution_callable';
    const TAB_EXEC_CONFIG_KEY_REDIRECT_COUNT_MAX = 'redirect_count_max';
    const TAB_EXEC_CONFIG_KEY_REDIRECT_BEFORE_EXECUTION_CALLABLE = 'redirect_before_execution_callable';
    const TAB_EXEC_CONFIG_KEY_CURL_ADD_OPTION = 'curl_add_option';



    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the HTTP client configuration standard.';
    const EXCEPT_MSG_EXEC_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array, following the HTTP client execution configuration standard.';
    const EXCEPT_MSG_SND_INFO_INVALID_FORMAT =
        'Following info "%1$s" invalid! 
        The info must be an array and following the HTTP client sending information standard.';
    const EXCEPT_MSG_CURL_EXECUTION_FAIL = 'cURL Execution failed!%1$s';



}