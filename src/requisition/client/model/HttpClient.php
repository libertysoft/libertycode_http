<?php
/**
 * This class allows to define HTTP client class.
 * HTTP client is info client, allows to deal with HTTP (sending, reception) information,
 * to execute HTTP request sending.
 *
 * HTTP client uses the following specified configuration:
 * [
 *     Info client configuration,
 *
 *     after_execution_callable(optional):
 *         after execution callback function: mixed function(
 *             string &$strResponseStatusLine (get editable calculated response status line, to return),
 *             array &$tabResponseHeaderLine (get editable calculated response header lines, to return),
 *             string &$strResponseBody (get editable calculated response body, to return),
 *             integer $intRedirectCount,
 *             null|string $strProtocolVersionNum,
 *             string $strMethod,
 *             string $strUrl,
 *             array $tabHeader,
 *             null|string $strBody,
 *             array $tabCurlAddOption
 *         ),
 *
 *     redirect_count_max(optional: got 0 if not found):
 *         maximum number of redirects allowed (-1 means no restriction),
 *
 *     redirect_before_execution_callable(optional):
 *         redirect before execution callback function: mixed function(
 *             null|string &$strProtocolVersionNum (get editable request protocol version number, to use),
 *             string &$strMethod (get editable request method, to use),
 *             string &$strUrl (get editable redirect response header location URL, to use),
 *             array &$tabHeader (get editable request array of headers, to use),
 *             null|string &$strBody (get editable request body, to use),
 *             array &$tabCurlAddOption (get editable request cURL additional options, to use),
 *             integer $intRedirectCount,
 *             string $strRequestUrl,
 *             string $strRedirectRespStatusLine
 *             array $tabRedirectRespHeaderLine
 *             string $strRedirectRespBody
 *         ),
 *
 *     curl_add_option(optional): [
 *         // Optional additional cURL options
 *         @see ToolBoxCurl::getStrResponseBody() additional option array format
 *     ]
 * ]
 *
 * HTTP client uses the following specified sending information, to execute request:
 * [
 *     HTTP request sending information,
 *
 *     method(required): @see HttpRequest sending information method,
 *
 *     url(not used): @see HttpRequest sending information url,
 *
 *     url_schema(required): @see HttpRequest sending information url_schema,
 *
 *     url_host(required): @see HttpRequest sending information url_host,
 *
 *     url_route(required): @see HttpRequest sending information url_route
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\requisition\client\model;

use liberty_code\requisition\client\info\model\InfoClient;

use liberty_code\requisition\request\api\RequestInterface;
use liberty_code\requisition\client\library\ConstClient;
use liberty_code\http\header\library\ToolBoxHeader;
use liberty_code\http\response\library\ToolBoxResponse;
use liberty_code\http\curl\library\ToolBoxCurl;
use liberty_code\http\requisition\request\library\ConstHttpRequest;
use liberty_code\http\requisition\request\library\ToolBoxHttpRequest;
use liberty_code\http\requisition\response\library\ConstHttpResponse;
use liberty_code\http\requisition\client\library\ConstHttpClient;
use liberty_code\http\requisition\client\exception\ConfigInvalidFormatException;
use liberty_code\http\requisition\client\exception\ExecConfigInvalidFormatException;
use liberty_code\http\requisition\client\exception\SndInfoInvalidFormatException;
use liberty_code\http\requisition\client\exception\CurlExecutionFailException;



class HttpClient extends InfoClient
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();

	

	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        // $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstClient::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get redirects maximum count.
     *
     * Configuration array format:
     * Execution configuration can be provided.
     * @see getTabRcpInfoFromSndInfo() configuration array format.
     *
     * @param array $tabConfig = null
     * @return integer
     * @throws ExecConfigInvalidFormatException
     */
    public function getIntRedirectMaxCount(array $tabConfig = null)
    {
        // Set check argument
        ExecConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $tabExecConfig = $tabConfig;
        $tabConfig = $this->getTabConfig();
        $result = (
            isset($tabExecConfig[ConstHttpClient::TAB_EXEC_CONFIG_KEY_REDIRECT_COUNT_MAX]) ?
                $tabExecConfig[ConstHttpClient::TAB_EXEC_CONFIG_KEY_REDIRECT_COUNT_MAX] :
                (
                    isset($tabConfig[ConstHttpClient::TAB_CONFIG_KEY_REDIRECT_COUNT_MAX]) ?
                        $tabConfig[ConstHttpClient::TAB_CONFIG_KEY_REDIRECT_COUNT_MAX] :
                        0
                )
        );

        // Return result
        return $result;
    }



    /**
     * Get cURL additional options array.
     *
     * Configuration array format:
     * Execution configuration can be provided.
     * @see getTabRcpInfoFromSndInfo() configuration array format.
     *
     * @param array $tabConfig = null
     * @return array
     */
    protected function getTabCurlAddOption(array $tabConfig = null)
    {
        // Init var
        $tabExecConfig = $tabConfig;
        $tabConfig = $this->getTabConfig();
        $result = (
            isset($tabExecConfig[ConstHttpClient::TAB_EXEC_CONFIG_KEY_CURL_ADD_OPTION]) ?
                $tabExecConfig[ConstHttpClient::TAB_EXEC_CONFIG_KEY_CURL_ADD_OPTION] :
                (
                    isset($tabConfig[ConstHttpClient::TAB_CONFIG_KEY_CURL_ADD_OPTION]) ?
                        $tabConfig[ConstHttpClient::TAB_CONFIG_KEY_CURL_ADD_OPTION] :
                        array()
                )
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws SndInfoInvalidFormatException
     */
    protected function getTabSndInfo(RequestInterface $objRequest)
    {
        // Init var
        $result = parent::getTabSndInfo($objRequest);

        // Check sending information
        SndInfoInvalidFormatException::setCheck($result);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     *
     * Configuration array format:
     * [
     *     @see InfoClient::executeRequestEngine() configuration array format,
     *
     *     after_execution_callable(optional: got configuration after_execution_callable, if not found):
     *         after execution callback function: @see HttpClient configuration after_execution_callable,
     *
     *     redirect_count_max (optional: got configuration redirect_count_max, if not found):
     *         maximum number of redirects allowed (-1 means no restriction),
     *
     *     redirect_before_execution_callable (optional: got configuration redirect_before_execution_callable, if not found):
     *         redirect before execution callback function: @see HttpClient configuration redirect_before_execution_callable,
     *
     *     curl_add_option (optional: got configuration curl_add_option, if not found): [
     *         // Optional additional cURL options
     *         @see ToolBoxCurl::getStrResponseBody() additional option array format
     *     ]
     * ]
     *
     * @return array
     * @throws SndInfoInvalidFormatException
     * @throws CurlExecutionFailException
     */
    protected function getTabRcpInfoFromSndInfo(array $tabSndInfo, array $tabConfig = null)
    {
        // Init var
        $result = array();
        $tabExecConfig = $tabConfig;
        $strProtocolVersionNum = (
            isset($tabSndInfo[ConstHttpRequest::TAB_SND_INFO_KEY_PROTOCOL_VERSION_NUM]) ?
                $tabSndInfo[ConstHttpRequest::TAB_SND_INFO_KEY_PROTOCOL_VERSION_NUM] :
                null
        );
        $strMethod = $tabSndInfo[ConstHttpRequest::TAB_SND_INFO_KEY_METHOD];
        $strUrl = ToolBoxHttpRequest::getStrUrl(
            $tabSndInfo[ConstHttpRequest::TAB_SND_INFO_KEY_URL_HOST],
            $tabSndInfo[ConstHttpRequest::TAB_SND_INFO_KEY_URL_SCHEMA],
            (
                isset($tabSndInfo[ConstHttpRequest::TAB_SND_INFO_KEY_URL_PORT]) ?
                    $tabSndInfo[ConstHttpRequest::TAB_SND_INFO_KEY_URL_PORT] :
                    null
            ),
            $tabSndInfo[ConstHttpRequest::TAB_SND_INFO_KEY_URL_ROUTE],
            (
                isset($tabSndInfo[ConstHttpRequest::TAB_SND_INFO_KEY_URL_ROUTE_ARG]) ?
                    $tabSndInfo[ConstHttpRequest::TAB_SND_INFO_KEY_URL_ROUTE_ARG] :
                    null
            ),
            (
                array_key_exists(ConstHttpRequest::TAB_SND_INFO_KEY_URL_ROUTE_ARG_SPEC, $tabSndInfo) ?
                    $tabSndInfo[ConstHttpRequest::TAB_SND_INFO_KEY_URL_ROUTE_ARG_SPEC] :
                    array()
            ),
            (
                isset($tabSndInfo[ConstHttpRequest::TAB_SND_INFO_KEY_URL_ARG]) ?
                    $tabSndInfo[ConstHttpRequest::TAB_SND_INFO_KEY_URL_ARG] :
                    null
            )
        );
        $tabHeader = (
            isset($tabSndInfo[ConstHttpRequest::TAB_SND_INFO_KEY_HEADER]) ?
                $tabSndInfo[ConstHttpRequest::TAB_SND_INFO_KEY_HEADER] :
                array()
        );
        $strBody = (
            isset($tabSndInfo[ConstHttpRequest::TAB_SND_INFO_KEY_BODY]) ?
                $tabSndInfo[ConstHttpRequest::TAB_SND_INFO_KEY_BODY] :
                null
        );
        $intRedirectMaxCount = $this->getIntRedirectMaxCount($tabExecConfig);
        $tabCurlAddOption = $this->getTabCurlAddOption($tabExecConfig);

        // Check sending information
        if(is_null($strUrl))
        {
            throw new SndInfoInvalidFormatException(serialize($tabSndInfo));
        }

        // Execute sending info
        $intRedirectCount = 0;
        do{
            // Send sending info and get reception info (via cURL object)
            $strErrorMsg = null;
            $strResponseStatusLine = '';
            $tabResponseHeaderLine = array();
            $strResponseBody = ToolBoxCurl::getStrResponseBody(
                $strMethod,
                $strUrl,
                $strProtocolVersionNum,
                $tabHeader,
                $strBody,
                $tabCurlAddOption,
                $strResponseStatusLine,
                $tabResponseHeaderLine,
                $objCurl
            );
            $strErrorMsg = (
                ($strResponseBody === false) ?
                    curl_error($objCurl) :
                    null
            );
            curl_close($objCurl);

            // Check cURL execution
            if(!is_null($strErrorMsg))
            {
                throw new CurlExecutionFailException($strErrorMsg);
            }

            $this->setAfterExecutionCallable(
                $strResponseStatusLine,
                $tabResponseHeaderLine,
                $strResponseBody,
                $intRedirectCount,
                $strProtocolVersionNum,
                $strMethod,
                $strUrl,
                $tabHeader,
                $strBody,
                $tabCurlAddOption,
                $tabExecConfig
            );

            // Check redirect required
            $intResponseStatusCode = ToolBoxResponse::getIntStatusCode($strResponseStatusLine);
            $tabResponseHeader = ToolBoxHeader::getTabData($tabResponseHeaderLine);
            $boolRedirectRequired = (
                // Redirect allowed
                (
                    ($intRedirectMaxCount == -1) ||
                    (
                        ($intRedirectMaxCount > 0) &&
                        ($intRedirectCount < $intRedirectMaxCount)
                    )
                ) &&
                // Redirect required, from reception info
                ($intResponseStatusCode >= 300) &&
                ($intResponseStatusCode < 400) &&
                // Valid info to redirect, from reception info
                isset($tabResponseHeader['Location']) &&
                is_string($tabResponseHeader['Location'])
            );

            // Get redirect info, if required
            if($boolRedirectRequired)
            {
                $intRedirectCount++;
                $strRequestUrl = $strUrl;
                $strUrl = $tabResponseHeader['Location'];

                $this->setRedirectBeforeExecutionCallable(
                    $strProtocolVersionNum,
                    $strMethod,
                    $strUrl,
                    $tabHeader,
                    $strBody,
                    $tabCurlAddOption,
                    $intRedirectCount,
                    $strRequestUrl,
                    $strResponseStatusLine,
                    $tabResponseHeaderLine,
                    $strResponseBody,
                    $tabExecConfig
                );
            }
        } while($boolRedirectRequired);

        // Build reception information
        $result[ConstHttpResponse::TAB_RCP_INFO_KEY_PROTOCOL_VERSION_NUM] = ToolBoxResponse::getStrProtocolVersionNum($strResponseStatusLine);
        $result[ConstHttpResponse::TAB_RCP_INFO_KEY_STATUS_CODE] = $intResponseStatusCode;
        if(!is_null($strResponseStatusMsg = ToolBoxResponse::getStrStatusMsg($strResponseStatusLine)))
        {
            $result[ConstHttpResponse::TAB_RCP_INFO_KEY_STATUS_MSG] = $strResponseStatusMsg;
        }
        $result[ConstHttpResponse::TAB_RCP_INFO_KEY_HEADER] = $tabResponseHeader;
        $result[ConstHttpResponse::TAB_RCP_INFO_KEY_BODY] = $strResponseBody;

        // Return result
        return $result;
    }





    // Methods setters
    // ******************************************************************************

    /**
     * Set after execution callable.
     *
     * Configuration array format:
     * Execution configuration can be provided.
     * @see getTabRcpInfoFromSndInfo() configuration array format.
     *
     * @param string &$strResponseStatusLine
     * @param array &$tabResponseHeaderLine
     * @param string &$strResponseBody
     * @param integer $intRedirectCount
     * @param null|string $strProtocolVersionNum
     * @param string $strMethod
     * @param string $strUrl
     * @param array $tabHeader
     * @param null|string $strBody
     * @param array $tabCurlAddOption
     * @param array $tabConfig = null
     */
    protected function setAfterExecutionCallable(
        &$strResponseStatusLine,
        array &$tabResponseHeaderLine,
        &$strResponseBody,
        $intRedirectCount,
        $strProtocolVersionNum,
        $strMethod,
        $strUrl,
        array $tabHeader,
        $strBody,
        array $tabCurlAddOption,
        array $tabConfig = null
    )
    {
        // Init var
        $tabExecConfig = $tabConfig;
        $tabConfig = $this->getTabConfig();
        $callable = (
            isset($tabExecConfig[ConstHttpClient::TAB_EXEC_CONFIG_KEY_AFTER_EXECUTION_CALLABLE]) ?
                $tabExecConfig[ConstHttpClient::TAB_EXEC_CONFIG_KEY_AFTER_EXECUTION_CALLABLE] :
                (
                    isset($tabConfig[ConstHttpClient::TAB_CONFIG_KEY_AFTER_EXECUTION_CALLABLE]) ?
                        $tabConfig[ConstHttpClient::TAB_CONFIG_KEY_AFTER_EXECUTION_CALLABLE] :
                        null
                )
        );

        if(!is_null($callable))
        {
            $callable(
                $strResponseStatusLine,
                $tabResponseHeaderLine,
                $strResponseBody,
                $intRedirectCount,
                $strProtocolVersionNum,
                $strMethod,
                $strUrl,
                $tabHeader,
                $strBody,
                $tabCurlAddOption
            );
        }
    }



    /**
     * Set redirect before execution callable.
     *
     * Configuration array format:
     * Execution configuration can be provided.
     * @see getTabRcpInfoFromSndInfo() configuration array format.
     *
     * @param null|string &$strProtocolVersionNum
     * @param string &$strMethod
     * @param string &$strUrl
     * @param array &$tabHeader
     * @param null|string &$strBody
     * @param array &$tabCurlAddOption
     * @param integer $intRedirectCount
     * @param string $strRequestUrl
     * @param string $strRedirectRespStatusLine
     * @param array $tabRedirectRespHeaderLine
     * @param string $strRedirectRespBody
     * @param array $tabConfig = null
     */
    protected function setRedirectBeforeExecutionCallable(
        &$strProtocolVersionNum,
        &$strMethod,
        &$strUrl,
        array &$tabHeader,
        &$strBody,
        array &$tabCurlAddOption,
        $intRedirectCount,
        $strRequestUrl,
        $strRedirectRespStatusLine,
        $tabRedirectRespHeaderLine,
        $strRedirectRespBody,
        array $tabConfig = null
    )
    {
        // Init var
        $tabExecConfig = $tabConfig;
        $tabConfig = $this->getTabConfig();
        $callable = (
            isset($tabExecConfig[ConstHttpClient::TAB_EXEC_CONFIG_KEY_REDIRECT_BEFORE_EXECUTION_CALLABLE]) ?
                $tabExecConfig[ConstHttpClient::TAB_EXEC_CONFIG_KEY_REDIRECT_BEFORE_EXECUTION_CALLABLE] :
                (
                    isset($tabConfig[ConstHttpClient::TAB_CONFIG_KEY_REDIRECT_BEFORE_EXECUTION_CALLABLE]) ?
                        $tabConfig[ConstHttpClient::TAB_CONFIG_KEY_REDIRECT_BEFORE_EXECUTION_CALLABLE] :
                        null
                )
        );

        if(!is_null($callable))
        {
            $callable(
                $strProtocolVersionNum,
                $strMethod,
                $strUrl,
                $tabHeader,
                $strBody,
                $tabCurlAddOption,
                $intRedirectCount,
                $strRequestUrl,
                $strRedirectRespStatusLine,
                $tabRedirectRespHeaderLine,
                $strRedirectRespBody
            );
        }
    }



}