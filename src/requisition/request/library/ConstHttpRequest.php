<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\requisition\request\library;



class ConstHttpRequest
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_URL_ROUTE_ARG_DATA = 'objUrlRouteArgData';
    const DATA_KEY_DEFAULT_URL_ARG_DATA = 'objUrlArgData';
    const DATA_KEY_DEFAULT_HEADER_DATA = 'objHeaderData';



    // Sending information configuration
    const TAB_SND_INFO_KEY_PROTOCOL_VERSION_NUM = 'protocol_version_num';
    const TAB_SND_INFO_KEY_METHOD = 'method';
    const TAB_SND_INFO_KEY_URL = 'url';
    const TAB_SND_INFO_KEY_URL_SCHEMA = 'url_schema';
    const TAB_SND_INFO_KEY_URL_HOST = 'url_host';
    const TAB_SND_INFO_KEY_URL_PORT = 'url_port';
    const TAB_SND_INFO_KEY_URL_ROUTE = 'url_route';
    const TAB_SND_INFO_KEY_URL_ROUTE_ARG = 'url_route_argument';
    const TAB_SND_INFO_KEY_URL_ROUTE_ARG_SPEC = 'url_route_argument_spec';
    const TAB_SND_INFO_KEY_URL_ARG = 'url_argument';
    const TAB_SND_INFO_KEY_HEADER = 'header';
    const TAB_SND_INFO_KEY_BODY = 'body';

    // Schema configuration
    const SCHEMA_HTTP = 'http';
    const SCHEMA_HTTPS = 'https';


	
    // Exception message constants
    const EXCEPT_MSG_URL_ROUTE_ARG_DATA_INVALID_FORMAT = 'Following URL route argument data "%1$s" invalid! It must be an URL route argument data object.';
    const EXCEPT_MSG_URL_ARG_DATA_INVALID_FORMAT = 'Following URL argument data "%1$s" invalid! It must be an URL argument data object.';
    const EXCEPT_MSG_HEADER_DATA_INVALID_FORMAT = 'Following header data "%1$s" invalid! It must be a header data object.';
    const EXCEPT_MSG_SND_INFO_INVALID_FORMAT =
        'Following info "%1$s" invalid! 
        The info must be an array and following the HTTP request sending information standard.';



}