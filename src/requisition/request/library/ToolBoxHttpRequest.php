<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\requisition\request\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\http\url_route\library\ToolBoxUrlRoute;



class ToolBoxHttpRequest extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get string specified URL, if possible.
     *
     * @param string $strHost
     * @param null|string $strSchema = null
     * @param null|integer $intPort = null
     * @param null|string $strRoute = null
     * @param null|array $tabRouteArg = null
     * @param null|array $tabRouteArgSpec = null
     * @param null|string|array $arg = null
     * @return null|string
     */
    public static function getStrUrl(
        $strHost,
        $strSchema = null,
        $intPort = null,
        $strRoute = null,
        array $tabRouteArg = null,
        array $tabRouteArgSpec = null,
        $arg = null
    )
    {
        // Init var
        $strSchema = ((is_string($strSchema) && (trim($strSchema) != '')) ? $strSchema : null);
        $strHost = ((is_string($strHost) && (trim($strHost) != '')) ? $strHost : null);
        $intPort = ((is_int($intPort) && ($intPort > 0)) ? $intPort : null);
        $strRoute = ((is_string($strRoute) && (trim($strRoute) != '')) ? $strRoute : null);
        $tabRouteArg = ((!is_null($tabRouteArg)) ? $tabRouteArg : array());
        $strArg = (
            (is_string($arg) && (trim($arg) == '')) ?
                $arg :
                (
                    (is_array($arg) && (count($arg) > 0)) ?
                        http_build_query($arg) :
                        null
                )
        );
        $result = null;

        // Build URL text, if required
        if(!is_null($strHost))
        {
            // Set host
            $result = $strHost;

            // Set schema, if required
            $result = (
                (!is_null($strSchema)) ?
                    sprintf('%2$s://%1$s', $result, $strSchema) :
                    $result
            );

            // Set port, if required
            $result = (
                (!is_null($intPort)) ?
                    sprintf('%1$s:%2$d', $result, $intPort) :
                    $result
            );

            // Set route, if required
            if(!is_null($strRoute))
            {
                $strRoute = ((!is_null($strRoute)) ? ToolBoxUrlRoute::getStrRoute($strRoute, $tabRouteArg, $tabRouteArgSpec) : $strRoute);
                $result = (
                    (!is_null($strRoute)) ?
                        sprintf(
                            ((preg_match('#^/.*$#', $strRoute) === 1) ? '%1$s%2$s' : '%1$s/%2$s'),
                            $result,
                            $strRoute
                        ) :
                        null
                );
            }

            // Set arguments, if required
            $result = (
                ((!is_null($result)) && (!is_null($strArg))) ?
                    sprintf('%1$s?%2$s', $result, $strArg) :
                    $result
            );
        }

        // Return result
        return $result;
    }



}