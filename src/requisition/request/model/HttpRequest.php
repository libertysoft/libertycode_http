<?php
/**
 * Description :
 * This class allows to define HTTP request class.
 * HTTP request is request, allowing to handle all HTTP information,
 * required for HTTP sending.
 * Can be consider is base of all HTTP request type.
 *
 * HTTP request uses the following specified configuration:
 * [
 *     Default request configuration,
 * ]
 *
 * HTTP request handles the following specified sending information:
 * [
 *     Default request sending information,
 *
 *     protocol_version_num(optional): "string HTTP protocol version number",
 *
 *     method(optional): "string HTTP method",
 *
 *     url(optional): "string full URL",
 *
 *     url_schema(optional): "string URL schema: http|https",
 *
 *     url_host(optional): "string URL host",
 *
 *     url_port(optional): integer URL port,
 *
 *     url_route(optional): "string URL route",
 *
 *     url_route_argument(optional: got [], if not found): [
 *         @see UrlRouteArgData data source array format
 *     ],
 *
 *     url_route_argument_spec(optional: got [], if not found): [
 *         @see ToolBoxUrlRoute::checkArgSpecIsValid() argument specification array format
 *     ],
 *
 *     url_argument(optional: got [], if not found): [
 *         @see UrlArgData data source array format
 *     ],
 *
 *     header(optional: got [], if not found): [
 *         @see HeaderData data source array format
 *     ],
 *
 *     body(optional): "string body content"
 * ]
 *
 * Note:
 * -> Sending information url format:
 *     Additional feature only available for setting.
 *     Provided URL will be split to set url_schema, url_host, url_port url_route and url_argument.
 *     It allows to fill entire URL, via configuration.
 * -> Sending information url route info:
 *     Possible to specify url route arguments, and url route argument specification,
 *     to customize specified url route, following @see ToolBoxUrlRoute::getStrRoute() logic.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\requisition\request\model;

use liberty_code\requisition\request\model\DefaultRequest;

use liberty_code\requisition\request\library\ConstRequest;
use liberty_code\http\url_route\argument\model\UrlRouteArgData;
use liberty_code\http\url_arg\model\UrlArgData;
use liberty_code\http\header\model\HeaderData;
use liberty_code\http\requisition\request\library\ConstHttpRequest;
use liberty_code\http\requisition\request\library\ToolBoxHttpRequest;
use liberty_code\http\requisition\request\exception\UrlRouteArgDataInvalidFormatException;
use liberty_code\http\requisition\request\exception\UrlArgDataInvalidFormatException;
use liberty_code\http\requisition\request\exception\HeaderDataInvalidFormatException;
use liberty_code\http\requisition\request\exception\SndInfoInvalidFormatException;



/**
 * @method null|UrlRouteArgData getObjUrlRouteArgData() Get URL route argument data object.
 * @method null|UrlArgData getObjUrlArgData() Get URL argument data object.
 * @method null|HeaderData getObjHeaderData() Get header data object.
 */
class HttpRequest extends DefaultRequest
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();


	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param null|UrlRouteArgData $objUrlRouteArgData = null
     * @param null|UrlArgData $objUrlArgData = null
     * @param null|HeaderData $objHeaderData = null
     */
    public function __construct(
        UrlRouteArgData $objUrlRouteArgData = null,
        UrlArgData $objUrlArgData = null,
        HeaderData $objHeaderData = null,
        array $tabConfig = null,
        array $tabSndInfo = null
    )
    {
        // Call parent constructor
        parent::__construct($tabConfig);

        // Init URL route argument data, if required
        if(!is_null($objUrlRouteArgData))
        {
            $this->setObjUrlRouteArgData($objUrlRouteArgData);
        }

        // Init URL argument data, if required
        if(!is_null($objUrlArgData))
        {
            $this->setObjUrlArgData($objUrlArgData);
        }

        // Init header data, if required
        if(!is_null($objHeaderData))
        {
            $this->setObjHeaderData($objHeaderData);
        }

        // Init sending information, if required
        if(!is_null($tabSndInfo))
        {
            $this->setSndInfo($tabSndInfo);
        }
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstHttpRequest::DATA_KEY_DEFAULT_URL_ROUTE_ARG_DATA))
        {
            $this->__beanTabData[ConstHttpRequest::DATA_KEY_DEFAULT_URL_ROUTE_ARG_DATA] = null;
        }

        if(!$this->beanExists(ConstHttpRequest::DATA_KEY_DEFAULT_URL_ARG_DATA))
        {
            $this->__beanTabData[ConstHttpRequest::DATA_KEY_DEFAULT_URL_ARG_DATA] = null;
        }

        if(!$this->beanExists(ConstHttpRequest::DATA_KEY_DEFAULT_HEADER_DATA))
        {
            $this->__beanTabData[ConstHttpRequest::DATA_KEY_DEFAULT_HEADER_DATA] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstHttpRequest::DATA_KEY_DEFAULT_URL_ROUTE_ARG_DATA,
            ConstHttpRequest::DATA_KEY_DEFAULT_URL_ARG_DATA,
            ConstHttpRequest::DATA_KEY_DEFAULT_HEADER_DATA
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstHttpRequest::DATA_KEY_DEFAULT_URL_ROUTE_ARG_DATA:
                    UrlRouteArgDataInvalidFormatException::setCheck($value);
                    break;

                case ConstHttpRequest::DATA_KEY_DEFAULT_URL_ARG_DATA:
                    UrlArgDataInvalidFormatException::setCheck($value);
                    break;

                case ConstHttpRequest::DATA_KEY_DEFAULT_HEADER_DATA:
                    HeaderDataInvalidFormatException::setCheck($value);
                    break;

                case ConstRequest::DATA_KEY_DEFAULT_SND_INFO:
                    SndInfoInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getTabSndInfo()
    {
        // Init var
        $result = parent::getTabSndInfo();

        // Register URL route argument data, if required
        $objUrlRouteArgData = $this->getObjUrlRouteArgData();
        if(!is_null($objUrlRouteArgData))
        {
            $result[ConstHttpRequest::TAB_SND_INFO_KEY_URL_ROUTE_ARG] = $objUrlRouteArgData->getDataSrc();
        }

        // Register URL argument data, if required
        $objUrlArgData = $this->getObjUrlArgData();
        if(!is_null($objUrlArgData))
        {
            $result[ConstHttpRequest::TAB_SND_INFO_KEY_URL_ARG] = $objUrlArgData->getDataSrc();
        }

        // Register header data, if required
        $objHeaderData = $this->getObjHeaderData();
        if(!is_null($objHeaderData))
        {
            $result[ConstHttpRequest::TAB_SND_INFO_KEY_HEADER] = $objHeaderData->getDataSrc();
        }

        // Return result
        return $result;
    }



    /**
     * Get sending information value.
     *
     * @param string $strKey
     * @param mixed $default = null
     * @return mixed
     */
    protected function getSndInfoValue($strKey, $default = null)
    {
        // Init var
        $tabInfo = $this->getTabSndInfo();
        $result = (
            array_key_exists($strKey, $tabInfo) ?
                $tabInfo[$strKey] :
                $default
        );

        // Return result
        return $result;
    }



    /**
     * Get string protocol version number.
     *
     * @return null|string
     */
    public function getStrProtocolVersionNum()
    {
        // Return result
        return $this->getSndInfoValue(ConstHttpRequest::TAB_SND_INFO_KEY_PROTOCOL_VERSION_NUM);
    }



    /**
     * Get string method.
     *
     * @return null|string
     */
    public function getStrMethod()
    {
        // Return result
        return $this->getSndInfoValue(ConstHttpRequest::TAB_SND_INFO_KEY_METHOD);
    }



    /**
     * Get string URL schema.
     *
     * @return null|string
     */
    public function getStrUrlSchema()
    {
        // Return result
        return $this->getSndInfoValue(ConstHttpRequest::TAB_SND_INFO_KEY_URL_SCHEMA);
    }



    /**
     * Get string URL host.
     *
     * @return null|string
     */
    public function getStrUrlHost()
    {
        // Return result
        return $this->getSndInfoValue(ConstHttpRequest::TAB_SND_INFO_KEY_URL_HOST);
    }



    /**
     * Get integer URL port.
     *
     * @return null|integer
     */
    public function getIntUrlPort()
    {
        // Init var
        $result = $this->getSndInfoValue(ConstHttpRequest::TAB_SND_INFO_KEY_URL_PORT);
        $result = ((!is_null($result)) ? intval($result) : $result);

        // Return result
        return $result;
    }



    /**
     * Get string URL route.
     *
     * @return null|string
     */
    public function getStrUrlRoute()
    {
        // Return result
        return $this->getSndInfoValue(ConstHttpRequest::TAB_SND_INFO_KEY_URL_ROUTE);
    }



    /**
     * Get URL route arguments array.
     *
     * @return array
     */
    public function getTabUrlRouteArg()
    {
        // Return result
        return $this->getSndInfoValue(
            ConstHttpRequest::TAB_SND_INFO_KEY_URL_ROUTE_ARG,
            array()
        );
    }



    /**
     * Get string URL route argument specification.
     *
     * @return null|array
     */
    public function getTabUrlRouteArgSpec()
    {
        // Return result
        return $this->getSndInfoValue(
            ConstHttpRequest::TAB_SND_INFO_KEY_URL_ROUTE_ARG_SPEC,
            array()
        );
    }



    /**
     * Get URL arguments array.
     *
     * @return array
     */
    public function getTabUrlArg()
    {
        // Return result
        return $this->getSndInfoValue(
            ConstHttpRequest::TAB_SND_INFO_KEY_URL_ARG,
            array()
        );
    }



    /**
     * Get string URL.
     *
     * @return null|string
     */
    public function getStrUrl()
    {
        // Return result
        return ToolBoxHttpRequest::getStrUrl(
            $this->getStrUrlHost(),
            $this->getStrUrlSchema(),
            $this->getIntUrlPort(),
            $this->getStrUrlRoute(),
            $this->getTabUrlRouteArg(),
            $this->getTabUrlRouteArgSpec(),
            $this->getTabUrlArg()
        );
    }



    /**
     * Get headers array.
     *
     * @return array
     */
    public function getTabHeader()
    {
        // Return result
        return $this->getSndInfoValue(
            ConstHttpRequest::TAB_SND_INFO_KEY_HEADER,
            array()
        );
    }



    /**
     * Get string body.
     *
     * @return null|string
     */
    public function getStrBody()
    {
        // Return result
        return $this->getSndInfoValue(ConstHttpRequest::TAB_SND_INFO_KEY_BODY);
    }





    // Methods setters
    // ******************************************************************************

    /**
     * Set URL route argument data object.
     *
     * @param null|UrlRouteArgData $objUrlRouteArgData
     */
    public function setObjUrlRouteArgData($objUrlRouteArgData)
    {
        // Init var
        $objUrlRouteArgDataBase = $this->getObjUrlRouteArgData();

        // Set URL argument data
        $this->beanPut(ConstHttpRequest::DATA_KEY_DEFAULT_URL_ROUTE_ARG_DATA, $objUrlRouteArgData);

        // Init sending information URL arguments, if required
        $tabDataSrc = (
            (!is_null($objUrlRouteArgData)) ?
                $objUrlRouteArgData->getDataSrc() :
                (
                    (!is_null($objUrlRouteArgDataBase)) ?
                        $objUrlRouteArgDataBase->getDataSrc() :
                        null
                )
        );
        if(!is_null($tabDataSrc))
        {
            $this->setUrlRouteArg($tabDataSrc);
        }
    }



    /**
     * Set URL argument data object.
     *
     * @param null|UrlArgData $objUrlArgData
     */
    public function setObjUrlArgData($objUrlArgData)
    {
        // Init var
        $objUrlArgDataBase = $this->getObjUrlArgData();

        // Set URL argument data
        $this->beanPut(ConstHttpRequest::DATA_KEY_DEFAULT_URL_ARG_DATA, $objUrlArgData);

        // Init sending information URL arguments, if required
        $tabDataSrc = (
            (!is_null($objUrlArgData)) ?
                $objUrlArgData->getDataSrc() :
                (
                    (!is_null($objUrlArgDataBase)) ?
                        $objUrlArgDataBase->getDataSrc() :
                        null
                )
        );
        if(!is_null($tabDataSrc))
        {
            $this->setUrlArg($tabDataSrc);
        }
    }



    /**
     * Set header data object.
     *
     * @param null|HeaderData $objHeaderData
     */
    public function setObjHeaderData($objHeaderData)
    {
        // Init var
        $objHeaderDataBase = $this->getObjHeaderData();

        // Set header data
        $this->beanPut(ConstHttpRequest::DATA_KEY_DEFAULT_HEADER_DATA, $objHeaderData);

        // Init sending information headers, if required
        $tabDataSrc = (
            (!is_null($objHeaderData)) ?
                $objHeaderData->getDataSrc() :
                (
                    (!is_null($objHeaderDataBase)) ?
                        $objHeaderDataBase->getDataSrc() :
                        null
                )
        );
        if(!is_null($tabDataSrc))
        {
            $this->setHeader($tabDataSrc);
        }
    }



    /**
     * @inheritdoc
     */
    public function setSndInfo(array $tabInfo)
    {
        // Init URL, if required
        $strUrl = null;
        if(isset($tabInfo[ConstHttpRequest::TAB_SND_INFO_KEY_URL]))
        {
            $strUrl = (
                is_string($tabInfo[ConstHttpRequest::TAB_SND_INFO_KEY_URL]) ?
                    $tabInfo[ConstHttpRequest::TAB_SND_INFO_KEY_URL] :
                    $strUrl
            );
            unset($tabInfo[ConstHttpRequest::TAB_SND_INFO_KEY_URL]);
        }

        // Call parent method
        parent::setSndInfo($tabInfo);

        // Format sending information, if required
        if(!is_null($strUrl))
        {
            $this->setUrl($strUrl);
        }

        // Init URL route argument data, if required
        $objUrlRouteArgData = $this->getObjUrlRouteArgData();
        if(
            (!is_null($objUrlRouteArgData)) &&
            isset($tabInfo[ConstHttpRequest::TAB_SND_INFO_KEY_URL_ROUTE_ARG])
        )
        {
            $objUrlRouteArgData->setDataSrc($tabInfo[ConstHttpRequest::TAB_SND_INFO_KEY_URL_ROUTE_ARG]);
        }

        // Init URL argument data, if required
        $objUrlArgData = $this->getObjUrlArgData();
        if(
            (!is_null($objUrlArgData)) &&
            isset($tabInfo[ConstHttpRequest::TAB_SND_INFO_KEY_URL_ARG])
        )
        {
            $objUrlArgData->setDataSrc($tabInfo[ConstHttpRequest::TAB_SND_INFO_KEY_URL_ARG]);
        }

        // Init header data, if required
        $objHeaderData = $this->getObjHeaderData();
        if(
            (!is_null($objHeaderData)) &&
            isset($tabInfo[ConstHttpRequest::TAB_SND_INFO_KEY_HEADER])
        )
        {
            $objHeaderData->setDataSrc($tabInfo[ConstHttpRequest::TAB_SND_INFO_KEY_HEADER]);
        }
    }



    /**
     * Set specified sending information value.
     *
     * @param string $strKey
     * @param mixed $value
     * @return $this
     */
    protected function setSndInfoValue($strKey, $value)
    {
        // Init var
        $tabInfo = $this->beanGet(ConstRequest::DATA_KEY_DEFAULT_SND_INFO);

        // Set value
        $tabInfo[$strKey] = $value;

        // Set sending information
        $this->setSndInfo($tabInfo);

        // Return result
        return $this;
    }



    /**
     * Set specified protocol version number.
     *
     * @param null|string $strVersionNum
     * @return $this
     */
    public function setProtocolVersion($strVersionNum)
    {
        // Set protocol version number and return result
        return $this->setSndInfoValue(
            ConstHttpRequest::TAB_SND_INFO_KEY_PROTOCOL_VERSION_NUM,
            $strVersionNum
        );
    }



    /**
     * Set specified method.
     *
     * @param null|string $strMethod
     * @return $this
     */
    public function setMethod($strMethod)
    {
        // Set method and return result
        return $this->setSndInfoValue(
            ConstHttpRequest::TAB_SND_INFO_KEY_METHOD,
            $strMethod
        );
    }



    /**
     * Set specified URL schema.
     *
     * @param null|string $strSchema
     * @return $this
     */
    public function setUrlSchema($strSchema)
    {
        // Set URL schema and return result
        return $this->setSndInfoValue(
            ConstHttpRequest::TAB_SND_INFO_KEY_URL_SCHEMA,
            $strSchema
        );
    }



    /**
     * Set specified URL host.
     *
     * @param null|string $strHost
     * @return $this
     */
    public function setUrlHost($strHost)
    {
        // Set URL host and return result
        return $this->setSndInfoValue(
            ConstHttpRequest::TAB_SND_INFO_KEY_URL_HOST,
            $strHost
        );
    }



    /**
     * Set specified URL port.
     *
     * @param null|int $intPort
     * @return $this
     */
    public function setUrlPort($intPort)
    {
        // Set URL port and return result
        return $this->setSndInfoValue(
            ConstHttpRequest::TAB_SND_INFO_KEY_URL_PORT,
            $intPort
        );
    }



    /**
     * Set specified URL route.
     *
     * @param null|string $strRoute
     * @return $this
     */
    public function setUrlRoute($strRoute)
    {
        // Set URL route and return result
        return $this->setSndInfoValue(
            ConstHttpRequest::TAB_SND_INFO_KEY_URL_ROUTE,
            $strRoute
        );
    }



    /**
     * Set specified URL route arguments array.
     *
     * @param array $tabArg
     * @return $this
     */
    public function setUrlRouteArg(array $tabArg)
    {
        // Set URL route arguments and return result
        return $this->setSndInfoValue(
            ConstHttpRequest::TAB_SND_INFO_KEY_URL_ROUTE_ARG,
            $tabArg
        );
    }



    /**
     * Set specified URL route argument specification.
     *
     * @param null|array $tabRouteArgSpec
     * @return $this
     */
    public function setUrlRouteArgSpec($tabRouteArgSpec)
    {
        // Set URL route argument specification and return result
        return $this->setSndInfoValue(
            ConstHttpRequest::TAB_SND_INFO_KEY_URL_ROUTE_ARG_SPEC,
            $tabRouteArgSpec
        );
    }



    /**
     * Set specified URL arguments array.
     *
     * @param array $tabArg
     * @return $this
     */
    public function setUrlArg(array $tabArg)
    {
        // Set URL arguments and return result
        return $this->setSndInfoValue(
            ConstHttpRequest::TAB_SND_INFO_KEY_URL_ARG,
            $tabArg
        );
    }



    /**
     * Set specified URL.
     *
     * @param string $url
     * @return $this
     */
    public function setUrl($url)
    {
        // Init var
        $tabUrlInfo = @parse_url($url);

        if(is_array($tabUrlInfo))
        {
            // Set URL schema, if required
            if(isset($tabUrlInfo['scheme']))
            {
                $this->setUrlSchema($tabUrlInfo['scheme']);
            }

            // Set URL host, if required
            if(isset($tabUrlInfo['host']))
            {
                $this->setUrlHost($tabUrlInfo['host']);
            }

            // Set URL port, if required
            if(isset($tabUrlInfo['port']))
            {
                $this->setUrlPort($tabUrlInfo['port']);
            }

            // Set URL route, if required
            if(isset($tabUrlInfo['path']))
            {
                $this->setUrlRoute($tabUrlInfo['path']);
            }

            // Set URL arguments, if required
            if(isset($tabUrlInfo['query']))
            {
                $tabArg = array();
                parse_str($tabUrlInfo['query'], $tabArg);
                $this->setUrlArg($tabArg);
            }
        }

        // Return result
        return $this;
    }



    /**
     * Set specified headers array.
     *
     * @param array $tabHeader
     * @return $this
     */
    public function setHeader(array $tabHeader)
    {
        // Set headers and return result
        return $this->setSndInfoValue(
            ConstHttpRequest::TAB_SND_INFO_KEY_HEADER,
            $tabHeader
        );
    }



    /**
     * Set specified body.
     *
     * @param null|string $strBody
     * @return $this
     */
    public function setBody($strBody)
    {
        // Set body and return result
        return $this->setSndInfoValue(
            ConstHttpRequest::TAB_SND_INFO_KEY_BODY,
            $strBody
        );
    }



}