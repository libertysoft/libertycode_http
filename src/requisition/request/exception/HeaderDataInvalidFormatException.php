<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\requisition\request\exception;

use Exception;

use liberty_code\http\header\model\HeaderData;
use liberty_code\http\requisition\request\library\ConstHttpRequest;



class HeaderDataInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $data
     */
	public function __construct($data)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstHttpRequest::EXCEPT_MSG_HEADER_DATA_INVALID_FORMAT,
            mb_strimwidth(strval($data), 0, 50, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified data has valid format.
	 * 
     * @param mixed $data
	 * @return boolean
	 * @throws static
     */
	public static function setCheck($data)
    {
		// Init var
		$result = (
		    is_null($data) ||
            ($data instanceof HeaderData)
        );

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($data);
		}
		
		// Return result
		return $result;
    }
	
	
	
}