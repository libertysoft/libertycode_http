<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\requisition\request\exception;

use Exception;

use liberty_code\http\url_route\library\ToolBoxUrlRoute;
use liberty_code\http\url_route\argument\exception\DataSrcInvalidFormatException as UrlRouteArgDataSrcInvalidFormatException;
use liberty_code\http\url_arg\exception\DataSrcInvalidFormatException as UrlArgDataSrcInvalidFormatException;
use liberty_code\http\header\exception\DataSrcInvalidFormatException as HeaderDataSrcInvalidFormatException;
use liberty_code\http\requisition\request\library\ConstHttpRequest;



class SndInfoInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $info
     */
	public function __construct($info)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstHttpRequest::EXCEPT_MSG_SND_INFO_INVALID_FORMAT,
            mb_strimwidth(strval($info), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified info has valid format.
     *
     * @param mixed $info
     * @return boolean
     */
    public static function checkInfoIsValid($info)
    {
        // Init var
        $result =
            // Check valid array
            is_array($info) &&

            // Check valid protocol version number
            (
                (!isset($info[ConstHttpRequest::TAB_SND_INFO_KEY_PROTOCOL_VERSION_NUM])) ||
                (
                    is_string($info[ConstHttpRequest::TAB_SND_INFO_KEY_PROTOCOL_VERSION_NUM]) &&
                    (trim($info[ConstHttpRequest::TAB_SND_INFO_KEY_PROTOCOL_VERSION_NUM]) != '')
                )
            ) &&

            // Check valid method
            (
                (!isset($info[ConstHttpRequest::TAB_SND_INFO_KEY_METHOD])) ||
                (
                    is_string($info[ConstHttpRequest::TAB_SND_INFO_KEY_METHOD]) &&
                    (trim($info[ConstHttpRequest::TAB_SND_INFO_KEY_METHOD]) != '') &&
                    ctype_alpha($info[ConstHttpRequest::TAB_SND_INFO_KEY_METHOD])
                )
            ) &&

            // Check valid URL schema
            (
                (!isset($info[ConstHttpRequest::TAB_SND_INFO_KEY_URL_SCHEMA])) ||
                (
                    is_string($info[ConstHttpRequest::TAB_SND_INFO_KEY_URL_SCHEMA]) &&
                    in_array(
                        $info[ConstHttpRequest::TAB_SND_INFO_KEY_URL_SCHEMA],
                        array(ConstHttpRequest::SCHEMA_HTTP, ConstHttpRequest::SCHEMA_HTTPS)
                    )
                )
            ) &&

            // Check valid URL host
            (
                (!isset($info[ConstHttpRequest::TAB_SND_INFO_KEY_URL_HOST])) ||
                (
                    is_string($info[ConstHttpRequest::TAB_SND_INFO_KEY_URL_HOST]) &&
                    (trim($info[ConstHttpRequest::TAB_SND_INFO_KEY_URL_HOST]) != '')
                )
            ) &&

            // Check valid URL port
            (
                (!isset($info[ConstHttpRequest::TAB_SND_INFO_KEY_URL_PORT])) ||
                (
                    (
                        (
                            is_string($info[ConstHttpRequest::TAB_SND_INFO_KEY_URL_PORT]) &&
                            ctype_digit($info[ConstHttpRequest::TAB_SND_INFO_KEY_URL_PORT])
                        ) ||
                        is_int($info[ConstHttpRequest::TAB_SND_INFO_KEY_URL_PORT])
                    ) &&
                    (intval($info[ConstHttpRequest::TAB_SND_INFO_KEY_URL_PORT]) > 0)
                )
            ) &&

            // Check valid URL route
            (
                (!isset($info[ConstHttpRequest::TAB_SND_INFO_KEY_URL_ROUTE])) ||
                (
                    is_string($info[ConstHttpRequest::TAB_SND_INFO_KEY_URL_ROUTE]) &&
                    (trim($info[ConstHttpRequest::TAB_SND_INFO_KEY_URL_ROUTE]) != '')
                )
            ) &&

            // Check valid URL route arguments
            (
                (!isset($info[ConstHttpRequest::TAB_SND_INFO_KEY_URL_ROUTE_ARG])) ||
                UrlRouteArgDataSrcInvalidFormatException::checkDataSrcIsValid($info[ConstHttpRequest::TAB_SND_INFO_KEY_URL_ROUTE_ARG])
            ) &&

            // Check valid URL route argument specification
            (
                (!isset($info[ConstHttpRequest::TAB_SND_INFO_KEY_URL_ROUTE_ARG_SPEC])) ||
                (
                    is_array($info[ConstHttpRequest::TAB_SND_INFO_KEY_URL_ROUTE_ARG_SPEC]) &&
                    ToolBoxUrlRoute::checkArgSpecIsValid($info[ConstHttpRequest::TAB_SND_INFO_KEY_URL_ROUTE_ARG_SPEC])
                )
            ) &&

            // Check valid URL arguments
            (
                (!isset($info[ConstHttpRequest::TAB_SND_INFO_KEY_URL_ARG])) ||
                UrlArgDataSrcInvalidFormatException::checkDataSrcIsValid($info[ConstHttpRequest::TAB_SND_INFO_KEY_URL_ARG])
            ) &&

            // Check valid headers
            (
                (!isset($info[ConstHttpRequest::TAB_SND_INFO_KEY_HEADER])) ||
                HeaderDataSrcInvalidFormatException::checkDataSrcIsValid($info[ConstHttpRequest::TAB_SND_INFO_KEY_HEADER])
            ) &&

            // Check valid body
            (
                (!isset($info[ConstHttpRequest::TAB_SND_INFO_KEY_BODY])) ||
                is_string($info[ConstHttpRequest::TAB_SND_INFO_KEY_BODY])
            );

        // Return result
        return $result;
    }



	/**
	 * Check if specified info has valid format.
	 * 
     * @param mixed $info
	 * @return boolean
	 * @throws static
     */
    public static function setCheck($info)
    {
		// Init var
		$result =
            // Check valid info
            static::checkInfoIsValid($info);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($info) ? serialize($info) : $info));
		}
		
		// Return result
		return $result;
    }
	
	
	
}