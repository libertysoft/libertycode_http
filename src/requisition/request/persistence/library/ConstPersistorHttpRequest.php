<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\requisition\request\persistence\library;



class ConstPersistorHttpRequest
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration
    const TAB_CONFIG_KEY_PATH_SEPARATOR = 'path_separator';
    const TAB_CONFIG_KEY_DATA_ID_KEY = 'data_id_key';
    const TAB_CONFIG_KEY_ID_SUPPORT_TYPE = 'id_support_type';
    const TAB_CONFIG_KEY_ID_KEY = 'id_key';
    const TAB_CONFIG_KEY_MULTI_ID_SUPPORT_TYPE = 'multi_id_support_type';
    const TAB_CONFIG_KEY_MULTI_ID_KEY = 'multi_id_key';
    const TAB_CONFIG_KEY_QUERY_SUPPORT_TYPE = 'query_support_type';
    const TAB_CONFIG_KEY_QUERY_DATA_PATH = 'query_data_path';
    const TAB_CONFIG_KEY_CREATE_DATA_SUPPORT_TYPE = 'create_data_support_type';
    const TAB_CONFIG_KEY_CREATE_DATA_PATH = 'create_data_path';
    const TAB_CONFIG_KEY_CREATE_MULTI_DATA_SUPPORT_TYPE = 'create_multi_data_support_type';
    const TAB_CONFIG_KEY_CREATE_MULTI_DATA_PATH = 'create_multi_data_path';
    const TAB_CONFIG_KEY_UPDATE_DATA_SUPPORT_TYPE = 'update_data_support_type';
    const TAB_CONFIG_KEY_UPDATE_DATA_PATH = 'update_data_path';
    const TAB_CONFIG_KEY_UPDATE_ID_SUPPORT_TYPE = 'update_id_support_type';
    const TAB_CONFIG_KEY_UPDATE_ID_KEY = 'update_id_key';
    const TAB_CONFIG_KEY_UPDATE_MULTI_DATA_SUPPORT_TYPE = 'update_multi_data_support_type';
    const TAB_CONFIG_KEY_UPDATE_MULTI_DATA_PATH = 'update_multi_data_path';
    const TAB_CONFIG_KEY_DELETE_DATA_SUPPORT_TYPE = 'delete_data_support_type';
    const TAB_CONFIG_KEY_DELETE_DATA_PATH = 'delete_data_path';
    const TAB_CONFIG_KEY_DELETE_ID_SUPPORT_TYPE = 'delete_id_support_type';
    const TAB_CONFIG_KEY_DELETE_ID_KEY = 'delete_id_key';
    const TAB_CONFIG_KEY_DELETE_MULTI_DATA_SUPPORT_TYPE = 'delete_multi_data_support_type';
    const TAB_CONFIG_KEY_DELETE_MULTI_DATA_PATH = 'delete_multi_data_path';
    const TAB_CONFIG_KEY_DELETE_MULTI_ID_SUPPORT_TYPE = 'delete_multi_id_support_type';
    const TAB_CONFIG_KEY_DELETE_MULTI_ID_KEY = 'delete_multi_id_key';

    // Support type configuration
    const CONFIG_SUPPORT_TYPE_URL_ROUTE_ARG = 'url_route_argument';
    const CONFIG_SUPPORT_TYPE_URL_ARG = 'url_argument';
    const CONFIG_SUPPORT_TYPE_BODY = 'body';


	
    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the persistor HTTP request configuration standard.';
    const EXCEPT_MSG_ID_INVALID_FORMAT =
        'Following identifier "%1$s" invalid! 
        The identifier must be a valid value, depending on which support type configured.';
    const EXCEPT_MSG_QUERY_INVALID_FORMAT = 'Following query "%1$s" invalid! The query must be an array.';
    const EXCEPT_MSG_TAB_DATA_INVALID_FORMAT =
        'Following array of data "%1$s" invalid! 
        It must be an array of data, where each data must be an array.';





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get configuration array of id support types.
     *
     * @return array
     */
    public static function getTabConfigIdSupportType()
    {
        // Init var
        $result = array(
            self::CONFIG_SUPPORT_TYPE_URL_ROUTE_ARG,
            self::CONFIG_SUPPORT_TYPE_URL_ARG,
            self::CONFIG_SUPPORT_TYPE_BODY
        );

        // Return result
        return $result;
    }



    /**
     * Get configuration array of data support types.
     *
     * @return array
     */
    public static function getTabConfigDataSupportType()
    {
        // Init var
        $result = array(
            self::CONFIG_SUPPORT_TYPE_URL_ARG,
            self::CONFIG_SUPPORT_TYPE_BODY
        );

        // Return result
        return $result;
    }



}