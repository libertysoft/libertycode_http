<?php
/**
 * Description :
 * This class allows to define persistor HTTP request class.
 * Persistor HTTP request is data HTTP request,
 * allowing to design persistor request,
 * to set all information, required for sending to storage support, via HTTP protocol.
 *
 * Persistor HTTP request uses the following specified configuration:
 * [
 *     Data HTTP request configuration,
 *
 *     path_separator(optional: got "/", if not found): "string path separator",
 *
 *     data_id_key(optional): "string id key, used on provided data",
 *
 *     id_support_type(optional: got "url_argument", if not found):
 *         "string support type, to set id to search.
 *         Scope of available values: @see ConstPersistorHttpRequest::getTabConfigIdSupportType()",
 *
 *     id_key(optional: required only when set identifier): "string key (path or key), to set id to search",
 *
 *     multi_id_support_type(optional:
 *         - got id_support_type, if not found.
 *         - got "url_argument", if id_support_type not found or invalid.
 *     ):
 *         "string support type, to set array of ids to search.
 *         Scope of available values: @see ConstPersistorHttpRequest::getTabConfigDataSupportType()",
 *
 *     multi_id_key(optional:
 *         - got id_key, if not found.
 *         - required only when set array of identifiers and if id_key not found.
 *     ): "string key (path or key), to set array of ids to search",
 *
 *     query_support_type(optional: got "url_argument", if not found):
 *         "string support type, to set searching query.
 *         Scope of available values: @see ConstPersistorHttpRequest::getTabConfigDataSupportType()",
 *
 *     query_data_path (optional: got "", if not found):
 *         "string path, to set searching query",
 *
 *     create_data_support_type(optional: got "body", if not found):
 *         "string support type, to set data to create.
 *         Scope of available values: @see ConstPersistorHttpRequest::getTabConfigDataSupportType()",
 *
 *     create_data_path (optional: got "", if not found):
 *         "string path, to set data to create",
 *
 *     create_multi_data_support_type(optional: got create_data_support_type, if not found):
 *         "string support type, to set array of data to create.
 *         Scope of available values: @see ConstPersistorHttpRequest::getTabConfigDataSupportType()",
 *
 *     create_multi_data_path (optional: got create_data_path, if not found):
 *         "string path, to set array of data to create",
 *
 *     update_data_support_type(optional: got "body", if not found):
 *         "string support type, to set data to update.
 *         Scope of available values: @see ConstPersistorHttpRequest::getTabConfigDataSupportType()",
 *
 *     update_data_path (optional: got "", if not found):
 *         "string path, to set data to update",
 *
 *     update_id_support_type(optional: got "url_argument", if not found):
 *         "string support type, to set id to update.
 *         Scope of available values: @see ConstPersistorHttpRequest::getTabConfigIdSupportType()",
 *
 *     update_id_key(optional): "string key (path or key), to set id to update",
 *
 *     update_multi_data_support_type(optional: got update_data_support_type, if not found):
 *         "string support type, to set array of data to update.
 *         Scope of available values: @see ConstPersistorHttpRequest::getTabConfigDataSupportType()",
 *
 *     update_multi_data_path (optional: got update_data_path, if not found):
 *         "string path, to set array of data to update",
 *
 *     delete_data_support_type(optional: got "body", if not found):
 *         "string support type, to set data to delete.
 *         Scope of available values: @see ConstPersistorHttpRequest::getTabConfigDataSupportType()",
 *
 *     delete_data_path (optional: got "", if not found):
 *         "string path, to set data to delete",
 *
 *     delete_id_support_type(optional: got "url_argument", if not found):
 *         "string support type, to set id to delete.
 *         Scope of available values: @see ConstPersistorHttpRequest::getTabConfigIdSupportType()",
 *
 *     delete_id_key(optional): "string key (path or key), to set id to delete",
 *
 *     delete_multi_data_support_type(optional: got delete_data_support_type, if not found):
 *         "string support type, to set array of data to delete.
 *         Scope of available values: @see ConstPersistorHttpRequest::getTabConfigDataSupportType()",
 *
 *     delete_multi_data_path (optional: got delete_data_path, if not found):
 *         "string path, to set array of data to delete",
 *
 *     delete_multi_id_support_type(optional:
 *         - got delete_id_support_type, if not found.
 *         - got "url_argument", if delete_id_support_type not found or invalid.
 *     ):
 *         "string support type, to set array of ids to delete.
 *         Scope of available values: @see ConstPersistorHttpRequest::getTabConfigDataSupportType()",
 *
 *     delete_multi_id_key(optional): "string key (path or key), to set array of ids to delete"
 * ]
 *
 * Persistor HTTP request handles the following specified sending information:
 * [
 *     Data HTTP request sending information
 * ]
 *
 * Note:
 * -> Configuration support type:
 *     - "url_route":
 *         Set data on route, where data key is replaced by data value.
 *         Generally used for one specific data (ex: id).
 *     - "url_argument":
 *         Data put on URL arguments array.
 *     - "body":
 *         Data put on body, as body data array.
 * -> Configuration update_id_key:
 *     If set, try to set id (in addition to set data), on request, to update data.
 *     Id is removed from data, before data setting.
 *     Else, set directly data on request, to update data.
 * -> Configuration delete_id_key, delete_multi_id_key:
 *     If set, try to set id (instead of data) on request, to delete data.
 *     Else, set directly data on request, to delete data.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\requisition\request\persistence\model;

use liberty_code\requisition\request\persistence\api\PersistorRequestInterface;
use liberty_code\http\requisition\request\data\model\DataHttpRequest;

use liberty_code\data\data\table\path\library\ToolBoxPathTableData;
use liberty_code\requisition\request\library\ConstRequest;
use liberty_code\http\requisition\request\persistence\library\ConstPersistorHttpRequest;
use liberty_code\http\requisition\request\persistence\exception\ConfigInvalidFormatException;
use liberty_code\http\requisition\request\persistence\exception\IdInvalidFormatException;
use liberty_code\http\requisition\request\persistence\exception\QueryInvalidFormatException;
use liberty_code\http\requisition\request\persistence\exception\TabDataInvalidFormatException;



class PersistorHttpRequest extends DataHttpRequest implements PersistorRequestInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();


	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        // $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstRequest::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get specified configuration value.
     *
     * @param string $strConfigKey
     * @param null|string $strDefaultConfigKey = null
     * @param null|mixed $default = null
     * @return null|mixed
     * @throws ConfigInvalidFormatException
     */
    protected function getConfigValue(
        $strConfigKey,
        $strDefaultConfigKey = null,
        $default = null
    )
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            isset($tabConfig[$strConfigKey]) ?
                $tabConfig[$strConfigKey] :
                (
                    isset($tabConfig[$strDefaultConfigKey]) ?
                        $tabConfig[$strDefaultConfigKey] :
                        $default
                )
        );

        // Return result
        return $result;
    }



    /**
     * Get string path separator.
     *
     * @return string
     */
    protected function getStrPathSeparator()
    {
        // Return result
        return $this->getConfigValue(
            ConstPersistorHttpRequest::TAB_CONFIG_KEY_PATH_SEPARATOR,
            null,
            '/'
        );
    }



    /**
     * Get specified data id key.
     *
     * @return null|string
     */
    protected function getStrDataIdKey()
    {
        // Return result
        return $this->getConfigValue(
            ConstPersistorHttpRequest::TAB_CONFIG_KEY_DATA_ID_KEY
        );
    }



    /**
     * Get specified support type.
     *
     * @param string $strConfigKey
     * @param array $tabValidSupportType
     * @param null|string $strDefaultConfigKey = null
     * @param null|string $strDefault = null
     * @return string
     * @throws ConfigInvalidFormatException
     */
    protected function getStrSupportType(
        $strConfigKey,
        array $tabValidSupportType,
        $strDefaultConfigKey = null,
        $strDefault = null
    )
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = $this->getConfigValue($strConfigKey, $strDefaultConfigKey, $strDefault);
        $result = (in_array($result, $tabValidSupportType) ? $result : $strDefault);

        // Check valid support type
        if(is_null($result))
        {
            throw new ConfigInvalidFormatException(serialize($tabConfig));
        }

        // Return result
        return $result;
    }



    /**
     * Get specified id key,
     * used on request.
     *
     * @param string $strConfigKey
     * @param null|string $strDefaultConfigKey = null
     * @param boolean $boolRequired = false
     * @return null|string
     * @throws ConfigInvalidFormatException
     */
    protected function getStrIdKey(
        $strConfigKey,
        $strDefaultConfigKey = null,
        $boolRequired = false
    )
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = $this->getConfigValue($strConfigKey, $strDefaultConfigKey);

        // Check valid id key
        if($boolRequired && is_null($result))
        {
            throw new ConfigInvalidFormatException(serialize($tabConfig));
        }

        // Return result
        return $result;
    }



    /**
     * Get specified id,
     * from specified data.
     *
     * @param array $data
     * @return mixed
     * @throws ConfigInvalidFormatException
     */
    protected function getDataId(array $data)
    {
        // Init var
        $strIdKey = $this->getStrDataIdKey();
        $result = (
            (
                (!is_null($strIdKey)) &&
                isset($data[$strIdKey])
            ) ?
                $data[$strIdKey] :
                null
        );

        // Check valid id
        if(is_null($result))
        {
            throw new ConfigInvalidFormatException(serialize($this->getTabConfig()));
        }

        // Return result
        return $result;
    }



    /**
     * Get specified index array of ids,
     * from specified index array of data.
     *
     * @param array $tabData
     * @return array
     */
    protected function getTabDataId(array $tabData)
    {
        // Init var
        $result = array_values(array_map(
            function(array $data) {
                return $this->getDataId($data);
            },
            $tabData
        ));

        // Return result
        return $result;
    }





    // Methods setters
    // ******************************************************************************

    /**
     * Set specified data,
     * on specified support.
     *
     * @param array $data
     * @param string $strSupportType
     * @param null|string $strPath = null
     */
    protected function setDataEngine(
        array $data,
        $strSupportType,
        $strPath = null
    )
    {
        // Init get formatted data function
        $getDataFormat = function(array $tabBaseData) use ($data, $strPath) {
            $result = $tabBaseData;

            // Data added on specified base data, on specified location, if required
            if(!is_null($strPath))
            {
                // Get data
                $previousData = ToolBoxPathTableData::getValue(
                    $result,
                    $strPath,
                    $this->getStrPathSeparator()
                );
                $previousData = (
                    is_null($previousData) ?
                        array() :
                        (
                            is_array($previousData) ?
                                $previousData :
                                array($previousData)
                        )
                );
                $data = array_merge(
                    $previousData,
                    $data
                );

                // Set data on specified location
                ToolBoxPathTableData::setValue(
                    $result,
                    $strPath,
                    $data,
                    $this->getStrPathSeparator(),
                    true
                );
            }
            // Else: Data added on specified base data, directly on root
            else
            {
                $result = array_merge(
                    $result,
                    $data
                );
            }

            return $result;
        };

        // Process by support
        switch($strSupportType)
        {
            case ConstPersistorHttpRequest::CONFIG_SUPPORT_TYPE_URL_ARG:
                $data = $getDataFormat($this->getTabUrlArg());
                $this->setUrlArg($data);
                break;

            case ConstPersistorHttpRequest::CONFIG_SUPPORT_TYPE_BODY:
                $data = $getDataFormat((is_null($tabData = $this->getTabBodyData()) ? array() : $tabData));
                $this->setBodyData($data);
                break;
        }
    }



    /**
     * Set specified id(s),
     * on specified support.
     *
     * @param mixed $strKey
     * @param mixed|array $id
     * @param string $strSupportType
     * @throws IdInvalidFormatException
     */
    protected function setIdEngine($strKey, $id, $strSupportType)
    {
        // Init var
        $id = (is_array($id) ? array_values($id) : $id);

        // Set id(s), depending on support type
        switch($strSupportType)
        {
            case ConstPersistorHttpRequest::CONFIG_SUPPORT_TYPE_URL_ROUTE_ARG:
                IdInvalidFormatException::setCheck($id);
                //$strUrl = str_replace($strKey, strval($id), $this->getStrUrl());
                //$this->setUrl($strUrl);

                // Set URL route argument
                $tabArg = $this->getTabUrlRouteArg();
                $tabArg[$strKey] = $id;
                $this->setUrlRouteArg($tabArg);

                // Set URL route argument specification, if required
                $tabArgSpec = (is_null($tabArgSpec = $this->getTabUrlRouteArgSpec()) ? array() : $tabArgSpec);
                if(!array_key_exists($strKey, $tabArgSpec))
                {
                    $tabArgSpec[$strKey] = null;
                    $this->setUrlRouteArgSpec($tabArgSpec);
                }
                break;

            case ConstPersistorHttpRequest::CONFIG_SUPPORT_TYPE_URL_ARG:
                $tabArg = $this->getTabUrlArg();
                ToolBoxPathTableData::setValue(
                    $tabArg,
                    $strKey,
                    $id,
                    $this->getStrPathSeparator(),
                    true
                );
                $this->setUrlArg($tabArg);
                break;

            case ConstPersistorHttpRequest::CONFIG_SUPPORT_TYPE_BODY:
                $tabData = (is_null($tabData = $this->getTabBodyData()) ? array() : $tabData);
                ToolBoxPathTableData::setValue(
                    $tabData,
                    $strKey,
                    $id,
                    $this->getStrPathSeparator(),
                    true
                );
                $this->setBodyData($tabData);
                break;
        }
    }



    /**
     * @inheritdoc
     */
    public function setId($id)
    {
        // Init var
        $strSupportType = $this->getStrSupportType(
            ConstPersistorHttpRequest::TAB_CONFIG_KEY_ID_SUPPORT_TYPE,
            ConstPersistorHttpRequest::getTabConfigIdSupportType(),
            null,
            ConstPersistorHttpRequest::CONFIG_SUPPORT_TYPE_URL_ARG
        );
        $strKey = $this->getStrIdKey(
            ConstPersistorHttpRequest::TAB_CONFIG_KEY_ID_KEY,
            null,
            true
        );

        // Set id
        $this->setIdEngine($strKey, $id, $strSupportType);
    }



    /**
     * @inheritdoc
     */
    public function setTabId(array $tabId)
    {
        // Init var
        $strSupportType = $this->getStrSupportType(
            ConstPersistorHttpRequest::TAB_CONFIG_KEY_MULTI_ID_SUPPORT_TYPE,
            ConstPersistorHttpRequest::getTabConfigDataSupportType(),
            ConstPersistorHttpRequest::TAB_CONFIG_KEY_ID_SUPPORT_TYPE,
            ConstPersistorHttpRequest::CONFIG_SUPPORT_TYPE_URL_ARG
        );
        $strKey = $this->getStrIdKey(
            ConstPersistorHttpRequest::TAB_CONFIG_KEY_MULTI_ID_KEY,
            ConstPersistorHttpRequest::TAB_CONFIG_KEY_ID_KEY,
            true
        );

        // Set array of ids
        $this->setIdEngine($strKey, $tabId, $strSupportType);
    }



    /**
     * @inheritdoc
     * @throws QueryInvalidFormatException
     */
    public function setQuery($query)
    {
        // Check valid args
        QueryInvalidFormatException::setCheck($query);

        // Init var
        $strSupportType = $this->getStrSupportType(
            ConstPersistorHttpRequest::TAB_CONFIG_KEY_QUERY_SUPPORT_TYPE,
            ConstPersistorHttpRequest::getTabConfigDataSupportType(),
            null,
            ConstPersistorHttpRequest::CONFIG_SUPPORT_TYPE_URL_ARG
        );
        $strPath = $this->getConfigValue(
            ConstPersistorHttpRequest::TAB_CONFIG_KEY_QUERY_DATA_PATH
        );

        // Set query
        $this->setDataEngine($query, $strSupportType, $strPath);
    }



    /**
     * @inheritdoc
     */
    public function setCreateData(array $data)
    {
        // Init var
        $strSupportType = $this->getStrSupportType(
            ConstPersistorHttpRequest::TAB_CONFIG_KEY_CREATE_DATA_SUPPORT_TYPE,
            ConstPersistorHttpRequest::getTabConfigDataSupportType(),
            null,
            ConstPersistorHttpRequest::CONFIG_SUPPORT_TYPE_BODY
        );
        $strPath = $this->getConfigValue(
            ConstPersistorHttpRequest::TAB_CONFIG_KEY_CREATE_DATA_PATH
        );

        // Set data
        $this->setDataEngine($data, $strSupportType, $strPath);
    }



    /**
     * @inheritdoc
     * @throws TabDataInvalidFormatException
     */
    public function setCreateTabData(array $tabData)
    {
        // Check valid args
        TabDataInvalidFormatException::setCheck($tabData);

        // Init var
        $strSupportType = $this->getStrSupportType(
            ConstPersistorHttpRequest::TAB_CONFIG_KEY_CREATE_MULTI_DATA_SUPPORT_TYPE,
            ConstPersistorHttpRequest::getTabConfigDataSupportType(),
            ConstPersistorHttpRequest::TAB_CONFIG_KEY_CREATE_DATA_SUPPORT_TYPE,
            ConstPersistorHttpRequest::CONFIG_SUPPORT_TYPE_BODY
        );
        $strPath = $this->getConfigValue(
            ConstPersistorHttpRequest::TAB_CONFIG_KEY_CREATE_MULTI_DATA_PATH,
            ConstPersistorHttpRequest::TAB_CONFIG_KEY_CREATE_DATA_PATH
        );

        // Set array of data
        $this->setDataEngine($tabData, $strSupportType, $strPath);
    }



    /**
     * @inheritdoc
     */
    public function setUpdateData(array $data)
    {
        // Init var
        $strDataSupportType = $this->getStrSupportType(
            ConstPersistorHttpRequest::TAB_CONFIG_KEY_UPDATE_DATA_SUPPORT_TYPE,
            ConstPersistorHttpRequest::getTabConfigDataSupportType(),
            null,
            ConstPersistorHttpRequest::CONFIG_SUPPORT_TYPE_BODY
        );
        $strDataPath = $this->getConfigValue(
            ConstPersistorHttpRequest::TAB_CONFIG_KEY_UPDATE_DATA_PATH
        );
        $strIdKey = $this->getStrIdKey(ConstPersistorHttpRequest::TAB_CONFIG_KEY_UPDATE_ID_KEY);

        // Set id, if required
        if(!is_null($strIdKey))
        {
            // Get info
            $strIdSupportType = $this->getStrSupportType(
                ConstPersistorHttpRequest::TAB_CONFIG_KEY_UPDATE_ID_SUPPORT_TYPE,
                ConstPersistorHttpRequest::getTabConfigIdSupportType(),
                null,
                ConstPersistorHttpRequest::CONFIG_SUPPORT_TYPE_URL_ARG
            );
            $id = $this->getDataId($data);

            // Set id
            $this->setIdEngine($strIdKey, $id, $strIdSupportType);

            // Format data (remove id)
            $strDataIdKey = $this->getStrDataIdKey();
            unset($data[$strDataIdKey]);
        }

        // Set data
        $this->setDataEngine($data, $strDataSupportType, $strDataPath);
    }



    /**
     * @inheritdoc
     * @throws TabDataInvalidFormatException
     */
    public function setUpdateTabData(array $tabData)
    {
        // Check valid args
        TabDataInvalidFormatException::setCheck($tabData);

        // Init var
        $strSupportType = $this->getStrSupportType(
            ConstPersistorHttpRequest::TAB_CONFIG_KEY_UPDATE_MULTI_DATA_SUPPORT_TYPE,
            ConstPersistorHttpRequest::getTabConfigDataSupportType(),
            ConstPersistorHttpRequest::TAB_CONFIG_KEY_UPDATE_DATA_SUPPORT_TYPE,
            ConstPersistorHttpRequest::CONFIG_SUPPORT_TYPE_BODY
        );
        $strPath = $this->getConfigValue(
            ConstPersistorHttpRequest::TAB_CONFIG_KEY_UPDATE_MULTI_DATA_PATH,
            ConstPersistorHttpRequest::TAB_CONFIG_KEY_UPDATE_DATA_PATH
        );

        // Set array of data
        $this->setDataEngine($tabData, $strSupportType, $strPath);
    }



    /**
     * @inheritdoc
     */
    public function setDeleteData(array $data)
    {
        // Init var
        $strIdKey = $this->getStrIdKey(ConstPersistorHttpRequest::TAB_CONFIG_KEY_DELETE_ID_KEY);

        // Set id, if required
        if(!is_null($strIdKey))
        {
            // Get info
            $strIdSupportType = $this->getStrSupportType(
                ConstPersistorHttpRequest::TAB_CONFIG_KEY_DELETE_ID_SUPPORT_TYPE,
                ConstPersistorHttpRequest::getTabConfigIdSupportType(),
                null,
                ConstPersistorHttpRequest::CONFIG_SUPPORT_TYPE_URL_ARG
            );
            $id = $this->getDataId($data);

            // Set id
            $this->setIdEngine($strIdKey, $id, $strIdSupportType);
        }
        // Else: set data
        else
        {
            // Get info
            $strDataSupportType = $this->getStrSupportType(
                ConstPersistorHttpRequest::TAB_CONFIG_KEY_DELETE_DATA_SUPPORT_TYPE,
                ConstPersistorHttpRequest::getTabConfigDataSupportType(),
                null,
                ConstPersistorHttpRequest::CONFIG_SUPPORT_TYPE_BODY
            );
            $strDataPath = $this->getConfigValue(
                ConstPersistorHttpRequest::TAB_CONFIG_KEY_DELETE_DATA_PATH
            );

            // Set data
            $this->setDataEngine($data, $strDataSupportType, $strDataPath);
        }
    }



    /**
     * @inheritdoc
     * @throws TabDataInvalidFormatException
     */
    public function setDeleteTabData(array $tabData)
    {
        // Check valid args
        TabDataInvalidFormatException::setCheck($tabData);

        // Init var
        $strIdKey = $this->getStrIdKey(ConstPersistorHttpRequest::TAB_CONFIG_KEY_DELETE_MULTI_ID_KEY);

        // Set array of ids, if required
        if(!is_null($strIdKey))
        {
            // Get info
            $strIdSupportType = $this->getStrSupportType(
                ConstPersistorHttpRequest::TAB_CONFIG_KEY_DELETE_MULTI_ID_SUPPORT_TYPE,
                ConstPersistorHttpRequest::getTabConfigDataSupportType(),
                ConstPersistorHttpRequest::TAB_CONFIG_KEY_DELETE_ID_SUPPORT_TYPE,
                ConstPersistorHttpRequest::CONFIG_SUPPORT_TYPE_URL_ARG
            );
            $tabId = $this->getTabDataId($tabData);

            // Set array of ids
            $this->setIdEngine($strIdKey, $tabId, $strIdSupportType);
        }
        // Else: set array of data
        else
        {
            // Get info
            $strDataSupportType = $this->getStrSupportType(
                ConstPersistorHttpRequest::TAB_CONFIG_KEY_DELETE_MULTI_DATA_SUPPORT_TYPE,
                ConstPersistorHttpRequest::getTabConfigDataSupportType(),
                ConstPersistorHttpRequest::TAB_CONFIG_KEY_DELETE_DATA_SUPPORT_TYPE,
                ConstPersistorHttpRequest::CONFIG_SUPPORT_TYPE_BODY
            );
            $strPath = $this->getConfigValue(
                ConstPersistorHttpRequest::TAB_CONFIG_KEY_DELETE_MULTI_DATA_PATH,
                ConstPersistorHttpRequest::TAB_CONFIG_KEY_DELETE_DATA_PATH
            );

            // Set array of data
            $this->setDataEngine($tabData, $strDataSupportType, $strPath);
        }
    }



}