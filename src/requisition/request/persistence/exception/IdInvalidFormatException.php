<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\requisition\request\persistence\exception;

use Exception;

use liberty_code\library\str\library\ToolBoxString;
use liberty_code\http\requisition\request\persistence\library\ConstPersistorHttpRequest;



class IdInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $id
     */
	public function __construct($id)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstPersistorHttpRequest::EXCEPT_MSG_ID_INVALID_FORMAT,
            mb_strimwidth(strval($id), 0, 50, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified id has valid format.
	 * 
     * @param mixed $id
	 * @return boolean
	 * @throws static
     */
	public static function setCheck($id)
    {
		// Init var
		$result =
            // Check is valid string convertible
            is_string($id) ||
            ToolBoxString::checkConvertString($id);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static(serialize($id));
		}
		
		// Return result
		return $result;
    }
	
	
	
}