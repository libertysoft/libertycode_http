<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\requisition\request\persistence\exception;

use Exception;

use liberty_code\http\requisition\request\persistence\library\ConstPersistorHttpRequest;



class ConfigInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstPersistorHttpRequest::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $result =
            // Check valid path separator
            (
                (!isset($config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_PATH_SEPARATOR])) ||
                (
                    is_string($config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_PATH_SEPARATOR]) &&
                    (trim($config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_PATH_SEPARATOR]) != '')
                )
            ) &&

            // Check valid data id key
            (
                (!isset($config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_DATA_ID_KEY])) ||
                (
                    is_string($config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_DATA_ID_KEY]) &&
                    (trim($config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_DATA_ID_KEY]) != '')
                )
            ) &&

            // Check valid support type, for id to search
            (
                (!isset($config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_ID_SUPPORT_TYPE])) ||
                (
                    is_string($config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_ID_SUPPORT_TYPE]) &&
                    in_array(
                        $config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_ID_SUPPORT_TYPE],
                        ConstPersistorHttpRequest::getTabConfigIdSupportType()
                    )
                )
            ) &&

            // Check valid key, to set id to search
            (
                (!isset($config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_ID_KEY])) ||
                (
                    is_string($config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_ID_KEY]) &&
                    (trim($config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_ID_KEY]) != '')
                )
            ) &&

            // Check valid support type, for array of ids to search
            (
                (!isset($config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_MULTI_ID_SUPPORT_TYPE])) ||
                (
                    is_string($config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_MULTI_ID_SUPPORT_TYPE]) &&
                    in_array(
                        $config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_MULTI_ID_SUPPORT_TYPE],
                        ConstPersistorHttpRequest::getTabConfigDataSupportType()
                    )
                )
            ) &&

            // Check valid key, to set array of ids to search
            (
                (!isset($config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_MULTI_ID_KEY])) ||
                (
                    is_string($config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_MULTI_ID_KEY]) &&
                    (trim($config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_MULTI_ID_KEY]) != '')
                )
            ) &&

            // Check valid searching query support type
            (
                (!isset($config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_QUERY_SUPPORT_TYPE])) ||
                (
                    is_string($config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_QUERY_SUPPORT_TYPE]) &&
                    in_array(
                        $config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_QUERY_SUPPORT_TYPE],
                        ConstPersistorHttpRequest::getTabConfigDataSupportType()
                    )
                )
            ) &&

            // Check valid path, to set searching query
            (
                (!isset($config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_QUERY_DATA_PATH])) ||
                (
                    is_string($config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_QUERY_DATA_PATH]) &&
                    (trim($config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_QUERY_DATA_PATH]) != '')
                )
            ) &&

            // Check valid support type, for data to create
            (
                (!isset($config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_CREATE_DATA_SUPPORT_TYPE])) ||
                (
                    is_string($config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_CREATE_DATA_SUPPORT_TYPE]) &&
                    in_array(
                        $config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_CREATE_DATA_SUPPORT_TYPE],
                        ConstPersistorHttpRequest::getTabConfigDataSupportType()
                    )
                )
            ) &&

            // Check valid path, to set data to create
            (
                (!isset($config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_CREATE_DATA_PATH])) ||
                (
                    is_string($config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_CREATE_DATA_PATH]) &&
                    (trim($config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_CREATE_DATA_PATH]) != '')
                )
            ) &&

            // Check valid support type, for array of data to create
            (
                (!isset($config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_CREATE_MULTI_DATA_SUPPORT_TYPE])) ||
                (
                    is_string($config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_CREATE_MULTI_DATA_SUPPORT_TYPE]) &&
                    in_array(
                        $config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_CREATE_MULTI_DATA_SUPPORT_TYPE],
                        ConstPersistorHttpRequest::getTabConfigDataSupportType()
                    )
                )
            ) &&

            // Check valid path, to set array of data to create
            (
                (!isset($config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_CREATE_MULTI_DATA_PATH])) ||
                (
                    is_string($config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_CREATE_MULTI_DATA_PATH]) &&
                    (trim($config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_CREATE_MULTI_DATA_PATH]) != '')
                )
            ) &&

            // Check valid support type, for data to update
            (
                (!isset($config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_UPDATE_DATA_SUPPORT_TYPE])) ||
                (
                    is_string($config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_UPDATE_DATA_SUPPORT_TYPE]) &&
                    in_array(
                        $config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_UPDATE_DATA_SUPPORT_TYPE],
                        ConstPersistorHttpRequest::getTabConfigDataSupportType()
                    )
                )
            ) &&

            // Check valid path, to set data to update
            (
                (!isset($config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_UPDATE_DATA_PATH])) ||
                (
                    is_string($config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_UPDATE_DATA_PATH]) &&
                    (trim($config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_UPDATE_DATA_PATH]) != '')
                )
            ) &&

            // Check valid support type, for id to update
            (
                (!isset($config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_UPDATE_ID_SUPPORT_TYPE])) ||
                (
                    is_string($config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_UPDATE_ID_SUPPORT_TYPE]) &&
                    in_array(
                        $config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_UPDATE_ID_SUPPORT_TYPE],
                        ConstPersistorHttpRequest::getTabConfigIdSupportType()
                    )
                )
            ) &&

            // Check valid key, to set id to update
            (
                (!isset($config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_UPDATE_ID_KEY])) ||
                (
                    is_string($config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_UPDATE_ID_KEY]) &&
                    (trim($config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_UPDATE_ID_KEY]) != '')
                )
            ) &&

            // Check valid support type, for array of data to update
            (
                (!isset($config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_UPDATE_MULTI_DATA_SUPPORT_TYPE])) ||
                (
                    is_string($config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_UPDATE_MULTI_DATA_SUPPORT_TYPE]) &&
                    in_array(
                        $config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_UPDATE_MULTI_DATA_SUPPORT_TYPE],
                        ConstPersistorHttpRequest::getTabConfigDataSupportType()
                    )
                )
            ) &&

            // Check valid path, to set array of data to update
            (
                (!isset($config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_UPDATE_MULTI_DATA_PATH])) ||
                (
                    is_string($config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_UPDATE_MULTI_DATA_PATH]) &&
                    (trim($config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_UPDATE_MULTI_DATA_PATH]) != '')
                )
            ) &&

            // Check valid support type, for data to delete
            (
                (!isset($config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_DELETE_DATA_SUPPORT_TYPE])) ||
                (
                    is_string($config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_DELETE_DATA_SUPPORT_TYPE]) &&
                    in_array(
                        $config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_DELETE_DATA_SUPPORT_TYPE],
                        ConstPersistorHttpRequest::getTabConfigDataSupportType()
                    )
                )
            ) &&

            // Check valid path, to set data to delete
            (
                (!isset($config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_DELETE_DATA_PATH])) ||
                (
                    is_string($config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_DELETE_DATA_PATH]) &&
                    (trim($config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_DELETE_DATA_PATH]) != '')
                )
            ) &&

            // Check valid support type, for id to delete
            (
                (!isset($config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_DELETE_ID_SUPPORT_TYPE])) ||
                (
                    is_string($config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_DELETE_ID_SUPPORT_TYPE]) &&
                    in_array(
                        $config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_DELETE_ID_SUPPORT_TYPE],
                        ConstPersistorHttpRequest::getTabConfigIdSupportType()
                    )
                )
            ) &&

            // Check valid key, to set id to delete
            (
                (!isset($config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_DELETE_ID_KEY])) ||
                (
                    is_string($config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_DELETE_ID_KEY]) &&
                    (trim($config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_DELETE_ID_KEY]) != '')
                )
            ) &&

            // Check valid support type, for array of data to delete
            (
                (!isset($config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_DELETE_MULTI_DATA_SUPPORT_TYPE])) ||
                (
                    is_string($config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_DELETE_MULTI_DATA_SUPPORT_TYPE]) &&
                    in_array(
                        $config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_DELETE_MULTI_DATA_SUPPORT_TYPE],
                        ConstPersistorHttpRequest::getTabConfigDataSupportType()
                    )
                )
            ) &&

            // Check valid path, to set array of data to delete
            (
                (!isset($config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_DELETE_MULTI_DATA_PATH])) ||
                (
                    is_string($config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_DELETE_MULTI_DATA_PATH]) &&
                    (trim($config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_DELETE_MULTI_DATA_PATH]) != '')
                )
            ) &&

            // Check valid support type, for array of ids to delete
            (
                (!isset($config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_DELETE_MULTI_ID_SUPPORT_TYPE])) ||
                (
                    is_string($config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_DELETE_MULTI_ID_SUPPORT_TYPE]) &&
                    in_array(
                        $config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_DELETE_MULTI_ID_SUPPORT_TYPE],
                        ConstPersistorHttpRequest::getTabConfigDataSupportType()
                    )
                )
            ) &&

            // Check valid key, to set array of ids to delete
            (
                (!isset($config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_DELETE_MULTI_ID_KEY])) ||
                (
                    is_string($config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_DELETE_MULTI_ID_KEY]) &&
                    (trim($config[ConstPersistorHttpRequest::TAB_CONFIG_KEY_DELETE_MULTI_ID_KEY]) != '')
                )
            );

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format.
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
    public static function setCheck($config)
    {
		// Init var
		$result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}