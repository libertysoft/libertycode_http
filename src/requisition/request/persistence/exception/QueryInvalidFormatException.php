<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\requisition\request\persistence\exception;

use Exception;

use liberty_code\http\requisition\request\persistence\library\ConstPersistorHttpRequest;



class QueryInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $query
     */
	public function __construct($query)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstPersistorHttpRequest::EXCEPT_MSG_QUERY_INVALID_FORMAT,
            mb_strimwidth(strval($query), 0, 50, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified query has valid format.
	 * 
     * @param mixed $query
	 * @return boolean
	 * @throws static
     */
	public static function setCheck($query)
    {
		// Init var
		$result =
            // Check is valid array
            is_array($query);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($query) ? serialize($query) : $query));
		}
		
		// Return result
		return $result;
    }
	
	
	
}