<?php
/**
 * Description :
 * This class allows to define HTTP request factory class.
 * HTTP request factory allows to provide and hydrate HTTP request instance.
 *
 * HTTP request factory uses the following specified configuration, to get and hydrate request:
 * [
 *     -> Configuration key(optional): "string request key"
 *
 *     Template request factory configuration,
 *
 *     type(required): "http",
 *
 *     @see HttpRequest configuration array format,
 *
 *     snd_info(optional): [
 *         @see HttpRequest sending information array format
 *     ]
 *
 *     OR
 *
 *     -> Configuration key(optional): "string request key"
 *
 *     Template request factory configuration,
 *
 *     type(required): "http_data",
 *
 *     @see DataHttpRequest configuration array format,
 *
 *     snd_info(optional): [
 *         @see DataHttpRequest sending information array format
 *     ]
 *
 *     OR
 *
 *     -> Configuration key(optional): "string request key"
 *
 *     Template request factory configuration,
 *
 *     type(required): "http_persistor",
 *
 *     @see PersistorHttpRequest configuration array format,
 *
 *     snd_info(optional): [
 *         @see PersistorHttpRequest sending information array format
 *     ]
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\requisition\request\factory\model;

use liberty_code\requisition\request\factory\template\model\TmpRequestFactory;

use liberty_code\parser\parser\factory\api\ParserFactoryInterface;
use liberty_code\data\data\table\path\model\PathTableData;
use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\requisition\request\library\ConstRequest;
use liberty_code\requisition\request\api\RequestInterface;
use liberty_code\requisition\request\api\RequestCollectionInterface;
use liberty_code\requisition\request\factory\api\RequestFactoryInterface;
use liberty_code\http\url_route\argument\model\UrlRouteArgData;
use liberty_code\http\url_arg\model\UrlArgData;
use liberty_code\http\header\model\HeaderData;
use liberty_code\http\requisition\request\model\HttpRequest;
use liberty_code\http\requisition\request\data\exception\ParserFactoryInvalidFormatException;
use liberty_code\http\requisition\request\data\model\DataHttpRequest;
use liberty_code\http\requisition\request\persistence\model\PersistorHttpRequest;
use liberty_code\http\requisition\request\factory\library\ConstHttpRequestFactory;



/**
 * @method ParserFactoryInterface getObjParserFactory() Get parser factory object.
 * @method void setObjParserFactory(ParserFactoryInterface $objParserFactory) Set parser factory object.
 */
class HttpRequestFactory extends TmpRequestFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param ParserFactoryInterface $objParserFactory
     */
    public function __construct(
        ParserFactoryInterface $objParserFactory,
        RequestCollectionInterface $objCollection = null,
        RequestFactoryInterface $objFactory = null,
        ProviderInterface $objProvider = null
    )
    {
        // Call parent constructor
        parent::__construct(
            $objCollection,
            $objFactory,
            $objProvider
        );

        // Init parser factory
        $this->setObjParserFactory($objParserFactory);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstHttpRequestFactory::DATA_KEY_DEFAULT_PARSER_FACTORY))
        {
            $this->__beanTabData[ConstHttpRequestFactory::DATA_KEY_DEFAULT_PARSER_FACTORY] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }



    /**
     * @inheritdoc
     */
    protected function hydrateRequest(RequestInterface $objRequest, array $tabConfigFormat)
    {
        // Call parent method
        parent::hydrateRequest($objRequest, $tabConfigFormat);

        // Hydrate HTTP request, if required
        if($objRequest instanceof HttpRequest)
        {
            // Init URL route argument data object
            $objUrlRouteArgData = new UrlRouteArgData($objRequest->getTabUrlRouteArg());
            $objRequest->setObjUrlRouteArgData($objUrlRouteArgData);

            // Init URL argument data object
            $objUrlArgData = new UrlArgData(null, $objRequest->getTabUrlArg());
            $objRequest->setObjUrlArgData($objUrlArgData);

            // Init header data object
            $objHeaderData = new HeaderData($objRequest->getTabHeader());
            $objRequest->setObjHeaderData($objHeaderData);

            // Hydrate data HTTP request, if required
            if(
                ($objRequest instanceof DataHttpRequest) &&
                (!is_null($objRequest->getStrBody()))
            )
            {
                // Init body data object
                $objBodyData = new PathTableData(null, $objRequest->getTabBodyData());
                $objRequest->setObjBodyData($objBodyData);
            }
        }
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstHttpRequestFactory::DATA_KEY_DEFAULT_PARSER_FACTORY
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstHttpRequestFactory::DATA_KEY_DEFAULT_PARSER_FACTORY:
                    ParserFactoryInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
	// ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabConfigFormat(array $tabConfig, $strConfigKey = null)
    {
        // Init var
        $result = parent::getTabConfigFormat($tabConfig, $strConfigKey);

        // Format configuration, if required
        if(!is_null($strConfigKey))
        {
            // Add configured key as request key, if required
            if(!array_key_exists(ConstRequest::TAB_CONFIG_KEY_KEY, $result))
            {
                $result[ConstRequest::TAB_CONFIG_KEY_KEY] = $strConfigKey;
            }
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getStrRequestClassPathFromType($strConfigType)
    {
        // Init var
        $result = null;

        // Get class path of request, from type
        switch($strConfigType)
        {
            case ConstHttpRequestFactory::CONFIG_TYPE_HTTP:
                $result = HttpRequest::class;
                break;

            case ConstHttpRequestFactory::CONFIG_TYPE_HTTP_DATA:
                $result = DataHttpRequest::class;
                break;

            case ConstHttpRequestFactory::CONFIG_TYPE_HTTP_PERSISTOR:
                $result = PersistorHttpRequest::class;
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getObjRequestNew($strConfigType)
    {
        // Init var
        $result = null;
        $objParserFactory = $this->getObjParserFactory();

        // Get request, from type
        switch($strConfigType)
        {
            case ConstHttpRequestFactory::CONFIG_TYPE_HTTP:
                $result = new HttpRequest();
                break;

            case ConstHttpRequestFactory::CONFIG_TYPE_HTTP_DATA:
                $result = new DataHttpRequest($objParserFactory);
                break;

            case ConstHttpRequestFactory::CONFIG_TYPE_HTTP_PERSISTOR:
                $result = new PersistorHttpRequest($objParserFactory);
                break;
        }

        // Return result
        return $result;
    }



}