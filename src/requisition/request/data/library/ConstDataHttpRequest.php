<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\requisition\request\data\library;



class ConstDataHttpRequest
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_PARSER_FACTORY = 'objParserFactory';
    const DATA_KEY_DEFAULT_BODY_DATA = 'objBodyData';



    // Configuration
    const TAB_CONFIG_KEY_BODY_PARSER_CONFIG = 'body_parser_config';

    // Sending information configuration
    const TAB_SND_INFO_KEY_BODY_DATA = 'body_data';


	
    // Exception message constants
    const EXCEPT_MSG_PARSER_FACTORY_INVALID_FORMAT = 'Following parser factory "%1$s" invalid! It must be a factory object.';
    const EXCEPT_MSG_BODY_DATA_INVALID_FORMAT = 'Following body data "%1$s" invalid! It must be null or a table data object.';
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the data HTTP request configuration standard.';



}