<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\requisition\request\data\exception;

use Exception;

use liberty_code\data\data\table\model\TableData;
use liberty_code\http\requisition\request\data\library\ConstDataHttpRequest;



class BodyDataInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $data
     */
	public function __construct($data)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstDataHttpRequest::EXCEPT_MSG_BODY_DATA_INVALID_FORMAT,
            mb_strimwidth(strval($data), 0, 50, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified data has valid format.
	 * 
     * @param mixed $data
	 * @return boolean
	 * @throws static
     */
	public static function setCheck($data)
    {
		// Init var
		$result = (
		    is_null($data) ||
            ($data instanceof TableData)
        );

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($data);
		}
		
		// Return result
		return $result;
    }
	
	
	
}