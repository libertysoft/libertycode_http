<?php
/**
 * Description :
 * This class allows to define data HTTP request class.
 * Data HTTP request is HTTP request,
 * allowing to handle data, on sending information.
 *
 * Data HTTP request uses the following specified configuration:
 * [
 *     HTTP request configuration,
 *
 *     body_parser_config(required): [
 *         @see ParserFactoryInterface configuration array format
 *     ]
 * ]
 *
 * Data HTTP request handles the following specified sending information:
 * [
 *     HTTP request sending information,
 *
 *     body_data(optional): [
 *         ...array of data
 *     ]
 * ]
 *
 * Note:
 * -> Sending information body_data format:
 *     Additional feature only available for setting.
 *     Provided data will be parsed to set body, if possible (if parser components valid).
 *     It allows to fill body with array of data, via configuration.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\requisition\request\data\model;

use liberty_code\http\requisition\request\model\HttpRequest;

use liberty_code\parser\parser\factory\api\ParserFactoryInterface;
use liberty_code\data\data\table\model\TableData;
use liberty_code\requisition\request\library\ConstRequest;
use liberty_code\http\url_route\argument\model\UrlRouteArgData;
use liberty_code\http\url_arg\model\UrlArgData;
use liberty_code\http\header\model\HeaderData;
use liberty_code\http\requisition\request\library\ConstHttpRequest;
use liberty_code\http\requisition\request\data\library\ConstDataHttpRequest;
use liberty_code\http\requisition\request\data\exception\ParserFactoryInvalidFormatException;
use liberty_code\http\requisition\request\data\exception\BodyDataInvalidFormatException;
use liberty_code\http\requisition\request\data\exception\ConfigInvalidFormatException;



/**
 * @method ParserFactoryInterface getObjParserFactory() Get parser factory object.
 * @method void setObjParserFactory(ParserFactoryInterface $objParserFactory) Set parser factory object.
 * @method null|TableData getObjBodyData() Get body data object.
 */
class DataHttpRequest extends HttpRequest
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();


	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param ParserFactoryInterface $objParserFactory
     * @param TableData $objBodyData = null
     */
    public function __construct(
        ParserFactoryInterface $objParserFactory,
        UrlRouteArgData $objUrlRouteArgData = null,
        UrlArgData $objUrlArgData = null,
        HeaderData $objHeaderData = null,
        TableData $objBodyData = null,
        array $tabConfig = null,
        array $tabSndInfo = null
    )
    {
        // Call parent constructor
        parent::__construct(
            $objUrlRouteArgData,
            $objUrlArgData,
            $objHeaderData,
            $tabConfig
        );

        // Init parser factory
        $this->setObjParserFactory($objParserFactory);

        // Init body data, if required
        if(!is_null($objBodyData))
        {
            $this->setObjBodyData($objBodyData);
        }

        // Init sending information, if required
        if(!is_null($tabSndInfo))
        {
            $this->setSndInfo($tabSndInfo);
        }
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstDataHttpRequest::DATA_KEY_DEFAULT_PARSER_FACTORY))
        {
            $this->__beanTabData[ConstDataHttpRequest::DATA_KEY_DEFAULT_PARSER_FACTORY] = null;
        }

        if(!$this->beanExists(ConstDataHttpRequest::DATA_KEY_DEFAULT_BODY_DATA))
        {
            $this->__beanTabData[ConstDataHttpRequest::DATA_KEY_DEFAULT_BODY_DATA] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstDataHttpRequest::DATA_KEY_DEFAULT_PARSER_FACTORY,
            ConstDataHttpRequest::DATA_KEY_DEFAULT_BODY_DATA
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstDataHttpRequest::DATA_KEY_DEFAULT_PARSER_FACTORY:
                    ParserFactoryInvalidFormatException::setCheck($value);
                    break;

                case ConstDataHttpRequest::DATA_KEY_DEFAULT_BODY_DATA:
                    BodyDataInvalidFormatException::setCheck($value);
                    break;

                case ConstRequest::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get body parser configuration array.
     *
     * @return array
     */
    protected function getTabBodyParserConfig()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = $tabConfig[ConstDataHttpRequest::TAB_CONFIG_KEY_BODY_PARSER_CONFIG];

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getTabSndInfo()
    {
        // Init var
        $result = parent::getTabSndInfo();

        // Register body data, if required
        $objBodyData = $this->getObjBodyData();
        if(!is_null($objBodyData))
        {
            $tabBodyData = $objBodyData->getDataSrc();
            if(
                (count($tabBodyData) > 0) ||
                isset($result[ConstHttpRequest::TAB_SND_INFO_KEY_BODY])
            )
            {
                $result[ConstHttpRequest::TAB_SND_INFO_KEY_BODY] = $this->getStrBodyFromData($tabBodyData);
            }
        }

        // Return result
        return $result;
    }



    /**
     * Get string body,
     * from specified body data array.
     *
     * @param null|array $tabData
     * @return string
     * @throws ConfigInvalidFormatException
     */
    protected function getStrBodyFromData($tabData)
    {
        // Init var
        $tabData = (is_array($tabData) ? $tabData : null);
        $result = null;

        // Get source, if required
        if(!is_null($tabData))
        {
            $objParserFactory = $this->getObjParserFactory();
            $tabParserConfig = $this->getTabBodyParserConfig();
            $objParser = $objParserFactory->getObjParser($tabParserConfig);
            $result = (
                (
                    (!is_null($objParser)) &&
                    is_string($body = $objParser->getSource($tabData))
                ) ?
                    $body :
                    null
            );

            // Check result
            if(is_null($result))
            {
                throw new ConfigInvalidFormatException(serialize($this->getTabConfig()));
            }
        }

        // Return result
        return $result;
    }



    /**
     * Get body data array,
     * from specified string body.
     *
     * @param null|string $strBody
     * @return array
     * @throws ConfigInvalidFormatException
     */
    protected function getTabDataFromBody($strBody)
    {
        // Init var
        $strBody = (is_string($strBody) ? $strBody : null);
        $result = null;

        // Get parsed data, if required
        if(!is_null($strBody))
        {
            $objParserFactory = $this->getObjParserFactory();
            $tabParserConfig = $this->getTabBodyParserConfig();
            $objParser = $objParserFactory->getObjParser($tabParserConfig);
            $result = (
                (
                    (!is_null($objParser)) &&
                    is_array($data = $objParser->getData($strBody))
                ) ?
                    $data :
                    null
            );

            // Check result
            if(is_null($result))
            {
                throw new ConfigInvalidFormatException(serialize($this->getTabConfig()));
            }

        }

        // Return result
        return $result;
    }



    /**
     * Get body data array.
     *
     * @return null|array
     * @throws ConfigInvalidFormatException
     */
    public function getTabBodyData()
    {
        // Init var
        $strBody = $this->getStrBody();
        $result = $this->getTabDataFromBody($strBody);

        // Return result
        return $result;
    }





    // Methods setters
    // ******************************************************************************

    /**
     * Set body data object.
     *
     * @param null|TableData $objBodyData
     */
    public function setObjBodyData($objBodyData)
    {
        // Init var
        $objBodyDataBase = $this->getObjBodyData();

        // Set header data
        $this->beanPut(ConstDataHttpRequest::DATA_KEY_DEFAULT_BODY_DATA, $objBodyData);

        // Init sending information headers, if required
        $tabDataSrc = (
            (!is_null($objBodyData)) ?
                $objBodyData->getDataSrc() :
                (
                    (!is_null($objBodyDataBase)) ?
                        $objBodyDataBase->getDataSrc() :
                        null
                )
        );
        if(!is_null($tabDataSrc))
        {
            $this->setBodyData($tabDataSrc);
        }
    }



    /**
     * @inheritdoc
     */
    public function setSndInfo(array $tabInfo)
    {
        // Init body data, if required
        $tabBodyData = null;
        if(isset($tabInfo[ConstDataHttpRequest::TAB_SND_INFO_KEY_BODY_DATA]))
        {
            $tabBodyData = (
                is_array($tabInfo[ConstDataHttpRequest::TAB_SND_INFO_KEY_BODY_DATA]) ?
                    $tabInfo[ConstDataHttpRequest::TAB_SND_INFO_KEY_BODY_DATA] :
                    $tabBodyData
            );
            unset($tabInfo[ConstDataHttpRequest::TAB_SND_INFO_KEY_BODY_DATA]);
        }

        // Call parent method
        parent::setSndInfo($tabInfo);

        // Format sending information, if required
        if(!is_null($tabBodyData))
        {
            $this->setBodyData($tabBodyData);
        }

        // Init body data, if required
        $objBodyData = $this->getObjBodyData();
        if(!is_null($objBodyData))
        {
            $tabBodyData = (
                (!is_null($tabBodyData)) ?
                    $tabBodyData :
                    (
                        array_key_exists(ConstHttpRequest::TAB_SND_INFO_KEY_BODY, $tabInfo) ?
                            (
                                (!is_null($tabInfo[ConstHttpRequest::TAB_SND_INFO_KEY_BODY])) ?
                                    $this->getTabDataFromBody($tabInfo[ConstHttpRequest::TAB_SND_INFO_KEY_BODY]) :
                                    array()
                            ) :
                            null
                    )
            );

            if(!is_null($tabBodyData))
            {
                $objBodyData->setDataSrc($tabBodyData);
            }
        }
    }



    /**
     * Set specified body data array.
     *
     * @param null|array $tabData
     * @return $this
     */
    public function setBodyData($tabData)
    {
        // Init var
        $strBody = $this->getStrBodyFromData($tabData);
        $result = $this->setBody($strBody);

        // Return result
        return $result;
    }



}