<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\header\exception;

use Exception;

use liberty_code\http\header\library\ConstHeaderData;



class KeyInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $key
     */
	public function __construct($key)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
        $this->message = sprintf
        (
            ConstHeaderData::EXCEPT_MSG_KEY_INVALID_FORMAT,
            mb_strimwidth(strval($key), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified key has valid format.
     *
     * @param mixed $key
     * @return boolean
     * @throws static
     */
    public static function checkKeyIsValid($key)
    {
        // Init var
        $result =
            // Check valid string
            is_string($key) &&
            (trim($key) != '');

        // Return result
        return $result;
    }



	/**
	 * Check if specified key has valid format.
	 * 
     * @param mixed $key
	 * @return boolean
	 * @throws static
     */
    public static function setCheck($key)
    {
		// Init var
		$result = static::checkKeyIsValid($key);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($key);
		}
		
		// Return result
		return $result;
    }
	
	
	
}