<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\header\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\library\str\library\ToolBoxString;
use liberty_code\http\header\exception\DataSrcInvalidFormatException;
use liberty_code\http\header\model\HeaderData;



class ToolBoxHeader extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get key,
     * from specified header line.
     *
     * @param string $strHeaderLine
     * @return null|string
     */
    public static function getStrKey($strHeaderLine)
    {
        // Init var
        $strHeaderLine = static::getStrHeaderLineFormat($strHeaderLine);
        $strPattern = '#^([^:]+).*$#';
        $tabMatch = array();
        $result = (
            (
                is_string($strHeaderLine) &&
                (preg_match($strPattern, $strHeaderLine, $tabMatch) === 1)
            ) ?
                $tabMatch[1] :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get value,
     * from specified header line.
     *
     * @param string $strHeaderLine
     * @return null|string
     */
    public static function getStrValue($strHeaderLine)
    {
        // Init var
        $strHeaderLine = static::getStrHeaderLineFormat($strHeaderLine);
        $strPattern = '#^[^:]+:[ ]?(.*)$#';
        $tabMatch = array();
        $result = (
            (
                is_string($strHeaderLine) &&
                (preg_match($strPattern, $strHeaderLine, $tabMatch) === 1)
            ) ?
                $tabMatch[1] :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get specified formatted header line.
     *
     * @param string $strHeaderLine
     * @return null|string
     */
    public static function getStrHeaderLineFormat($strHeaderLine)
    {
        // Return result
        return (
            is_string($strHeaderLine) ?
                trim($strHeaderLine, "\r\n") :
                null
        );
    }



    /**
     * Get header line,
     * from specified key
     * and specified value.
     *
     * @param string $strKey
     * @param string $strValue
     * @return null|string
     */
    public static function getStrHeaderLine($strKey, $strValue)
    {
        // Return result
        return (
            (is_string($strKey) && ToolBoxString::checkConvertString($strValue)) ?
                static::getStrHeaderLineFormat(
                    sprintf('%1$s: %2$s', $strKey, strval($strValue))
                ):
                null
        );
    }



    /**
     * Get data array,
     * from specified header lines.
     *
     * Header lines format:
     * string OR index array of string header lines.
     *
     * Return array format:
     * If success: @see HeaderData data source array format.
     * Else: null.
     *
     * @param string|array $headerLine
     * @return null|array
     */
    public static function getTabData($headerLine)
    {
        // Init var
        $result = null;
        $tabHeaderLine = (
            is_string($headerLine) ?
                explode("\n", $headerLine) :
                (
                    is_array($headerLine) ?
                        array_values(array_filter(
                            $headerLine,
                            function($headerLine) {return is_string($headerLine);}
                        )) :
                        null
                )
        );

        // Register headers, if required
        if(!is_null($tabHeaderLine))
        {
            $result = array();

            // Run each header line
            foreach($tabHeaderLine as $strHeaderLine)
            {
                // Get info
                $strKey = static::getStrKey($strHeaderLine);
                $strValue = static::getStrValue($strHeaderLine);
                $strValue = ((!is_null($strValue)) ? $strValue : '');

                // Register header, if required
                if(!is_null($strKey))
                {
                    // Register header, in case of multiple values
                    if(array_key_exists($strKey, $result))
                    {
                        if(!is_array($result[$strKey]))
                        {
                            $result[$strKey] = array($result[$strKey]);
                        }
                        $result[$strKey][] = $strValue;
                    }
                    // Register header, in case of single value
                    else
                    {
                        $result[$strKey] = $strValue;
                    }
                }
            }
        }

        // Return result
        return $result;
    }



    /**
     * Get header lines
     * from specified data array.
     *
     * Data array format:
     * @see HeaderData data source array format.
     *
     * Return format:
     * If success: string OR index array of string header lines if required.
     * Else: null.
     *
     * @param array $tabData
     * @param boolean $boolArrayRequired = false
     * @return null|string|array
     */
    public static function getHeaderLine(
        array $tabData,
        $boolArrayRequired = false
    )
    {
        // Init var
        $boolArrayRequired = (is_bool($boolArrayRequired) ? $boolArrayRequired : false);
        $result = null;

        // Build header lines, if required
        if(DataSrcInvalidFormatException::checkDataSrcIsValid($tabData))
        {
            $result = array();

            // Run each header
            foreach($tabData as $strKey => $value)
            {
                // Get header multi lines
                if(is_array($value))
                {
                    foreach($value as $subValue)
                    {
                        $result[] = static::getStrHeaderLine($strKey, $subValue);
                    }
                }
                // Get header line
                else
                {
                    $result[] = static::getStrHeaderLine($strKey, $value);
                }
            }

            // Format result, if required
            $result = (
                $boolArrayRequired ?
                    $result :
                    implode("\r\n", $result)
            );
        }

        // Return result
        return $result;
    }



}