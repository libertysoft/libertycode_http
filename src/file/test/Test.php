<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load test
require_once($strRootAppPath . '/src/file/test/FileTest.php');

// Use
use liberty_code\file\file\name\api\NameFileInterface;
use liberty_code\file\file\name\file_system\model\FsFile;
use liberty_code\http\file\factory\library\ToolBoxHttpFactory;



// Test file
$objFile1 = new FsFile(array(
    'full_path' => __DIR__ . '/file/File test 1.pdf'
));
$strFile1DataSrc = (
        'Content-Disposition: form-data; name="key-1[][key-1-1]"; filename="' . $objFile1->getStrName() . '"
Content-Type: application/test-1
Content-Length: 40
Content-Transfer-Encoding: binary

' . $objFile1->getStrContent()
);

$objFile2 = new FsFile(array(
    'full_path' => __DIR__ . '/file/File test 2.docx'
));
$strFile2DataSrc = (
    'Content-Disposition: form-data; name="key-2"; filename="' . $objFile2->getStrName() . '"
Content-Type: application/test-2
Content-Length: 10
Content-Transfer-Encoding: binary

' . $objFile2->getStrContent()
);

$tabTabConfig = array(
    'file_1' => [
        'type' => 'http_name_upload',
        'input_name' => 'file_input_1'
    ], // Ok
    'file_2' => [
        'type' => 'http_name_upload',
        'input_name' => 'file_input',
        'input_path' => '0/key-1'
    ], // Ko, bad upload config format (wrong input name)
    'file_3' => [
        'type' => 'http_name_upload',
        'input_name' => 'file_input_2',
        'input_path' => '0/key-1/test'
    ], // Ko, bad upload config format (wrong input path)
    'file_4' => [
        'type' => 'http_name_upload',
        'input_name' => 'file_input_2',
        'input_path' => '0/key-1'
    ], // Ok
    'file_5' => [
        'type' => 'http_name_multipart_data'
    ], // Ko, bad multipart data config format (no data source found)
    'file_6' => [
        'type' => 'http_name_multipart_data',
        'data_source' => $strFile1DataSrc,
        'data_source_mime_type_require' => 'test',
        'data_source_size_require' => 'test'
    ], // Ko, bad multipart data config format (wrong data source mime type/ size required options)
    'file_7' => [
        'type' => 'http_name_multipart_data',
        'data_source' => $strFile1DataSrc,
        'data_source_mime_type_require' => false,
        'data_source_size_require' => false
    ], // Ok
    'file_8' => [
        'type' => 'http_name_multipart_data',
        'data_source' => true
    ], // Ko, bad multipart data config format (wrong data source)
    'file_9' => [
        'type' => 'http_name_multipart_data',
        'data_source' => $strFile2DataSrc,
        //'data_source_mime_type_require' => false,
        //'data_source_size_require' => false
    ], // Ok
    'file_10' => [
        'type' => 'http_response',
        'header' => 'test',
        'body' => $objFile2->getStrContent(),
        //'header_mime_type_require' => false,
        //'header_size_require' => false
    ], // Ko, bad response headers config format
    'file_11' => [
        'type' => 'http_response',
        'header' => [
            'Content-Type' => 'application/msword',
            'Content-Length' => ($objFile2->getIntSize() - 1000)
        ],
        'body' => true,
        //'header_mime_type_require' => false,
        //'header_size_require' => false
    ], // Ko, bad response body config format
    'file_12' => [
        'type' => 'http_response',
        'header' => [
            'Content-Type' => 'application/msword',
            'Content-Length' => ($objFile2->getIntSize() - 1000)
        ],
        'body' => $objFile2->getStrContent(),
        //'header_mime_type_require' => false,
        //'header_size_require' => false
    ], // Ok
    'file_13' => [
        'type' => 'http_name_download_response',
        'header' => 'test',
        'body' => $objFile2->getStrContent(),
        //'header_mime_type_require' => false,
        //'header_size_require' => false
    ], // Ko, bad response headers config format
    'file_14' => [
        'type' => 'http_name_download_response',
        'header' => [
            'Content-Type' => 'application/msword',
            'Content-Length' => ($objFile2->getIntSize() - 1000),
            'Content-Disposition' => sprintf('attachment; filenametest="%1$s"', $objFile2->getStrName())
        ],
        'body' => $objFile2->getStrContent(),
        //'header_mime_type_require' => false,
        //'header_size_require' => false
    ], // Ko, bad response headers config format (name not found, via Content-Disposition header)
    'file_15' => [
        'type' => 'http_name_download_response',
        'header' => [
            'Content-Type' => 'application/msword',
            'Content-Length' => ($objFile2->getIntSize() - 1000),
            'Content-Disposition' => sprintf('attachment; filename="%1$s"', $objFile2->getStrName())
        ],
        'body' => true,
        //'header_mime_type_require' => false,
        //'header_size_require' => false
    ], // Ko, bad response body config format
    'file_16' => [
        'type' => 'http_name_download_response',
        'header' => [
            'Content-Type' => 'application/msword',
            'Content-Length' => ($objFile2->getIntSize() - 1000),
            'Content-Disposition' => sprintf('attachment; filename="%1$s"', $objFile2->getStrName())
        ],
        'body' => $objFile2->getStrContent(),
        'header_mime_type_require' => false,
        'header_size_require' => false
    ] // Ok
);

foreach($tabTabConfig as $strConfigKey => $tabConfig)
{
    echo('Test file "'.$strConfigKey.'": <br />');
    try{
        $objFile = $objFileFactory->getObjFile($tabConfig, $strConfigKey);

        if(!is_null($objFile))
        {
            $tabConfigRender = $objFile->getTabConfig();
            if(isset($tabConfigRender['data_source']))
            {
                $tabConfigRender['data_source'] = mb_strimwidth(
                    $tabConfigRender['data_source'],
                        0,
                        10,
                        '...'
                );
            }
            else if(isset($tabConfigRender['body']))
            {
                $tabConfigRender['body'] = mb_strimwidth(
                    $tabConfigRender['body'],
                    0,
                    10,
                    '...'
                );
            }

            echo('Get config: <pre>');var_dump($tabConfigRender);echo('</pre>');
            echo('Get extension: <pre>');var_dump($objFile->getStrExtension());echo('</pre>');
            echo('Get mime type: <pre>');var_dump($objFile->getStrMimeType());echo('</pre>');
            echo('Get size: <pre>');var_dump($objFile->getIntSize());echo('</pre>');
            echo('Get size (Ko): <pre>');var_dump($objFile->getIntSize() / 1024);echo('</pre>');
            //echo('Get content: <pre>');var_dump($objFile->getStrContent());echo('</pre>');

            if($objFile instanceof NameFileInterface)
            {
                echo('Get base name: <pre>');var_dump($objFile->getStrBaseName());echo('</pre>');
                echo('Get name: <pre>');var_dump($objFile->getStrName());echo('</pre>');
            }
        }
        else
        {
            echo('File not provided.');
            echo('<br />');
        }

    } catch(\Exception $e) {
        echo(get_class($e) . ' - ' . htmlentities($e->getMessage()));
        echo('<br />');
    }
    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');



// Test format
echo('Test (index) array of files get: <br />');
echo('<pre>');print_r(ToolBoxHttpFactory::getTabFile($objFileFactory));echo('</pre>');
echo('<br />');

echo('Test (associative) array of files get: <br />');
echo('<pre>');print_r(ToolBoxHttpFactory::getTabFile($objFileFactory, array(), false));echo('</pre>');
echo('<br />');

echo('<br /><br /><br />');



// Set render
?>
<form enctype="multipart/form-data" method="post">
    Fichier 1: <input name="file_input_1" type="file" /><br />
    Fichier 2[][key-1]: <input name="file_input_2[][key-1]" type="file" /><br />
    Fichier root[0][file]: <input name="root[0][file]" type="file" /><br />
    Fichier root[1][file]: <input name="root[1][file]" type="file" /><br />
    Fichier root[3][file]: <input name="root[3][file]" type="file" /><br />
    <input type="submit" value="Envoyer le fichier" />
</form>


