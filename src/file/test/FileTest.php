<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Use
use liberty_code\file\file\factory\standard\model\StandardFileFactory;
use liberty_code\file\file\factory\name\model\NameFileFactory;
use liberty_code\http\file\factory\model\HttpFileFactory;
use liberty_code\http\file\factory\name\model\HttpNameFileFactory;



// Init factory
$objFileFactory = new StandardFileFactory();
$objFileFactory = new NameFileFactory($objFileFactory);
$objFileFactory = new HttpFileFactory($objFileFactory);
$objFileFactory = new HttpNameFileFactory($objFileFactory);


