<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\file\factory\name\library;



class ConstHttpNameFileFactory
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Type configuration
    const CONFIG_TYPE_HTTP_NAME_UPLOAD = 'http_name_upload';
    const CONFIG_TYPE_HTTP_NAME_MULTIPART_DATA = 'http_name_multipart_data';
    const CONFIG_TYPE_HTTP_NAME_DOWNLOAD_RESPONSE = 'http_name_download_response';


    
}