<?php
/**
 * Description :
 * This class allows to define HTTP named file factory class.
 * HTTP named file factory allows to provide and hydrate HTTP named file instance.
 *
 * HTTP named file factory uses the following specified configuration, to get and hydrate HTTP named file:
 * [
 *     type(required): "http_name_upload",
 *
 *     @see UploadFile configuration array format
 *
 *     OR
 *
 *     type(required): "http_name_multipart_data",
 *
 *     @see MultipartDataFile configuration array format
 *
 *     OR
 *
 *     type(required): "http_name_download_response",
 *
 *     @see DnlResponseFile configuration array format
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\file\factory\name\model;

use liberty_code\file\file\factory\model\DefaultFileFactory;

use liberty_code\http\file\name\upload\model\UploadFile;
use liberty_code\http\file\name\multipart_data\model\MultipartDataFile;
use liberty_code\http\file\name\download_response\model\DnlResponseFile;
use liberty_code\http\file\factory\name\library\ConstHttpNameFileFactory;



class HttpNameFileFactory extends DefaultFileFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods getters
	// ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFileClassPathFromType($strConfigType)
    {
        // Init var
        $result = null;

        // Get class path of file, from type
        switch($strConfigType)
        {
            case ConstHttpNameFileFactory::CONFIG_TYPE_HTTP_NAME_UPLOAD:
                $result = UploadFile::class;
                break;

            case ConstHttpNameFileFactory::CONFIG_TYPE_HTTP_NAME_MULTIPART_DATA:
                $result = MultipartDataFile::class;
                break;

            case ConstHttpNameFileFactory::CONFIG_TYPE_HTTP_NAME_DOWNLOAD_RESPONSE:
                $result = DnlResponseFile::class;
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getObjFileNew($strConfigType)
    {
        // Init var
        $result = parent::getObjFileNew($strConfigType);

        // Get class path of file, from type, if required
        if(is_null($result))
        {
            switch($strConfigType)
            {
                case ConstHttpNameFileFactory::CONFIG_TYPE_HTTP_NAME_UPLOAD:
                    $result = new UploadFile();
                    break;

                case ConstHttpNameFileFactory::CONFIG_TYPE_HTTP_NAME_MULTIPART_DATA:
                    $result = new MultipartDataFile();
                    break;

                case ConstHttpNameFileFactory::CONFIG_TYPE_HTTP_NAME_DOWNLOAD_RESPONSE:
                    $result = new DnlResponseFile();
                    break;
            }
        }

        // Return result
        return $result;
    }



}