<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\file\factory\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\file\file\api\FileInterface;
use liberty_code\file\file\factory\library\ConstFileFactory;
use liberty_code\file\file\factory\api\FileFactoryInterface;
use liberty_code\http\file\name\upload\library\ConstUploadFile;
use liberty_code\http\file\factory\name\library\ConstHttpNameFileFactory;



class ToolBoxHttpFactory extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods getters
	// ******************************************************************************

    /**
     * Get file object(s),
     * from specified uploaded file data,
     * using specified file factory.
     *
     * @param FileFactoryInterface $objFileFactory
     * @param string $strInputNm
     * @param mixed $fileData
     * @param string $strInputPath = null
     * @param array $tabAddFileConfig = array()
     * @param boolean $boolIndexRequire = true
     * @return null|FileInterface|FileInterface[]
     */
    protected static function getFileEngine(
        FileFactoryInterface $objFileFactory,
        $strInputNm,
        $fileData,
        $strInputPath = null,
        array $tabAddFileConfig = array(),
        $boolIndexRequire = true
    )
    {
        // Init var
        $result = null;
        $boolIndexRequire = (is_bool($boolIndexRequire) ? $boolIndexRequire : false);

        // Case array of files
        if(is_array($fileData))
        {
            // Init var
            $strInputPathSeparator = (
                (
                    isset($tabAddFileConfig[ConstUploadFile::TAB_CONFIG_KEY_INPUT_PATH_SEPARATOR]) &&
                    is_string($tabAddFileConfig[ConstUploadFile::TAB_CONFIG_KEY_INPUT_PATH_SEPARATOR])
                ) ?
                    $tabAddFileConfig[ConstUploadFile::TAB_CONFIG_KEY_INPUT_PATH_SEPARATOR] :
                    '/'
            );

            // Run each files
            foreach($fileData as $key => $subFileData)
            {
                // Get file
                $strSubInputPath = (
                    is_null($strInputPath) ?
                        strval($key) :
                        sprintf(
                            '%2$s%1$s%3$s',
                            $strInputPathSeparator,
                            $strInputPath,
                            strval($key)
                        )
                );
                $file = static::getFileEngine(
                    $objFileFactory,
                    $strInputNm,
                    $subFileData,
                    $strSubInputPath,
                    $tabAddFileConfig,
                    $boolIndexRequire
                );

                // Register file, if required
                if(!is_null($file))
                {
                    $result = (is_null($result) ? array() : $result);
                    $result = (
                        $boolIndexRequire ?
                            array_merge(
                                $result,
                                (is_array($file) ? $file : array($file))
                            ) :
                            ($result + array($key => $file))
                    );
                }
            }
        }
        // Case else: get new file instance and register it
        else if(!empty($fileData))
        {
            // Get file config
            $tabFileConfig = array_merge(
                array(
                    ConstFileFactory::TAB_CONFIG_KEY_TYPE => ConstHttpNameFileFactory::CONFIG_TYPE_HTTP_NAME_UPLOAD,
                    ConstUploadFile::TAB_CONFIG_KEY_INPUT_NAME => $strInputNm
                ),
                $tabAddFileConfig,
                (
                    is_null($strInputPath) ?
                        array() :
                        array(ConstUploadFile::TAB_CONFIG_KEY_INPUT_PATH => $strInputPath)
                )
            );

            // Get new file and register it
            $result = $objFileFactory->getObjFile($tabFileConfig);
        }

        // Return result
        return $result;
    }



	/**
     * Get array of file objects,
     * from uploaded files ($_FILES),
     * using specified file factory.
     * Return index array if index option required,
     * return associative array (following $_FILES structure) else.
     *
     * @param FileFactoryInterface $objFileFactory
     * @param array $tabAddFileConfig = array()
	 * @param boolean $boolIndexRequire = true
     * @return array
     */
    public static function getTabFile(
        FileFactoryInterface $objFileFactory,
        array $tabAddFileConfig = array(),
        $boolIndexRequire = true
    )
    {
        // Init var
        $result = array();
        $boolIndexRequire = (is_bool($boolIndexRequire) ? $boolIndexRequire : false);

        // Run each files
        foreach($_FILES as $strInputNm => $fileData)
        {
            // Get file and register it
            $fileData = $fileData['name'];
            $strInputPath = null;
            $file = static::getFileEngine(
                $objFileFactory,
                $strInputNm,
                $fileData,
                $strInputPath,
                $tabAddFileConfig,
                $boolIndexRequire
            );
            $result = (
                is_null($file) ?
                    $result :
                    (
                        $boolIndexRequire ?
                            array_merge(
                                $result,
                                (is_array($file) ? $file : array($file))
                            ) :
                            ($result + array($strInputNm => $file))
                    )

            );
        }
		
		// Return result
		return $result;
    }



}