<?php
/**
 * Description :
 * This class allows to define HTTP file factory class.
 * HTTP file factory allows to provide and hydrate HTTP file instance.
 *
 * HTTP file factory uses the following specified configuration, to get and hydrate HTTP file:
 * [
 *     type(required): "http_response",
 *
 *     @see ResponseFile configuration array format
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\file\factory\model;

use liberty_code\file\file\factory\model\DefaultFileFactory;

use liberty_code\http\file\response\model\ResponseFile;
use liberty_code\http\file\factory\library\ConstHttpFileFactory;



class HttpFileFactory extends DefaultFileFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods getters
	// ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFileClassPathFromType($strConfigType)
    {
        // Init var
        $result = null;

        // Get class path of file, from type
        switch($strConfigType)
        {
            case ConstHttpFileFactory::CONFIG_TYPE_HTTP_RESPONSE:
                $result = ResponseFile::class;
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getObjFileNew($strConfigType)
    {
        // Init var
        $result = parent::getObjFileNew($strConfigType);

        // Get class path of file, from type, if required
        if(is_null($result))
        {
            switch($strConfigType)
            {
                case ConstHttpFileFactory::CONFIG_TYPE_HTTP_RESPONSE:
                    $result = new ResponseFile();
                    break;
            }
        }

        // Return result
        return $result;
    }



}