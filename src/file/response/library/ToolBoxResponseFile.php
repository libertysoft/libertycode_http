<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\file\response\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\http\header\model\HeaderData;
use liberty_code\http\file\response\library\ConstResponseFile;



class ToolBoxResponseFile extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods getters
	// ******************************************************************************

    /**
     * Get string mime type,
     * from specified response headers.
     *
     * Header array format:
     * @see HeaderData data source array format.
     *
     * @param array $tabHeader
     * @return null|string
     */
    public static function getStrMimeTypeFromHeader(array $tabHeader)
    {
        // Init var
        $result = (
            (
                array_key_exists(ConstResponseFile::HEADER_KEY_MIME_TYPE, $tabHeader) &&
                is_string($tabHeader[ConstResponseFile::HEADER_KEY_MIME_TYPE]) &&
                (trim($tabHeader[ConstResponseFile::HEADER_KEY_MIME_TYPE] != ''))
            ) ?
                explode(';', $tabHeader[ConstResponseFile::HEADER_KEY_MIME_TYPE])[0] :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get integer size,
     * from specified response headers.
     *
     * Header array format:
     * @see HeaderData data source array format.
     *
     * @param array $tabHeader
     * @return null|integer
     */
    public static function getIntSizeFromHeader(array $tabHeader)
    {
        // Init var
        $result = (
            (
                array_key_exists(ConstResponseFile::HEADER_KEY_SIZE, $tabHeader) &&
                is_numeric($tabHeader[ConstResponseFile::HEADER_KEY_SIZE])
            ) ?
                intval($tabHeader[ConstResponseFile::HEADER_KEY_SIZE]) :
                null
        );

        // Return result
        return $result;
    }



}