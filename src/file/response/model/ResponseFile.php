<?php
/**
 * This class allows to define response file class.
 * Response file is file, allows to get file information and content,
 * from specified HTTP response.
 *
 * Response file uses the following specified configuration:
 * [
 *     Default file configuration,
 *
 *     header(optional): [
 *         Response @see HeaderData data source array format
 *     ],
 *
 *     body(required): "string response body content"
 *
 *     header_mime_type_require(optional: got true, if not found):
 *         true / false (If true, try to get mime type from response headers, if not found (prior content try)),
 *
 *     header_size_require(optional: got true, if not found):
 *         true / false (If true, try to get size from response headers, if not found (prior content try))
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\file\response\model;

use liberty_code\file\file\model\DefaultFile;

use liberty_code\file\file\library\ConstFile;
use liberty_code\http\file\response\library\ConstResponseFile;
use liberty_code\http\file\response\library\ToolBoxResponseFile;
use liberty_code\http\file\response\exception\ConfigInvalidFormatException;



class ResponseFile extends DefaultFile
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        //$result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstFile::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check headers mime type required.
     *
     * @return boolean
     */
    public function checkHeaderMimeTypeRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            (!isset($tabConfig[ConstResponseFile::TAB_CONFIG_KEY_HEADER_MIME_TYPE_REQUIRE])) ||
            (intval($tabConfig[ConstResponseFile::TAB_CONFIG_KEY_HEADER_MIME_TYPE_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }



    /**
     * Check headers size required.
     *
     * @return boolean
     */
    public function checkHeaderSizeRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            (!isset($tabConfig[ConstResponseFile::TAB_CONFIG_KEY_HEADER_SIZE_REQUIRE])) ||
            (intval($tabConfig[ConstResponseFile::TAB_CONFIG_KEY_HEADER_SIZE_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get headers,
     * from configuration.
     *
     * Return format:
     * @see HeaderData data source array format
     *
     * @return array
     */
    public function getTabHeader()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = $tabConfig[ConstResponseFile::TAB_CONFIG_KEY_HEADER];

        // Return result
        return $result;
    }



    /**
     * Get string mime type,
     * from headers, if found.
     *
     * @return null|string
     */
    protected function getStrMimeTypeFromHeader()
    {
        // Return result
        return ToolBoxResponseFile::getStrMimeTypeFromHeader($this->getTabHeader());
    }



    /**
     * @inheritdoc
     */
    public function getStrMimeType()
    {
        // Init var
        $result = (
            (!is_null($strMimeType = $this->getStrMimeTypeFromConfig())) ?
                // Get mime type from config, if required
                $strMimeType :
                (
                    (
                        $this->checkHeaderMimeTypeRequired() &&
                        (!is_null($strMimeType = $this->getStrMimeTypeFromHeader()))
                    ) ?
                        // Get mime type from headers, if required
                        $strMimeType :
                        parent::getStrMimeType()
                )
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getStrContent()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = $tabConfig[ConstResponseFile::TAB_CONFIG_KEY_BODY];

        // Return result
        return $result;
    }



    /**
     * Get integer size,
     * from headers, if found.
     *
     * @return null|integer
     */
    protected function getIntSizeFromHeader()
    {
        // Return result
        return ToolBoxResponseFile::getIntSizeFromHeader($this->getTabHeader());
    }



    /**
     * @inheritdoc
     */
    public function getIntSize()
    {
        // Init var
        $result = (
            (!is_null($intSize = $this->getIntSizeFromConfig())) ?
                // Get size from config, if required
                $intSize :
                (
                    (
                        $this->checkHeaderSizeRequired() &&
                        (!is_null($intSize = $this->getIntSizeFromHeader()))
                    ) ?
                        // Get size from headers, if required
                        $intSize :
                        parent::getIntSize()
                )
        );

        // Return result
        return $result;
    }



}