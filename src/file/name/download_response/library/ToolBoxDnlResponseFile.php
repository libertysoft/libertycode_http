<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\file\name\download_response\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\http\header\model\HeaderData;
use liberty_code\http\file\name\download_response\library\ConstDnlResponseFile;



class ToolBoxDnlResponseFile extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods getters
	// ******************************************************************************

    /**
     * Get string name,
     * from specified response headers.
     *
     * Header array format:
     * @see HeaderData data source array format.
     *
     * @param array $tabHeader
     * @return null|string
     */
    public static function getStrNameFromHeader(array $tabHeader)
    {
        // Init var
        $tabMatch = array();
        $strPattern = '#filename="(.+)"#';
        $result = (
            (
                array_key_exists(ConstDnlResponseFile::HEADER_KEY_NAME, $tabHeader) &&
                is_string($tabHeader[ConstDnlResponseFile::HEADER_KEY_NAME]) &&
                (preg_match($strPattern, $tabHeader[ConstDnlResponseFile::HEADER_KEY_NAME], $tabMatch) === 1) &&
                isset($tabMatch[1])
            ) ?
                $tabMatch[1] :
                null
        );

        // Return result
        return $result;
    }



}