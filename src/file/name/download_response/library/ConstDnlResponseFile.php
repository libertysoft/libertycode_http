<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\file\name\download_response\library;



class ConstDnlResponseFile
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration
    const TAB_CONFIG_KEY_HEADER = 'header';
    const TAB_CONFIG_KEY_BODY = 'body';
    const TAB_CONFIG_KEY_HEADER_MIME_TYPE_REQUIRE = 'header_mime_type_require';
    const TAB_CONFIG_KEY_HEADER_SIZE_REQUIRE = 'header_size_require';



    // Headers configuration
    const HEADER_KEY_NAME = 'Content-Disposition';



    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the downloaded response file configuration standard.';



}