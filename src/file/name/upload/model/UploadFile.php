<?php
/**
 * This class allows to define uploaded file class.
 * Uploaded file is file, allows to get file information and content,
 * from uploaded files ($_FILES).
 *
 * Uploaded file uses the following specified configuration:
 * [
 *     Default named file configuration,
 *
 *     input_name(required): "string input name",
 *
 *     input_path(optional): "string input path",
 *
 *     input_path_separator(optional: got '/', if not found): "string input path separator",
 *
 *     upload_mime_type_require(optional: got true if not found):
 *         true / false (If true, get mime type from uploading, if not found),
 *
 *     upload_size_require(optional: got true if not found):
 *         true / false (If true, get size from uploading, if not found)
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\file\name\upload\model;

use liberty_code\file\file\name\model\DefaultNameFile;

use liberty_code\file\file\library\ConstFile;
use liberty_code\http\file\name\upload\library\ConstUploadFile;
use liberty_code\http\file\name\upload\library\ToolBoxUploadFile;
use liberty_code\http\file\name\upload\exception\ConfigInvalidFormatException;



class UploadFile extends DefaultNameFile
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        //$result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstFile::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check uploaded mime type required.
     *
     * @return boolean
     */
    public function checkUploadMimeTypeRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            (!isset($tabConfig[ConstUploadFile::TAB_CONFIG_KEY_UPLOAD_MIME_TYPE_REQUIRE])) ||
            (intval($tabConfig[ConstUploadFile::TAB_CONFIG_KEY_UPLOAD_MIME_TYPE_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }



    /**
     * Check uploaded size required.
     *
     * @return boolean
     */
    public function checkUploadSizeRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            (!isset($tabConfig[ConstUploadFile::TAB_CONFIG_KEY_UPLOAD_SIZE_REQUIRE])) ||
            (intval($tabConfig[ConstUploadFile::TAB_CONFIG_KEY_UPLOAD_SIZE_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get associative array of file information,
     * from configuration.
     *
     * Return array format:
     * @see ToolBoxUploadFile::getTabInfo() return array format.
     *
     * @return array
     */
    public function getTabInfo()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = ToolBoxUploadFile::getTabInfo(
            $tabConfig[ConstUploadFile::TAB_CONFIG_KEY_INPUT_NAME],
            (
                isset($tabConfig[ConstUploadFile::TAB_CONFIG_KEY_INPUT_PATH]) ?
                    $tabConfig[ConstUploadFile::TAB_CONFIG_KEY_INPUT_PATH] :
                    null
            ),
            (
                isset($tabConfig[ConstUploadFile::TAB_CONFIG_KEY_INPUT_PATH_SEPARATOR]) ?
                    $tabConfig[ConstUploadFile::TAB_CONFIG_KEY_INPUT_PATH_SEPARATOR] :
                    null
            )
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getStrName()
    {
        // Init var
        $tabInfo = $this->getTabInfo();
        $result = $tabInfo['name'];

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getStrMimeType()
    {
        // Init var
        $tabInfo = $this->getTabInfo();
        $result = (
            (!is_null($strMimeType = $this->getStrMimeTypeFromConfig())) ?
                // Get mime type from config, if required
                $strMimeType :
                (
                    $this->checkUploadMimeTypeRequired() ?
                        // Get mime type from uploaded info, if required
                        $tabInfo['type'] :
                        parent::getStrMimeType()
                )
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getStrContent()
    {
        // Init var
        $tabInfo = $this->getTabInfo();
        $strFullPath = $tabInfo['tmp_name'];
        $result = file_get_contents($strFullPath);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getIntSize()
    {
        // Init var
        $tabInfo = $this->getTabInfo();
        $result = (
            (!is_null($intSize = $this->getIntSizeFromConfig())) ?
                // Get size from config, if required
                $intSize :
                (
                    $this->checkUploadSizeRequired() ?
                        // Get size from uploaded info, if required
                        intval($tabInfo['size']) :
                        parent::getIntSize()
                )
        );

        // Return result
        return $result;
    }



}