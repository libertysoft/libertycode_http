<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\file\name\upload\exception;

use Exception;

use liberty_code\http\file\name\upload\library\ConstUploadFile;
use liberty_code\http\file\name\upload\library\ToolBoxUploadFile;



class ConfigInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstUploadFile::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $result =
            // Check valid input name
            isset($config[ConstUploadFile::TAB_CONFIG_KEY_INPUT_NAME]) &&
            is_string($config[ConstUploadFile::TAB_CONFIG_KEY_INPUT_NAME]) &&
            (trim($config[ConstUploadFile::TAB_CONFIG_KEY_INPUT_NAME]) != '') &&

            // Check valid input path
            (
                (!isset($config[ConstUploadFile::TAB_CONFIG_KEY_INPUT_PATH])) ||
                (
                    is_string($config[ConstUploadFile::TAB_CONFIG_KEY_INPUT_PATH]) &&
                    (trim($config[ConstUploadFile::TAB_CONFIG_KEY_INPUT_PATH]) != '')
                )
            ) &&

            // Check valid input path separator
            (
                (!isset($config[ConstUploadFile::TAB_CONFIG_KEY_INPUT_PATH_SEPARATOR])) ||
                (
                    is_string($config[ConstUploadFile::TAB_CONFIG_KEY_INPUT_PATH_SEPARATOR]) &&
                    (trim($config[ConstUploadFile::TAB_CONFIG_KEY_INPUT_PATH_SEPARATOR]) != '')
                )
            ) &&

            // Check valid input file
            is_array($tabInfo = ToolBoxUploadFile::getTabInfo(
                $config[ConstUploadFile::TAB_CONFIG_KEY_INPUT_NAME],
                (
                    isset($config[ConstUploadFile::TAB_CONFIG_KEY_INPUT_PATH]) ?
                        $config[ConstUploadFile::TAB_CONFIG_KEY_INPUT_PATH] :
                        null
                ),
                (
                    isset($config[ConstUploadFile::TAB_CONFIG_KEY_INPUT_PATH_SEPARATOR]) ?
                        $config[ConstUploadFile::TAB_CONFIG_KEY_INPUT_PATH_SEPARATOR] :
                        null
                )
            )) &&
            ($tabInfo['error'] == UPLOAD_ERR_OK) &&

            // Check valid uploaded mime type required option
            (
                (!isset($config[ConstUploadFile::TAB_CONFIG_KEY_UPLOAD_MIME_TYPE_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstUploadFile::TAB_CONFIG_KEY_UPLOAD_MIME_TYPE_REQUIRE]) ||
                    is_int($config[ConstUploadFile::TAB_CONFIG_KEY_UPLOAD_MIME_TYPE_REQUIRE]) ||
                    (
                        is_string($config[ConstUploadFile::TAB_CONFIG_KEY_UPLOAD_MIME_TYPE_REQUIRE]) &&
                        ctype_digit($config[ConstUploadFile::TAB_CONFIG_KEY_UPLOAD_MIME_TYPE_REQUIRE])
                    )
                )
            ) &&

            // Check valid uploaded size required option
            (
                (!isset($config[ConstUploadFile::TAB_CONFIG_KEY_UPLOAD_SIZE_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstUploadFile::TAB_CONFIG_KEY_UPLOAD_SIZE_REQUIRE]) ||
                    is_int($config[ConstUploadFile::TAB_CONFIG_KEY_UPLOAD_SIZE_REQUIRE]) ||
                    (
                        is_string($config[ConstUploadFile::TAB_CONFIG_KEY_UPLOAD_SIZE_REQUIRE]) &&
                        ctype_digit($config[ConstUploadFile::TAB_CONFIG_KEY_UPLOAD_SIZE_REQUIRE])
                    )
                )
            );

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format.
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($config)
    {
		// Init var
		$result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}