<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\file\name\upload\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\data\data\table\path\library\ToolBoxPathTableData;



class ToolBoxUploadFile extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods getters
	// ******************************************************************************

    /**
     * Get associative array of file information,
     * from specified input name,
     * and specified input path, if required.
     *
     * Return array format:
     * @see $_FILES for one file name return format:
     * [
     *     name(required): 'string file name',
     *
     *     type(required): 'string mime type',
     *
     *     tmp_name(required): 'string temporary path',
     *
     *     error(required): integer upload error code,
     *
     *     size(required): integer file size
     * ]
     *
     * @param string $strInputNm
     * @param string $strInputPath = null
     * @param string $strInputPathSeparator = '/'
     * @return null|array
     */
    public static function getTabInfo(
        $strInputNm,
        $strInputPath = null,
        $strInputPathSeparator = '/'
    )
    {
        // Init var
        $strInputPath = (is_string($strInputPath) ? $strInputPath : null);
        $strInputPathSeparator = (is_string($strInputPathSeparator) ? $strInputPathSeparator : '/');
        $tabKey = array(
            'name',
            'type',
            'tmp_name',
            'error',
            'size'
        );
        $result = null;

        // Run each key, if required
        if(is_string($strInputNm))
        {
            $result = array();
            for($intCpt = 0; ($intCpt < count($tabKey)) && is_array($result); $intCpt++)
            {
                // Get file info
                $strKey = $tabKey[$intCpt];
                $value = ToolBoxPathTableData::getValue(
                    $_FILES,
                    (
                        (is_string($strInputPath)) ?
                            sprintf(
                                '%2$s%1$s%3$s%1$s%4$s',
                                $strInputPathSeparator,
                                $strInputNm,
                                $strKey,
                                $strInputPath
                            ) :
                            sprintf(
                                '%2$s%1$s%3$s',
                                $strInputPathSeparator,
                                $strInputNm,
                                $strKey
                            )
                    ),
                    $strInputPathSeparator
                );

                // Register file info, if required (found)
                if((!is_null($value)) && (!is_array($value)))
                {
                    $result[$strKey] = $value;
                }
                else
                {
                    $result = null;
                };
            }
        }

        // Return result
        return $result;
    }



}