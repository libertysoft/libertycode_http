<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\file\name\upload\library;



class ConstUploadFile
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration
    const TAB_CONFIG_KEY_INPUT_NAME = 'input_name';
    const TAB_CONFIG_KEY_INPUT_PATH = 'input_path';
    const TAB_CONFIG_KEY_INPUT_PATH_SEPARATOR = 'input_path_separator';
    const TAB_CONFIG_KEY_UPLOAD_MIME_TYPE_REQUIRE = 'upload_mime_type_require';
    const TAB_CONFIG_KEY_UPLOAD_SIZE_REQUIRE = 'upload_size_require';



    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the uploaded file configuration standard.';
}