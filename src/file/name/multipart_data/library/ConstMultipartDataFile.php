<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\file\name\multipart_data\library;



class ConstMultipartDataFile
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration
    const TAB_CONFIG_KEY_DATA_SOURCE = 'data_source';
    const TAB_CONFIG_KEY_DATA_SOURCE_MIME_TYPE_REQUIRE = 'data_source_mime_type_require';
    const TAB_CONFIG_KEY_DATA_SOURCE_SIZE_REQUIRE = 'data_source_size_require';



    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the multipart data file configuration standard.';
}