<?php
/**
 * This class allows to define multipart data file class.
 * Multipart data file is file, allows to get file information and content,
 * from specified part data source.
 *
 * Multipart data file uses the following specified configuration:
 * [
 *     Default named file configuration,
 *
 *     data_source(required): "string part data source",
 *
 *     data_source_mime_type_require(optional: got true, if not found):
 *         true / false (If true, try to get mime type from data source, if not found (prior content try)),
 *
 *     data_source_size_require(optional: got true, if not found):
 *         true / false (If true, try to get size from data source, if not found (prior content try))
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\file\name\multipart_data\model;

use liberty_code\file\file\name\model\DefaultNameFile;

use liberty_code\file\file\library\ConstFile;
use liberty_code\http\multipart\library\ToolBoxMultipart;
use liberty_code\http\multipart\data\library\ToolBoxMultipartData;
use liberty_code\http\file\name\multipart_data\library\ConstMultipartDataFile;
use liberty_code\http\file\name\multipart_data\exception\ConfigInvalidFormatException;



class MultipartDataFile extends DefaultNameFile
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        //$result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstFile::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check data source mime type required.
     *
     * @return boolean
     */
    public function checkDataSrcMimeTypeRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            (!isset($tabConfig[ConstMultipartDataFile::TAB_CONFIG_KEY_DATA_SOURCE_MIME_TYPE_REQUIRE])) ||
            (intval($tabConfig[ConstMultipartDataFile::TAB_CONFIG_KEY_DATA_SOURCE_MIME_TYPE_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }



    /**
     * Check data source size required.
     *
     * @return boolean
     */
    public function checkDataSrcSizeRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            (!isset($tabConfig[ConstMultipartDataFile::TAB_CONFIG_KEY_DATA_SOURCE_SIZE_REQUIRE])) ||
            (intval($tabConfig[ConstMultipartDataFile::TAB_CONFIG_KEY_DATA_SOURCE_SIZE_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get string data source,
     * from configuration.
     *
     * @return string
     */
    public function getStrDataSrc()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = $tabConfig[ConstMultipartDataFile::TAB_CONFIG_KEY_DATA_SOURCE];

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws ConfigInvalidFormatException
     */
    public function getStrName()
    {
        // Init var
        $strHeaderLines = ToolBoxMultipart::getStrPartHeaderLines($this->getStrDataSrc());
        $result = ToolBoxMultipartData::getStrFileName($strHeaderLines);

        // Check valid name
        if(is_null($result))
        {
            throw new ConfigInvalidFormatException(serialize($this->getTabConfig()));
        }

        // Return result
        return $result;
    }



    /**
     * Get string mime type,
     * from data source, if found.
     *
     * @return null|string
     */
    protected function getStrMimeTypeFromDataSrc()
    {
        // Init var
        $strHeaderLines = ToolBoxMultipart::getStrPartHeaderLines($this->getStrDataSrc());
        $result = ToolBoxMultipartData::getStrMimeType($strHeaderLines);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getStrMimeType()
    {
        // Init var
        $result = (
            (!is_null($strMimeType = $this->getStrMimeTypeFromConfig())) ?
                // Get mime type from config, if required
                $strMimeType :
                (
                    (
                        $this->checkDataSrcMimeTypeRequired() &&
                        (!is_null($strMimeType = $this->getStrMimeTypeFromDataSrc()))
                    ) ?
                        // Get mime type from data source, if required
                        $strMimeType :
                        parent::getStrMimeType()
                )
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws ConfigInvalidFormatException
     */
    public function getStrContent()
    {
        // Init var
        $result = ToolBoxMultipart::getStrPartBody($this->getStrDataSrc());

        // Check valid content
        if(is_null($result))
        {
            throw new ConfigInvalidFormatException(serialize($this->getTabConfig()));
        }

        // Return result
        return $result;
    }



    /**
     * Get integer size,
     * from data source, if found.
     *
     * @return null|integer
     */
    protected function getIntSizeFromDataSrc()
    {
        // Init var
        $strHeaderLines = ToolBoxMultipart::getStrPartHeaderLines($this->getStrDataSrc());
        $result = ToolBoxMultipartData::getIntSize($strHeaderLines);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getIntSize()
    {
        // Init var
        $result = (
            (!is_null($intSize = $this->getIntSizeFromConfig())) ?
                // Get size from config, if required
                $intSize :
                (
                    (
                        $this->checkDataSrcSizeRequired() &&
                        (!is_null($intSize = $this->getIntSizeFromDataSrc()))
                    ) ?
                        // Get size from data source info, if required
                        $intSize :
                        parent::getIntSize()
                )
        );

        // Return result
        return $result;
    }



}