<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\file\name\multipart_data\exception;

use Exception;

use liberty_code\http\file\name\multipart_data\library\ConstMultipartDataFile;



class ConfigInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstMultipartDataFile::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $result =
            // Check valid data source
            isset($config[ConstMultipartDataFile::TAB_CONFIG_KEY_DATA_SOURCE]) &&
            is_string($config[ConstMultipartDataFile::TAB_CONFIG_KEY_DATA_SOURCE]) &&
            (trim($config[ConstMultipartDataFile::TAB_CONFIG_KEY_DATA_SOURCE]) != '') &&

            // Check valid data source mime type required option
            (
                (!isset($config[ConstMultipartDataFile::TAB_CONFIG_KEY_DATA_SOURCE_MIME_TYPE_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstMultipartDataFile::TAB_CONFIG_KEY_DATA_SOURCE_MIME_TYPE_REQUIRE]) ||
                    is_int($config[ConstMultipartDataFile::TAB_CONFIG_KEY_DATA_SOURCE_MIME_TYPE_REQUIRE]) ||
                    (
                        is_string($config[ConstMultipartDataFile::TAB_CONFIG_KEY_DATA_SOURCE_MIME_TYPE_REQUIRE]) &&
                        ctype_digit($config[ConstMultipartDataFile::TAB_CONFIG_KEY_DATA_SOURCE_MIME_TYPE_REQUIRE])
                    )
                )
            ) &&

            // Check valid data source size required option
            (
                (!isset($config[ConstMultipartDataFile::TAB_CONFIG_KEY_DATA_SOURCE_SIZE_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstMultipartDataFile::TAB_CONFIG_KEY_DATA_SOURCE_SIZE_REQUIRE]) ||
                    is_int($config[ConstMultipartDataFile::TAB_CONFIG_KEY_DATA_SOURCE_SIZE_REQUIRE]) ||
                    (
                        is_string($config[ConstMultipartDataFile::TAB_CONFIG_KEY_DATA_SOURCE_SIZE_REQUIRE]) &&
                        ctype_digit($config[ConstMultipartDataFile::TAB_CONFIG_KEY_DATA_SOURCE_SIZE_REQUIRE])
                    )
                )
            );

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format.
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
    public static function setCheck($config)
    {
		// Init var
		$result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}