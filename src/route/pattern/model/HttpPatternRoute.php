<?php
/**
 * Description :
 * This class allows to define HTTP pattern route.
 * HTTP pattern route is pattern route, using HTTP request route source.
 *
 * HTTP pattern route uses the following specified configuration:
 * [
 *     Pattern route configuration,
 *
 *     source_method(optional: got from default config if not found):
 *         "string REGEXP pattern (applied on http method)"
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\route\pattern\model;

use liberty_code\route\route\pattern\model\PatternRoute;

use liberty_code\http\route\library\ToolBoxHttpRoute;



class HttpPatternRoute extends PatternRoute
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods check
	// ******************************************************************************
	
    /**
     * @inheritdoc
     */
    public function checkMatches($strSrc)
    {
        // Init var
		$tabConfig = $this->getTabConfig();
		$strMethod = ToolBoxHttpRoute::getStrMethodFromRequestSrc($strSrc, '');
		$strConfigMethod = ToolBoxHttpRoute::getStrMethodFromRouteConfig($tabConfig);
        $result =
            ToolBoxHttpRoute::checkMethodMatches($strMethod, $strConfigMethod) && // Check method matches
            parent::checkMatches($strSrc); // Check URL matches
		
        // Return result
        return $result;
    }


	
	
	
	// Methods getters
    // ******************************************************************************
	
	/**
     * @inheritdoc
     */
    protected function getStrMatchFormatSource($strSrc)
	{
        // Return result
        return ToolBoxHttpRoute::getStrUrlFromRequestSrc(
            $strSrc,
            (is_string($strSrc) ? $strSrc : '')
        );
    }



    /**
     * @inheritdoc
     */
    protected function getStrCallElmFormatSource($strSrc)
    {
        // Return result
        return $this->getStrMatchFormatSource($strSrc);
    }



    /**
     * @inheritdoc
     */
    protected function getStrCallArgFormatSource($strSrc)
    {
        // Return result
        return $this->getStrMatchFormatSource($strSrc);
    }



}