<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\route\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\http\config\library\ConstConfig;
use liberty_code\http\config\model\DefaultConfig;
use liberty_code\http\route\library\ConstHttpRoute;



class ToolBoxHttpRoute extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods check
	// ******************************************************************************
	
    /**
     * Check if specified request method is valid with specified configuration.
     *
	 * @param string $strRequestSrcMethod
	 * @param string $strRouteConfigMethod
     * @return boolean
     */
    public static function checkMethodMatches($strRequestSrcMethod, $strRouteConfigMethod)
    {
        // Init var
		$result = false;
		
		// Check match if possible
		if(is_string($strRequestSrcMethod) && is_string($strRouteConfigMethod))
		{
			// Get info
			$strPatternMethod = sprintf(ConstHttpRoute::CONF_SOURCE_METHOD_PATTERN_DECORATION, $strRouteConfigMethod);
			
			// Check method matches
			$result = (@preg_match($strPatternMethod, $strRequestSrcMethod) == 1);
		}
		
        // Return result
        return $result;
    }
	
	
	
	
	
	// Methods getters
    // ******************************************************************************
	
	/**
     * Get route configuration.
     *
     * @return DefaultConfig
     */
    public static function getObjConfig()
    {
        // Return result
        return DefaultConfig::instanceGetDefault();
    }
	
	
	
	/**
     * Get URL from specified request source.
     * 
	 * @param string $strRequestSrc
     * @param mixed $default = null
	 * @return null|mixed|string
     */
    public static function getStrUrlFromRequestSrc($strRequestSrc, $default = null)
    {
		// Init var
        $result = $default;
		$strLink = static::getObjConfig()->getStrHttpRequestSourceLink();
		
		// Get URL, if required
		if(is_string($strRequestSrc) && (strpos($strRequestSrc, $strLink) !== false))
		{
			$tabStr = explode($strLink, $strRequestSrc);
			if(isset($tabStr[1]))
			{
				$result = $tabStr[1];
			}
		}
		
		// Return result
		return $result;
    }
	
	
	
	/**
     * Get method from specified request source.
     * 
	 * @param string $strRequestSrc
     * @param mixed $default = null
	 * @return null|mixed|string
     */
    public static function getStrMethodFromRequestSrc($strRequestSrc, $default = null)
    {
		// Init var
        $result = $default;
		$strLink = static::getObjConfig()->getStrHttpRequestSourceLink();
		
		// Get method
		if(is_string($strRequestSrc) && (strpos($strRequestSrc, $strLink) !== false))
		{
			$tabStr = explode($strLink, $strRequestSrc);
			if(isset($tabStr[0]))
			{
				$result = $tabStr[0];
			}
		}
        $result = strtoupper($result);
		
		// Return result
		return $result;
    }



	/**
     * Get method from specified route configuration.
     * 
	 * @param array $tabConfig
	 * @return string
     */
    public static function getStrMethodFromRouteConfig(array $tabConfig)
    {
		// Init var
		$strDefaultMethod = static::getObjConfig()->getStrHttpRouteMethod();
        $result = (
            // Register config method in result if found
            (
                (isset($tabConfig[ConstHttpRoute::TAB_CONFIG_KEY_SOURCE_METHOD])) &&
                is_string($tabConfig[ConstHttpRoute::TAB_CONFIG_KEY_SOURCE_METHOD]) &&
                (trim($tabConfig[ConstHttpRoute::TAB_CONFIG_KEY_SOURCE_METHOD]) != '')
            ) ?
                $tabConfig[ConstHttpRoute::TAB_CONFIG_KEY_SOURCE_METHOD] :
                (
                    // Register default method in result if found
                    (
                        (!is_null($strDefaultMethod)) &&
                        is_string($strDefaultMethod)
                    ) ?
                        $strDefaultMethod :
                        ConstConfig::DATA_DEFAULT_VALUE_HTTP_ROUTE_METHOD
                )
        );
        $result = strtoupper($result);
		
		// Return result
		return $result;
    }
	
	
	
}