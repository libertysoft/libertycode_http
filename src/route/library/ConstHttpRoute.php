<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\route\library;



class ConstHttpRoute
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************
	
	// Configuration keys
	const TAB_CONFIG_KEY_SOURCE_METHOD = 'source_method';
	
	// Configuration
	const CONF_SOURCE_METHOD_PATTERN_DECORATION = '#%1$s#i'; // Without case sensitive



}