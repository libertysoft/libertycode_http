<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\route\factory\library;



class ConstHttpRouteFactory
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Type configuration
    const CONFIG_TYPE_HTTP_PATTERN = 'http_pattern';
    const CONFIG_TYPE_HTTP_PARAM = 'http_param';
    const CONFIG_TYPE_HTTP_FIX = 'http_fix';
    const CONFIG_TYPE_HTTP_SEPARATOR = 'http_separator';
}