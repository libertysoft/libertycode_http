<?php
/**
 * Description :
 * This class allows to define HTTP route factory class.
 * HTTP route factory allows to provide and hydrate HTTP route instance.
 *
 * HTTP route factory uses the following specified configuration, to get and hydrate route:
 * [
 *     -> Configuration key(optional):
 *         "route key OR source (if source not found on HTTP pattern route configuration)"
 *     type(required): "http_pattern",
 *     HTTP pattern route configuration (@see HttpPatternRoute )
 *
 *     OR
 *
 *     -> Configuration key(optional):
 *         "route key OR source (if source not found on HTTP param route configuration)"
 *     type(optional): "http_param",
 *     HTTP param route configuration (@see HttpParamRoute )
 *
 *     OR
 *
 *     -> Configuration key(optional):
 *         "route key OR source (if source not found on HTTP fix route configuration)"
 *     type(required): "http_fix",
 *     HTTP fix route configuration (@see HttpFixRoute )
 *
 *     OR
 *
 *     -> Configuration key(optional): "route key"
 *     type(required): "http_separator",
 *     HTTP separator route configuration (@see HttpSeparatorRoute )
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\route\factory\model;

use liberty_code\route\route\factory\model\DefaultRouteFactory;

use liberty_code\route\route\library\ConstRoute;
use liberty_code\route\route\pattern\library\ConstPatternRoute;
use liberty_code\route\route\param\library\ConstParamRoute;
use liberty_code\route\route\fix\library\ConstFixRoute;
use liberty_code\route\route\factory\library\ConstRouteFactory;
use liberty_code\http\route\pattern\model\HttpPatternRoute;
use liberty_code\http\route\param\model\HttpParamRoute;
use liberty_code\http\route\fix\model\HttpFixRoute;
use liberty_code\http\route\separator\model\HttpSeparatorRoute;
use liberty_code\http\route\factory\library\ConstHttpRouteFactory;



class HttpRouteFactory extends DefaultRouteFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods getters
	// ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabConfigFormat(array $tabConfig, $strConfigKey = null)
    {
        // Init var
        $result = parent::getTabConfigFormat($tabConfig, $strConfigKey);

        // Format configuration, if required
        if(!is_null($strConfigKey))
        {
            // Get source configuration, from type
            $strConfigKeySource = null;
            $strConfigType = (
                array_key_exists(ConstRouteFactory::TAB_CONFIG_KEY_TYPE, $tabConfig) ?
                    $tabConfig[ConstRouteFactory::TAB_CONFIG_KEY_TYPE] :
                    null
            );
            switch($strConfigType)
            {
                case ConstHttpRouteFactory::CONFIG_TYPE_HTTP_PATTERN:
                    $strConfigKeySource = ConstPatternRoute::TAB_CONFIG_KEY_SOURCE;
                    break;

                case null:
                case ConstHttpRouteFactory::CONFIG_TYPE_HTTP_PARAM:
                    $strConfigKeySource = ConstParamRoute::TAB_CONFIG_KEY_SOURCE;
                    break;

                case ConstHttpRouteFactory::CONFIG_TYPE_HTTP_FIX:
                    $strConfigKeySource = ConstFixRoute::TAB_CONFIG_KEY_SOURCE;
                    break;
            }

            // Add configured key as source, if required
            if(
                (!is_null($strConfigKeySource)) &&
                (!array_key_exists($strConfigKeySource, $result))
            )
            {
                $result[$strConfigKeySource] = $strConfigKey;
            }
            // Add configured key as route key, if required
            else if(!array_key_exists(ConstRoute::TAB_CONFIG_KEY_KEY, $result))
            {
                $result[ConstRoute::TAB_CONFIG_KEY_KEY] = $strConfigKey;
            }
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getStrRouteClassPathFromType($strConfigType)
    {
        // Init var
        $result = null;

        // Get class path of route, from type
        switch($strConfigType)
        {
            case ConstHttpRouteFactory::CONFIG_TYPE_HTTP_PATTERN:
                $result = HttpPatternRoute::class;
                break;

            case null:
            case ConstHttpRouteFactory::CONFIG_TYPE_HTTP_PARAM:
                $result = HttpParamRoute::class;
                break;

            case ConstHttpRouteFactory::CONFIG_TYPE_HTTP_FIX:
                $result = HttpFixRoute::class;
                break;

            case ConstHttpRouteFactory::CONFIG_TYPE_HTTP_SEPARATOR:
                $result = HttpSeparatorRoute::class;
                break;
        }

        // Return result
        return $result;
    }

	
	
}