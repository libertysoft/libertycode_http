<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\cookie\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\http\header\library\ToolBoxHeader;



class ToolBoxCookie extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods setters
    // ******************************************************************************

    /**
     * Reset cookie response headers.
     */
    protected static function resetResponseHeader()
    {
        if (!headers_sent()) {
            // Init var
            $strCookieHeaderKey = 'Set-Cookie';

            // Get last cookie headers
            $tabHeaderLine = array();
            foreach(headers_list() as $strHeaderLine)
            {
                // Get info
                $strHeaderKey = ToolBoxHeader::getStrKey($strHeaderLine);
                $strHeaderValue = ToolBoxHeader::getStrValue($strHeaderLine);

                // Register cookie header, if required
                $strPattern = '#^([^=]+)=.*$#';
                $tabMatch = array();
                if(
                    (!is_null($strHeaderKey)) &&
                    ($strHeaderKey === $strCookieHeaderKey) &&
                    (!is_null($strHeaderValue)) &&
                    (preg_match($strPattern, $strHeaderValue, $tabMatch) === 1)
                )
                {
                    $strCookieKey = trim($tabMatch[1]);
                    $tabHeaderLine[$strCookieKey] = $strHeaderLine;
                }
            }

            // Remove cookie headers
            header_remove($strCookieHeaderKey);

            // Set cookie headers
            foreach($tabHeaderLine as $strHeaderLine)
            {
                header($strHeaderLine, false);
            }
        }
    }



    /**
     * Set specified cookie.
     *
     * @param string $strKey
     * @param string $strValue
     * @param integer $intExpireTimoutTimestamp = 0
     * @param string $strPath = ''
     * @param string $strDomainNm = ''
     * @param boolean $boolSecureRequire = false
     * @param boolean $boolHttpOnlyRequire = false
     * @param boolean $boolUrlEncodeRequire = true
     * @return bool
     */
    public static function set(
        $strKey,
        $strValue,
        $intExpireTimoutTimestamp = 0,
        $strPath = '',
        $strDomainNm = '',
        $boolSecureRequire = false,
        $boolHttpOnlyRequire = false,
        $boolUrlEncodeRequire = true
    )
    {
        // Init var
        $boolUrlEncodeRequire = (is_bool($boolUrlEncodeRequire) ? $boolUrlEncodeRequire : true);
        $result = (
            $boolUrlEncodeRequire ?
                setcookie(
                    $strKey,
                    $strValue,
                    $intExpireTimoutTimestamp,
                    $strPath,
                    $strDomainNm,
                    $boolSecureRequire,
                    $boolHttpOnlyRequire
                ) :
                setrawcookie(
                    $strKey,
                    $strValue,
                    $intExpireTimoutTimestamp,
                    $strPath,
                    $strDomainNm,
                    $boolSecureRequire,
                    $boolHttpOnlyRequire
                )
        );

        // Reset cookie headers
        static::resetResponseHeader();

        // Return result
        return $result;
    }



}