<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\curl\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\http\header\library\ToolBoxHeader;
use liberty_code\http\header\exception\DataSrcInvalidFormatException;
use liberty_code\http\header\model\HeaderData;



class ToolBoxCurl extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get cURL object,
     * from specified HTTP info.
     *
     * Header array format:
     * @see HeaderData data source array format.
     *
     * Additional option array format:
     * [key: cURL option constant name => value: cURL option value]
     *
     * @param string $strMethod
     * @param string $strUrl
     * @param null|string $strProtocolVersionNum = null
     * @param array $tabHeader = array()
     * @param null|string $strBody = null
     * @param array $tabAddOption = array()
     * @return null|mixed
     */
    public static function getObjCurl(
        $strMethod,
        $strUrl,
        $strProtocolVersionNum = null,
        array $tabHeader = array(),
        $strBody = null,
        array $tabAddOption = array()
    )
    {
        // Init var
        $result = null;
        $strMethod = ((is_string($strMethod) && (trim($strMethod) != '')) ? strtoupper($strMethod) : null);
        $strUrl = ((is_string($strUrl) && (trim($strUrl) != '')) ? $strUrl : null);
        $strBody = (is_string($strBody) ? $strBody : null);

        // Init protocol version
        $curlProtocolVersion = (
            (
                is_string($strProtocolVersionNum) &&
                defined($strConstNm = sprintf(
                    'CURL_HTTP_VERSION_%s',
                    str_replace('.', '_', $strProtocolVersionNum)
                ))
            ) ?
                constant($strConstNm) :
                null
        );

        // Init headers
        $tabHeader = array_merge(
            array(
                // Init headers, to prevent curl addition
                'Expect' => '',
                'Content-Type' => ''
            ),
            (
                DataSrcInvalidFormatException::checkDataSrcIsValid($tabHeader) ?
                    $tabHeader :
                    array()
            )
        );
        $tabHeader = ToolBoxHeader::getHeaderLine($tabHeader, true);

        // Build result, if possible
        if((!is_null($strMethod)) && (!is_null($strUrl)))
        {
            // Init result
            $result = curl_init();

            // Add additional options, if required
            if (count($tabAddOption) > 0) {
                foreach ($tabAddOption as $key => $value) {
                    curl_setopt($result, $key, $value);
                }
            }

            // Init cURL options
            curl_setopt($result, CURLOPT_CUSTOMREQUEST, $strMethod);
            curl_setopt($result, CURLOPT_URL, $strUrl);

            // Init cURL protocol version option, if required
            if (!is_null($curlProtocolVersion)) {
                curl_setopt($result, CURLOPT_HTTP_VERSION, $curlProtocolVersion);
            }

            // Init cURL headers options, if required
            if (count($tabHeader) > 0) {
                curl_setopt($result, CURLOPT_HTTPHEADER, $tabHeader);
            }

            // Init cURL body options, if required
            if (!is_null($strBody)) {
                curl_setopt($result, CURLOPT_POST, true);
                curl_setopt($result, CURLOPT_POSTFIELDS, $strBody);
            }
        }

        // Return result
        return $result;
    }



    /**
     * Get response body,
     * from specified HTTP info.
     * Return string if success, false else.
     *
     * Header array format:
     * @see getObjCurl() header array format.
     *
     * Additional option array format:
     * @see getObjCurl() additional option array format.
     *
     * Response status line return format:
     * If success and string provided: string response status line (first line of response headers).
     * Else: null.
     *
     * Response header lines return array format:
     * If success and array provided: index array of string response header lines (exclude status line (first line)).
     * Else: null.
     *
     * @param string $strMethod
     * @param string $strUrl
     * @param null|string $strProtocolVersionNum = null
     * @param array $tabHeader = array()
     * @param null|string $strBody = null
     * @param array $tabAddOption = array()
     * @param null|string &$strResponseStatusLine = null
     * @param null|array &$tabResponseHeaderLine = null
     * @param mixed &$objCurl = null
     * @return boolean|string
     */
    public static function getStrResponseBody(
        $strMethod,
        $strUrl,
        $strProtocolVersionNum = null,
        array $tabHeader = array(),
        $strBody = null,
        array $tabAddOption = array(),
        &$strResponseStatusLine = null,
        array &$tabResponseHeaderLine = null,
        &$objCurl = null
    )
    {
        // Init var
        $strResponseStatusLine = (is_string($strResponseStatusLine) ? '' : null);
        $tabResponseHeaderLine = (is_array($tabResponseHeaderLine) ? array() : null);
        $objCurl = static::getObjCurl(
            $strMethod,
            $strUrl,
            $strProtocolVersionNum,
            $tabHeader,
            $strBody,
            $tabAddOption
        );
        
        if(!is_null($objCurl))
        {
            // Init cURL return options
            curl_setopt($objCurl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($objCurl, CURLOPT_HEADER, false);
            curl_setopt($objCurl, CURLOPT_NOBODY, false);

            // Init cURL return headers options
            if(
                (!is_null($strResponseStatusLine)) ||
                (!is_null($tabResponseHeaderLine))
            )
            {
                $boolResponseStatusLineFound = false;
                curl_setopt(
                    $objCurl,
                    CURLOPT_HEADERFUNCTION,
                    function($objCurl, $strHeaderLine) use
                    (
                        &$boolResponseStatusLineFound,
                        &$strResponseStatusLine,
                        &$tabResponseHeaderLine
                    )
                    {
                        // Register status line, if required
                        if(!$boolResponseStatusLineFound)
                        {
                            if(!is_null($strResponseStatusLine))
                            {
                                $strResponseStatusLine = $strHeaderLine;
                            }
                        }
                        // Else: register header line, if required
                        else if(!is_null($tabResponseHeaderLine))
                        {
                            $tabResponseHeaderLine[] = $strHeaderLine;
                        }

                        $boolResponseStatusLineFound = true;

                        return strlen($strHeaderLine);
                    }
                );
            }
        }

        // Execute request and get response
        $result = (
            (!is_null($objCurl)) ?
                curl_exec($objCurl) :
                false
        );

        // Reset response info, if required
        $strResponseStatusLine = (($result !== false) ? $strResponseStatusLine : null);
        $tabResponseHeaderLine = (($result !== false) ? $tabResponseHeaderLine : null);

        // Return result
        return $result;
    }



}