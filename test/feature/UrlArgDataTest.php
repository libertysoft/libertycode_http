<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\test\feature;

use PHPUnit\Framework\TestCase;

use DateTime;
use liberty_code\http\url_arg\exception\DataSrcInvalidFormatException;
use liberty_code\http\url_arg\exception\PathInvalidFormatException;
use liberty_code\http\url_arg\exception\ValueInvalidFormatException;
use liberty_code\http\url_arg\model\UrlArgData;



/**
 * @cover UrlArgData
 */
class UrlArgDataTest extends TestCase
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods test
    // ******************************************************************************

    /**
     * Test can set data source.
     *
     * @param array $tabDataSrc
     * @param string|array $expectResult
     * @dataProvider providerSetDataSrc
     */
    public function testCanSetDataSrc(
        $tabDataSrc,
        $expectResult
    )
    {
        // Init var
        $boolExceptionExpected = is_string($expectResult);

        // Expect exception, if required
        if($boolExceptionExpected)
        {
            $this->expectException($expectResult);
        }

        // Get URL argument data
        $objUrlArgData = new UrlArgData();

        // Set data source
        $objUrlArgData->setDataSrc($tabDataSrc);

        // Set assertions (check data source), if required
        if(!$boolExceptionExpected)
        {
            // Get info
            $tabDataSrc = $objUrlArgData->getDataSrc();

            // Set assertions (check data source)
            $this->assertEquals($expectResult, $tabDataSrc);

            // Print
            /*
            echo('Get data source: ' . PHP_EOL);var_dump($tabDataSrc);echo(PHP_EOL);
            //*/
        }
    }



    /**
     * Data provider,
     * to test can set data source.
     *
     * @return array
     */
    public function providerSetDataSrc()
    {
        // Return result
        return array(
            'Set data source: fail to set data source (invalid key format)' => [
                [
                    'value1',
                    'arg2' => 2,
                    'arg3' => true
                ],
                DataSrcInvalidFormatException::class
            ],
            'Set data source: fail to set data source (invalid value format)' => [
                [
                    'arg1' => new DateTime(),
                    'arg2' => 2,
                    'arg3' => true
                ],
                DataSrcInvalidFormatException::class
            ],
            'Set data source: success to set data source' => [
                [
                    'arg1' => 'value1',
                    'arg2' => 2,
                    'arg3' => true
                ],
                [
                    'arg1' => 'value1',
                    'arg2' => 2,
                    'arg3' => true
                ]
            ],
            'Set data source: fail to set data source (with array value) (invalid value format)' => [
                [
                    'arg1' => 'value1',
                    'arg2' => [
                        'value2-1',
                        'arg2-2' => new DateTime(),
                    ],
                    'arg3' => 'value3'
                ],
                DataSrcInvalidFormatException::class
            ],
            'Set data source: success to set data source (with array value)' => [
                [
                    'arg1' => 'value1',
                    'arg2' => [
                        'value2-1',
                        'arg2-2' => 2,
                    ],
                    'arg3' => true
                ],
                [
                    'arg1' => 'value1',
                    'arg2' => [
                        'value2-1',
                        'arg2-2' => 2,
                    ],
                    'arg3' => true
                ]
            ]
        );
    }



    /**
     * Test can use path.
     *
     * @param array $tabDataSrc
     * @param string $strPath
     * @param string|array $expectResult
     * @dataProvider providerUsePath
     */
    public function testCanUsePath(
        array $tabDataSrc,
        $strPath,
        $expectResult
    )
    {
        // Init var
        $boolExceptionExpected = is_string($expectResult);

        // Expect exception, if required
        if($boolExceptionExpected)
        {
            $this->expectException($expectResult);
        }

        // Get URL argument data
        $objUrlArgData = new UrlArgData();

        // Set data source
        $objUrlArgData->setDataSrc($tabDataSrc);

        // Get info
        $boolExists = $objUrlArgData->checkValueExists($strPath);

        // Set assertions (check path), if required
        if(!$boolExceptionExpected)
        {
            // Get info
            $boolExpectExists = $expectResult[0];
            $expectValue = $expectResult[1];
            $value = $objUrlArgData->getValue($strPath);

            // Set assertions (check path)
            $this->assertEquals($boolExpectExists, $boolExists);
            $this->assertEquals($expectValue, $value);

            // Print
            /*
            echo('Get path: ' . PHP_EOL);var_dump($strPath);echo(PHP_EOL);
            echo('Check exists: ' . PHP_EOL);var_dump($boolExists);echo(PHP_EOL);
            echo('Get value: ' . PHP_EOL);var_dump($value);echo(PHP_EOL);
            //*/
        }
    }



    /**
     * Data provider,
     * to test can use path.
     *
     * @return array
     */
    public function providerUsePath()
    {
        // Init var
        $tabDataSrc = array(
            'arg1' => 'value1',
            'arg2' => [
                'value2-1',
                'arg2-2' => 'value2-2',
            ],
            'arg3' => 'value3'
        );

        // Return result
        return array(
            'Use path: fail to use path 1 (invalid path format: string numeric)' => [
                $tabDataSrc,
                '1',
                PathInvalidFormatException::class
            ],
            'Use path: fail to use path 1 (path not found)' => [
                $tabDataSrc,
                'test',
                [
                    false,
                    null
                ]
            ],
            'Use path: success to use path arg1' => [
                $tabDataSrc,
                'arg1',
                [
                    true,
                    'value1'
                ]
            ],
            'Use path: fail to use path 2/0 (invalid path format: start with numeric)' => [
                $tabDataSrc,
                '2/0',
                PathInvalidFormatException::class
            ],
            'Use path: success to use path arg2/0' => [
                $tabDataSrc,
                'arg2/0',
                [
                    true,
                    'value2-1'
                ]
            ],
            'Use path: fail to use path arg2/arg2-2 (path not found)' => [
                $tabDataSrc,
                'arg2/test',
                [
                    false,
                    null
                ]
            ],
            'Use path: success to use path arg2/arg2-2' => [
                $tabDataSrc,
                'arg2/arg2-2',
                [
                    true,
                    'value2-2'
                ]
            ]
        );
    }



    /**
     * Test can put value.
     *
     * @param array $tabDataSrc
     * @param string $strPath
     * @param mixed $value
     * @param string|array $expectResult
     * @dataProvider providerPutValue
     */
    public function testCanPutValue(
        array $tabDataSrc,
        $strPath,
        $value,
        $expectResult
    )
    {
        // Init var
        $boolExceptionExpected = is_string($expectResult);

        // Expect exception, if required
        if($boolExceptionExpected)
        {
            $this->expectException($expectResult);
        }

        // Get URL argument data
        $objUrlArgData = new UrlArgData();

        // Set data source
        $objUrlArgData->setDataSrc($tabDataSrc);

        // Put value
        $objUrlArgData->putValue($strPath, $value);

        // Set assertions (check value), if required
        if(!$boolExceptionExpected)
        {
            // Get info
            $expectValue = $expectResult[0];
            $value = $objUrlArgData->getValue($strPath);

            // Set assertions (check value)
            $this->assertEquals($expectValue, $value);

            // Print
            /*
            echo('Get path: ' . PHP_EOL);var_dump($strPath);echo(PHP_EOL);
            echo('Get value: ' . PHP_EOL);var_dump($value);echo(PHP_EOL);
            echo('Get data source: ' . PHP_EOL);var_dump($objUrlArgData->getDataSrc());echo(PHP_EOL);
            //*/
        }
    }



    /**
     * Data provider,
     * to test can put value.
     *
     * @return array
     */
    public function providerPutValue()
    {
        // Init var
        $tabDataSrc = array();

        // Return result
        return array(
            'Put value: fail to put value for path arg1 (invalid value format: not string convertible)' => [
                $tabDataSrc,
                'arg1',
                new DateTime(),
                ValueInvalidFormatException::class
            ],
            'Put value: success to put value for path arg1' => [
                $tabDataSrc,
                'arg1',
                'value1',
                [
                    'value1'
                ]
            ],
            'Put value: fail to put value for path arg2 (invalid value format: empty array)' => [
                $tabDataSrc,
                'arg2',
                [],
                ValueInvalidFormatException::class
            ],
            'Put value: fail to put value for path arg2 (invalid value format: invalid array format)' => [
                $tabDataSrc,
                'arg2',
                [
                    new DateTime(),
                    'arg2-2' => 2
                ],
                ValueInvalidFormatException::class
            ],
            'Put value: success to put value for path arg2' => [
                $tabDataSrc,
                'arg2',
                [
                    'value2-1',
                    'arg2-2' => 2
                ],
                [
                    [
                        'value2-1',
                        'arg2-2' => 2
                    ]
                ]
            ],
            'Put value: fail to put value for path arg2/arg2-3/arg2-3-1 (invalid value format: not string convertible)' => [
                $tabDataSrc,
                'arg2/arg2-3/arg2-3-1',
                new DateTime(),
                ValueInvalidFormatException::class
            ],
            'Put value: success to put value for path arg2/arg2-3/arg2-3-1' => [
                $tabDataSrc,
                'arg2/arg2-3/arg2-3-1',
                'value2-3-1',
                [
                    'value2-3-1'
                ]
            ]
        );
    }



}