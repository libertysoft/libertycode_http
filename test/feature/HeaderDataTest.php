<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\test\feature;

use PHPUnit\Framework\TestCase;

use DateTime;
use liberty_code\http\header\exception\DataSrcInvalidFormatException;
use liberty_code\http\header\exception\KeyInvalidFormatException;
use liberty_code\http\header\exception\ValueInvalidFormatException;
use liberty_code\http\header\model\HeaderData;



/**
 * @cover HeaderData
 */
class HeaderDataTest extends TestCase
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods test
    // ******************************************************************************

    /**
     * Test can set data source.
     *
     * @param array $tabDataSrc
     * @param string|array $expectResult
     * @dataProvider providerSetDataSrc
     */
    public function testCanSetDataSrc(
        array $tabDataSrc,
        $expectResult
    )
    {
        // Init var
        $boolExceptionExpected = is_string($expectResult);

        // Expect exception, if required
        if($boolExceptionExpected)
        {
            $this->expectException($expectResult);
        }

        // Get header data
        $objHeaderData = new HeaderData();

        // Set data source
        $objHeaderData->setDataSrc($tabDataSrc);

        // Set assertions (check data source), if required
        if(!$boolExceptionExpected)
        {
            // Get info
            $tabDataSrc = $objHeaderData->getDataSrc();

            // Set assertions (check data source)
            $this->assertEquals($expectResult, $tabDataSrc);

            // Print
            /*
            echo('Get data source: ' . PHP_EOL);var_dump($tabDataSrc);echo(PHP_EOL);
            //*/
        }
    }



    /**
     * Data provider,
     * to test can set data source.
     *
     * @return array
     */
    public function providerSetDataSrc()
    {
        // Return result
        return array(
            'Set data source: fail to set data source (invalid key format)' => [
                [
                    'application/json',
                    'Accept' => 'application/json',
                    'Cache-Control' => 'no-cache'
                ],
                DataSrcInvalidFormatException::class
            ],
            'Set data source: fail to set data source (invalid value format)' => [
                [
                    'Content-type' => 'application/json',
                    'Accept' => new DateTime(),
                    'Cache-Control' => 'no-cache'
                ],
                DataSrcInvalidFormatException::class
            ],
            'Set data source: success to set data source' => [
                [
                    'Content-type' => 'application/json',
                    'Accept' => 'application/json',
                    'Cache-Control' => 'no-cache'
                ],
                [
                    'Content-type' => 'application/json',
                    'Accept' => 'application/json',
                    'Cache-Control' => 'no-cache'
                ]
            ]
        );
    }



    /**
     * Test can use key.
     *
     * @param array $tabDataSrc
     * @param $key
     * @param string|array $expectResult
     * @dataProvider providerUseKey
     */
    public function testCanUseKey(
        array $tabDataSrc,
        $key,
        $expectResult
    )
    {
        // Init var
        $boolExceptionExpected = is_string($expectResult);

        // Expect exception, if required
        if($boolExceptionExpected)
        {
            $this->expectException($expectResult);
        }

        // Get header data
        $objHeaderData = new HeaderData();

        // Set data source
        $objHeaderData->setDataSrc($tabDataSrc);

        // Get info
        $boolExists = $objHeaderData->checkValueExists($key);

        // Set assertions (check key), if required
        if(!$boolExceptionExpected)
        {
            // Get info
            $boolExpectExists = $expectResult[0];
            $expectValue = $expectResult[1];
            $value = $objHeaderData->getValue($key);

            // Set assertions (check key)
            $this->assertEquals($boolExpectExists, $boolExists);
            $this->assertEquals($expectValue, $value);

            // Print
            /*
            echo('Get key: ' . PHP_EOL);var_dump($key);echo(PHP_EOL);
            echo('Check exists: ' . PHP_EOL);var_dump($boolExists);echo(PHP_EOL);
            echo('Get value: ' . PHP_EOL);var_dump($value);echo(PHP_EOL);
            //*/
        }
    }



    /**
     * Data provider,
     * to test can use key.
     *
     * @return array
     */
    public function providerUseKey()
    {
        // Init var
        $tabDataSrc = array(
            'Content-type' => 'application/json',
            'Accept' => 'application/json',
            'Cache-Control' => 'no-cache'
        );

        // Return result
        return array(
            'Use key: fail to use key 1 (invalid key format)' => [
                $tabDataSrc,
                1,
                KeyInvalidFormatException::class
            ],
            'Use key: fail to use key test (key not found)' => [
                $tabDataSrc,
                'test',
                [
                    false,
                    null
                ]
            ],
            'Use key: success to use key Content-type' => [
                $tabDataSrc,
                'Content-type',
                [
                    true,
                    'application/json'
                ]
            ]
        );
    }



    /**
     * Test can put value.
     *
     * @param array $tabDataSrc
     * @param string $strKey
     * @param mixed $value
     * @param string|array $expectResult
     * @dataProvider providerPutValue
     */
    public function testCanPutValue(
        array $tabDataSrc,
        $strKey,
        $value,
        $expectResult
    )
    {
        // Init var
        $boolExceptionExpected = is_string($expectResult);

        // Expect exception, if required
        if($boolExceptionExpected)
        {
            $this->expectException($expectResult);
        }

        // Get header data
        $objHeaderData = new HeaderData();

        // Set data source
        $objHeaderData->setDataSrc($tabDataSrc);

        // Put value
        $objHeaderData->putValue($strKey, $value);

        // Set assertions (check value), if required
        if(!$boolExceptionExpected)
        {
            // Get info
            $expectValue = $expectResult[0];
            $value = $objHeaderData->getValue($strKey);

            // Set assertions (check value)
            $this->assertEquals($expectValue, $value);

            // Print
            /*
            echo('Get key: ' . PHP_EOL);var_dump($strKey);echo(PHP_EOL);
            echo('Get value: ' . PHP_EOL);var_dump($value);echo(PHP_EOL);
            //*/
        }
    }



    /**
     * Data provider,
     * to test can put value.
     *
     * @return array
     */
    public function providerPutValue()
    {
        // Init var
        $tabDataSrc = array();

        // Return result
        return array(
            'Put value: fail to put value for key Content-type (invalid value format: not string convertible)' => [
                $tabDataSrc,
                'Content-type',
                new DateTime(),
                ValueInvalidFormatException::class
            ],
            'Put value: success to put empty value for key Content-type' => [
                $tabDataSrc,
                'Content-type',
                '',
                [
                    ''
                ]
            ],
            'Put value: success to put value for key Content-type' => [
                $tabDataSrc,
                'Content-type',
                'application/json',
                [
                    'application/json'
                ]
            ],
            'Put value: fail to put value for key Accept (invalid value format: not string convertible)' => [
                $tabDataSrc,
                'Accept',
                [
                    'application/json',
                    'application/xml',
                    new DateTime(),
                ],
                ValueInvalidFormatException::class
            ],
            'Put value: success to put value for key Accept' => [
                $tabDataSrc,
                'Accept',
                [
                    'application/json',
                    'application/xml',
                    '',
                    1
                ],
                [
                    [
                        'application/json',
                        'application/xml',
                        '',
                        1
                    ]
                ]
            ]
        );
    }



}