<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\test\feature\requisition;

use PHPUnit\Framework\TestCase;

use liberty_code\parser\parser\factory\string_table\model\StrTableParserFactory;
use liberty_code\data\data\table\model\TableData;
use liberty_code\http\url_route\argument\model\UrlRouteArgData;
use liberty_code\http\url_route\library\ToolBoxUrlRoute;
use liberty_code\http\url_arg\model\UrlArgData;
use liberty_code\http\header\model\HeaderData;
use liberty_code\http\requisition\request\library\ToolBoxHttpRequest;
use liberty_code\http\requisition\request\exception\SndInfoInvalidFormatException;
use liberty_code\http\requisition\request\model\HttpRequest;
use liberty_code\http\requisition\request\data\exception\ConfigInvalidFormatException as DataConfigInvalidFormatException;
use liberty_code\http\requisition\request\data\model\DataHttpRequest;
use liberty_code\http\requisition\request\persistence\exception\ConfigInvalidFormatException as PersistorConfigInvalidFormatException;
use liberty_code\http\requisition\request\persistence\exception\IdInvalidFormatException;
use liberty_code\http\requisition\request\persistence\exception\QueryInvalidFormatException;
use liberty_code\http\requisition\request\persistence\exception\TabDataInvalidFormatException;
use liberty_code\http\requisition\request\persistence\model\PersistorHttpRequest;



/**
 * @cover ToolBoxUrlRoute
 * @cover ToolBoxHttpRequest
 * @cover HttpRequest
 * @cover DataHttpRequest
 * @cover PersistorHttpRequest
 */
class HttpRequestTest extends TestCase
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods test
    // ******************************************************************************

    /**
     * Test can use HTTP request object.
     *
     * @param boolean $boolUrlRouteArgDataRequire
     * @param boolean $boolUrlArgDataRequire
     * @param boolean $boolHeaderDataRequire
     * @param null|array $tabConfig
     * @param null|array $tabSndInfo
     * @param string|array $expectResult
     * @dataProvider providerUseHttpRequest
     */
    public function testCanUseHttpRequest(
        $boolUrlRouteArgDataRequire,
        $boolUrlArgDataRequire,
        $boolHeaderDataRequire,
        $tabConfig,
        $tabSndInfo,
        $expectResult
    )
    {
        // Init var
        $boolExceptionExpected = is_string($expectResult);

        // Expect exception, if required
        if($boolExceptionExpected)
        {
            $this->expectException($expectResult);
        }

        // Get request
        $objUrlRouteArgData = ($boolUrlRouteArgDataRequire ? new UrlRouteArgData(): null);
        $objUrlArgData = ($boolUrlArgDataRequire ? new UrlArgData(): null);
        $objHeaderData = ($boolHeaderDataRequire ? new HeaderData(): null);
        $objRequest = new HttpRequest(
            $objUrlRouteArgData,
            $objUrlArgData,
            $objHeaderData,
            $tabConfig,
            $tabSndInfo
        );

        // Set assertions (check request detail), if required
        if(!$boolExceptionExpected)
        {
            // Get info
            $strExpectKey = $expectResult[0];
            $strExpectUrl = $expectResult[1];
            $tabExpectConfig = $expectResult[2];
            $tabExpectSndInfo = $expectResult[3];
            $tabRequestConfig = $objRequest->getTabConfig();
            $tabRequestSndInfo = $objRequest->getTabSndInfo();

            // Set assertions (check request detail)
            if(!is_null($strExpectKey))
            {
                $this->assertEquals($strExpectKey, $objRequest->getStrKey());
            }
            $this->assertEquals($tabExpectConfig, $tabRequestConfig);
            $this->assertEquals($tabExpectSndInfo, $tabRequestSndInfo);

            // Print
            /*
            echo('Get key: ' . PHP_EOL);var_dump($objRequest->getStrKey());echo(PHP_EOL);
            echo('Get URL: ' . PHP_EOL);var_dump($objRequest->getStrUrl());echo(PHP_EOL);
            echo('Get config: ' . PHP_EOL);var_dump($tabRequestConfig);echo(PHP_EOL);
            echo('Get sending info: ' . PHP_EOL);var_dump($tabRequestSndInfo);echo(PHP_EOL);
            //*/

            // Set assertions (check request URL)
            $this->assertEquals($strExpectUrl, $objRequest->getStrUrl());
            if(isset($tabSndInfo['url']))
            {
                $this->assertEquals($tabSndInfo['url'], $objRequest->getStrUrl());
            }

            // Set assertions (check request URL route argument data), if required
            if(!is_null($objUrlRouteArgData))
            {
                $this->assertEquals(true, isset($tabRequestSndInfo['url_route_argument']));
                $this->assertEquals($tabRequestSndInfo['url_route_argument'], $objUrlRouteArgData->getDataSrc());
                $this->assertEquals($objRequest->getTabUrlRouteArg(), $objUrlRouteArgData->getDataSrc());

                $objRequest->getObjUrlRouteArgData()->putValue('key-add-1', 'Value Add 1');
                $objUrlRouteArgData->putValue('key-add-2', 'Value Add 2');
                $objUrlRouteArgData->putValue('key-add-3', 'Value Add 3');
                $objRequest->setObjUrlRouteArgData(null);
                $this->assertEquals(
                    array_merge(
                        $tabRequestSndInfo['url_route_argument'],
                        array(
                            'key-add-1' => 'Value Add 1',
                            'key-add-2' => 'Value Add 2',
                            'key-add-3' => 'Value Add 3'
                        )
                    ),
                    $objRequest->getTabUrlRouteArg()
                );
            }

            // Set assertions (check request URL argument data), if required
            if(!is_null($objUrlArgData))
            {
                $this->assertEquals(true, isset($tabRequestSndInfo['url_argument']));
                $this->assertEquals($tabRequestSndInfo['url_argument'], $objUrlArgData->getDataSrc());
                $this->assertEquals($objRequest->getTabUrlArg(), $objUrlArgData->getDataSrc());

                $objRequest->getObjUrlArgData()->putValue('key-add-1', 'Value Add 1');
                $objUrlArgData->putValue('key-add-2', 'Value Add 2');
                $objUrlArgData->putValue('key-add-3', 'Value Add 3');
                $objRequest->setObjUrlArgData(null);
                $this->assertEquals(
                    array_merge(
                        $tabRequestSndInfo['url_argument'],
                        array(
                            'key-add-1' => 'Value Add 1',
                            'key-add-2' => 'Value Add 2',
                            'key-add-3' => 'Value Add 3'
                        )
                    ),
                    $objRequest->getTabUrlArg()
                );
            }

            // Set assertions (check request header data), if required
            if(!is_null($objHeaderData))
            {
                $this->assertEquals(true, isset($tabRequestSndInfo['header']));
                $this->assertEquals($tabRequestSndInfo['header'], $objHeaderData->getDataSrc());
                $this->assertEquals($objRequest->getTabHeader(), $objHeaderData->getDataSrc());

                $objRequest->getObjHeaderData()->putValue('key-add-1', 'Value Add 1');
                $objHeaderData->putValue('key-add-2', 'Value Add 2');
                $objHeaderData->putValue('key-add-3', 'Value Add 3');
                $objRequest->setObjHeaderData(null);
                $this->assertEquals(
                    array_merge(
                        $tabRequestSndInfo['header'],
                        array(
                            'key-add-1' => 'Value Add 1',
                            'key-add-2' => 'Value Add 2',
                            'key-add-3' => 'Value Add 3'
                        )
                    ),
                    $objRequest->getTabHeader()
                );
            }
        }
    }



    /**
     * Data provider,
     * to test can use HTTP request object.
     *
     * @return array
     */
    public function providerUseHttpRequest()
    {
        // Init var
        $strBody = json_encode(array(
            'key-1' => 'Value 1',
            'key-2' => [
                'key-2-1' => 'Value 2-1',
                'key-2-2' => 2.2
            ],
            'key-3' => true
        ));

        // Return result
        return array(
            'Use HTTP request: fail to create request_get (invalid method format)' => [
                true,
                true,
                true,
                [
                    'key' => 'request_get'
                ],
                [
                    'method' => 'GET 1',
                    'url_port' => 80,
                    'url_route_argument' => [
                        '{key-1}' => 'value-1-test',
                        '{key-2}' => 'value-2-test',
                        '{key-3}' => 'value-3-test'
                    ],
                    'url_route_argument_spec' => [
                        '{key-1}' => 'value-1',
                        '{key-2}' => null
                    ],
                    'header' => [
                        'Accept' => 'application/json',
                        'Cache-Control' => 'no-cache'
                    ]
                ],
                SndInfoInvalidFormatException::class
            ],
            'Use HTTP request: fail to create request_get (invalid port format)' => [
                false,
                false,
                false,
                [
                    'key' => 'request_get'
                ],
                [
                    'method' => 'GET',
                    'url_port' => 'test',
                    'url_route_argument' => [
                        '{key-1}' => 'value-1-test',
                        '{key-2}' => 'value-2-test',
                        '{key-3}' => 'value-3-test'
                    ],
                    'url_route_argument_spec' => [
                        '{key-1}' => 'value-1',
                        '{key-2}' => null
                    ],
                    'header' => [
                        'Accept' => 'application/json',
                        'Cache-Control' => 'no-cache'
                    ]
                ],
                SndInfoInvalidFormatException::class
            ],
            'Use HTTP request: fail to create request_get (invalid URL route argument format)' => [
                true,
                true,
                true,
                [
                    'key' => 'request_get'
                ],
                [
                    'method' => 'GET',
                    'url_port' => 80,
                    'url_route_argument' => [
                        '{key-1}' => ['value-1-test'],
                        '{key-2}' => 'value-2-test',
                        '{key-3}' => 'value-3-test'
                    ],
                    'url_route_argument_spec' => [
                        '{key-1}' => 'value-1',
                        '{key-2}' => null
                    ],
                    'header' => [
                        'Accept' => 'application/json',
                        'Cache-Control' => 'no-cache'
                    ]
                ],
                SndInfoInvalidFormatException::class
            ],
            'Use HTTP request: fail to create request_get (invalid URL route argument specification format)' => [
                true,
                true,
                true,
                [
                    'key' => 'request_get'
                ],
                [
                    'method' => 'GET',
                    'url_port' => 80,
                    'url_route_argument' => [
                        '{key-1}' => 'value-1-test',
                        '{key-2}' => 'value-2-test',
                        '{key-3}' => 'value-3-test'
                    ],
                    'url_route_argument_spec' => 'test',
                    'header' => [
                        'Accept' => 'application/json',
                        'Cache-Control' => 'no-cache'
                    ]
                ],
                SndInfoInvalidFormatException::class
            ],
            'Use HTTP request: fail to create request_get (invalid header format)' => [
                true,
                true,
                true,
                [
                    'key' => 'request_get'
                ],
                [
                    'method' => 'GET',
                    'url_port' => 80,
                    'url_route_argument' => [
                        '{key-1}' => 'value-1-test',
                        '{key-2}' => 'value-2-test',
                        '{key-3}' => 'value-3-test'
                    ],
                    'url_route_argument_spec' => [
                        'key-1' => 'value-1',
                        'key-2' => null
                    ],
                    'header' => 'test'
                ],
                SndInfoInvalidFormatException::class
            ],
            'Use HTTP request: success to create and use request_get' => [
                false,
                true,
                false,
                [
                    'key' => 'request_get'
                ],
                [
                    'protocol_version_num' => '2.0',
                    'method' => 'GET',
                    'url' => 'https://www.test.com:80/path/to/resource?arg1=value1&arg2=value2&arg3=value3',
                    'header' => [
                        'Accept' => 'application/json',
                        'Cache-Control' => 'no-cache'
                    ],
                    'body' => null
                ],
                [
                    'request_get',
                    'https://www.test.com:80/path/to/resource?arg1=value1&arg2=value2&arg3=value3',
                    [
                        'key' => 'request_get'
                    ],
                    [
                        'protocol_version_num' => '2.0',
                        'method' => 'GET',
                        'url_schema' => 'https',
                        'url_host' => 'www.test.com',
                        'url_port' => 80,
                        'url_route' => '/path/to/resource',
                        'url_argument' => [
                            'arg1' => 'value1',
                            'arg2' => 'value2',
                            'arg3' => 'value3'
                        ],
                        'header' => [
                            'Accept' => 'application/json',
                            'Cache-Control' => 'no-cache'
                        ],
                        'body' => null
                    ]
                ]
            ],
            'Use HTTP request: success to create and use request_get_2' => [
                false,
                false,
                false,
                [
                    'key' => 'request_get_2'
                ],
                [
                    'protocol_version_num' => null,
                    'method' => 'GET',
                    'url_schema' => 'https',
                    'url_host' => 'www.test.com',
                    'url_port' => 80,
                    'url_route' => 'path/to/resource',
                    'url_argument' => [
                        'arg1' => 'value1',
                        'arg2' => 2,
                        'arg3' => true
                    ],
                    'header' => [
                        'Accept' => 'application/json',
                        'Cache-Control' => 'no-cache'
                    ]
                ],
                [
                    'request_get_2',
                    'https://www.test.com:80/path/to/resource?arg1=value1&arg2=2&arg3=1',
                    [
                        'key' => 'request_get_2'
                    ],
                    [
                        'protocol_version_num' => null,
                        'method' => 'GET',
                        'url_schema' => 'https',
                        'url_host' => 'www.test.com',
                        'url_port' => 80,
                        'url_route' => 'path/to/resource',
                        'url_argument' => [
                            'arg1' => 'value1',
                            'arg2' => 2,
                            'arg3' => true
                        ],
                        'header' => [
                            'Accept' => 'application/json',
                            'Cache-Control' => 'no-cache'
                        ]
                    ]
                ]
            ],
            'Use HTTP request: success to create and use request_post' => [
                true,
                false,
                true,
                [
                    'key' => 'request_post'
                ],
                [
                    'method' => 'POST',
                    'url' => 'https://www.test.com:80/path/to/resource',
                    'url_route' => 'path/to/resource-bis',
                    'header' => [
                        'Content-type' => 'application/json',
                        'Accept' => 'application/json',
                        'Cache-Control' => 'no-cache'
                    ],
                    'body' => $strBody
                ],
                [
                    'request_post',
                    'https://www.test.com:80/path/to/resource',
                    [
                        'key' => 'request_post'
                    ],
                    [
                        'method' => 'POST',
                        'url_schema' => 'https',
                        'url_host' => 'www.test.com',
                        'url_port' => 80,
                        'url_route' => '/path/to/resource',
                        // Due to set URL route argument data, during request construction, information URL route arguments already initialized
                        'url_route_argument' => [],
                        'header' => [
                            'Content-type' => 'application/json',
                            'Accept' => 'application/json',
                            'Cache-Control' => 'no-cache'
                        ],
                        'body' => $strBody
                    ]
                ]
            ],
            'Use HTTP request: success to create and use request_post_2' => [
                true,
                false,
                true,
                [
                    'key' => 'request_post'
                ],
                [
                    'method' => 'POST',
                    'url_schema' => 'https',
                    'url_host' => 'www.test.com',
                    'url_port' => 80,
                    'url_route' => 'path/to/resource/{key-1}/{key-2}',
                    'url_route_argument' => [
                        '{key-2}' => 'value-2-test',
                        '{key-3}' => 'value-3-test',
                        'resource' => 'resource-value'
                    ],
                    'url_route_argument_spec' => [
                        '{key-1}' => 'value-1',
                        '{key-2}' => null
                    ],
                    'header' => [
                        'Content-type' => 'application/json',
                        'Accept' => 'application/json',
                        'Cache-Control' => 'no-cache'
                    ],
                    'body' => $strBody
                ],
                [
                    'request_post',
                    'https://www.test.com:80/path/to/resource/value-1/value-2-test',
                    [
                        'key' => 'request_post'
                    ],
                    [
                        'method' => 'POST',
                        'url_schema' => 'https',
                        'url_host' => 'www.test.com',
                        'url_port' => 80,
                        'url_route' => 'path/to/resource/{key-1}/{key-2}',
                        'url_route_argument' => [
                            '{key-2}' => 'value-2-test',
                            '{key-3}' => 'value-3-test',
                            'resource' => 'resource-value'
                        ],
                        'url_route_argument_spec' => [
                            '{key-1}' => 'value-1',
                            '{key-2}' => null
                        ],
                        'header' => [
                            'Content-type' => 'application/json',
                            'Accept' => 'application/json',
                            'Cache-Control' => 'no-cache'
                        ],
                        'body' => $strBody
                    ]
                ]
            ],
            'Use HTTP request: success to create and use request_post_3' => [
                false,
                false,
                true,
                [
                    'key' => 'request_post'
                ],
                [
                    'method' => 'POST',
                    'url_schema' => 'https',
                    'url_host' => 'www.test.com',
                    'url_port' => 80,
                    'url_route' => 'path/to/resource/{key-1}/{key-2}',
                    'url_route_argument' => [
                        '{key-1}' => 'value-1-test',
                        '{key-3}' => 'value-3-test',
                        'resource' => 'resource-value'
                    ],
                    'url_route_argument_spec' => [
                        '{key-1}' => 'value-1',
                        '{key-2}' => null
                    ],
                    'header' => [
                        'Content-type' => 'application/json',
                        'Accept' => 'application/json',
                        'Cache-Control' => 'no-cache'
                    ],
                    'body' => $strBody
                ],
                [
                    'request_post',
                    null,
                    [
                        'key' => 'request_post'
                    ],
                    [
                        'method' => 'POST',
                        'url_schema' => 'https',
                        'url_host' => 'www.test.com',
                        'url_port' => 80,
                        'url_route' => 'path/to/resource/{key-1}/{key-2}',
                        'url_route_argument' => [
                            '{key-1}' => 'value-1-test',
                            '{key-3}' => 'value-3-test',
                            'resource' => 'resource-value'
                        ],
                        'url_route_argument_spec' => [
                            '{key-1}' => 'value-1',
                            '{key-2}' => null
                        ],
                        'header' => [
                            'Content-type' => 'application/json',
                            'Accept' => 'application/json',
                            'Cache-Control' => 'no-cache'
                        ],
                        'body' => $strBody
                    ]
                ]
            ],
            'Use HTTP request: success to create and use request_post_4' => [
                false,
                false,
                true,
                [
                    'key' => 'request_post'
                ],
                [
                    'method' => 'POST',
                    'url_schema' => 'https',
                    'url_host' => 'www.test.com',
                    'url_port' => 80,
                    'url_route' => 'path/to/resource/{key-1}/{key-2}',
                    'url_route_argument' => [
                        '{key-1}' => 'value-1-test',
                        '{key-3}' => 'value-3-test',
                        'resource' => 'resource-value'
                    ],
                    'url_route_argument_spec' => null,
                    'header' => [
                        'Content-type' => 'application/json',
                        'Accept' => 'application/json',
                        'Cache-Control' => 'no-cache'
                    ],
                    'body' => $strBody
                ],
                [
                    'request_post',
                    'https://www.test.com:80/path/to/resource-value/value-1-test/{key-2}',
                    [
                        'key' => 'request_post'
                    ],
                    [
                        'method' => 'POST',
                        'url_schema' => 'https',
                        'url_host' => 'www.test.com',
                        'url_port' => 80,
                        'url_route' => 'path/to/resource/{key-1}/{key-2}',
                        'url_route_argument' => [
                            '{key-1}' => 'value-1-test',
                            '{key-3}' => 'value-3-test',
                            'resource' => 'resource-value'
                        ],
                        'url_route_argument_spec' => null,
                        'header' => [
                            'Content-type' => 'application/json',
                            'Accept' => 'application/json',
                            'Cache-Control' => 'no-cache'
                        ],
                        'body' => $strBody
                    ]
                ]
            ]
        );
    }



    /**
     * Test can use data HTTP request object.
     *
     * @param boolean $boolBodyDataRequire
     * @param null|array $tabConfig
     * @param null|array $tabSndInfo
     * @param string|array $expectResult
     * @depends testCanUseHttpRequest
     * @dataProvider providerUseDataHttpRequest
     */
    public function testCanUseDataHttpRequest(
        $boolBodyDataRequire,
        $tabConfig,
        $tabSndInfo,
        $expectResult
    )
    {
        // Init var
        $boolExceptionExpected = is_string($expectResult);
        $objParserFactory = new StrTableParserFactory();

        // Expect exception, if required
        if($boolExceptionExpected)
        {
            $this->expectException($expectResult);
        }

        // Get request
        $objBodyData = ($boolBodyDataRequire ? new TableData(): null);
        $objRequest = new DataHttpRequest(
            $objParserFactory,
            null,
            null,
            null,
            $objBodyData,
            $tabConfig,
            $tabSndInfo
        );
        $tabRequestConfig = $objRequest->getTabConfig();
        $tabRequestSndInfo = $objRequest->getTabSndInfo();
        $tabRequestBodyData = $objRequest->getTabBodyData();

        // Set assertions (check request detail), if required
        if(!$boolExceptionExpected)
        {
            // Get info
            $tabExpectConfig = $expectResult[0];
            $tabExpectSndInfo = $expectResult[1];
            $tabExpectBodyData = $expectResult[2];

            // Set assertions (check request detail)
            $this->assertEquals($tabExpectConfig, $tabRequestConfig);
            $this->assertEquals($tabExpectSndInfo, $tabRequestSndInfo);
            $this->assertEquals($tabExpectBodyData, $tabRequestBodyData);

            // Print
            /*
            echo('Get key: ' . PHP_EOL);var_dump($objRequest->getStrKey());echo(PHP_EOL);
            echo('Get config: ' . PHP_EOL);var_dump($tabRequestConfig);echo(PHP_EOL);
            echo('Get sending info: ' . PHP_EOL);var_dump($tabRequestSndInfo);echo(PHP_EOL);
            echo('Get body data: ' . PHP_EOL);var_dump($tabRequestBodyData);echo(PHP_EOL);
            //*/

            // Set assertions (check request body data), if required
            if(!is_null($objBodyData))
            {
                if(is_null($tabRequestBodyData))
                {
                    $this->assertEquals(array(), $objBodyData->getDataSrc());
                }
                else
                {
                    $this->assertEquals($tabRequestBodyData, $objBodyData->getDataSrc());
                }

                $objRequest->getObjBodyData()->putValue('key-add-1', 'Value Add 1');
                $objBodyData->putValue('key-add-2', 'Value Add 2');
                $objBodyData->putValue('key-add-3', 'Value Add 3');
                $objRequest->setObjBodyData(null);
                $this->assertEquals(
                    array_merge(
                        (is_null($tabRequestBodyData) ? array() : $tabRequestBodyData),
                        array(
                            'key-add-1' => 'Value Add 1',
                            'key-add-2' => 'Value Add 2',
                            'key-add-3' => 'Value Add 3'
                        )
                    ),
                    $objRequest->getTabBodyData()
                );
            }
        }
    }



    /**
     * Data provider,
     * to test can use data HTTP request object.
     *
     * @return array
     */
    public function providerUseDataHttpRequest()
    {
        // Init var
        $tabBodyData = array(
            'key-1' => 'Value 1',
            'key-2' => [
                'key-2-1' => 'Value 2-1',
                'key-2-2' => 2.2
            ],
            'key-3' => true
        );
        $strBody = json_encode($tabBodyData);

        // Return result
        return array(
            'Use data HTTP request: fail to create request_post (invalid config format: body parser config not found)' => [
                true,
                [
                    'key' => 'request_post'
                ],
                [
                    'method' => 'POST',
                    'url' => 'https://www.test.com/path/to/resource',
                    'header' => [
                        'Content-type' => 'application/json',
                        'Accept' => 'application/json'
                    ],
                    'body' => $strBody
                ],
                DataConfigInvalidFormatException::class
            ],
            'Use data HTTP request: fail to use request_post (invalid parsable body)' => [
                false,
                [
                    'key' => 'request_post',
                    'body_parser_config' => [
                        'type' => 'string_table_json'
                    ]
                ],
                [
                    'method' => 'POST',
                    'url' => 'https://www.test.com/path/to/resource',
                    'header' => [
                        'Content-type' => 'application/json',
                        'Accept' => 'application/json'
                    ],
                    'body' => ''
                ],
                DataConfigInvalidFormatException::class
            ],
            'Use data HTTP request: success to create and use request_post (without body)' => [
                true,
                [
                    'key' => 'request_post',
                    'body_parser_config' => [
                        'type' => 'string_table_json'
                    ]
                ],
                [
                    'method' => 'POST',
                    'url' => 'https://www.test.com/path/to/resource',
                    'header' => [
                        'Content-type' => 'application/json',
                        'Accept' => 'application/json'
                    ]
                ],
                [
                    [
                        'key' => 'request_post',
                        'body_parser_config' => [
                            'type' => 'string_table_json'
                        ]
                    ],
                    [
                        'method' => 'POST',
                        'url_schema' => 'https',
                        'url_host' => 'www.test.com',
                        'url_route' => '/path/to/resource',
                        'header' => [
                            'Content-type' => 'application/json',
                            'Accept' => 'application/json'
                        ]
                    ],
                    null
                ]
            ],
            'Use data HTTP request: success to create and use request_post (using body)' => [
                true,
                [
                    'key' => 'request_post',
                    'body_parser_config' => [
                        'type' => 'string_table_json'
                    ]
                ],
                [
                    'method' => 'POST',
                    'url' => 'https://www.test.com/path/to/resource',
                    'header' => [
                        'Content-type' => 'application/json',
                        'Accept' => 'application/json'
                    ],
                    'body' => $strBody
                ],
                [
                    [
                        'key' => 'request_post',
                        'body_parser_config' => [
                            'type' => 'string_table_json'
                        ]
                    ],
                    [
                        'method' => 'POST',
                        'url_schema' => 'https',
                        'url_host' => 'www.test.com',
                        'url_route' => '/path/to/resource',
                        'header' => [
                            'Content-type' => 'application/json',
                            'Accept' => 'application/json'
                        ],
                        'body' => $strBody
                    ],
                    $tabBodyData
                ]
            ],
            'Use data HTTP request: success to create and use request_post (using empty body data)' => [
                false,
                [
                    'key' => 'request_post',
                    'body_parser_config' => [
                        'type' => 'string_table_json'
                    ]
                ],
                [
                    'method' => 'POST',
                    'url' => 'https://www.test.com/path/to/resource',
                    'header' => [
                        'Content-type' => 'application/json',
                        'Accept' => 'application/json'
                    ],
                    'body_data' => array()
                ],
                [
                    [
                        'key' => 'request_post',
                        'body_parser_config' => [
                            'type' => 'string_table_json'
                        ]
                    ],
                    [
                        'method' => 'POST',
                        'url_schema' => 'https',
                        'url_host' => 'www.test.com',
                        'url_route' => '/path/to/resource',
                        'header' => [
                            'Content-type' => 'application/json',
                            'Accept' => 'application/json'
                        ],
                        'body' => '[]'
                    ],
                    []
                ]
            ],
            'Use data HTTP request: success to create and use request_post (using body data)' => [
                true,
                [
                    'key' => 'request_post',
                    'body_parser_config' => [
                        'type' => 'string_table_json'
                    ]
                ],
                [
                    'method' => 'POST',
                    'url' => 'https://www.test.com/path/to/resource',
                    'header' => [
                        'Content-type' => 'application/json',
                        'Accept' => 'application/json'
                    ],
                    'body_data' => $tabBodyData
                ],
                [
                    [
                        'key' => 'request_post',
                        'body_parser_config' => [
                            'type' => 'string_table_json'
                        ]
                    ],
                    [
                        'method' => 'POST',
                        'url_schema' => 'https',
                        'url_host' => 'www.test.com',
                        'url_route' => '/path/to/resource',
                        'header' => [
                            'Content-type' => 'application/json',
                            'Accept' => 'application/json'
                        ],
                        'body' => $strBody
                    ],
                    $tabBodyData
                ]
            ]
        );
    }



    /**
     * Test can use persistor HTTP request object.
     *
     * @param null|array $tabConfig
     * @param null|array $tabSndInfo
     * @param string $strMethodNm
     * @param mixed $value
     * @param string|array $expectResult
     * @depends testCanUseDataHttpRequest
     * @dataProvider providerUsePersistorHttpRequest
     */
    public function testCanUsePersistorHttpRequest(
        $tabConfig,
        $tabSndInfo,
        $strMethodNm,
        $value,
        $expectResult
    )
    {
        // Init var
        $boolExceptionExpected = is_string($expectResult);
        $objParserFactory = new StrTableParserFactory();

        // Expect exception, if required
        if($boolExceptionExpected)
        {
            $this->expectException($expectResult);
        }

        // Get request
        $objRequest = new PersistorHttpRequest(
            $objParserFactory,
            null,
            null,
            null,
            null,
            $tabConfig,
            $tabSndInfo
        );

        // Set value
        $objRequest->$strMethodNm($value);

        // Set assertions (check request detail), if required
        if(!$boolExceptionExpected)
        {
            // Get info
            $tabRequestConfig = $objRequest->getTabConfig();
            $tabRequestSndInfo = $objRequest->getTabSndInfo();
            $tabExpectConfig = $expectResult[0];
            $tabExpectSndInfo = $expectResult[1];

            // Set assertions (check request detail)
            $this->assertEquals($tabExpectConfig, $tabRequestConfig);
            $this->assertEquals($tabExpectSndInfo, $tabRequestSndInfo);

            // Print
            /*
            echo('Get key: ' . PHP_EOL);var_dump($objRequest->getStrKey());echo(PHP_EOL);
            echo('Get config: ' . PHP_EOL);var_dump($tabRequestConfig);echo(PHP_EOL);
            echo('Get sending info: ' . PHP_EOL);var_dump($tabRequestSndInfo);echo(PHP_EOL);
            //*/
        }
    }



    /**
     * Data provider,
     * to test can use persistor HTTP request object.
     *
     * @return array
     */
    public function providerUsePersistorHttpRequest()
    {
        // Init get data function
        $getData = function($nm, $fnm, $age, $id = null)
        {
            $result = array(
                'name' => $nm,
                'first-name' => $fnm,
                'age' => $age
            );
            $result = (
                (!is_null($id)) ?
                    array_merge(
                        array('id' => $id),
                        $result
                    ) :
                    $result
            );
            return $result;
        };

        // Init var
        $id = 1;
        $data = $getData('NM-1', 'Fnm-1', 20);
        $dataWithId = $getData('NM-1', 'Fnm-1', 20, $id);
        $tabId = array($id, 2, 3);
        $tabData = array(
            $getData('NM-1', 'Fnm-1', 20),
            $getData('NM-2', 'Fnm-2', 30),
            $getData('NM-3', 'Fnm-3', 40)
        );
        $tabDataWithId = array(
            $getData('NM-1', 'Fnm-1', 20, $tabId[0]),
            $getData('NM-2', 'Fnm-2', 30, $tabId[1]),
            $getData('NM-3', 'Fnm-3', 40, $tabId[2])
        );

        // Return result
        return array(
            'Use persistor HTTP request: fail to use request_get (unable to set id: data id key not found)' => [
                [
                    'key' => 'request_get',
                    'body_parser_config' => [
                        'type' => 'string_table_json'
                    ],
                    'id_support_type' => 'url_route_argument'
                ],
                [
                    'method' => 'GET',
                    'url' => 'https://www.test.com/path/to/resource/{id}',
                    'header' => [
                        'Accept' => 'application/json'
                    ]
                ],
                'setId',
                1,
                PersistorConfigInvalidFormatException::class
            ],
            'Use persistor HTTP request: success to create and use request_get (set id)' => [
                [
                    'key' => 'request_get',
                    'body_parser_config' => [
                        'type' => 'string_table_json'
                    ],
                    'id_support_type' => 'url_route_argument',
                    'id_key' => '{id}'
                ],
                [
                    'method' => 'GET',
                    'url' => 'https://www.test.com/path/to/resource/{id}',
                    'header' => [
                        'Accept' => 'application/json'
                    ]
                ],
                'setId',
                1,
                [
                    [
                        'key' => 'request_get',
                        'body_parser_config' => [
                            'type' => 'string_table_json'
                        ],
                        'id_support_type' => 'url_route_argument',
                        'id_key' => '{id}'
                    ],
                    [
                        'method' => 'GET',
                        'url_schema' => 'https',
                        'url_host' => 'www.test.com',
                        'url_route' => '/path/to/resource/{id}',
                        'url_route_argument' => [
                            '{id}' => 1
                        ],
                        'url_route_argument_spec' => [
                            '{id}' => null
                        ],
                        'header' => [
                            'Accept' => 'application/json'
                        ]
                    ]
                ]
            ],
            'Use persistor HTTP request: fail to use request_get (unable to set array of ids: invalid support type)' => [
                [
                    'key' => 'request_get',
                    'body_parser_config' => [
                        'type' => 'string_table_json'
                    ],
                    'path_separator' => '.',
                    'multi_id_support_type' => 'url_route_argument',
                    'id_key' => 'search.ids'
                ],
                [
                    'method' => 'GET',
                    'url' => 'https://www.test.com/path/to/resource',
                    'header' => [
                        'Accept' => 'application/json'
                    ]
                ],
                'setTabId',
                $tabId,
                PersistorConfigInvalidFormatException::class
            ],
            'Use persistor HTTP request: success to create and use request_get (set array of ids)' => [
                [
                    'key' => 'request_get',
                    'body_parser_config' => [
                        'type' => 'string_table_json'
                    ],
                    'path_separator' => '.',
                    'id_support_type' => 'url_route_argument',
                    'id_key' => 'search.ids'
                ],
                [
                    'method' => 'GET',
                    'url' => 'https://www.test.com/path/to/resource',
                    'header' => [
                        'Accept' => 'application/json'
                    ]
                ],
                'setTabId',
                $tabId,
                [
                    [
                        'key' => 'request_get',
                        'body_parser_config' => [
                            'type' => 'string_table_json'
                        ],
                        'path_separator' => '.',
                        'id_support_type' => 'url_route_argument',
                        'id_key' => 'search.ids'
                    ],
                    [
                        'method' => 'GET',
                        'url_schema' => 'https',
                        'url_host' => 'www.test.com',
                        'url_route' => '/path/to/resource',
                        'url_argument' => [
                            'search' => [
                                'ids' => $tabId
                            ]
                        ],
                        'header' => [
                            'Accept' => 'application/json'
                        ]
                    ]
                ]
            ],
            'Use persistor HTTP request: fail to use request_get (unable to set query: invalid query format)' => [
                [
                    'key' => 'request_get',
                    'body_parser_config' => [
                        'type' => 'string_table_json'
                    ],
                    'query_data_path' => 'search/query'
                ],
                [
                    'method' => 'GET',
                    'url_schema' => 'https',
                    'url_host' => 'www.test.com',
                    'url_route' => '/path/to/resource',
                    'url_argument' => [
                        'search' => [
                            'count' => 30,
                            'page' => 3,
                            'query' => [
                                'key-ini-1' => 7,
                                'key-ini-2' => 'Value 7',
                                'key-ini-3' => false
                            ]
                        ]
                    ],
                    'header' => [
                        'Accept' => 'application/json'
                    ]
                ],
                'setQuery',
                'test',
                QueryInvalidFormatException::class
            ],
            'Use persistor HTTP request: success to create and use request_get (set query)' => [
                [
                    'key' => 'request_get',
                    'body_parser_config' => [
                        'type' => 'string_table_json'
                    ],
                    'query_data_path' => 'search/query'
                ],
                [
                    'method' => 'GET',
                    'url_schema' => 'https',
                    'url_host' => 'www.test.com',
                    'url_route' => '/path/to/resource',
                    'url_argument' => [
                        'search' => [
                            'count' => 30,
                            'page' => 3,
                            'query' => [
                                'key-ini-1' => 7,
                                'key-ini-2' => 'Value 7',
                                'key-ini-3' => false
                            ]
                        ]
                    ],
                    'header' => [
                        'Accept' => 'application/json'
                    ]
                ],
                'setQuery',
                [
                    'key-1' => 1,
                    'key-2' => 'Value 2',
                    'key-3' => true
                ],
                [
                    [
                        'key' => 'request_get',
                        'body_parser_config' => [
                            'type' => 'string_table_json'
                        ],
                        'query_data_path' => 'search/query'
                    ],
                    [
                        'method' => 'GET',
                        'url_schema' => 'https',
                        'url_host' => 'www.test.com',
                        'url_route' => '/path/to/resource',
                        'url_argument' => [
                            'search' => [
                                'count' => 30,
                                'page' => 3,
                                'query' => [
                                    'key-ini-1' => 7,
                                    'key-ini-2' => 'Value 7',
                                    'key-ini-3' => false,
                                    'key-1' => 1,
                                    'key-2' => 'Value 2',
                                    'key-3' => true
                                ]
                            ]
                        ],
                        'header' => [
                            'Accept' => 'application/json'
                        ]
                    ]
                ]
            ],
            'Use persistor HTTP request: success to create and use request_post (set data to create)' => [
                [
                    'key' => 'request_post',
                    'body_parser_config' => [
                        'type' => 'string_table_json'
                    ]
                ],
                [
                    'method' => 'POST',
                    'url' => 'https://www.test.com/path/to/resource',
                    'header' => [
                        'Content-type' => 'application/json',
                        'Accept' => 'application/json'
                    ],
                    'body_data' => [
                        'email' => 'fnm-1.nm-1@test.com'
                    ]
                ],
                'setCreateData',
                $data,
                [
                    [
                        'key' => 'request_post',
                        'body_parser_config' => [
                            'type' => 'string_table_json'
                        ]
                    ],
                    [
                        'method' => 'POST',
                        'url_schema' => 'https',
                        'url_host' => 'www.test.com',
                        'url_route' => '/path/to/resource',
                        'header' => [
                            'Content-type' => 'application/json',
                            'Accept' => 'application/json'
                        ],
                        'body' => json_encode(array_merge(
                            ['email' => 'fnm-1.nm-1@test.com'],
                            $data
                        ))
                    ]
                ]
            ],
            'Use persistor HTTP request: fail to use request_post (unable to set array of data to create: invalid array of data format)' => [
                [
                    'key' => 'request_post',
                    'body_parser_config' => [
                        'type' => 'string_table_json'
                    ]
                ],
                [
                    'method' => 'POST',
                    'url' => 'https://www.test.com/path/to/resource',
                    'header' => [
                        'Content-type' => 'application/json',
                        'Accept' => 'application/json'
                    ]
                ],
                'setCreateTabData',
                ['test'],
                TabDataInvalidFormatException::class
            ],
            'Use persistor HTTP request: success to create and use request_post (set array of data to create)' => [
                [
                    'key' => 'request_post',
                    'body_parser_config' => [
                        'type' => 'string_table_json'
                    ]
                ],
                [
                    'method' => 'POST',
                    'url' => 'https://www.test.com/path/to/resource',
                    'header' => [
                        'Content-type' => 'application/json',
                        'Accept' => 'application/json'
                    ]
                ],
                'setCreateTabData',
                $tabData,
                [
                    [
                        'key' => 'request_post',
                        'body_parser_config' => [
                            'type' => 'string_table_json'
                        ]
                    ],
                    [
                        'method' => 'POST',
                        'url_schema' => 'https',
                        'url_host' => 'www.test.com',
                        'url_route' => '/path/to/resource',
                        'header' => [
                            'Content-type' => 'application/json',
                            'Accept' => 'application/json'
                        ],
                        'body' => json_encode($tabData)
                    ]
                ]
            ],
            'Use persistor HTTP request: fail to use request_post (unable to set data to update, with id: data id key not found)' => [
                [
                    'key' => 'request_post',
                    'body_parser_config' => [
                        'type' => 'string_table_json'
                    ],
                    'update_data_path' => 'update',
                    'update_id_support_type' => 'url_route_argument',
                    'update_id_key' => '{id}'
                ],
                [
                    'method' => 'POST',
                    'url' => 'https://www.test.com/path/to/resource/{id}',
                    'header' => [
                        'Content-type' => 'application/json',
                        'Accept' => 'application/json'
                    ]
                ],
                'setUpdateData',
                $dataWithId,
                PersistorConfigInvalidFormatException::class
            ],
            'Use persistor HTTP request: success to create and use request_post (set data to update, with id)' => [
                [
                    'key' => 'request_post',
                    'body_parser_config' => [
                        'type' => 'string_table_json'
                    ],
                    'data_id_key' => 'id',
                    'update_data_path' => 'update',
                    'update_id_support_type' => 'url_route_argument',
                    'update_id_key' => '{id}'
                ],
                [
                    'method' => 'POST',
                    'url' => 'https://www.test.com/path/to/resource/{id}',
                    'header' => [
                        'Content-type' => 'application/json',
                        'Accept' => 'application/json'
                    ]
                ],
                'setUpdateData',
                $dataWithId,
                [
                    [
                        'key' => 'request_post',
                        'body_parser_config' => [
                            'type' => 'string_table_json'
                        ],
                        'data_id_key' => 'id',
                        'update_data_path' => 'update',
                        'update_id_support_type' => 'url_route_argument',
                        'update_id_key' => '{id}'
                    ],
                    [
                        'method' => 'POST',
                        'url_schema' => 'https',
                        'url_host' => 'www.test.com',
                        'url_route' => '/path/to/resource/{id}',
                        'url_route_argument' => [
                            '{id}' => $id
                        ],
                        'url_route_argument_spec' => [
                            '{id}' => null
                        ],
                        'header' => [
                            'Content-type' => 'application/json',
                            'Accept' => 'application/json'
                        ],
                        'body' => json_encode([
                            'update' => $data
                        ])
                    ]
                ]
            ],
            'Use persistor HTTP request: success to create and use request_post (set data to update)' => [
                [
                    'key' => 'request_post',
                    'body_parser_config' => [
                        'type' => 'string_table_json'
                    ],
                    'update_data_path' => 'update'
                ],
                [
                    'method' => 'POST',
                    'url' => 'https://www.test.com/path/to/resource',
                    'header' => [
                        'Content-type' => 'application/json',
                        'Accept' => 'application/json'
                    ]
                ],
                'setUpdateData',
                $dataWithId,
                [
                    [
                        'key' => 'request_post',
                        'body_parser_config' => [
                            'type' => 'string_table_json'
                        ],
                        'update_data_path' => 'update'
                    ],
                    [
                        'method' => 'POST',
                        'url_schema' => 'https',
                        'url_host' => 'www.test.com',
                        'url_route' => '/path/to/resource',
                        'header' => [
                            'Content-type' => 'application/json',
                            'Accept' => 'application/json'
                        ],
                        'body' => json_encode([
                            'update' => $dataWithId
                        ])
                    ]
                ]
            ],
            'Use persistor HTTP request: success to create and use request_post (set array of data to update)' => [
                [
                    'key' => 'request_post',
                    'body_parser_config' => [
                        'type' => 'string_table_json'
                    ],
                    'update_data_path' => 'update'
                ],
                [
                    'method' => 'POST',
                    'url' => 'https://www.test.com/path/to/resource',
                    'header' => [
                        'Content-type' => 'application/json',
                        'Accept' => 'application/json'
                    ]
                ],
                'setUpdateData',
                $tabDataWithId,
                [
                    [
                        'key' => 'request_post',
                        'body_parser_config' => [
                            'type' => 'string_table_json'
                        ],
                        'update_data_path' => 'update'
                    ],
                    [
                        'method' => 'POST',
                        'url_schema' => 'https',
                        'url_host' => 'www.test.com',
                        'url_route' => '/path/to/resource',
                        'header' => [
                            'Content-type' => 'application/json',
                            'Accept' => 'application/json'
                        ],
                        'body' => json_encode([
                            'update' => $tabDataWithId
                        ])
                    ]
                ]
            ],
            'Use persistor HTTP request: fail to use request_delete (unable to set id to delete: invalid id format)' => [
                [
                    'key' => 'request_delete',
                    'body_parser_config' => [
                        'type' => 'string_table_json'
                    ],
                    'data_id_key' => 'id',
                    'delete_id_support_type' => 'url_route_argument',
                    'delete_id_key' => '{id}'
                ],
                [
                    'method' => 'DELETE',
                    'url' => 'https://www.test.com/path/to/resource/{id}',
                    'header' => [
                        'Accept' => 'application/json'
                    ]
                ],
                'setDeleteData',
                array_merge($data, ['id' => ['test']]),
                IdInvalidFormatException::class
            ],
            'Use persistor HTTP request: success to create and use request_delete (set id to delete)' => [
                [
                    'key' => 'request_delete',
                    'body_parser_config' => [
                        'type' => 'string_table_json'
                    ],
                    'data_id_key' => 'id',
                    'delete_id_support_type' => 'url_route_argument',
                    'delete_id_key' => '{id}'
                ],
                [
                    'method' => 'DELETE',
                    'url' => 'https://www.test.com/path/to/resource/{id}',
                    'header' => [
                        'Accept' => 'application/json'
                    ]
                ],
                'setDeleteData',
                $dataWithId,
                [
                    [
                        'key' => 'request_delete',
                        'body_parser_config' => [
                            'type' => 'string_table_json'
                        ],
                        'data_id_key' => 'id',
                        'delete_id_support_type' => 'url_route_argument',
                        'delete_id_key' => '{id}'
                    ],
                    [
                        'method' => 'DELETE',
                        'url_schema' => 'https',
                        'url_host' => 'www.test.com',
                        'url_route' => '/path/to/resource/{id}',
                        'url_route_argument' => [
                            '{id}' => $id
                        ],
                        'url_route_argument_spec' => [
                            '{id}' => null
                        ],
                        'header' => [
                            'Accept' => 'application/json'
                        ]
                    ]
                ]
            ],
            'Use persistor HTTP request: success to create and use request_delete (set data to delete)' => [
                [
                    'key' => 'request_delete',
                    'body_parser_config' => [
                        'type' => 'string_table_json'
                    ],
                    'delete_data_support_type' => 'url_argument'
                ],
                [
                    'method' => 'DELETE',
                    'url' => 'https://www.test.com/path/to/resource',
                    'header' => [
                        'Accept' => 'application/json'
                    ]
                ],
                'setDeleteData',
                $dataWithId,
                [
                    [
                        'key' => 'request_delete',
                        'body_parser_config' => [
                            'type' => 'string_table_json'
                        ],
                        'delete_data_support_type' => 'url_argument'
                    ],
                    [
                        'method' => 'DELETE',
                        'url_schema' => 'https',
                        'url_host' => 'www.test.com',
                        'url_route' => '/path/to/resource',
                        'url_argument' => $dataWithId,
                        'header' => [
                            'Accept' => 'application/json'
                        ]
                    ]
                ]
            ],
            'Use persistor HTTP request: fail to use request_delete (unable to set array of ids to delete: invalid array of data format)' => [
                [
                    'key' => 'request_delete',
                    'body_parser_config' => [
                        'type' => 'string_table_json'
                    ],
                    'data_id_key' => 'id',
                    'delete_multi_id_key' => 'ids'
                ],
                [
                    'method' => 'DELETE',
                    'url' => 'https://www.test.com/path/to/resource',
                    'header' => [
                        'Accept' => 'application/json'
                    ]
                ],
                'setDeleteTabData',
                ['test'],
                TabDataInvalidFormatException::class
            ],
            'Use persistor HTTP request: success to create and  to use request_delete (set array of ids to delete)' => [
                [
                    'key' => 'request_delete',
                    'body_parser_config' => [
                        'type' => 'string_table_json'
                    ],
                    'data_id_key' => 'id',
                    'delete_multi_id_key' => 'ids'
                ],
                [
                    'method' => 'DELETE',
                    'url' => 'https://www.test.com/path/to/resource',
                    'header' => [
                        'Accept' => 'application/json'
                    ]
                ],
                'setDeleteTabData',
                $tabDataWithId,
                [
                    [
                        'key' => 'request_delete',
                        'body_parser_config' => [
                            'type' => 'string_table_json'
                        ],
                        'data_id_key' => 'id',
                        'delete_multi_id_key' => 'ids'
                    ],
                    [
                        'method' => 'DELETE',
                        'url_schema' => 'https',
                        'url_host' => 'www.test.com',
                        'url_route' => '/path/to/resource',
                        'url_argument' => [
                            'ids' => $tabId
                        ],
                        'header' => [
                            'Accept' => 'application/json'
                        ]
                    ]
                ]
            ],
            'Use persistor HTTP request: success to create and use request_post (set array of data to delete)' => [
                [
                    'key' => 'request_post',
                    'body_parser_config' => [
                        'type' => 'string_table_json'
                    ]
                ],
                [
                    'method' => 'POST',
                    'url' => 'https://www.test.com/path/to/resource',
                    'header' => [
                        'Content-type' => 'application/json',
                        'Accept' => 'application/json'
                    ]
                ],
                'setDeleteTabData',
                $tabDataWithId,
                [
                    [
                        'key' => 'request_post',
                        'body_parser_config' => [
                            'type' => 'string_table_json'
                        ]
                    ],
                    [
                        'method' => 'POST',
                        'url_schema' => 'https',
                        'url_host' => 'www.test.com',
                        'url_route' => '/path/to/resource',
                        'header' => [
                            'Content-type' => 'application/json',
                            'Accept' => 'application/json'
                        ],
                        'body' => json_encode($tabDataWithId)
                    ]
                ]
            ]
        );
    }



}