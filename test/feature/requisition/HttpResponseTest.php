<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\test\feature\requisition;

use PHPUnit\Framework\TestCase;

use liberty_code\parser\parser\factory\string_table\model\StrTableParserFactory;
use liberty_code\data\data\table\model\TableData;
use liberty_code\http\header\model\HeaderData;
use liberty_code\http\requisition\response\exception\RcpInfoInvalidFormatException;
use liberty_code\http\requisition\response\model\HttpResponse;
use liberty_code\http\requisition\response\data\exception\ConfigInvalidFormatException as DataConfigInvalidFormatException;
use liberty_code\http\requisition\response\data\model\DataHttpResponse;
use liberty_code\http\requisition\response\persistence\exception\ConfigInvalidFormatException as PersistorConfigInvalidFormatException;
use liberty_code\http\requisition\response\persistence\exception\DataRequiredException;
use liberty_code\http\requisition\response\persistence\exception\TabDataInvalidFormatException;
use liberty_code\http\requisition\response\persistence\model\PersistorHttpResponse;



/**
 * @cover HttpResponse
 * @cover DataHttpResponse
 * @cover PersistorHttpResponse
 */
class HttpResponseTest extends TestCase
{
    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods test
    // ******************************************************************************

    /**
     * Test can use HTTP response object.
     *
     * @param boolean $boolHeaderDataRequire
     * @param null|array $tabConfig
     * @param null|array $tabRcpInfo
     * @param string|array $expectResult
     * @dataProvider providerUseHttpResponse
     */
    public function testCanUseHttpResponse(
        $boolHeaderDataRequire,
        $tabConfig,
        $tabRcpInfo,
        $expectResult
    )
    {
        // Init var
        $boolExceptionExpected = is_string($expectResult);

        // Expect exception, if required
        if($boolExceptionExpected)
        {
            $this->expectException($expectResult);
        }

        // Get response
        $objHeaderData = ($boolHeaderDataRequire ? new HeaderData(): null);
        $objResponse = new HttpResponse(
            $objHeaderData,
            $tabConfig,
            $tabRcpInfo
        );

        // Set assertions (check response detail), if required
        if(!$boolExceptionExpected)
        {
            // Get info
            $tabExpectStatusCode = $expectResult[0];
            $tabExpectStatusMsg = $expectResult[1];
            $tabExpectConfig = $expectResult[2];
            $tabExpectRcpInfo = $expectResult[3];
            $tabResponseConfig = $objResponse->getTabConfig();
            $tabResponseRcpInfo = $objResponse->getTabRcpInfo();

            // Set assertions (check response detail)
            $this->assertEquals($tabExpectConfig, $tabResponseConfig);
            $this->assertEquals($tabExpectRcpInfo, $tabResponseRcpInfo);

            // Print
            /*
            echo('Get config: ' . PHP_EOL);var_dump($tabResponseConfig);echo(PHP_EOL);
            echo('Get sending info: ' . PHP_EOL);var_dump($tabResponseRcpInfo);echo(PHP_EOL);
            //*/

            // Set assertion (check response status code)
            $this->assertEquals($tabExpectStatusCode, $objResponse->getIntStatusCode());

            // Set assertion (check response status message)
            $this->assertEquals($tabExpectStatusMsg, $objResponse->getStrStatusMsg());

            // Set assertions (check response header data), if required
            if(!is_null($objHeaderData))
            {
                $this->assertEquals(true, isset($tabResponseRcpInfo['header']));
                $this->assertEquals($tabResponseRcpInfo['header'], $objHeaderData->getDataSrc());
                $this->assertEquals($objResponse->getTabHeader(), $objHeaderData->getDataSrc());

                $objResponse->getObjHeaderData()->putValue('key-add-1', 'Value Add 1');
                $objHeaderData->putValue('key-add-2', 'Value Add 2');
                $objHeaderData->putValue('key-add-3', 'Value Add 3');
                $objResponse->setObjHeaderData(null);
                $this->assertEquals(
                    array_merge(
                        $tabResponseRcpInfo['header'],
                        array(
                            'key-add-1' => 'Value Add 1',
                            'key-add-2' => 'Value Add 2',
                            'key-add-3' => 'Value Add 3'
                        )
                    ),
                    $objResponse->getTabHeader()
                );
            }
        }
    }



    /**
     * Data provider,
     * to test can use HTTP response object.
     *
     * @return array
     */
    public function providerUseHttpResponse()
    {
        // Init var
        $strBody = json_encode(array(
            'key-1' => 'Value 1',
            'key-2' => [
                'key-2-1' => 'Value 2-1',
                'key-2-2' => 2.2
            ],
            'key-3' => true
        ));

        // Return result
        return array(
            'Use HTTP response: fail to create response get (invalid status code format)' => [
                true,
                [
                    'key-config-1' => 'Value config 1',
                    'key-config-2' => 2
                ],
                [
                    'status_code' => 'test',
                    'status_msg' => 'OK',
                    'header' => [
                        'Content-type' => 'application/json',
                        'Content-Length' => mb_strlen($strBody),
                        'Cache-Control' => 'no-cache'
                    ],
                    'body' => $strBody
                ],
                RcpInfoInvalidFormatException::class
            ],
            'Use HTTP response: fail to create response get (invalid status message format)' => [
                false,
                [
                    'key-config-1' => 'Value config 1',
                    'key-config-2' => 2
                ],
                [
                    'status_code' => 200,
                    'status_msg' => false,
                    'header' => [
                        'Content-type' => 'application/json',
                        'Content-Length' => mb_strlen($strBody),
                        'Cache-Control' => 'no-cache'
                    ],
                    'body' => $strBody
                ],
                RcpInfoInvalidFormatException::class
            ],
            'Use HTTP response: fail to create response get (invalid header format)' => [
                true,
                [
                    'key-config-1' => 'Value config 1',
                    'key-config-2' => 2
                ],
                [
                    'status_code' => 200,
                    'status_msg' => 'OK',
                    'header' => 'test',
                    'body' => $strBody
                ],
                RcpInfoInvalidFormatException::class
            ],
            'Use HTTP response: success to create and use response get' => [
                true,
                [
                    'key-config-1' => 'Value config 1',
                    'key-config-2' => 2
                ],
                [
                    'protocol_version_num' => '2.0',
                    'status_code' => 200,
                    'status_msg' => 'OK',
                    'header' => [
                        'Content-type' => 'application/json',
                        'Content-Length' => mb_strlen($strBody),
                        'Cache-Control' => 'no-cache'
                    ],
                    'body' => $strBody
                ],
                [
                    200,
                    'OK',
                    [
                        'key-config-1' => 'Value config 1',
                        'key-config-2' => 2
                    ],
                    [
                        'protocol_version_num' => '2.0',
                        'status_code' => 200,
                        'status_msg' => 'OK',
                        'header' => [
                            'Content-type' => 'application/json',
                            'Content-Length' => mb_strlen($strBody),
                            'Cache-Control' => 'no-cache'
                        ],
                        'body' => $strBody
                    ]
                ]
            ],
            'Use HTTP response: success to create and use response post' => [
                false,
                [],
                [
                    'protocol_version_num' => null,
                    'status_code' => 201,
                    'header' => [
                        'Cache-Control' => 'no-cache'
                    ]
                ],
                [
                    201,
                    null,
                    [],
                    [
                        'protocol_version_num' => null,
                        'status_code' => 201,
                        'header' => [
                            'Cache-Control' => 'no-cache'
                        ]
                    ]
                ]
            ]
        );
    }



    /**
     * Test can use data HTTP response object.
     *
     * @param boolean $boolBodyDataRequire
     * @param null|array $tabConfig
     * @param null|array $tabRcpInfo
     * @param string|array $expectResult
     * @depends testCanUseHttpResponse
     * @dataProvider providerUseDataHttpResponse
     */
    public function testCanUseDataHttpResponse(
        $boolBodyDataRequire,
        $tabConfig,
        $tabRcpInfo,
        $expectResult
    )
    {
        // Init var
        $boolExceptionExpected = is_string($expectResult);
        $objParserFactory = new StrTableParserFactory();

        // Expect exception, if required
        if($boolExceptionExpected)
        {
            $this->expectException($expectResult);
        }

        // Get response
        $objBodyData = ($boolBodyDataRequire ? new TableData(): null);
        $objResponse = new DataHttpResponse(
            $objParserFactory,
            null,
            $objBodyData,
            $tabConfig,
            $tabRcpInfo
        );
        $tabResponseConfig = $objResponse->getTabConfig();
        $tabResponseRcpInfo = $objResponse->getTabRcpInfo();
        $tabResponseBodyData = $objResponse->getTabBodyData();

        // Set assertions (check response detail), if required
        if(!$boolExceptionExpected)
        {
            // Get info
            $tabExpectConfig = $expectResult[0];
            $tabExpectRcpInfo = $expectResult[1];
            $tabExpectBodyData = $expectResult[2];

            // Set assertions (check response detail)
            $this->assertEquals($tabExpectConfig, $tabResponseConfig);
            $this->assertEquals($tabExpectRcpInfo, $tabResponseRcpInfo);
            $this->assertEquals($tabExpectBodyData, $tabResponseBodyData);

            // Print
            /*
            echo('Get config: ' . PHP_EOL);var_dump($tabResponseConfig);echo(PHP_EOL);
            echo('Get sending info: ' . PHP_EOL);var_dump($tabResponseRcpInfo);echo(PHP_EOL);
            echo('Get body data: ' . PHP_EOL);var_dump($tabResponseBodyData);echo(PHP_EOL);
            //*/

            // Set assertions (check response body data), if required
            if(!is_null($objBodyData))
            {
                if(is_null($tabResponseBodyData))
                {
                    $this->assertEquals(array(), $objBodyData->getDataSrc());
                }
                else
                {
                    $this->assertEquals($tabResponseBodyData, $objBodyData->getDataSrc());
                }

                $objResponse->getObjBodyData()->putValue('key-add-1', 'Value Add 1');
                $objBodyData->putValue('key-add-2', 'Value Add 2');
                $objBodyData->putValue('key-add-3', 'Value Add 3');
                $objResponse->setObjBodyData(null);
                $this->assertEquals(
                    array_merge(
                        (is_null($tabResponseBodyData) ? array() : $tabResponseBodyData),
                        array(
                            'key-add-1' => 'Value Add 1',
                            'key-add-2' => 'Value Add 2',
                            'key-add-3' => 'Value Add 3'
                        )
                    ),
                    $objResponse->getTabBodyData()
                );
            }
        }
    }



    /**
     * Data provider,
     * to test can use data HTTP response object.
     *
     * @return array
     */
    public function providerUseDataHttpResponse()
    {
        // Init var
        $tabBodyData = array(
            'key-1' => 'Value 1',
            'key-2' => [
                'key-2-1' => 'Value 2-1',
                'key-2-2' => 2.2
            ],
            'key-3' => true
        );
        $strBody = json_encode($tabBodyData);

        // Return result
        return array(
            'Use data HTTP response: fail to create response get (invalid config format: body parser config not found)' => [
                true,
                [],
                [
                    'status_code' => 200,
                    'status_msg' => 'OK',
                    'header' => [
                        'Content-type' => 'application/json',
                        'Content-Length' => mb_strlen($strBody)
                    ],
                    'body' => $strBody
                ],
                DataConfigInvalidFormatException::class
            ],
            'Use data HTTP response: fail to create response get (invalid config format: invalid body parser config format)' => [
                false,
                [
                    'body_parser_config' => 'test'
                ],
                [
                    'status_code' => 200,
                    'status_msg' => 'OK',
                    'header' => [
                        'Content-type' => 'application/json',
                        'Content-Length' => mb_strlen($strBody)
                    ],
                    'body' => $strBody
                ],
                DataConfigInvalidFormatException::class
            ],
            'Use data HTTP response: fail to create response get (invalid config format: body parser config type not found)' => [
                true,
                [
                    'body_parser_config' => [
                        'type' => 'test'
                    ]
                ],
                [
                    'status_code' => 200,
                    'status_msg' => 'OK',
                    'header' => [
                        'Content-type' => 'application/json',
                        'Content-Length' => mb_strlen($strBody)
                    ],
                    'body' => $strBody
                ],
                DataConfigInvalidFormatException::class
            ],
            'Use data HTTP response: fail to use response get (invalid parsable body)' => [
                false,
                [
                    'body_parser_config' => [
                        'type' => 'string_table_json'
                    ]
                ],
                [
                    'status_code' => 200,
                    'status_msg' => 'OK',
                    'header' => [
                        'Content-type' => 'application/json',
                        'Content-Length' => 0
                    ],
                    'body' => ''
                ],
                DataConfigInvalidFormatException::class
            ],
            'Use data HTTP response: success to create and use response get (without body)' => [
                false,
                [
                    'body_parser_config' => [
                        'type' => 'string_table_json'
                    ]
                ],
                [
                    'status_code' => 200,
                    'status_msg' => 'OK',
                    'header' => [
                        'Content-type' => 'application/json',
                        'Content-Length' => 0
                    ]
                ],
                [
                    [
                        'body_parser_config' => [
                            'type' => 'string_table_json'
                        ]
                    ],
                    [
                        'status_code' => 200,
                        'status_msg' => 'OK',
                        'header' => [
                            'Content-type' => 'application/json',
                            'Content-Length' => 0
                        ]
                    ],
                    null
                ]
            ],
            'Use data HTTP response: success to create response get (using body)' => [
                true,
                [
                    'body_parser_config' => [
                        'type' => 'string_table_json'
                    ]
                ],
                [
                    'status_code' => 200,
                    'status_msg' => 'OK',
                    'header' => [
                        'Content-type' => 'application/json',
                        'Content-Length' => mb_strlen($strBody)
                    ],
                    'body' => $strBody
                ],
                [
                    [
                        'body_parser_config' => [
                            'type' => 'string_table_json'
                        ]
                    ],
                    [
                        'status_code' => 200,
                        'status_msg' => 'OK',
                        'header' => [
                            'Content-type' => 'application/json',
                            'Content-Length' => mb_strlen($strBody)
                        ],
                        'body' => $strBody
                    ],
                    $tabBodyData
                ]
            ]
        );
    }



    /**
     * Test can use persistor HTTP response object.
     *
     * @param null|array $tabConfig
     * @param null|array $tabRcpInfo
     * @param string $strMethodNm
     * @param string|array $expectResult
     * @depends testCanUseDataHttpResponse
     * @dataProvider providerUsePersistorHttpResponse
     */
    public function testCanUsePersistorHttpResponse(
        $tabConfig,
        $tabRcpInfo,
        $strMethodNm,
        $expectResult
    )
    {
        // Init var
        $boolExceptionExpected = is_string($expectResult);
        $objParserFactory = new StrTableParserFactory();

        // Expect exception, if required
        if($boolExceptionExpected)
        {
            $this->expectException($expectResult);
        }

        // Get response
        $objResponse = new PersistorHttpResponse(
            $objParserFactory,
            null,
            null,
            $tabConfig,
            $tabRcpInfo
        );
        $boolResponseIsSucceeded = $objResponse->checkIsSucceeded();
        $responseValue = $objResponse->$strMethodNm();

        // Set assertions (check response detail), if required
        if(!$boolExceptionExpected)
        {
            // Get info
            $boolExpectIsSucceeded = $expectResult[0];
            $expectValue = $expectResult[1];

            // Set assertions (check response detail)
            $this->assertEquals($boolExpectIsSucceeded, $boolResponseIsSucceeded);
            $this->assertEquals($expectValue, $responseValue);

            // Print
            /*
            echo('Get config: ' . PHP_EOL);var_dump($objResponse->getTabConfig());echo(PHP_EOL);
            echo('Get sending info: ' . PHP_EOL);var_dump($objResponse->getTabRcpInfo());echo(PHP_EOL);
            echo('Check success: ' . PHP_EOL);var_dump($boolResponseIsSucceeded);echo(PHP_EOL);
            echo('Get value (for "' . $strMethodNm . '"): ' . PHP_EOL);var_dump($responseValue);echo(PHP_EOL);
            //*/
        }
    }



    /**
     * Data provider,
     * to test can use persistor HTTP response object.
     *
     * @return array
     */
    public function providerUsePersistorHttpResponse()
    {
        // Init get data function
        $getData = function($id, $nm, $fnm, $age)
        {
            $result = array();
            if(!is_null($id))
            {
                $result['id'] = $id;
            }
            if(!is_null($nm))
            {
                $result['name'] = $nm;
            }
            if(!is_null($fnm))
            {
                $result['first-name'] = $fnm;
            }
            if(!is_null($id))
            {
                $result['age'] = $age;
            }
            return $result;
        };

        // Init var
        $data = $getData(1, 'NM-1', 'Fnm-1', 20);
        $tabData = array(
            $getData(1, 'NM-1', 'Fnm-1', 20),
            $getData(2, 'NM-2', 'Fnm-2', 30),
            $getData(3, 'NM-3', 'Fnm-3', 40)
        );

        // Return result
        return array(
            'Use persistor HTTP response: fail to create response get (invalid configuration success status code)' => [
                [
                    'body_parser_config' => [
                        'type' => 'string_table_json'
                    ],
                    'success_status_code' => false
                ],
                [
                    'status_code' => 200,
                    'status_msg' => 'OK',
                    'header' => [
                        'Content-type' => 'application/json',
                        'Content-Length' => 0
                    ],
                    'body' => json_encode($data)
                ],
                'getData',
                PersistorConfigInvalidFormatException::class
            ],
            'Use persistor HTTP response: success to create and use response get (get data)' => [
                [
                    'body_parser_config' => [
                        'type' => 'string_table_json'
                    ]
                ],
                [
                    'status_code' => 200,
                    'status_msg' => 'OK',
                    'header' => [
                        'Content-type' => 'application/json',
                        'Content-Length' => 0
                    ],
                    'body' => json_encode($data)
                ],
                'getData',
                [
                    true,
                    $data
                ]
            ],
            'Use persistor HTTP response: fail to create response get (invalid configuration data select path)' => [
                [
                    'body_parser_config' => [
                        'type' => 'string_table_json'
                    ],
                    'success_status_code' => [200, 201],
                    'multi_data_select_path' => 7
                ],
                [
                    'status_code' => 400,
                    'status_msg' => 'Bad request',
                    'header' => [
                        'Content-type' => 'application/json',
                        'Content-Length' => 0
                    ],
                    'body' => json_encode([
                        'result' => [
                            'data' =>
                                $tabData
                        ]
                    ])
                ],
                'getTabData',
                PersistorConfigInvalidFormatException::class
            ],
            'Use persistor HTTP response: fail to use response get (unable to get array of data: data not found)' => [
                [
                    'body_parser_config' => [
                        'type' => 'string_table_json'
                    ],
                    'success_status_code' => [200, 201],
                    'multi_data_select_path' => 'result/data',
                    ''
                ],
                [
                    'status_code' => 400,
                    'status_msg' => 'Bad request',
                    'header' => [
                        'Content-type' => 'application/json',
                        'Content-Length' => 0
                    ]
                ],
                'getTabData',
                DataRequiredException::class
            ],
            'Use persistor HTTP response: success to create and use response get (get array of data)' => [
                [
                    'body_parser_config' => [
                        'type' => 'string_table_json'
                    ],
                    'success_status_code' => [200, 201],
                    'multi_data_select_path' => 'result/data',
                    'multi_data_include_key' => [
                        'name',
                        'first-name'
                    ]
                ],
                [
                    'status_code' => 400,
                    'status_msg' => 'Bad request',
                    'header' => [
                        'Content-type' => 'application/json',
                        'Content-Length' => 0
                    ],
                    'body' => json_encode([
                        'result' => [
                            'data' => $tabData
                        ]
                    ])
                ],
                'getTabData',
                [
                    false,
                    [
                        $getData(null, 'NM-1', 'Fnm-1', null),
                        $getData(null, 'NM-2', 'Fnm-2', null),
                        $getData(null, 'NM-3', 'Fnm-3', null)
                    ]
                ]
            ],
            'Use persistor HTTP response: fail to use response get (unable to get array of searched data: invalid configuration data select path)' => [
                [
                    'body_parser_config' => [
                        'type' => 'string_table_json'
                    ],
                    'path_separator' => '.',
                    'search_data_select_path' => 'result.search.data'
                ],
                [
                    'status_code' => 400,
                    'status_msg' => 'Bad request',
                    'header' => [
                        'Content-type' => 'application/json',
                        'Content-Length' => 0
                    ],
                    'body' => json_encode([
                        'result' => [
                            'search' => [
                                'data' => 'test'
                            ]
                        ]
                    ])
                ],
                'getTabSearchData',
                DataRequiredException::class
            ],
            'Use persistor HTTP response: success to create and use response get (get array of searched data)' => [
                [
                    'body_parser_config' => [
                        'type' => 'string_table_json'
                    ],
                    'path_separator' => '.',
                    'search_data_select_path' => 'result.search.data',
                    'search_data_exclude_key' => [
                        'id',
                        'age'
                    ]
                ],
                [
                    'status_code' => 400,
                    'status_msg' => 'Bad request',
                    'header' => [
                        'Content-type' => 'application/json',
                        'Content-Length' => 0
                    ],
                    'body' => json_encode([
                        'result' => [
                            'search' => [
                                'data' => $tabData
                            ]
                        ]
                    ])
                ],
                'getTabSearchData',
                [
                    false,
                    [
                        $getData(null, 'NM-1', 'Fnm-1', null),
                        $getData(null, 'NM-2', 'Fnm-2', null),
                        $getData(null, 'NM-3', 'Fnm-3', null)
                    ]
                ]
            ],
            'Use persistor HTTP response: success to create and use response post (get created id: null)' => [
                [
                    'body_parser_config' => [
                        'type' => 'string_table_json'
                    ],
                    'data_id_key' => 'id'
                ],
                [
                    'status_code' => 201,
                    'status_msg' => 'Created',
                    'header' => [
                        'Content-type' => 'application/json',
                        'Content-Length' => 0
                    ]
                ],
                'getCreateId',
                [
                    true,
                    null
                ]
            ],
            'Use persistor HTTP response: success to create and use response post (get created id)' => [
                [
                    'body_parser_config' => [
                        'type' => 'string_table_json'
                    ],
                    'data_id_key' => 'id'
                ],
                [
                    'status_code' => 201,
                    'status_msg' => 'Created',
                    'header' => [
                        'Content-type' => 'application/json',
                        'Content-Length' => 0
                    ],
                    'body' => json_encode($data)
                ],
                'getCreateId',
                [
                    true,
                    1
                ]
            ],
            'Use persistor HTTP response: success to create and use response post (get array of created ids: null)' => [
                [
                    'body_parser_config' => [
                        'type' => 'string_table_json'
                    ]
                ],
                [
                    'status_code' => 201,
                    'status_msg' => 'Created',
                    'header' => [
                        'Content-type' => 'application/json',
                        'Content-Length' => 0
                    ],
                    'body' => json_encode($tabData)
                ],
                'getTabCreateId',
                [
                    true,
                    null
                ]
            ],
            'Use persistor HTTP response: success to create and use response post (get array of created ids)' => [
                [
                    'body_parser_config' => [
                        'type' => 'string_table_json'
                    ],
                    'data_id_key' => 'id'
                ],
                [
                    'status_code' => 201,
                    'status_msg' => 'Created',
                    'header' => [
                        'Content-type' => 'application/json',
                        'Content-Length' => 0
                    ],
                    'body' => json_encode($tabData)
                ],
                'getTabCreateId',
                [
                    true,
                    [1, 2, 3]
                ]
            ],
            'Use persistor HTTP response: fail to create response post (get updated data: invalid configuration data return status format)' => [
                [
                    'body_parser_config' => [
                        'type' => 'string_table_json'
                    ],
                    'update_data_return_status' => 'test'
                ],
                [
                    'status_code' => 400,
                    'status_msg' => 'Bad request',
                    'header' => [
                        'Content-type' => 'application/json',
                        'Content-Length' => 0
                    ],
                    'body' => json_encode($data)
                ],
                'getUpdateData',
                PersistorConfigInvalidFormatException::class
            ],
            'Use persistor HTTP response: success to create and use response post (get updated data: null)' => [
                [
                    'body_parser_config' => [
                        'type' => 'string_table_json'
                    ],
                    'update_data_return_status' => 'none'
                ],
                [
                    'status_code' => 400,
                    'status_msg' => 'Bad request',
                    'header' => [
                        'Content-type' => 'application/json',
                        'Content-Length' => 0
                    ],
                    'body' => json_encode($data)
                ],
                'getUpdateData',
                [
                    false,
                    null
                ]
            ],
            'Use persistor HTTP response: success to create and use response post (get updated data)' => [
                [
                    'body_parser_config' => [
                        'type' => 'string_table_json'
                    ]
                ],
                [
                    'status_code' => 400,
                    'status_msg' => 'Bad request',
                    'header' => [
                        'Content-type' => 'application/json',
                        'Content-Length' => 0
                    ],
                    'body' => json_encode($data)
                ],
                'getUpdateData',
                [
                    false,
                    $data
                ]
            ],
            'Use persistor HTTP response: fail to use response post (unable to get array of updated data: invalid array of data format)' => [
                [
                    'body_parser_config' => [
                        'type' => 'string_table_json'
                    ]
                ],
                [
                    'status_code' => 200,
                    'status_msg' => 'OK',
                    'header' => [
                        'Content-type' => 'application/json',
                        'Content-Length' => 0
                    ],
                    'body' => json_encode(['test'])
                ],
                'getTabUpdateData',
                TabDataInvalidFormatException::class
            ],
            'Use persistor HTTP response: success to create and use response post (get array of updated data)' => [
                [
                    'body_parser_config' => [
                        'type' => 'string_table_json'
                    ]
                ],
                [
                    'status_code' => 200,
                    'status_msg' => 'OK',
                    'header' => [
                        'Content-type' => 'application/json',
                        'Content-Length' => 0
                    ],
                    'body' => json_encode($tabData)
                ],
                'getTabUpdateData',
                [
                    true,
                    $tabData
                ]
            ],
            'Use persistor HTTP response: fail to use response delete (unable to get deleted data: data not found)' => [
                [
                    'body_parser_config' => [
                        'type' => 'string_table_json'
                    ],
                    'delete_data_select_path' => 'result/data',
                    'delete_data_return_status' => 'required'
                ],
                [
                    'status_code' => 200,
                    'status_msg' => 'OK',
                    'header' => [
                        'Content-type' => 'application/json',
                        'Content-Length' => 0
                    ]
                ],
                'getDeleteData',
                DataRequiredException::class
            ],
            'Use persistor HTTP response: success to create and use response delete (get deleted data)' => [
                [
                    'body_parser_config' => [
                        'type' => 'string_table_json'
                    ],
                    'delete_data_select_path' => 'result/data',
                    'delete_data_return_status' => 'required'
                ],
                [
                    'status_code' => 200,
                    'status_msg' => 'OK',
                    'header' => [
                        'Content-type' => 'application/json',
                        'Content-Length' => 0
                    ],
                    'body' => json_encode([
                        'result' => [
                            'data' => $data
                        ]
                    ])
                ],
                'getDeleteData',
                [
                    true,
                    $data
                ]
            ],
            'Use persistor HTTP response: success to create and use response delete (get array of deleted data: null)' => [
                [
                    'body_parser_config' => [
                        'type' => 'string_table_json'
                    ],
                    'delete_data_select_path' => 'result/data',
                    'delete_data_return_status' => 'none'
                ],
                [
                    'status_code' => 200,
                    'status_msg' => 'OK',
                    'header' => [
                        'Content-type' => 'application/json',
                        'Content-Length' => 0
                    ],
                    'body' => json_encode([
                        'result' => [
                            'data' => $tabData
                        ]
                    ])
                ],
                'getTabDeleteData',
                [
                    true,
                    null
                ]
            ],
            'Use persistor HTTP response: success to create and use response delete (get array of deleted data)' => [
                [
                    'body_parser_config' => [
                        'type' => 'string_table_json'
                    ],
                    'delete_data_select_path' => 'result/data'
                ],
                [
                    'status_code' => 200,
                    'status_msg' => 'OK',
                    'header' => [
                        'Content-type' => 'application/json',
                        'Content-Length' => 0
                    ],
                    'body' => json_encode([
                        'result' => [
                            'data' => $tabData
                        ]
                    ])
                ],
                'getTabDeleteData',
                [
                    true,
                    $tabData
                ]
            ]
        );
    }



}