<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\test\feature\requisition;

use PHPUnit\Framework\TestCase;

use liberty_code\http\requisition\request\exception\SndInfoInvalidFormatException;
use liberty_code\http\requisition\request\model\HttpRequest;
use liberty_code\http\requisition\request\data\exception\ConfigInvalidFormatException as DataConfigInvalidFormatException;
use liberty_code\http\requisition\request\data\model\DataHttpRequest;
use liberty_code\http\requisition\request\persistence\exception\ConfigInvalidFormatException as PersistorConfigInvalidFormatException;
use liberty_code\http\requisition\request\persistence\model\PersistorHttpRequest;
use liberty_code\http\requisition\request\factory\model\HttpRequestFactory;



/**
 * @cover HttpRequestFactory
 */
class HttpRequestFactoryTest extends TestCase
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods test
    // ******************************************************************************

    /**
     * Test can create HTTP request object.
     *
     * @param null|string $strConfigKey
     * @param array $tabConfig
     * @param null|string|array $expectResult
     * @dataProvider providerCreateHttpRequest
     * @dataProvider providerCreateDataHttpRequest
     * @dataProvider providerCreatePersistorHttpRequest
     */
    public function testCanCreateHttpRequest(
        $strConfigKey,
        array $tabConfig,
        $expectResult
    )
    {
        // Load
        $strRootAppPath = dirname(__FILE__) . '/../..';
        require($strRootAppPath . '/requisition/request/factory/boot/HttpRequestFactoryBootstrap.php');

        // Init var
        $boolExceptionExpected = is_string($expectResult);

        // Expect exception, if required
        if($boolExceptionExpected)
        {
            $this->expectException($expectResult);
        }

        // Get request
        /** @var HttpRequestFactory $objHttpRequestFactory */
        $objRequest = $objHttpRequestFactory->getObjRequest($tabConfig, $strConfigKey);
        $strClassPath = $objHttpRequestFactory->getStrRequestClassPath($tabConfig, $strConfigKey);

        // Get info
        $boolCreate = (!is_null($objRequest));
        $boolExpectCreate = (!is_null($expectResult));

        // Set assertions (check request creation), if required
        if(!$boolExceptionExpected)
        {
            // Set assertion (check request creation)
            if(!$boolExpectCreate)
            {
                $this->assertEquals(null, $objRequest);
            }
            $this->assertEquals($boolExpectCreate, $boolCreate);

            // Set assertions (check request detail), if required
            if($boolCreate && $boolExpectCreate)
            {
                // Get info
                $strExpectClassPath = $expectResult[0];
                $strExpectKey = $expectResult[1];
                $tabExpectConfig = $expectResult[2];
                $tabExpectSndInfo = $expectResult[3];

                // Set assertions (check request detail)
                $this->assertEquals($strExpectClassPath, $strClassPath);
                $this->assertEquals($strExpectClassPath, get_class($objRequest));
                if(!is_null($strExpectKey))
                {
                    $this->assertEquals($strExpectKey, $objRequest->getStrKey());
                }
                $this->assertEquals($tabExpectConfig, $objRequest->getTabConfig());
                $this->assertEquals($tabExpectSndInfo, $objRequest->getTabSndInfo());

                // Set assertions (check request dependencies)
                /** @var HttpRequest $objRequest */
                $this->assertEquals(false, is_null($objRequest->getObjUrlRouteArgData()));
                $this->assertEquals(false, is_null($objRequest->getObjUrlArgData()));
                $this->assertEquals(false, is_null($objRequest->getObjHeaderData()));

                // Print
                /*
                echo('Get Class path: '. PHP_EOL);var_dump($strClassPath);echo(PHP_EOL);
                echo('Get key: ' . PHP_EOL);var_dump($objRequest->getStrKey());echo(PHP_EOL);
                echo('Get config: ' . PHP_EOL);var_dump($objRequest->getTabConfig());echo(PHP_EOL);
                echo('Get sending info: ' . PHP_EOL);var_dump($objRequest->getTabSndInfo());echo(PHP_EOL);
                //*/
            }
        }
    }



    /**
     * Data provider,
     * to test can create HTTP request object.
     *
     * @return array
     */
    public function providerCreateHttpRequest()
    {
        // Init var
        $strBody = json_encode(array(
            'key-1' => 'Value 1',
            'key-2' => [
                'key-2-1' => 'Value 2-1',
                'key-2-2' => 2.2
            ],
            'key-3' => true
        ));

        // Return result
        return array(
            'Create HTTP request: fail to create request_get (type not found)' => [
                null,
                [
                    'key' => 'request_get',
                    'snd_info' => [
                        'method' => 'GET',
                        'url' => 'https://www.test.com:80/path/to/resource?arg1=value1&arg2=value2&arg3=value3',
                        'header' => [
                            'Accept' => 'application/json',
                            'Cache-Control' => 'no-cache'
                        ]
                    ]
                ],
                null
            ],
            'Create HTTP request: fail to create request_get (type unknown)' => [
                null,
                [
                    'type' => 'test',
                    'key' => 'request_get',
                    'snd_info' => [
                        'method' => 'GET',
                        'url' => 'https://www.test.com:80/path/to/resource?arg1=value1&arg2=value2&arg3=value3',
                        'header' => [
                            'Accept' => 'application/json',
                            'Cache-Control' => 'no-cache'
                        ]
                    ]
                ],
                null
            ],
            'Create HTTP request: success to create request_get' => [
                null,
                [
                    'type' => 'http',
                    'key' => 'request_get',
                    'snd_info' => [
                        'method' => 'GET',
                        'url' => 'https://www.test.com:80/path/to/resource/{key-1}/{key-2}?arg1=value1&arg2=value2&arg3=value3',
                        'url_route_argument' => [
                            '{key-1}' => 'value-2-test',
                            '{key-2}' => 'value-3-test'
                        ],
                        'url_route_argument_spec' => [
                            '{key-1}' => 'value-1',
                            '{key-2}' => null
                        ],
                        'header' => [
                            'Accept' => 'application/json',
                            'Cache-Control' => 'no-cache'
                        ]
                    ]
                ],
                [
                    HttpRequest::class,
                    'request_get',
                    [
                        'key' => 'request_get'
                    ],
                    [
                        'method' => 'GET',
                        'url_schema' => 'https',
                        'url_host' => 'www.test.com',
                        'url_port' => 80,
                        'url_route' => '/path/to/resource/{key-1}/{key-2}',
                        'url_route_argument' => [
                            '{key-1}' => 'value-2-test',
                            '{key-2}' => 'value-3-test'
                        ],
                        'url_route_argument_spec' => [
                            '{key-1}' => 'value-1',
                            '{key-2}' => null
                        ],
                        'url_argument' => [
                            'arg1' => 'value1',
                            'arg2' => 'value2',
                            'arg3' => 'value3'
                        ],
                        'header' => [
                            'Accept' => 'application/json',
                            'Cache-Control' => 'no-cache'
                        ]
                    ]
                ]
            ],
            'Create HTTP request: fail to create request_post (invalid header format)' => [
                'request_post',
                [
                    'type' => 'http',
                    'snd_info' => [
                        'method' => 'POST',
                        'url' => 'https://www.test.com:80/path/to/resource',
                        'header' => 'test',
                        'body' => $strBody
                    ]
                ],
                SndInfoInvalidFormatException::class
            ],
            'Create HTTP request: success to create request_post' => [
                'request_post',
                [
                    'type' => 'http',
                    'snd_info' => [
                        'method' => 'POST',
                        'url' => 'https://www.test.com:80/path/to/resource',
                        'header' => [
                            'Content-type' => 'application/json',
                            'Accept' => 'application/json',
                            'Cache-Control' => 'no-cache'
                        ],
                        'body' => $strBody
                    ]
                ],
                [
                    HttpRequest::class,
                    'request_post',
                    [
                        'key' => 'request_post'
                    ],
                    [
                        'method' => 'POST',
                        'url_schema' => 'https',
                        'url_host' => 'www.test.com',
                        'url_port' => 80,
                        'url_route' => '/path/to/resource',
                        // Due to set URL route argument data, during request construction, information URL route arguments already initialized
                        'url_route_argument' => [],
                        // Due to set URL argument data, during request construction, information URL arguments already initialized
                        'url_argument' => [],
                        'header' => [
                            'Content-type' => 'application/json',
                            'Accept' => 'application/json',
                            'Cache-Control' => 'no-cache'
                        ],
                        'body' => $strBody
                    ]
                ]
            ]
        );
    }



    /**
     * Data provider,
     * to test can create data HTTP request object.
     *
     * @return array
     */
    public function providerCreateDataHttpRequest()
    {
        // Init var
        $tabBodyData = array(
            'key-1' => 'Value 1',
            'key-2' => [
                'key-2-1' => 'Value 2-1',
                'key-2-2' => 2.2
            ],
            'key-3' => true
        );
        $strBody = json_encode($tabBodyData);

        // Return result
        return array(
            'Create data HTTP request: fail to create request_post (invalid body format)' => [
                'request_post',
                [
                    'type' => 'http_data',
                    'body_parser_config' => 'test',
                    'snd_info' => [
                        'method' => 'POST',
                        'url' => 'https://www.test.com/path/to/resource',
                        'header' => [
                            'Content-type' => 'application/json',
                            'Accept' => 'application/json'
                        ],
                        'body' => ''
                    ]
                ],
                DataConfigInvalidFormatException::class
            ],
            'Create data HTTP request: success to create request_post (without body)' => [
                'request_post',
                [
                    'type' => 'http_data',
                    'body_parser_config' => [
                        'type' => 'string_table_json'
                    ],
                    'snd_info' => [
                        'method' => 'POST',
                        'url' => 'https://www.test.com/path/to/resource',
                        'header' => [
                            'Content-type' => 'application/json',
                            'Accept' => 'application/json'
                        ]
                    ]
                ],
                [
                    DataHttpRequest::class,
                    'request_post',
                    [
                        'key' => 'request_post',
                        'body_parser_config' => [
                            'type' => 'string_table_json'
                        ]
                    ],
                    [
                        'method' => 'POST',
                        'url_schema' => 'https',
                        'url_host' => 'www.test.com',
                        'url_route' => '/path/to/resource',
                        // Due to set URL route argument data, during request construction, information URL route arguments already initialized
                        'url_route_argument' => [],
                        // Due to set URL argument data, during request construction, information URL arguments already initialized
                        'url_argument' => [],
                        'header' => [
                            'Content-type' => 'application/json',
                            'Accept' => 'application/json',
                        ]
                    ]
                ]
            ],
            'Create data HTTP request: success to create request_post (using body)' => [
                'request_post',
                [
                    'type' => 'http_data',
                    'body_parser_config' => [
                        'type' => 'string_table_json'
                    ],
                    'snd_info' => [
                        'method' => 'POST',
                        'url' => 'https://www.test.com/path/to/resource',
                        'header' => [
                            'Content-type' => 'application/json',
                            'Accept' => 'application/json'
                        ],
                        'body' => $strBody
                    ]
                ],
                [
                    DataHttpRequest::class,
                    'request_post',
                    [
                        'key' => 'request_post',
                        'body_parser_config' => [
                            'type' => 'string_table_json'
                        ]
                    ],
                    [
                        'method' => 'POST',
                        'url_schema' => 'https',
                        'url_host' => 'www.test.com',
                        'url_route' => '/path/to/resource',
                        // Due to set URL route argument data, during request construction, information URL route arguments already initialized
                        'url_route_argument' => [],
                        // Due to set URL argument data, during request construction, information URL arguments already initialized
                        'url_argument' => [],
                        'header' => [
                            'Content-type' => 'application/json',
                            'Accept' => 'application/json',
                        ],
                        'body' => $strBody
                    ]
                ]
            ],
            'Create data HTTP request: success to create request_post (using body data)' => [
                'request_post',
                [
                    'type' => 'http_data',
                    'body_parser_config' => [
                        'type' => 'string_table_json'
                    ],
                    'snd_info' => [
                        'method' => 'POST',
                        'url' => 'https://www.test.com/path/to/resource',
                        'header' => [
                            'Content-type' => 'application/json',
                            'Accept' => 'application/json'
                        ],
                        'body_data' => $tabBodyData
                    ]
                ],
                [
                    DataHttpRequest::class,
                    'request_post',
                    [
                        'key' => 'request_post',
                        'body_parser_config' => [
                            'type' => 'string_table_json'
                        ]
                    ],
                    [
                        'method' => 'POST',
                        'url_schema' => 'https',
                        'url_host' => 'www.test.com',
                        'url_route' => '/path/to/resource',
                        // Due to set URL route argument data, during request construction, information URL route arguments already initialized
                        'url_route_argument' => [],
                        // Due to set URL argument data, during request construction, information URL arguments already initialized
                        'url_argument' => [],
                        'header' => [
                            'Content-type' => 'application/json',
                            'Accept' => 'application/json',
                        ],
                        'body' => $strBody
                    ]
                ]
            ]
        );
    }



    /**
     * Data provider,
     * to test can create persistor HTTP request object.
     *
     * @return array
     */
    public function providerCreatePersistorHttpRequest()
    {
        // Init get data function
        $getData = function($id, $nm, $fnm, $age)
        {
            $result = array(
                'id' => $id,
                'name' => $nm,
                'first-name' => $fnm,
                'age' => $age
            );
            return $result;
        };

        // Init var
        $tabData = array(
            $getData(1, 'NM-1', 'Fnm-1', 20),
            $getData(2, 'NM-2', 'Fnm-2', 30),
            $getData(3, 'NM-3', 'Fnm-3', 40)
        );

        // Return result
        return array(
            'Create persistor HTTP request: fail to create request_post (invalid configuration data path format)' => [
                'request_post',
                [
                    'type' => 'http_persistor',
                    'body_parser_config' => [
                        'type' => 'string_table_json'
                    ],
                    'create_data_path' => 'result/data',
                    'update_data_path' => 7,
                    'delete_data_path' => 'result/data',
                    'snd_info' => [
                        'method' => 'POST',
                        'url' => 'https://www.test.com/path/to/resource',
                        'header' => [
                            'Content-type' => 'application/json',
                            'Accept' => 'application/json'
                        ],
                        'body' => json_encode([
                            'result' => [
                                'data' => $tabData
                            ]
                        ])
                    ]
                ],
                PersistorConfigInvalidFormatException::class
            ],
            'Create persistor HTTP request: success to create request_post' => [
                'request_post',
                [
                    'type' => 'http_persistor',
                    'body_parser_config' => [
                        'type' => 'string_table_json'
                    ],
                    'create_data_path' => 'result/data',
                    'update_data_path' => 'result/data',
                    'delete_data_path' => 'result/data',
                    'snd_info' => [
                        'method' => 'POST',
                        'url' => 'https://www.test.com/path/to/resource',
                        'header' => [
                            'Content-type' => 'application/json',
                            'Accept' => 'application/json'
                        ],
                        'body' => json_encode([
                            'result' => [
                                'data' => $tabData
                            ]
                        ])
                    ]
                ],
                [
                    PersistorHttpRequest::class,
                    'request_post',
                    [
                        'key' => 'request_post',
                        'body_parser_config' => [
                            'type' => 'string_table_json'
                        ],
                        'create_data_path' => 'result/data',
                        'update_data_path' => 'result/data',
                        'delete_data_path' => 'result/data',
                    ],
                    [
                        'method' => 'POST',
                        'url_schema' => 'https',
                        'url_host' => 'www.test.com',
                        'url_route' => '/path/to/resource',
                        // Due to set URL route argument data, during request construction, information URL route arguments already initialized
                        'url_route_argument' => [],
                        // Due to set URL argument data, during request construction, information URL arguments already initialized
                        'url_argument' => [],
                        'header' => [
                            'Content-type' => 'application/json',
                            'Accept' => 'application/json'
                        ],
                        'body' => json_encode([
                            'result' => [
                                'data' => $tabData
                            ]
                        ])
                    ]
                ]
            ]
        );
    }



}