<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\test\feature\requisition;

use PHPUnit\Framework\TestCase;

use liberty_code\http\requisition\response\exception\RcpInfoInvalidFormatException;
use liberty_code\http\requisition\response\model\HttpResponse;
use liberty_code\http\requisition\response\data\exception\ConfigInvalidFormatException as DataConfigInvalidFormatException;
use liberty_code\http\requisition\response\data\model\DataHttpResponse;
use liberty_code\http\requisition\response\persistence\exception\ConfigInvalidFormatException as PersistorConfigInvalidFormatException;
use liberty_code\http\requisition\response\persistence\model\PersistorHttpResponse;
use liberty_code\http\requisition\response\factory\model\HttpResponseFactory;



/**
 * @cover HttpResponseFactory
 */
class HttpResponseFactoryTest extends TestCase
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods test
    // ******************************************************************************

    /**
     * Test can create HTTP response object.
     *
     * @param null|string $strConfigKey
     * @param array $tabConfig
     * @param null|string|array $expectResult
     * @dataProvider providerCreateHttpResponse
     * @dataProvider providerCreateDataHttpResponse
     * @dataProvider providerCreatePersistorHttpResponse
     */
    public function testCanCreateHttpResponse(
        $strConfigKey,
        array $tabConfig,
        $expectResult
    )
    {
        // Load
        $strRootAppPath = dirname(__FILE__) . '/../..';
        require($strRootAppPath . '/requisition/response/factory/boot/HttpResponseFactoryBootstrap.php');

        // Init var
        $boolExceptionExpected = is_string($expectResult);

        // Expect exception, if required
        if($boolExceptionExpected)
        {
            $this->expectException($expectResult);
        }

        // Get response
        /** @var HttpResponseFactory $objHttpResponseFactory */
        $objResponse = $objHttpResponseFactory->getObjResponse($tabConfig, $strConfigKey);
        $strClassPath = $objHttpResponseFactory->getStrResponseClassPath($tabConfig, $strConfigKey);

        // Get info
        $boolCreate = (!is_null($objResponse));
        $boolExpectCreate = (!is_null($expectResult));

        // Set assertions (check response creation), if required
        if(!$boolExceptionExpected)
        {
            // Set assertion (check response creation)
            if(!$boolExpectCreate)
            {
                $this->assertEquals(null, $objResponse);
            }
            $this->assertEquals($boolExpectCreate, $boolCreate);

            // Set assertions (check response detail), if required
            if($boolCreate && $boolExpectCreate)
            {
                // Get info
                $strExpectClassPath = $expectResult[0];
                $tabExpectConfig = $expectResult[1];
                $tabExpectRcpInfo = $expectResult[2];

                // Set assertions (check response detail)
                $this->assertEquals($strExpectClassPath, $strClassPath);
                $this->assertEquals($strExpectClassPath, get_class($objResponse));
                $this->assertEquals($tabExpectConfig, $objResponse->getTabConfig());
                $this->assertEquals($tabExpectRcpInfo, $objResponse->getTabRcpInfo());

                // Set assertions (check response dependencies)
                /** @var HttpResponse $objResponse */
                $this->assertEquals(false, is_null($objResponse->getObjHeaderData()));

                // Print
                /*
                echo('Get Class path: '. PHP_EOL);var_dump($strClassPath);echo(PHP_EOL);
                echo('Get config: ' . PHP_EOL);var_dump($objResponse->getTabConfig());echo(PHP_EOL);
                echo('Get reception info: ' . PHP_EOL);var_dump($objResponse->getTabRcpInfo());echo(PHP_EOL);
                //*/
            }
        }
    }



    /**
     * Data provider,
     * to test can create HTTP response object.
     *
     * @return array
     */
    public function providerCreateHttpResponse()
    {
        // Init var
        $strBody = json_encode(array(
            'key-1' => 'Value 1',
            'key-2' => [
                'key-2-1' => 'Value 2-1',
                'key-2-2' => 2.2
            ],
            'key-3' => true
        ));

        // Return result
        return array(
            'Create HTTP response: fail to create response get (type not found)' => [
                null,
                [
                    'key-config-1' => 'Value config 1',
                    'key-config-2' => 2,
                    'rcp_info' => [
                        'status_code' => 200,
                        'status_msg' => 'OK',
                        'header' => [
                            'Content-type' => 'application/json',
                            'Content-Length' => mb_strlen($strBody),
                            'Cache-Control' => 'no-cache'
                        ],
                        'body' => $strBody
                    ]
                ],
                null
            ],
            'Create HTTP response: fail to create response get (type unknown)' => [
                null,
                [
                    'type' => 'test',
                    'key-config-1' => 'Value config 1',
                    'key-config-2' => 2,
                    'rcp_info' => [
                        'status_code' => 200,
                        'status_msg' => 'OK',
                        'header' => [
                            'Content-type' => 'application/json',
                            'Content-Length' => mb_strlen($strBody),
                            'Cache-Control' => 'no-cache'
                        ],
                        'body' => $strBody
                    ]
                ],
                null
            ],
            'Create HTTP response: success to create response get' => [
                null,
                [
                    'type' => 'http',
                    'key-config-1' => 'Value config 1',
                    'key-config-2' => 2,
                    'rcp_info' => [
                        'status_code' => 200,
                        'status_msg' => 'OK',
                        'header' => [
                            'Content-type' => 'application/json',
                            'Content-Length' => mb_strlen($strBody),
                            'Cache-Control' => 'no-cache'
                        ],
                        'body' => $strBody
                    ]
                ],
                [
                    HttpResponse::class,
                    [
                        'key-config-1' => 'Value config 1',
                        'key-config-2' => 2
                    ],
                    [
                        'status_code' => 200,
                        'status_msg' => 'OK',
                        'header' => [
                            'Content-type' => 'application/json',
                            'Content-Length' => mb_strlen($strBody),
                            'Cache-Control' => 'no-cache'
                        ],
                        'body' => $strBody
                    ]
                ]
            ],
            'Create HTTP response: fail to create response post (invalid status code format)' => [
                null,
                [
                    'type' => 'http',
                    'rcp_info' => [
                        'status_code' => 'test',
                        'status_msg' => 'Created'
                    ]
                ],
                RcpInfoInvalidFormatException::class
            ],
            'Create HTTP response: success to create response post' => [
                null,
                [
                    'type' => 'http',
                    'rcp_info' => [
                        'status_code' => 201,
                        'status_msg' => 'Created'
                    ]
                ],
                [
                    HttpResponse::class,
                    [],
                    [
                        'status_code' => 201,
                        'status_msg' => 'Created',
                        // Due to set header data, during response construction, information headers already initialized
                        'header' => []
                    ]
                ]
            ]
        );
    }



    /**
     * Data provider,
     * to test can create data HTTP response object.
     *
     * @return array
     */
    public function providerCreateDataHttpResponse()
    {
        // Init var
        $strBody = json_encode(array(
            'key-1' => 'Value 1',
            'key-2' => [
                'key-2-1' => 'Value 2-1',
                'key-2-2' => 2.2
            ],
            'key-3' => true
        ));

        // Return result
        return array(
            'Create data HTTP response: fail to create response get (invalid body format)' => [
                null,
                [
                    'type' => 'http_data',
                    'body_parser_config' => [
                        'type' => 'string_table_json'
                    ],
                    'rcp_info' => [
                        'status_code' => 200,
                        'status_msg' => 'OK',
                        'header' => [
                            'Content-type' => 'application/json',
                            'Content-Length' => 0
                        ],
                        'body' => ''
                    ]
                ],
                DataConfigInvalidFormatException::class
            ],
            'Create data HTTP response: success to create response get (without body)' => [
                null,
                [
                    'type' => 'http_data',
                    'body_parser_config' => [
                        'type' => 'string_table_json'
                    ],
                    'rcp_info' => [
                        'status_code' => 200,
                        'status_msg' => 'OK',
                        'header' => [
                            'Content-type' => 'application/json',
                            'Content-Length' => 0
                        ]
                    ]
                ],
                [
                    DataHttpResponse::class,
                    [
                        'body_parser_config' => [
                            'type' => 'string_table_json'
                        ]
                    ],
                    [
                        'status_code' => 200,
                        'status_msg' => 'OK',
                        'header' => [
                            'Content-type' => 'application/json',
                            'Content-Length' => 0
                        ]
                    ]
                ]
            ],
            'Create data HTTP response: success to create response get' => [
                null,
                [
                    'type' => 'http_data',
                    'body_parser_config' => [
                        'type' => 'string_table_json'
                    ],
                    'rcp_info' => [
                        'status_code' => 200,
                        'status_msg' => 'OK',
                        'header' => [
                            'Content-type' => 'application/json',
                            'Content-Length' => mb_strlen($strBody)
                        ],
                        'body' => $strBody
                    ]
                ],
                [
                    DataHttpResponse::class,
                    [
                        'body_parser_config' => [
                            'type' => 'string_table_json'
                        ]
                    ],
                    [
                        'status_code' => 200,
                        'status_msg' => 'OK',
                        'header' => [
                            'Content-type' => 'application/json',
                            'Content-Length' => mb_strlen($strBody)
                        ],
                        'body' => $strBody
                    ]
                ]
            ]
        );
    }



    /**
     * Data provider,
     * to test can create persistor HTTP response object.
     *
     * @return array
     */
    public function providerCreatePersistorHttpResponse()
    {
        // Init get data function
        $getData = function($id, $nm, $fnm, $age)
        {
            $result = array(
                'id' => $id,
                'name' => $nm,
                'first-name' => $fnm,
                'age' => $age
            );
            return $result;
        };

        // Init var
        $tabData = array(
            $getData(1, 'NM-1', 'Fnm-1', 20),
            $getData(2, 'NM-2', 'Fnm-2', 30),
            $getData(3, 'NM-3', 'Fnm-3', 40)
        );

        // Return result
        return array(
            'Create persistor HTTP response: fail to create response get (invalid configuration success status code)' => [
                null,
                [
                    'type' => 'http_persistor',
                    'body_parser_config' => [
                        'type' => 'string_table_json'
                    ],
                    'success_status_code' => false,
                    'multi_data_select_path' => 'result/data',
                    'search_data_select_path' => 'result/data',
                    'rcp_info' => [
                        'status_code' => 200,
                        'status_msg' => 'OK',
                        'header' => [
                            'Content-type' => 'application/json',
                            'Content-Length' => 0
                        ],
                        'body' => json_encode([
                            'result' => [
                                'data' => $tabData
                            ]
                        ])
                    ]
                ],
                PersistorConfigInvalidFormatException::class
            ],
            'Create persistor HTTP response: success to create response get' => [
                null,
                [
                    'type' => 'http_persistor',
                    'body_parser_config' => [
                        'type' => 'string_table_json'
                    ],
                    'success_status_code' => [200, 201],
                    'multi_data_select_path' => 'result/data',
                    'search_data_select_path' => 'result/data',
                    'rcp_info' => [
                        'status_code' => 200,
                        'status_msg' => 'OK',
                        'header' => [
                            'Content-type' => 'application/json',
                            'Content-Length' => 0
                        ],
                        'body' => json_encode([
                            'result' => [
                                'data' => $tabData
                            ]
                        ])
                    ]
                ],
                [
                    PersistorHttpResponse::class,
                    [
                        'body_parser_config' => [
                            'type' => 'string_table_json'
                        ],
                        'success_status_code' => [200, 201],
                        'multi_data_select_path' => 'result/data',
                        'search_data_select_path' => 'result/data'
                    ],
                    [
                        'status_code' => 200,
                        'status_msg' => 'OK',
                        'header' => [
                            'Content-type' => 'application/json',
                            'Content-Length' => 0
                        ],
                        'body' => json_encode([
                            'result' => [
                                'data' => $tabData
                            ]
                        ])
                    ]
                ]
            ]
        );
    }



}