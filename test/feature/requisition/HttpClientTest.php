<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\test\feature\requisition;

use PHPUnit\Framework\TestCase;

use liberty_code\cache\repository\model\DefaultRepository;
use liberty_code\requisition\request\model\DefaultRequest;
use liberty_code\http\header\library\ToolBoxHeader;
use liberty_code\http\response\library\ToolBoxResponse;
use liberty_code\http\curl\library\ToolBoxCurl;
use liberty_code\http\requisition\response\model\HttpResponse;
use liberty_code\http\requisition\client\exception\ExecConfigInvalidFormatException;
use liberty_code\http\requisition\client\exception\SndInfoInvalidFormatException;
use liberty_code\http\requisition\client\exception\CurlExecutionFailException;
use liberty_code\http\requisition\client\model\HttpClient;



/**
 * @cover ToolBoxHeader
 * @cover ToolBoxResponse
 * @cover ToolBoxCurl
 * @cover HttpClient
 */
class HttpClientTest extends TestCase
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /** @var DefaultRepository */
    public static $objCacheRepo;



    /** @var HttpClient */
    public static $objHttpClient;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods set up
    // ******************************************************************************

    public static function setUpBeforeClass(): void
    {
        // Call parent method
        parent::setUpBeforeClass();

        // Load
        $strRootAppPath = dirname(__FILE__) . '/../..';
        require($strRootAppPath . '/requisition/client/boot/HttpClientBootstrap.php');

        // Init properties
        /** @var DefaultRepository $objCacheRepo */
        static::$objCacheRepo = $objCacheRepo;
        /** @var HttpClient $objHttpClient */
        static::$objHttpClient = $objHttpClient;
    }



    public static function tearDownAfterClass(): void
    {
        // Clear cache items
        static::$objCacheRepo->removeItemAll();
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get protocol version number,
     * used on tests.
     *
     * @return string
     */
    protected function getStrProtocolVersionNum()
    {
        // Return result
        return '1.1';
    }





    // Methods test
    // ******************************************************************************

    /**
     * Test can execute request objects.
     *
     * @param array $tabRequestConfig
     * @param null|array $tabExecConfig
     * @param string|array $expectResult
     * @dataProvider providerExecuteRequest
     */
    public function testCanExecuteRequest(
        array $tabRequestConfig,
        $tabExecConfig,
        $expectResult
    )
    {
        // Init var
        $boolExceptionExpected = is_string($expectResult);

        // Expect exception, if required
        if($boolExceptionExpected)
        {
            $this->expectException($expectResult);
        }

        // Init requests
        /** @var DefaultRequest[] $tabRequest */
        $tabRequest = array_map(
            function(array $tabRequestConfig) {
                $tabConfig = $tabRequestConfig[0];
                $tabSndInfo = $tabRequestConfig[1];
                return new DefaultRequest($tabConfig, $tabSndInfo);
            },
            $tabRequestConfig
        );

        // Execute requests
        /** @var HttpResponse[] $tabResponse */
        $tabResponse = static::$objHttpClient->executeTabRequest($tabRequest, $tabExecConfig);

        // Set assertions, if required
        if(!$boolExceptionExpected)
        {
            // Run each response
            $tabKey = array_keys($tabResponse);
            $tabExpectKey = array_keys($expectResult);
            for($intCpt = 0; $intCpt < count($tabKey);$intCpt++)
            {
                // Get info
                $strKey = $tabKey[$intCpt];
                $strExpectKey = $tabExpectKey[$intCpt];
                $objResponse = $tabResponse[$strKey];
                $tabResponseRcpInfo = $objResponse->getTabRcpInfo();
                $tabExpectResponseConfig = $expectResult[$strExpectKey];
                $tabExpectResponseRcpInfo = $tabExpectResponseConfig[1];
                $tabExpectResponseConfig = $tabExpectResponseConfig[0];

                // Set assertions (check response detail)
                $this->assertEquals($strKey, $strExpectKey);
                $this->assertEquals(HttpResponse::class, get_class($objResponse));
                $this->assertEquals($tabExpectResponseConfig, $objResponse->getTabConfig());

                // Set assertions (check response reception information)
                $this->assertEquals(array_keys($tabExpectResponseRcpInfo), array_keys($tabResponseRcpInfo));
                $this->assertEquals($tabExpectResponseRcpInfo['protocol_version_num'], $tabResponseRcpInfo['protocol_version_num']);
                $this->assertEquals($tabExpectResponseRcpInfo['status_code'], $tabResponseRcpInfo['status_code']);
                $this->assertEquals($tabExpectResponseRcpInfo['status_msg'], $tabResponseRcpInfo['status_msg']);
                $this->assertEquals(true, is_array($tabResponseRcpInfo['header']));
                $this->assertEquals(
                    array_merge($tabResponseRcpInfo['header'], $tabExpectResponseRcpInfo['header']),
                    $tabResponseRcpInfo['header']
                );
                $this->assertEquals($tabExpectResponseRcpInfo['body'], $tabResponseRcpInfo['body']);

                // Print
                /*
                echo('Get key: ' . PHP_EOL);var_dump($strKey);echo(PHP_EOL);
                echo('Get class: ' . PHP_EOL);var_dump(get_class($objResponse));echo(PHP_EOL);
                echo('Get config: ' . PHP_EOL);var_dump($objResponse->getTabConfig());echo(PHP_EOL);
                echo('Get reception info: ' . PHP_EOL);var_dump($tabResponseRcpInfo);echo(PHP_EOL);
                //*/
            }
        }
    }



    /**
     * Data provider,
     * to test can execute request objects.
     *
     * @return array
     */
    public function providerExecuteRequest()
    {
        // Init var
        $strBody = json_encode(array(
            'key-1' => 'Value 1',
            'key-2' => [
                'key-2-1' => 'Value 2-1',
                'key-2-2' => 2.2
            ],
            'key-3' => true
        ));
        $strProtocolVersionNum = $this->getStrProtocolVersionNum();

        // Return result
        return array(
            'Execute request: fail to execute request_get (method not found)' => [
                [
                    [
                        [
                            'key' => 'request_get'
                        ],
                        [
                            'url_schema' => 'http',
                            'url_host' => 'localhost',
                            'url_route' => 'LibertyCode/LibertyCode/libertycode_http/test/requisition/client/web/index.php',
                            'url_argument' => [
                                'arg-1' => 'Value 1',
                                'arg-2' => 2,
                                'arg-3' => false
                            ]
                        ]
                    ]
                ],
                null,
                SndInfoInvalidFormatException::class
            ],
            'Execute request: fail to execute request_get (invalid route format)' => [
                [
                    [
                        [
                            'key' => 'request_get'
                        ],
                        [
                            'method' => 'gEt',
                            'url_schema' => 'http',
                            'url_host' => 'localhost',
                            'url_route' => 3,
                            'url_argument' => [
                                'arg-1' => 'Value 1',
                                'arg-2' => 2,
                                'arg-3' => false
                            ]
                        ]
                    ]
                ],
                null,
                SndInfoInvalidFormatException::class
            ],
            'Execute request: fail to execute request_get (execution route argument fail)' => [
                [
                    [
                        [
                            'key' => 'request_get'
                        ],
                        [
                            'method' => 'gEt',
                            'url_schema' => 'http',
                            'url_host' => 'localhost',
                            'url_route' => 'LibertyCode/LibertyCode/{repo}/test/{dir}/client/web/index.php',
                            'url_route_argument_spec' => [
                                '{repo}' => 'libertycode_repo',
                                '{dir}' => null
                            ],
                            'url_route_argument' => [
                                '{repo}' => 'libertycode_http'
                            ],
                            'url_argument' => [
                                'arg-1' => 'Value 1',
                                'arg-2' => 2,
                                'arg-3' => false
                            ]
                        ]
                    ]
                ],
                null,
                SndInfoInvalidFormatException::class
            ],
            'Execute request: fail to execute request_get (execution host fail)' => [
                [
                    [
                        [
                            'key' => 'request_get'
                        ],
                        [
                            'method' => 'gEt',
                            'url_schema' => 'http',
                            'url_host' => 'test',
                            'url_route' => 'LibertyCode/LibertyCode/libertycode_http/test/requisition/client/web/index.php',
                            'url_argument' => [
                                'arg-1' => 'Value 1',
                                'arg-2' => 2,
                                'arg-3' => false
                            ]
                        ]
                    ]
                ],
                [
                    'curl_add_option' => [
                        CURLOPT_TIMEOUT_MS => 200
                    ]
                ],
                CurlExecutionFailException::class
            ],
            'Execute request: fail to execute request_get (execution port fail)' => [
                [
                    [
                        [
                            'key' => 'request_get'
                        ],
                        [
                            'method' => 'gEt',
                            'url_schema' => 'http',
                            'url_host' => 'localhost',
                            'url_port' => 500,
                            'url_route' => 'LibertyCode/LibertyCode/libertycode_http/test/requisition/client/web/index.php',
                            'url_argument' => [
                                'arg-1' => 'Value 1',
                                'arg-2' => 2,
                                'arg-3' => false
                            ]
                        ]
                    ]
                ],
                [
                    'curl_add_option' => [
                        CURLOPT_TIMEOUT_MS => 200
                    ]
                ],
                CurlExecutionFailException::class
            ],
            'Execute request: success to execute request_get' => [
                [
                    [
                        [
                            'key' => 'request_get',
                        ],
                        [
                            'protocol_version_num' => $strProtocolVersionNum,
                            'method' => 'gEt',
                            'url_schema' => 'http',
                            'url_host' => 'localhost',
                            'url_route' => 'LibertyCode/LibertyCode/{repo}/test/{dir}/client/web/index.php',
                            'url_route_argument_spec' => [
                                '{repo}' => 'test',
                                '{dir}' => null
                            ],
                            'url_route_argument' => [
                                '{repo}' => 'libertycode_http',
                                '{dir}' => 'requisition'
                            ],
                            'url_argument' => [
                                'arg-1' => 'Value 1',
                                'arg-2' => 2,
                                'arg-3' => false
                            ]
                        ]
                    ]
                ],
                null,
                [
                    'request_get' => [
                        [],
                        [
                            'protocol_version_num' => $strProtocolVersionNum,
                            'status_code' => 200,
                            'status_msg' => 'OK',
                            'header' => [
                                'Content-Type' => 'application/json',
                                'After-Execution' => '1'
                            ],
                            'body' => json_encode(array(
                                'method' => 'GET',
                                'url_arg' => [
                                    'arg-1' => 'Value 1',
                                    'arg-2' => '2',
                                    'arg-3' => '0'
                                ],
                                'header' => [
                                    'Host' => 'localhost',
                                    'Accept' => '*/*'
                                ],
                                'body' => ''
                            ))
                        ]
                    ]
                ]
            ],
            'Execute request: success to execute request_get, request_post and request_delete' => [
                [
                    [
                        [
                            'key' => 'request_get'
                        ],
                        [
                            'method' => 'gEt',
                            'url_schema' => 'http',
                            'url_host' => 'localhost',
                            'url_route' => 'LibertyCode/LibertyCode/libertycode_http/test/requisition/client/web/index.php',
                            'url_route_argument' => [
                                'libertycode_http' => 'test',
                                'requisition' => 'test'
                            ],
                            'url_argument' => [
                                'arg-1' => 'Value 1 Bis',
                                'arg-2' => 2,
                                'arg-3' => true
                            ]
                        ]
                    ],
                    [
                        [
                            'key' => 'request_post'
                        ],
                        [
                            'method' => 'post',
                            'url_schema' => 'http',
                            'url_host' => 'localhost',
                            'url_route' => 'LibertyCode/LibertyCode/libertycode_http/test/requisition/client/web/index.php',
                            'header' => [
                                'Content-Type' => 'application/json',
                                'Accept' => 'application/json',
                                'Cache-Control' => 'no-cache'
                            ],
                            'body' => $strBody
                        ]
                    ],
                    [
                        [
                            'key' => 'request_delete'
                        ],
                        [
                            'method' => 'delete',
                            'url_schema' => 'http',
                            'url_host' => 'localhost',
                            'url_route' => 'LibertyCode/LibertyCode/libertycode_http/test/requisition/client/web/index.php',
                            'url_argument' => [
                                'arg-1' => 'Value 1',
                                'arg-2' => 2,
                                'arg-3' => false
                            ],
                            'header' => [
                                'Accept' => 'application/json',
                                'Cache-Control' => 'no-cache'
                            ]
                        ]
                    ]
                ],
                [
                    'response_cache_require' => false,
                    'cache_require' => false
                ],
                [
                    'request_get' => [
                        [],
                        [
                            'protocol_version_num' => $strProtocolVersionNum,
                            'status_code' => 200,
                            'status_msg' => 'OK',
                            'header' => [
                                'Content-Type' => 'application/json',
                                'After-Execution' => '1'
                            ],
                            'body' => json_encode(array(
                                'method' => 'GET',
                                'url_arg' => [
                                    'arg-1' => 'Value 1 Bis',
                                    'arg-2' => '2',
                                    'arg-3' => '1'
                                ],
                                'header' => [
                                    'Host' => 'localhost',
                                    'Accept' => '*/*'
                                ],
                                'body' => ''
                            ))
                        ]
                    ],
                    'request_post' => [
                        [],
                        [
                            'protocol_version_num' => $strProtocolVersionNum,
                            'status_code' => 200,
                            'status_msg' => 'OK',
                            'header' => [
                                'Content-Type' => 'application/json',
                                'After-Execution' => '1'
                            ],
                            'body' => json_encode(array(
                                'method' => 'POST',
                                'url_arg' => [],
                                'header' => [
                                    'Host' => 'localhost',
                                    'Content-Type' => 'application/json',
                                    'Accept' => 'application/json',
                                    'Cache-Control' => 'no-cache',
                                    'Content-Length' => strval(strlen($strBody))
                                ],
                                'body' => $strBody
                            ))
                        ]
                    ],
                    'request_delete' => [
                        [],
                        [
                            'protocol_version_num' => $strProtocolVersionNum,
                            'status_code' => 200,
                            'status_msg' => 'OK',
                            'header' => [
                                'Content-Type' => 'application/json',
                                'After-Execution' => '1'
                            ],
                            'body' => json_encode(array(
                                'method' => 'DELETE',
                                'url_arg' => [
                                    'arg-1' => 'Value 1',
                                    'arg-2' => '2',
                                    'arg-3' => '0'
                                ],
                                'header' => [
                                    'Host' => 'localhost',
                                    'Accept' => 'application/json',
                                    'Cache-Control' => 'no-cache'
                                ],
                                'body' => ''
                            ))
                        ]
                    ]
                ]
            ],
            'Execute request: success to execute request_redirect_get and request_redirect_post (without redirect)' => [
                [
                    [
                        [
                            'key' => 'request_redirect_get',
                        ],
                        [
                            'method' => 'get',
                            'url_schema' => 'http',
                            'url_host' => 'localhost',
                            'url_route' => 'LibertyCode/LibertyCode/libertycode_http/test/requisition/client/web/index.php',
                            'url_argument' => [
                                'arg-1' => 'Value 1',
                                'arg-2' => 2,
                                'arg-3' => false
                            ],
                            'header' => [
                                'Redirect' => 1
                            ]
                        ]
                    ],
                    [
                        [
                            'key' => 'request_redirect_post'
                        ],
                        [
                            'method' => 'post',
                            'url_schema' => 'http',
                            'url_host' => 'localhost',
                            'url_route' => 'LibertyCode/LibertyCode/libertycode_http/test/requisition/client/web/index.php',
                            'header' => [
                                'Redirect' => 1,
                                'Content-Type' => 'application/json',
                                'Accept' => 'application/json',
                                'Cache-Control' => 'no-cache'
                            ],
                            'body' => $strBody
                        ]
                    ],
                ],
                null,
                [
                    'request_redirect_get' => [
                        [],
                        [
                            'protocol_version_num' => $strProtocolVersionNum,
                            'status_code' => 301,
                            'status_msg' => 'Moved Permanently',
                            'header' => [
                                'After-Execution' => '1'
                            ],
                            'body' => ''
                        ]
                    ],
                    'request_redirect_post' => [
                        [],
                        [
                            'protocol_version_num' => $strProtocolVersionNum,
                            'status_code' => 301,
                            'status_msg' => 'Moved Permanently',
                            'header' => [
                                'After-Execution' => '1'
                            ],
                            'body' => ''
                        ]
                    ]
                ]
            ],
            'Execute request: fail to execute request_redirect_get (invalid configuration redirect_count_max format)' => [
                [
                    [
                        [
                            'key' => 'request_redirect_get',
                        ],
                        [
                            'method' => 'get',
                            'url_schema' => 'http',
                            'url_host' => 'localhost',
                            'url_route' => 'LibertyCode/LibertyCode/libertycode_http/test/requisition/client/web/index.php',
                            'url_argument' => [
                                'arg-1' => 'Value 1',
                                'arg-2' => 2,
                                'arg-3' => false
                            ],
                            'header' => [
                                'Redirect' => 1
                            ]
                        ]
                    ]
                ],
                [
                    'response_cache_require' => false,
                    'cache_require' => false,
                    'redirect_count_max' => -3
                ],
                ExecConfigInvalidFormatException::class
            ],
            'Execute request: fail to execute request_redirect_get (invalid configuration redirect_before_execution_callable format)' => [
                [
                    [
                        [
                            'key' => 'request_redirect_get',
                        ],
                        [
                            'method' => 'get',
                            'url_schema' => 'http',
                            'url_host' => 'localhost',
                            'url_route' => 'LibertyCode/LibertyCode/libertycode_http/test/requisition/client/web/index.php',
                            'url_argument' => [
                                'arg-1' => 'Value 1',
                                'arg-2' => 2,
                                'arg-3' => false
                            ],
                            'header' => [
                                'Redirect' => 1
                            ]
                        ]
                    ]
                ],
                [
                    'response_cache_require' => false,
                    'cache_require' => false,
                    'redirect_count_max' => -1,
                    'redirect_before_execution_callable' => 'test'
                ],
                ExecConfigInvalidFormatException::class
            ],
            'Execute request: success to execute request_redirect_get and request_redirect_post' => [
                [
                    [
                        [
                            'key' => 'request_redirect_get',
                        ],
                        [
                            'method' => 'get',
                            'url_schema' => 'http',
                            'url_host' => 'localhost',
                            'url_route' => 'LibertyCode/LibertyCode/libertycode_http/test/requisition/client/web/index.php',
                            'url_argument' => [
                                'arg-1' => 'Value 1',
                                'arg-2' => 2,
                                'arg-3' => false
                            ],
                            'header' => [
                                'Redirect' => 1
                            ]
                        ]
                    ],
                    [
                        [
                            'key' => 'request_redirect_post'
                        ],
                        [
                            'method' => 'post',
                            'url_schema' => 'http',
                            'url_host' => 'localhost',
                            'url_route' => 'LibertyCode/LibertyCode/libertycode_http/test/requisition/client/web/index.php',
                            'header' => [
                                'Redirect' => 1,
                                'Content-Type' => 'application/json',
                                'Accept' => 'application/json',
                                'Cache-Control' => 'no-cache'
                            ],
                            'body' => $strBody
                        ]
                    ],
                ],
                [
                    'response_cache_require' => false,
                    'cache_require' => false,
                    'redirect_count_max' => -1,
                    'redirect_before_execution_callable' => function(
                        &$strProtocolVersionNum,
                        &$strMethod,
                        &$strUrl,
                        array &$tabHeader,
                        &$strBody,
                        &$tabCurlAddOption,
                        $intRedirectCount
                    ) {
                        unset($tabHeader['Redirect']);
                        $tabHeader['Redirect-Total'] = $intRedirectCount;
                    }
                ],
                [
                    'request_redirect_get' => [
                        [],
                        [
                            'protocol_version_num' => $strProtocolVersionNum,
                            'status_code' => 200,
                            'status_msg' => 'OK',
                            'header' => [
                                'Content-Type' => 'application/json',
                                'After-Execution' => '1'
                            ],
                            'body' => json_encode(array(
                                'method' => 'GET',
                                'url_arg' => [],
                                'header' => [
                                    'Host' => 'localhost',
                                    'Accept' => '*/*',
                                    'Redirect-Total' => '1'
                                ],
                                'body' => ''
                            ))
                        ]
                    ],
                    'request_redirect_post' => [
                        [],
                        [
                            'protocol_version_num' => $strProtocolVersionNum,
                            'status_code' => 200,
                            'status_msg' => 'OK',
                            'header' => [
                                'Content-Type' => 'application/json',
                                'After-Execution' => '1'
                            ],
                            'body' => json_encode(array(
                                'method' => 'POST',
                                'url_arg' => [],
                                'header' => [
                                    'Host' => 'localhost',
                                    'Content-Type' => 'application/json',
                                    'Accept' => 'application/json',
                                    'Cache-Control' => 'no-cache',
                                    'Redirect-Total' => '1',
                                    'Content-Length' => strval(strlen($strBody))
                                ],
                                'body' => $strBody
                            ))
                        ]
                    ]
                ]
            ]
        );
    }



}