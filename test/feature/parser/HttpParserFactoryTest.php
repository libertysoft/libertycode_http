<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\test\feature\parser;

use PHPUnit\Framework\TestCase;

use liberty_code\http\parser\string_table\url_arg\model\UrlArgParser;
use liberty_code\http\parser\string_table\multipart_data\exception\ConfigInvalidFormatException;
use liberty_code\http\parser\string_table\multipart_data\model\MultipartDataParser;
use liberty_code\http\parser\factory\string_table\model\HttpStrTableParserFactory;



/**
 * @cover HttpStrTableParserFactory
 */
class HttpParserFactoryTest extends TestCase
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods test
    // ******************************************************************************

    /**
     * Test can create HTTP string table parser object.
     *
     * @param null|string $strConfigKey
     * @param array $tabConfig
     * @param null|string|array $expectResult
     * @dataProvider providerCreateUrlArgParser
     * @dataProvider providerCreateMultipartDataParser
     */
    public function testCanCreateHttpStrTableParser(
        $strConfigKey,
        array $tabConfig,
        $expectResult
    )
    {
        // Load
        $strRootAppPath = dirname(__FILE__) . '/../..';
        require($strRootAppPath . '/parser/factory/boot/HttpParserFactoryBootstrap.php');

        // Init var
        $boolExceptionExpected = is_string($expectResult);

        // Expect exception, if required
        if($boolExceptionExpected)
        {
            $this->expectException($expectResult);
        }

        // Get parser
        /** @var HttpStrTableParserFactory $objParserFactory */
        $objParser = $objParserFactory->getObjParser($tabConfig, $strConfigKey);
        $strClassPath = $objParserFactory->getStrParserClassPath($tabConfig, $strConfigKey);

        // Get info
        $boolCreate = (!is_null($objParser));
        $boolExpectCreate = (!is_null($expectResult));

        // Set assertions (check parser creation), if required
        if(!$boolExceptionExpected)
        {
            // Set assertion (check parser creation)
            if(!$boolExpectCreate)
            {
                $this->assertEquals(null, $objParser);
            }
            $this->assertEquals($boolExpectCreate, $boolCreate);

            // Set assertions (check parser detail), if required
            if($boolCreate && $boolExpectCreate)
            {
                // Get info
                $tabConfig = $objParser->getTabConfig();
                $strExpectClassPath = $expectResult[0];
                $tabExpectConfig = $expectResult[1];

                // Set assertions (check parser detail)
                $this->assertEquals($strExpectClassPath, $strClassPath);
                $this->assertEquals($strExpectClassPath, get_class($objParser));
                $this->assertEquals($tabExpectConfig, $tabConfig);

                // Set assertions (check parser dependencies)
                if($objParser instanceof MultipartDataParser)
                {
                    $this->assertEquals(false, is_null($objParser->getObjFileFactory()));
                }

                // Print
                /*
                echo('Get Class path: '. PHP_EOL);var_dump($strClassPath);echo(PHP_EOL);
                echo('Get config: ' . PHP_EOL);var_dump($tabConfig);echo(PHP_EOL);
                //*/
            }
        }
    }



    /**
     * Data provider,
     * to test can create URL argument string table parser object.
     *
     * @return array
     */
    public function providerCreateUrlArgParser()
    {
        // Return result
        return array(
            'Create URL argument string table parser: fail to create parser (type not found)' => [
                null,
                [],
                null
            ],
            'Create URL argument string table parser: fail to create parser (type unknown)' => [
                null,
                [
                    'type' => 'test'
                ],
                null
            ],
            'Create URL argument string table parser: success to create parser' => [
                null,
                [
                    'type' => 'http_string_table_url_arg'
                ],
                [
                    UrlArgParser::class,
                    []
                ]
            ]
        );
    }



    /**
     * Data provider,
     * to test can create multipart data string table parser object.
     *
     * @return array
     */
    public function providerCreateMultipartDataParser()
    {
        // Return result
        return array(
            'Create multipart data string table parser: success to create parser' => [
                null,
                [
                    'type' => 'http_string_table_multipart_data'
                ],
                [
                    MultipartDataParser::class,
                    []
                ]
            ],
            'Create multipart data string table parser: fail to create parser (invalid configuration boundary format)' => [
                null,
                [
                    'type' => 'http_string_table_multipart_data',
                    'boundary' => 7,
                    'default_file_name' => 'file_test'
                ],
                ConfigInvalidFormatException::class
            ],
            'Create multipart data string table parser: success to create parser (with configuration)' => [
                null,
                [
                    'type' => 'http_string_table_multipart_data',
                    'boundary' => '123456789',
                    'default_file_name' => 'file_test'
                ],
                [
                    MultipartDataParser::class,
                    [
                        'boundary' => '123456789',
                        'default_file_name' => 'file_test'
                    ]
                ]
            ]
        );
    }



}