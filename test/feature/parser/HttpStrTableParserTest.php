<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\test\feature\parser;

use PHPUnit\Framework\TestCase;

use liberty_code\file\file\name\api\NameFileInterface;
use liberty_code\file\file\factory\api\FileFactoryInterface;
use liberty_code\http\multipart\library\ToolBoxMultipart;
use liberty_code\http\multipart\data\library\ToolBoxMultipartData;
use liberty_code\http\parser\string_table\url_arg\model\UrlArgParser;
use liberty_code\http\parser\string_table\multipart_data\library\ToolBoxMultipartDataParser;
use liberty_code\http\parser\string_table\multipart_data\exception\ConfigInvalidFormatException;
use liberty_code\http\parser\string_table\multipart_data\model\MultipartDataParser;

/**
 * @cover ToolBoxMultipart
 * @cover ToolBoxMultipartData
 * @cover UrlArgParser
 * @cover ToolBoxMultipartDataParser
 * @cover MultipartDataParser
 */
class HttpStrTableParserTest extends TestCase
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************



    /** @var FileFactoryInterface */
    public static $objFileFactory;





	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods set up
    // ******************************************************************************

    public static function setUpBeforeClass(): void
    {
        // Call parent method
        parent::setUpBeforeClass();

        // Load
        $strRootAppPath = dirname(__FILE__) . '/../../..';
        require($strRootAppPath . '/src/file/test/FileTest.php');

        // Init properties
        /** @var FileFactoryInterface $objFileFactory */
        static::$objFileFactory = $objFileFactory;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get boundary,
     * used on tests.
     *
     * @return string
     */
    protected function getStrBoundary()
    {
        // Return result
        return '123456789';
    }





    // Methods test
    // ******************************************************************************

    /**
     * Test can use URL argument string table parser object.
     *
     * @param null|array $tabConfig
     * @param string|array $data
     * @param string|array $expectResult
     * @dataProvider providerUseUrlArgParser
     */
    public function testCanUseUrlArgParser(
        $tabConfig,
        $data,
        $expectResult
    )
    {
        // Init var
        $boolExceptionExpected = is_string($expectResult);

        // Expect exception, if required
        if($boolExceptionExpected)
        {
            $this->expectException($expectResult);
        }

        // Get parser
        $objParser = new UrlArgParser($tabConfig);

        // Get parsed result
        $result = (
            (is_string($data)) ?
                $objParser->getData($data) :
                $objParser->getSource($data)
        );

        // Set assertions (check parsed result), if required
        if(!$boolExceptionExpected)
        {
            // Set assertions
            $this->assertEquals($expectResult[0], $result);

            // Print
            /*
            echo('Get config: ' . PHP_EOL);var_dump($tabConfig);echo(PHP_EOL);
            echo('Get data: ' . PHP_EOL);var_dump($data);echo(PHP_EOL);
            echo('Get result: ' . PHP_EOL);var_dump($result);echo(PHP_EOL);
            //*/
        }
    }



    /**
     * Data provider,
     * to test can use URL argument string table parser object.
     *
     * @return array
     */
    public function providerUseUrlArgParser()
    {
        // Init var
        $tabData = array(
            'key-1' => 'Value 1',
            'key-2' => [
                'key-2-1' => 'Value 2-1',
                'key-2-2' => 2.2
            ],
            'key-3' => true
        );

        // Return result
        return array(
            'Use URL argument string table parser: success to get parsed data' => [
                [],
                http_build_query($tabData),
                [
                    $tabData
                ]
            ],
            'Use URL argument string table parser: success to get source' => [
                [],
                $tabData,
                [
                    http_build_query($tabData)
                ]
            ]
        );
    }



    /**
     * Test can use multipart data string table parser object.
     *
     * @param null|array $tabConfig
     * @param string|array $data
     * @param string|array $expectResult
     * @dataProvider providerUseMultipartDataParser
     */
    public function testCanUseMultipartDataParser(
        $tabConfig,
        $data,
        $expectResult
    )
    {
        // Init var
        $boolExceptionExpected = is_string($expectResult);
        $strBoundary = $this->getStrBoundary();

        // Expect exception, if required
        if($boolExceptionExpected)
        {
            $this->expectException($expectResult);
        }

        // Get parser
        $objParser = new MultipartDataParser(
            $tabConfig,
            null,
            null,
            null,
            null,
            static::$objFileFactory
        );

        // Get parsed result
        $result = (
            (is_string($data)) ?
                $objParser->getData($data) :
                $objParser->getSource($data)
        );

        // Set assertions, if required
        if(!$boolExceptionExpected)
        {
            // Set assertions (check parsed result)
            $expectResult[0] = (
                is_string($expectResult[0]) ?
                    str_replace('--' . $strBoundary, '--' . $objParser->getStrBoundary(), $expectResult[0]) :
                    $expectResult[0]
            );
            $this->assertEquals($expectResult[0], $result);

            // Set assertions (check boundary)
            $strBoundary = $objParser->getStrBoundary();
            $boolBoundary = (
                (isset($tabConfig['boundary']) || $objParser->checkCacheBoundaryRequired()) ?
                    ($strBoundary === $objParser->getStrBoundary()) :
                    ($strBoundary !== $objParser->getStrBoundary())
            );
            $this->assertEquals(true, $boolBoundary);

            // Print
            /*
            echo('Get config: ' . PHP_EOL);var_dump($tabConfig);echo(PHP_EOL);
            echo('Get boundary: ' . PHP_EOL);var_dump($strBoundary);echo(PHP_EOL);
            echo('Get data: ' . PHP_EOL);var_dump($data);echo(PHP_EOL);
            echo('Get result: ' . PHP_EOL);var_dump($result);echo(PHP_EOL);
            //*/
        }
    }



    /**
     * Data provider,
     * to test can use multipart data string table parser object.
     *
     * @return array
     */
    public function providerUseMultipartDataParser()
    {
        // Load
        $strRootAppPath = dirname(__FILE__) . '/../../..';
        require($strRootAppPath . '/src/file/test/FileTest.php');

        /** @var FileFactoryInterface $objFileFactory */

        // Init var
        /** @var NameFileInterface $objFile1 */
        $objFile1 = $objFileFactory->getObjFile(array(
            'type' => 'name_file_system',
            'full_path' =>
                'C:\Users\Sergio\Documents\work\emploi\canada\candidature\cv\CV serge bouzid ca_qc_upd4_en.pdf'
                //'C:\Users\Sergio\Documents\work\emploi\canada\Monster\test-cv.txt' // Simple file
        ));
        $strFile1DataSrc = (
            'Content-Disposition: form-data; name="key-2[key-2-1]"; filename="' . $objFile1->getStrName() . '"
Content-Transfer-Encoding: binary
Content-Type: ' . $objFile1->getStrMimeType() . '
Content-Length: ' . strval($objFile1->getIntSize()) . '

' . $objFile1->getStrContent()
        );
        $objMulipartDataFile1 = $objFileFactory->getObjFile(array(
            'type' => 'http_name_multipart_data',
            'data_source' => $strFile1DataSrc,
        ));
        /** @var NameFileInterface $objFile2 */
        $objFile2 = $objFileFactory->getObjFile(array(
            'type' => 'name_file_system',
            'full_path' =>
                'C:\Users\Sergio\Documents\work\emploi\canada\candidature\cv\CV serge bouzid ca_qc_upd4_en.doc'
                //'C:\Users\Sergio\Documents\work\emploi\canada\Monster\test-cv.txt' // Simple file
        ));
        $strFile2DataSrc = (
            'Content-Disposition: form-data; name="key-2[key-2-2]"; filename="' . $objFile2->getStrName() . '"
Content-Transfer-Encoding: binary
Content-Type: ' . $objFile2->getStrMimeType() . '
Content-Length: ' . strval($objFile2->getIntSize()) . '

' . $objFile2->getStrContent()
        );
        $objMulipartDataFile2 = $objFileFactory->getObjFile(array(
            'type' => 'http_name_multipart_data',
            'data_source' => $strFile2DataSrc,
        ));
        $strDefaultFileNm = 'file_test';
        $objFile3 = $objFileFactory->getObjFile(array(
            'type' => 'base64',
            'data_source' => base64_encode(file_get_contents('C:\Users\Sergio\Documents\work\emploi\canada\Monster\test-cv.txt'))
        ));
        $strFile3DataSrc = (
            'Content-Disposition: form-data; name="key-2[key-2-4][0]"; filename="' . $strDefaultFileNm . '"
Content-Transfer-Encoding: binary
Content-Type: ' . $objFile3->getStrMimeType() . '
Content-Length: ' . strval($objFile3->getIntSize()) . '

' . $objFile3->getStrContent()
        );
        $strBoundary = $this->getStrBoundary();
        $strSrc = '--' . $strBoundary . '
Content-Disposition: form-data; name="key-1[]"

Value 1
--' . $strBoundary . '
Content-Disposition: form-data; name="key-1[]"

1
--' . $strBoundary . '
Content-Disposition: form-data; name="key-1[]"

1
--' . $strBoundary . '
Content-Disposition: form-data; name="key-1[]"

true
--' . $strBoundary . '
Content-Disposition: form-data; name="key-1[]"

false
--' . $strBoundary . '
' . $strFile1DataSrc . '
--' . $strBoundary . '
' . $strFile2DataSrc . '
--' . $strBoundary . '
Content-Disposition: form-data; name="key-2[key-2-3]"

Value 2
--' . $strBoundary . '
Content-Disposition: form-data; name="key-2[key-2-3][]"

Value 2.3
--' . $strBoundary . '
Content-Disposition: form-data; name="key-3"

0
--' . $strBoundary . '--';
        $strBaseDataSrc = '--' . $strBoundary . '
Content-Disposition: form-data; name="key-1[0]"

Value 1
--' . $strBoundary . '
Content-Disposition: form-data; name="key-1[1]"

1
--' . $strBoundary . '
Content-Disposition: form-data; name="key-1[2]"

1
--' . $strBoundary . '
Content-Disposition: form-data; name="key-1[3]"

true
--' . $strBoundary . '
Content-Disposition: form-data; name="key-1[4]"

false
--' . $strBoundary . '
<file-1-data-source>
--' . $strBoundary . '
<file-2-data-source>
--' . $strBoundary . '
Content-Disposition: form-data; name="key-2[key-2-3][0]"

Value 2
--' . $strBoundary . '
Content-Disposition: form-data; name="key-2[key-2-3][1]"

Value 2.3<file-3-boundary-data-source>
--' . $strBoundary . '
Content-Disposition: form-data; name="key-3"

0
--' . $strBoundary . '--';
        $strDataSrc = str_replace(
            '<file-3-boundary-data-source>',
            '',
            $strBaseDataSrc
        );
        $strDataSrc = str_replace("\r\n", "\n", $strDataSrc);
        $strDataSrc = str_replace("\n", "\r\n", $strDataSrc);
        $strDataSrc = str_replace('<file-1-data-source>', $strFile1DataSrc, $strDataSrc);
        $strDataSrc = str_replace('<file-2-data-source>', $strFile2DataSrc, $strDataSrc);
        $strDataSrcWithFile3 = str_replace(
            '<file-3-boundary-data-source>',
            ("\n" . '--' . $strBoundary . "\n" . '<file-3-data-source>'),
            $strBaseDataSrc
        );
        $strDataSrcWithFile3 = str_replace("\r\n", "\n", $strDataSrcWithFile3);
        $strDataSrcWithFile3 = str_replace("\n", "\r\n", $strDataSrcWithFile3);
        $strDataSrcWithFile3 = str_replace('<file-1-data-source>', $strFile1DataSrc, $strDataSrcWithFile3);
        $strDataSrcWithFile3 = str_replace('<file-2-data-source>', $strFile2DataSrc, $strDataSrcWithFile3);
        $strDataSrcWithFile3 = str_replace('<file-3-data-source>', $strFile3DataSrc, $strDataSrcWithFile3);
        $tabData = array(
            'key-1' => [
                'Value 1',
                1,
                true,
                'true',
                'false'
            ],
            'key-2' => [
                'key-2-1' => $objFile1,
                'key-2-2' => $objFile2,
                'key-2-3' => [
                    'Value 2',
                    'Value 2.3'
                ]
            ],
            'key-3' => false
        );
        $tabSrcData = array(
            'key-1' => [
                'Value 1',
                '1',
                '1',
                'true',
                'false'
            ],
            'key-2' => [
                'key-2-1' => $objMulipartDataFile1,
                'key-2-2' => $objMulipartDataFile2,
                'key-2-3' => [
                    'Value 2',
                    'Value 2.3'
                ]
            ],
            'key-3' => '0'
        );
        $tabDataWithFile3 = array(
            'key-1' => [
                'Value 1',
                1,
                true,
                'true',
                'false'
            ],
            'key-2' => [
                'key-2-1' => $objFile1,
                'key-2-2' => $objFile2,
                'key-2-3' => [
                    'Value 2',
                    'Value 2.3'
                ],
                'key-2-4' => [
                    $objFile3
                ]
            ],
            'key-3' => false
        );

        // Return result
        return array(
            'Use multipart data string table parser: fail to create parser (invalid configuration boundary format)' => [
                [
                    'boundary' => 7
                ],
                $strSrc,
                ConfigInvalidFormatException::class
            ],
            'Use multipart data string table parser: fail to create parser (invalid configuration cache_boundary_require format)' => [
                [
                    'boundary' => $strBoundary,
                    'cache_boundary_require' => 'test'
                ],
                $strSrc,
                ConfigInvalidFormatException::class
            ],
            'Use multipart data string table parser: success to get parsed data' => [
                [
                    'boundary' => $strBoundary,
                    'cache_boundary_require' => false
                ],
                $strSrc,
                [
                    $tabSrcData
                ]
            ],
            'Use multipart data string table parser: success to get source' => [
                [],
                $tabData,
                [
                    $strDataSrc
                ]
            ],
            'Use multipart data string table parser: fail to create parser (invalid configuration default_file_name format)' => [
                [
                    'default_file_name' => true
                ],
                $tabDataWithFile3,
                ConfigInvalidFormatException::class
            ],
            'Use multipart data string table parser: success to get source (exclude file 3)' => [
                [],
                $tabDataWithFile3,
                [
                    $strDataSrc
                ]
            ],
            'Use multipart data string table parser: fail to create parser (include file 3)' => [
                [
                    'default_file_name' => $strDefaultFileNm
                ],
                $tabDataWithFile3,
                [
                    $strDataSrcWithFile3
                ]
            ]
        );
    }



}