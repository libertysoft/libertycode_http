<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\http\test\feature;

use PHPUnit\Framework\TestCase;

use DateTime;
use liberty_code\http\url_route\argument\exception\DataSrcInvalidFormatException;
use liberty_code\http\url_route\argument\exception\KeyInvalidFormatException;
use liberty_code\http\url_route\argument\exception\ValueInvalidFormatException;
use liberty_code\http\url_route\argument\model\UrlRouteArgData;



/**
 * @cover UrlRouteArgData
 */
class UrlRouteArgDataTest extends TestCase
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods test
    // ******************************************************************************

    /**
     * Test can set data source.
     *
     * @param array $tabDataSrc
     * @param string|array $expectResult
     * @dataProvider providerSetDataSrc
     */
    public function testCanSetDataSrc(
        array $tabDataSrc,
        $expectResult
    )
    {
        // Init var
        $boolExceptionExpected = is_string($expectResult);

        // Expect exception, if required
        if($boolExceptionExpected)
        {
            $this->expectException($expectResult);
        }

        // Get URL route argument data
        $objUrlRouteArgData = new UrlRouteArgData();

        // Set data source
        $objUrlRouteArgData->setDataSrc($tabDataSrc);

        // Set assertions (check data source), if required
        if(!$boolExceptionExpected)
        {
            // Get info
            $tabDataSrc = $objUrlRouteArgData->getDataSrc();

            // Set assertions (check data source)
            $this->assertEquals($expectResult, $tabDataSrc);

            // Print
            /*
            echo('Get data source: ' . PHP_EOL);var_dump($tabDataSrc);echo(PHP_EOL);
            //*/
        }
    }



    /**
     * Data provider,
     * to test can set data source.
     *
     * @return array
     */
    public function providerSetDataSrc()
    {
        // Return result
        return array(
            'Set data source: fail to set data source (invalid key format)' => [
                [
                    'value1',
                    'arg2' => 2,
                    'arg3' => true
                ],
                DataSrcInvalidFormatException::class
            ],
            'Set data source: fail to set data source (invalid value format)' => [
                [
                    'arg1' => new DateTime(),
                    'arg2' => 2,
                    'arg3' => true
                ],
                DataSrcInvalidFormatException::class
            ],
            'Set data source: fail to set data source (invalid array value format)' => [
                [
                    'arg1' => [
                        'arg1-1' => 'Value 1',
                        'arg1-2' => 1,
                        'arg1-3' => false
                    ],
                    'arg2' => 2,
                    'arg3' => true
                ],
                DataSrcInvalidFormatException::class
            ],
            'Set data source: success to set data source' => [
                [
                    'arg1' => 'Value 1',
                    'arg2' => 2,
                    'arg3' => true
                ],
                [
                    'arg1' => 'Value 1',
                    'arg2' => 2,
                    'arg3' => true
                ]
            ]
        );
    }



    /**
     * Test can use key.
     *
     * @param array $tabDataSrc
     * @param $key
     * @param string|array $expectResult
     * @dataProvider providerUseKey
     */
    public function testCanUseKey(
        array $tabDataSrc,
        $key,
        $expectResult
    )
    {
        // Init var
        $boolExceptionExpected = is_string($expectResult);

        // Expect exception, if required
        if($boolExceptionExpected)
        {
            $this->expectException($expectResult);
        }

        // Get URL route argument data
        $objUrlRouteArgData = new UrlRouteArgData();

        // Set data source
        $objUrlRouteArgData->setDataSrc($tabDataSrc);

        // Get info
        $boolExists = $objUrlRouteArgData->checkValueExists($key);

        // Set assertions (check key), if required
        if(!$boolExceptionExpected)
        {
            // Get info
            $boolExpectExists = $expectResult[0];
            $expectValue = $expectResult[1];
            $value = $objUrlRouteArgData->getValue($key);

            // Set assertions (check key)
            $this->assertEquals($boolExpectExists, $boolExists);
            $this->assertEquals($expectValue, $value);

            // Print
            /*
            echo('Get key: ' . PHP_EOL);var_dump($key);echo(PHP_EOL);
            echo('Check exists: ' . PHP_EOL);var_dump($boolExists);echo(PHP_EOL);
            echo('Get value: ' . PHP_EOL);var_dump($value);echo(PHP_EOL);
            //*/
        }
    }



    /**
     * Data provider,
     * to test can use key.
     *
     * @return array
     */
    public function providerUseKey()
    {
        // Init var
        $tabDataSrc = array(
            'arg1' => 'Value 1',
            'arg2' => 2,
            'arg3' => true
        );

        // Return result
        return array(
            'Use key: fail to use key 1 (invalid key format)' => [
                $tabDataSrc,
                1,
                KeyInvalidFormatException::class
            ],
            'Use key: fail to use key test (key not found)' => [
                $tabDataSrc,
                'test',
                [
                    false,
                    null
                ]
            ],
            'Use key: success to use key arg1' => [
                $tabDataSrc,
                'arg1',
                [
                    true,
                    'Value 1'
                ]
            ],
            'Use key: success to use key arg2' => [
                $tabDataSrc,
                'arg2',
                [
                    true,
                    2
                ]
            ]
        );
    }



    /**
     * Test can put value.
     *
     * @param array $tabDataSrc
     * @param string $strKey
     * @param mixed $value
     * @param string|array $expectResult
     * @dataProvider providerPutValue
     */
    public function testCanPutValue(
        array $tabDataSrc,
        $strKey,
        $value,
        $expectResult
    )
    {
        // Init var
        $boolExceptionExpected = is_string($expectResult);

        // Expect exception, if required
        if($boolExceptionExpected)
        {
            $this->expectException($expectResult);
        }

        // Get URL route argument data
        $objUrlRouteArgData = new UrlRouteArgData();

        // Set data source
        $objUrlRouteArgData->setDataSrc($tabDataSrc);

        // Put value
        $objUrlRouteArgData->putValue($strKey, $value);

        // Set assertions (check value), if required
        if(!$boolExceptionExpected)
        {
            // Get info
            $expectValue = $expectResult[0];
            $value = $objUrlRouteArgData->getValue($strKey);

            // Set assertions (check value)
            $this->assertEquals($expectValue, $value);

            // Print
            /*
            echo('Get key: ' . PHP_EOL);var_dump($strKey);echo(PHP_EOL);
            echo('Get value: ' . PHP_EOL);var_dump($value);echo(PHP_EOL);
            //*/
        }
    }



    /**
     * Data provider,
     * to test can put value.
     *
     * @return array
     */
    public function providerPutValue()
    {
        // Init var
        $tabDataSrc = array();

        // Return result
        return array(
            'Put value: fail to put value for key arg1 (invalid value format: not string convertible)' => [
                $tabDataSrc,
                'arg1',
                new DateTime(),
                ValueInvalidFormatException::class
            ],
            'Put value: success to put empty value for key arg1' => [
                $tabDataSrc,
                'arg1',
                '',
                [
                    ''
                ]
            ],
            'Put value: success to put value for key Content-type' => [
                $tabDataSrc,
                'arg1',
                'Value 1',
                [
                    'Value 1'
                ]
            ],
            'Put value: fail to put value for key arg2 (invalid value format: not string convertible)' => [
                $tabDataSrc,
                'arg2',
                [
                    'arg2-1' => 'Value 2',
                    'arg2-2' => 2,
                    'arg2-3' => true
                ],
                ValueInvalidFormatException::class
            ],
            'Put value: success to put value for key arg2' => [
                $tabDataSrc,
                'arg2',
                2,
                [
                    2
                ]
            ]
        );
    }



}