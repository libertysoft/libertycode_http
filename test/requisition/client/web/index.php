<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load
require($strRootAppPath . '/vendor/autoload.php');
require($strRootAppPath . '/include/Include.php');

// Use
use liberty_code\http\requisition\request\library\ToolBoxHttpRequest;



// Init var
$tabHeader = getallheaders();

// Set redirect response
if(isset($tabHeader['Redirect']) && ($tabHeader['Redirect'] == '1'))
{
    // Get info
    $strUrl = ToolBoxHttpRequest::getStrUrl(
        'localhost',
        'http',
        null,
        'LibertyCode/LibertyCode/libertycode_http/test/requisition/client/web/index.php'
    );

    // Set response
    header('Location: ' . $strUrl, true);
    http_response_code(301);
}
// Set standard response
else
{
    header('Content-type: application/json', true);
    http_response_code(200);

    echo(json_encode(array(
        'method' => $_SERVER['REQUEST_METHOD'],
        'url_arg' => $_GET,
        'header' => $tabHeader,
        'body' => file_get_contents('php://input')
    )));
}


