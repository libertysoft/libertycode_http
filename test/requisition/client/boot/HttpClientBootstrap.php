<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load
require($strRootAppPath . '/requisition/response/factory/boot/HttpResponseFactoryBootstrap.php');

// Use
use liberty_code\register\register\table\model\DefaultTableRegister;
use liberty_code\cache\repository\model\DefaultRepository;
use liberty_code\http\requisition\client\model\HttpClient;



// Init cache repository
$objCacheRepo = new DefaultRepository(
    null,
    new DefaultTableRegister()
);

// Init var
$objHttpClient = new HttpClient(
    $objHttpResponseFactory,
    array(
        'response_cache_key_pattern' => 'response_%s',
        'cache_key_pattern' => 'cli_http_%s',
        'after_execution_callable' => function(
            &$strResponseStatusLine,
            array &$tabResponseHeaderLine
        ) {
            if(count(array_filter(
                $tabResponseHeaderLine,
                function($strHeaderLine) {
                    return (preg_match('#^After\-Execution:.+$#', $strHeaderLine) === 1);
                }
            )) == 0)
            {
                $tabResponseHeaderLine[] = 'After-Execution: 1';
            }
        },
        'response_config' => [
            'type' => 'http'
        ],
        'curl_add_option' => [
            CURLOPT_TIMEOUT_MS => 5000
        ]
    ),
    $objCacheRepo,
    $objCacheRepo
);


