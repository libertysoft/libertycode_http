<?php

// Use
use liberty_code\parser\parser\factory\string_table\model\StrTableParserFactory;
use liberty_code\register\register\memory\model\MemoryRegister;
use liberty_code\di\dependency\model\DefaultDependencyCollection;
use liberty_code\di\provider\model\DefaultProvider;
use liberty_code\requisition\request\model\DefaultRequestCollection;
use liberty_code\http\requisition\request\factory\model\HttpRequestFactory;



// Init parser
$objParserFactory = new StrTableParserFactory();

// Init DI
$objRegister = new MemoryRegister();
$objDepCollection = new DefaultDependencyCollection($objRegister);
$objProvider = new DefaultProvider($objDepCollection);

// Init var
$objTmpRequestCollection = new DefaultRequestCollection();
$objHttpRequestFactory = new HttpRequestFactory(
    $objParserFactory,
    $objTmpRequestCollection,
    null,
    $objProvider
);


