<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load
require($strRootAppPath . '/src/file/test/FileTest.php');

// Use
use liberty_code\http\parser\factory\string_table\model\HttpStrTableParserFactory;



// Init parser
$objParserFactory = new HttpStrTableParserFactory(
    null,
    null,
    null,
    null,
    null,
    null,
    $objFileFactory
);


