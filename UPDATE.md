LibertyCode_Http
================



Version "1.0.0"
---------------

- Create repository

- Set register

- Set route

- Set request

- Set response

- Set front controller

---



Version "1.0.1"
---------------

- Set URL route

- Set URL argument

- Set header

- Set multipart utility

- Set response utility

- Set cURL utility

- Set cookie utility

- Update route

    - Use route with call features

- Set file

- Set parser

- Set requisition request

- Set requisition response

- Set requisition client

- Set authentication

---


