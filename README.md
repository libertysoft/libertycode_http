LibertyCode_Http
================



Description
-----------

Library contains HTTP components, 
to use features and other libraries components, 
on HTTP context.

---



Requirement
-----------

- Script language: PHP: version 7 || 8

---



Installation
------------

Several ways are possible:

#### Composer

1. Requirement
    
    It requires composer installation.
    For more information: https://getcomposer.org
    
2. Command: Move in project root path
    
    ```sh
    cd "<project_root_path>"
    ```

3. Command: Installation
    
    ```sh
    php composer.phar require liberty_code/http ["<version>"]
    ```
    
4. Note

    - Include vendor
        
        If project uses composer, 
        vendor must be included:
        
        ```php
        require_once('<project_root_path>/vendor/autoload.php');
        ```
    
    - Configuration
    
        Installation command allows to add, 
        on composer file "<project_root_path>/composer.json",
        following configuration:
        
        ```json
        {
            "require": {
                "liberty_code/http": "<version>"
            }
        }
        ```

#### Include

1. Download
    
    - Download following repository.
    - Put it on repository root path.
    
2. Include source
    
    ```php
    require_once('<repository_root_path>/include/Include.php');
    ```

---



Configuration
-------------

#### Main configuration

- Use following class to configure specific elements
    
    ```php
    use liberty_code\http\config\model\DefaultConfig;
    DefaultConfig::instanceGetDefault()->get|set...();
    ```

- Elements configurables

    - Route options
    - Request options
    - Response options

---



Usage
-----

#### URL route argument

URL route components allows to handle route, 
used on URL.

_Elements_
  
- UrlRouteArgData
    
    Extends table data features.
    Allows to manage URL route argument values.

- UrlRoute utility
    
    Allows to provide URL route features.  
    
#### URL argument

URL argument components allows to handle arguments,
used on URL. 

_Elements_
  
- UrlArgData
    
    Extends path table data features. 
    Allows to manage URL argument values.

#### Header

Header components allows to handle headers. 

_Elements_

- HeaderData
    
    Extends table data features. 
    Allows to manage header values.
    
- Header utility
    
    Allows to provide header features.   

#### Utility

- Multipart

    Multipart and multipart data features.
    
- Response

    Response features.

- CURL

    CURL features.
    
- Cookie

    Cookie features.

#### Register

Register using specific HTTP data storage as storage support.

_Elements_

- SessionRegister

    Extends table register features.
    Uses session array, as storage support.

- CookieRegister

    Extends table register features.
    Uses cookie array, as storage support.

_Example_

```php
...
// Get register
use liberty_code\http\register\session\model\SessionRegister;
$register = SessionRegister::instanceGetDefault();
...
$register->putItem('key_1', '...'); // Register specified item for key 1
$register->putItem('key_N', '...'); // Register specified item for key N
...
foreach($register->getTabKey() as $key) {
    var_dump($register->getItem($key));
}
/**
 * Show: 
 * item for key 1
 * item for key N
 */
...
```

#### Route

Route allows to interpret specified source, 
from http request, on HTTP context, 
to get executable.

_Elements_

- HttpPatternRoute, HttpParamRoute, HttpFixRoute, HttpSeparatorRoute
    
    Extends route features. 
    Adapt all standard routes, 
    to interpret specified source, 
    from HTTP request.
    
- HttpRouteFactory

    Extends DI route factory features. 
    Provides HTTP route instance.

#### File

File using specific HTTP data, to get information and content.

_Elements_

- ResponseFile

    Extends file features. 
    Allows to get file information and content,
    from specified HTTP response.
    
- UploadFile

    Extends named file features. 
    Allows to get file information and content,
    from uploaded files.
    
- MultipartDataFile

    Extends named file features. 
    Allows to get file information and content,
    from specified part data source.
    
- DnlResponseFile

    Extends named file features. 
    Allows to get file information and content,
    from specified downloaded file HTTP response.
    
- HttpFileFactory

    Extends file factory features. 
    Provides HTTP file instance.
    
- HttpNameFileFactory

    Extends file factory features. 
    Provides HTTP named file instance.
    
```php
// Get file factory
use liberty_code\http\file\factory\name\model\HttpNameFileFactory;
$fileFactory = new HttpNameFileFactory();
...
// Get new file from configuration
$file = $fileFactory->getObjFile(array(...));
...
```

#### Parser

Parser allows to get specific formatted data, 
from specified HTTP source.

_Elements_

- UrlArgParser

    Extends string table parser features. 
    Allows to get parsed data, 
    from URL argument format, as string source.

- MultipartDataParser

    Extends string table parser features. 
    Allows to get parsed data, 
    from multipart format, as string source.
    
- HttpStrTableParserFactory

    Extends parser factory features. 
    Provides HTTP string table parser instance.
    
_Example_

```php
// Get parser factory
use liberty_code\http\parser\factory\string_table\model\HttpStrTableParserFactory;
$parserFactory = new HttpStrTableParserFactory(
    null,
    null,
    null,
    null,
    null,
    null,
    $fileFactory
);
...
// Get new parser, from configuration
$parser = $parserFactory->getObjParser(array(...));
...
// Get parsed data, from source
var_dump($parser->getData(...));
...
// Retrieve source, from parsed data
var_dump($parser->getSource(...));
...
```

#### Request

Request allows to provide route source, 
on request flow process and HTTP context.

_Elements_

- HttpRequest
    
    Extends request features. 
    Allows to design HTTP request.
    
_Example_

```php
class HttpRequestTest extends liberty_code\http\request_flow\request\model\HttpRequest
{
    // Define rules to get route source
    public function getStrRoute()
    {
        // Get route source from GET "route" argument
        return $this->getGet('route', 'N/A');
    }
}

$request = HttpRequestTest::instanceGetDefault();
echo($request->getStrRouteSrc()); // Show "<HTTP method>:<route>"
...
```

#### Response

Response allows to provide final send-able content, 
from request flow process, on HTTP context.

_Elements_

- HttpResponse
    
    Extends response features. 
    Allows to design HTTP response, 
    with adequate contents to send.
    
- RedirectResponse
    
    Extends HTTP response features. 
    Allows to design HTTP redirection response, 
    with adequate contents to send.
    
- Response utilities
    
    Parsed content response allows 
    to get response with specific formatted content, 
    on HTTP context.
    Example: HTTP JSON response, HTTP XML response, etc...    

_Example_

```php
use liberty_code\http\request_flow\response\model\HttpResponse;
$objResponse = new HttpResponse();
$objResponse->setContent(...);
...
```
    
#### Front controller

Front controller allows to design the request flow process, 
on HTTP context.

_Elements_

- HttpFrontController
    
    Extends front controller features. 
    Uses HTTP request, to get HTTP response.

_Example_

```php
use liberty_code\http\request_flow\front\model\HttpFrontController;
$frontController = new HttpFrontController();
$frontController->setRouter(...);
$frontController->setActiveRequest(...);
...
// Show response object
var_dump($frontController->execute());
...
```

#### Requisition request

Request allows to handle HTTP information, 
required for HTTP sending.

_Elements_

- HttpRequest
    
    Extends request features. 
    Allows to handle all HTTP information, 
    required for HTTP sending.

- DataHttpRequest
    
    Extends HTTP request features. 
    Allows to handle data, 
    on sending information.
    
- PersistorHttpRequest

    Extends data HTTP request and persistor request features. 
    Allows to set all information, 
    required for sending to storage support, 
    via HTTP protocol.

- HttpRequestFactory
    
    Extends template request factory features. 
    Provides HTTP request instance.
    
_Example_

```php
// Get request factory
use liberty_code\http\requisition\request\factory\model\HttpRequestFactory;
$requestFactory = new HttpRequestFactory($parserFactory);
...
// Get new request, from configuration
$request = $requestFactory->getObjRequest(array(...));
...
// Get sending information
var_dump($request->getTabSndInfo());
var_dump($request->getStrMethod());
var_dump($request->getStrUrl());
var_dump($request->getTabHeader());
var_dump($request->getStrBody());
...
```

#### Requisition response

Response allows to handle HTTP received information.

_Elements_

- HttpResponse
    
    Extends response features. 
    Allows to handle all HTTP received information.

- DataHttpResponse
    
    Extends HTTP response features. 
    Allows to handle data, 
    on received information.
    
- PersistorHttpResponse

    Extends data HTTP response and persistor response features. 
    Allows to get all received information, 
    from storage support, 
    via HTTP protocol.
    
- HttpResponseFactory

    Extends response factory features. 
    Provides HTTP response instance.
    
_Example_

```php
// Get response factory
use liberty_code\http\requisition\response\factory\model\HttpResponseFactory;
$responseFactory = new HttpResponseFactory($parserFactory);
...
// Get new response, from configuration
$response = $responseFactory->getObjResponse(array(...));
...
// Get reception information
var_dump($response->getIntStatusCode());
var_dump($response->getTabHeader());
var_dump($response->getStrBody());
...
```

#### Requisition client

Client allows to handle HTTP request sending execution, 
and HTTP response reception.

_Elements_

- HttpClient
    
    Extends info client features. 
    Allows to deal with HTTP (sending, reception) information,
    to execute HTTP request sending.
    
_Example_

```php
// Get client
use liberty_code\http\requisition\client\model\HttpClient;
$client = new HttpClient($responseFactory);
...
// Get new response, from request
$response = $client->executeRequest($request);
...
```

#### Authentication

Authentication allows to design an authentication class,
using HTTP request arguments.

_Elements_

- HttpAuthentication
    
    Extends authentication features. 
    Uses HTTP request arguments, 
    to get all identification and authentication information.
    
_Example_

```php
// Get HTTP authentication
use liberty_code\http\authentication\authentication\model\HttpAuthentication
$httpAuth = new HttpAuthentication($request);
$httpAuth->setAuthConfig(...);
...
// Get array of identification data
var_dump($httpAuth->getTabIdData());
...
// Get array of authentication data
var_dump($httpAuth->getTabAuthData());
...
```

---



Test
----

#### Unit test

Unit tests allows to test components features, 
and to automate their validation.

1. Requirement
    
    - Composer
    
        It requires composer installation.
        For more information: https://getcomposer.org
        
    - Command: Dependencies installation
    
        ```sh
        php composer.phar install
        ```
    
2. Command: Run unit tests
    
    ```sh
    vendor\bin\phpunit
    ```

3. Note

    It uses PHPUnit to handle unit tests.
    For more information: https://phpunit.readthedocs.io

---


